==<| RGB LED cube application |>==
 ![screenshot](./doc/screenshots/2018.09.02.jpg)

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Description](#description)
- [Download and Installation](#download-and-installation)
- [Dependencies](#dependencies)
	- [Binaries](#binaries)
	- [Development](#development)
- [How to run](#how-to-run)
	- [Binaries](#binaries)
	- [From sources](#from-sources)
- [License](#license)
- [Troubleshooting](#troubleshooting)
	- [Can not connect to real cube](#can-not-connect-to-real-cube)
	- [Can not execute application](#can-not-execute-application)
- [Version system](#version-system)
- [Credits](#credits)

<!-- /TOC -->


# Description
 * Highly flexible generic application for LED cube (Yes, not only 5x5x5
   dimension)
 * Application have view to 3D cube, but it also allow connect to HW cube and
   project images/animations/whatever to real cube.


# Download and Installation
 * If you want to just try it, we recommend
   [releases](https://gitlab.com/martin.stejskal/rgb_led_cube_app/-/releases).
 * More adventurous users may try run it from
   [sources](https://gitlab.com/martin.stejskal/rgb_led_cube_app).


# Dependencies
## Binaries
 * [OpenGL](https://www.opengl.org/) - 3D multi-platform library. Probably you
   already have it at your system.

## Development
 * Assuming that you're not using binaries, you have a little bit clue what
   Python is.
 * Based on
   [**Python3**](https://www.python.org)
   -> no Python2 support! You can not stop evolution...
 * [PIP](https://pypi.org/project/pip/) - Package system for Python. It allow
   to install dependencies quite easy.
 * [Virtualenv](https://virtualenv.pypa.io/en/stable/) - Assume that you do not
   want to mess up your system with some packages, this will install everything
   (well, nearly) in project folder instead of your system. Piece of mind!
 * [Tkinter](https://docs.python.org/3/library/tk.html) - Simple GUI. It should
   be part of Python, however in some distributions is missing.
   On Ubuntu you might need to install `python3-tk` package.
 * [FreeGLUT](http://freeglut.sourceforge.net/) - A system package that is used
   by Python wrappers ([pyOpenGL](http://pyopengl.sourceforge.net/))
 * [pyOpenGL](http://pyopengl.sourceforge.net/) - for 3D view. On Ubuntu you
   might need to install `freeglut3` package if you will face problems
   with GLUT extension.
 * [zmq](http://zeromq.org/) - interprocess communication through local TCP
 * [pyserial](https://pypi.python.org/pypi/pyserial) - needed only if you want
    use serial interface (some HW cube may require it)
 * That should be all. Maybe above mentioned dependencies have another
   dependencies, but they should be installed automatically through package
   system.
 * *May* run on
   [Windows](https://www.microsoft.com)
   as well, if you install all dependencies correctly and you pray enough.
   Not tested on Windows.


# How to run
## Binaries
 * Just execute `./led_cube_app`.
 * **For the first time it can take quite long**,
   since everything is compiling. Just be patient. Next time it will be
   much faster.
 * Beware that binaries are created on your actual system with actual API.
   This may lead to problems on older systems due to API differences.
 * Most of BFU does not need to continue reading. If you're developer and
   you're wondering where are binaries, then you need to build them first.
   Just check folder `scripts` and you will find what you looking for. Once
   binaries are built, you can find them at `./dist/led_cube_app/`.

## From sources
 * The easy and recommended way:

```bash
$ run.sh
```

 * That command will create virtual environment, download dependencies and
   execute application. So, pretty much pain you should be saved.
 * Or if you like hard way, you can run it under your native environment. But
   remember that you have to have installed all dependencies on your system.

 ```bash
 $ ./rgb_led_cube_app/led_cube_app.py
 ```

 * Note that you can execute this file from anywhere, so feel free to make link
   to your desktop if you like it ;)


# License
 * GPL v3.0
 * Feel free to modify source and distribute :)
 * PLEASE use SPACES! NO TABS! Please once more. It took me a LOT of time to
   keep code clean, so do not destroy my effort.
 * If possible, keep line limit to 80 characters. It have many reasons (IDE
   layout, easy reading, easy diff, easy to make it readable on small displays,
   ... it make your (and others) life easier). You can always run

```bash
$ ./scripts/pep8_check.sh
```
 * to make sure that your modification is according to
   [PEP8](https://www.python.org/dev/peps/pep-0008/)
   specification.


# Troubleshooting
 * DO NOT run application as root!!! Rather double check your access rights.
   There is good reason why access limitation is on your system! I warned you.

## Can not connect to real cube
 * I guess that you already checked wiring, so problem will be probably on SW
   side.
 * In case of `RGB 5x5x5 LED cube`, communication interface is UART, which
   is mapped at Linux system usually as `/dev/ttyUSB0`. Most probably you have
   access problem. On Ubuntu should be enough to add yourself to `dialout`
   group. Just copy and paste to your console:

```bash
$ sudo usermod -aG dialout $USER
```

 * Log out and log in.
 * If problem persists, do not be lazy and check access rights manually and if
   needed add yourself to another group, change rights, whatever.

## Can not execute application
 * Well, there is known problem with
   [Tkinter](https://docs.python.org/3/library/tk.html)
   which should be already integrated to Python, but sometimes is not.
   Try to install package `python3-tk`.
 * Still problems? Maybe you need install
   [FreeGLUT](http://freeglut.sourceforge.net/)
   package. On Ubuntu it is called `freeglut3`.

## Can not see application icon
 * This is know problem. On some systems this happen. Unfortunatelly reason is
   unknown. Juse deal with it.


# Version system
 * [GIT](https://git-scm.com/) - please keep it, unless there is SERIOUS reason
   for change.

# Notes
 * The submodule "animations" have to contain HTTPS link. The relative link can
   not be used, because it stop working when clonning RGB LED cube superproject
   which include this one. Anyways, the animations are set as public, so it
   shall not be the problem

# Credits
 * Martin Stejskal (martin.stej at gmail)

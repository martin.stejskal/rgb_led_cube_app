#!/bin/bash

# Obviously clear all generated files
#
# Created:  2018.05.27
# Modified: 2018.05.27
# Author:   Martin Stejskal

# Get project directory, so everything can be done through absolute paths ->
# -> can cann this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir"/../rgb_led_cube_app

echo "
Searching for __pycache__ ...
"
find -name '__pycache__' -exec echo {} \; -prune -exec rm -rf {} \;

echo "
Searching for .pyc files ...
"
find -name '*.pyc' -exec rm -v {} \;

echo "
Searching for led_cube_app.spec ...
"
find -name 'led_cube_app.spec' -exec rm -v {} \;

echo "
Searching for Log_* files ...
"
find -name 'Log_*' -exec rm -v {} \;

echo "
Deleting other generated files...
"
other_files="../dist ../doc/sphinx_doc/_build led_cube_app"
for file in $other_files ; do
  echo "$file"
  rm -rf "$file"
done

# Jump back
cd $user_dir

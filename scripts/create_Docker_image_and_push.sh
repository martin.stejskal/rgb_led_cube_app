#!/bin/bash
# Create docker image, which can be used for testing, deploment etc. Basically
# CI/CD stuff. Motivation was automatic building images and uploading them to
# another project (only binaries).
#
# Created:  2018.06.17
# Modified: 2018.06.17
# Author:   Martin Stejskal

# It is necessary to know where image will be pushed
docker_registry="registry.gitlab.com/martin.stejskal/rgb_led_cube_app"
echo 'Do not forget to run "$ docker login [server]" before this script'

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir"/docker_ci_img/
# we need serial support for RGB_LED_CUBE_5x5x5_00 and import is kinda hidden,
# so that is reason why there is hidden-import parameter
docker build -t "$docker_registry" . &&
docker push "$docker_registry"

# Go back - actually not needed, but just for case that user will source this
# script - you never know
cd $user_dir

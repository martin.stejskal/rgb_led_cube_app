# Script that automatically search all Python sources and check them against
# PEP8 standard.

# Created:  2017.05.31
# Modified: 2024.02.11
# Author:   Martin Stejskal

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir/.."
pwd

flake8 . \
  --exclude venv,rgb_led_cube_app/animations/scripts/venv,\
doc/sphinx_doc/conf.py,dist,performance_test


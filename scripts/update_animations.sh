#!/bin/bash
# Scrpt for simple updating animations

# Created:  2018.10.25
# Modified: 2018.10.25
# Author:   Martin Stejskal

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

clear
cd "$this_dir"
echo "Updating animations ..."
cd ../rgb_led_cube_app/animations/
git pull origin master

cd "$user_dir"

#!/bin/bash
# Created:  2017.05.31
# Modified: 2018.06.15
# Author:   Martin Stejskal

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

cd "$this_dir"/../
# we need serial support for RGB_LED_CUBE_5x5x5_00 and import is kinda hidden,
# so that is reason why there is hidden-import parameter
pyinstaller -y led_cube_app.spec

# Go back
cd $user_dir

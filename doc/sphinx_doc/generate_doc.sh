#!/bin/bash
clear
ls *.rst | grep -v 'index.rst' | xargs rm
sphinx-apidoc -o ./ ../../
sphinx-apidoc -o ./ ../../lib/
sphinx-apidoc -o ./ ../../lib/control_window
make html

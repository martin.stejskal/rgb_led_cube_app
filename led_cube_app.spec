# -*- mode: python -*-
from os.path import realpath
import shutil

# Define where is libglut - GLUT library as binary (not Python). Since on
# multiple systems can be at different folders, there is list. Not elegant
# but good for now. Builds are done via CI, so it should be OK.
glut_lib_paths=['/usr/lib/libglut.so',
                '/usr/lib/x86_64-linux-gnu/libglut.so.3']
# Will be filled later
glut_lib = ''

# This will allow to build it anywhere. Path is dynamically created
pwd=realpath('./')

block_cipher = None

def FreeGLUT_install_fix():
    """There is some bug in PyOpenGL, so this is workaround

    This function basically copy libglut as glut to project
    """
    # Find glut lib
    found = False
    for glut_lib_path in glut_lib_paths:
        if(os.path.isfile(glut_lib_path)):
            # Bingo. File found
            global glut_lib
            glut_lib = glut_lib_path
            found = True
            break
        # /if found
    # /for list
    if(not found):
        msg = 'Looks like we can not find "libglut.so" :/'
        raise Exception(msg)
    else:
        # Copy libglut.so as glut file
        shutil.copy(glut_lib, './glut')
# /FreeGLUT_install_fix
FreeGLUT_install_fix()

a = Analysis(['rgb_led_cube_app/led_cube_app.py'],
             pathex=['{}/rgb_led_cube_app'.format(pwd)],
             binaries=[ (glut_lib,'./'),
                        ('glut', './'), # Ugly hack, but it helps
                      ],
             datas=[ ('README.md', './'),
                     ('doc/screenshots', 'doc/screenshots'),
                     ('rgb_led_cube_app/cfg', 'cfg'),
                     ('rgb_led_cube_app/lib', 'lib'),
                     ('rgb_led_cube_app/animations', 'animations'),
                     ('LICENSE', './'),
                   ],
             hiddenimports=[
                'tkinter','tkinter.messagebox','tkinter.filedialog',
                'serial', 'serial.tools.list_ports',
                'setproctitle'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='led_cube_app',
          debug=False,
          strip=False,
          upx=True,
          console=True,
          icon='rgb_led_cube_app/lib/app_icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='led_cube_app')

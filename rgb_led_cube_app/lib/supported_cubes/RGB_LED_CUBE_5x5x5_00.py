#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
====================================
API for cube "RGB_LED_CUBE_5x5x5-00"
====================================

Some things need to be general through all devices, but data processing can be
  different. So that's why every supported cube have own API.

``author`` Martin Stejskal

``version`` 0.7.1
"""
from lib.common import CmnLgrCls

# This cube use UART for communication -> need pySerial library
try:
    import serial
except Exception:
    msg = ">> RGB_LED_CUBE_5x5x5_00:\n" \
          "Module pySerial is not probably installed, but we need it for" \
          " UART\n communication. Please install it and try again."
    raise Exception(msg)
else:
    # serial module found. Keep importing
    import serial.tools.list_ports


class RealCubeCls():
    """Class for basic control of real cube"""

    def __init__(self):
        # logger initialization
        self.logger = CmnLgrCls("real_cube_hw")

        self.uart = serial.Serial()
        # Do some common configuration (except port, it will be same all the
        # times)
        self.uart.baudrate = 115200
        self.uart.bytesize = 8
        self.uart.parity = 'N'
        self.uart.stopbits = 1
        self.uart.timeout = 0.5
        self.uart.write_timeout = 0.5
        self.uart.rtscts = 0

        # Mode is not set yet. Different modes treat to cube differently
        self.mode = "none"

    ##########################################################################
    def connect(self):
        """We will try to connect to HW cube

        Raise RuntimeError exception when disconnection failed.
        """
        # Will be used later
        connected = False

        # Function will try automatically connect with device
        for port in serial.tools.list_ports.comports():
            self.logger.debug('Trying port "{}"'.format(port[0]))
            self.uart.port = port[0]

            # Try open port
            try:
                self.uart.open()
            except Exception:
                # Port is not open -> break (let's move to another iteration)
                msg = 'Can not open port "{}"'.format(port[0])
                self.logger.info(msg)
                continue

            if (not self.uart.isOpen()):
                # Port is not open -> break (let's move to another iteration)
                msg = 'Can not open port "{}"'.format(port[0])
                self.logger.info(msg)
                continue

            # Else port is open
            msg = "Port is open. Trying to initialize..."
            self.logger.debug(msg)

            # Send Hello string
            self.uart.write(b'Hello\n\0')

            # Get device name (RGB_LED_cube_5x5x5-00\n\0) - expected 23 Bytes
            try:
                self.cube_real_name = self.uart.read(23)
            except Exception:
                # Fail -> maybe port is used by another application? Skip this
                # port
                msg = 'Exception occurred. Maybe port "{}" is using another' \
                      " application?".format(port[0])
                self.logger.warn(msg)
                self.uart.close()
                continue

            if (self.cube_real_name != b"RGB_LED_cube_5x5x5-00\n\0"):
                # Try it once more. It is possible that some application send
                # some odd data and now state machine expect something
                # different than "Hello"
                msg = "Get wrong response. Trying again..."
                self.logger.debug(msg)

                self.uart.close()
                self.uart.open()

                self.uart.write(b'Hello\n\0')

                self.cube_real_name = self.uart.read(23)
                if (self.cube_real_name != b"RGB_LED_cube_5x5x5-00\n\0"):
                    msg = 'Device on port "{}" is not compatible (different ' \
                          "name: {})".format(port[0], self.cube_real_name)
                    self.logger.info(msg)
                    self.uart.close()
                    continue
                # /if name != cube name (try 2)
            # /if name != cube name

            # Ask for dimension (just in case)
            self.uart.write(b'Dimension?\n\0')

            # Get cube dimension (3 Bytes)
            dimension = self.uart.read(3)
            if ((dimension != b"5\n\0")):
                # Wrong response from cube -> skip it
                msg = 'Device on port "{}" is not compatible (different .' \
                      "dimension)".format(port[0])
                self.logger.info(msg)
                self.uart.close()
                continue

            # If we go there -> device is connected
            connected = True
            msg = 'Cube initialized at "{}"'.format(port[0])
            self.logger.info(msg)
            break
        # /for all ports

        if (not connected):
            msg = "Could not find compatible device :("
            self.logger.error(msg)
            raise RuntimeError(msg)

    ##########################################################################
    def disconnect(self):
        """We will try to disconnect to HW cube

        Raise RuntimeError exception when disconnection failed.
        """
        if (self.uart.isOpen()):
            try:
                if (self.mode == "Fill buffer"):
                    # Need to abort command
                    Bytes = []
                    for i in range(50):
                        Bytes.append(255)
                    self.uart.write(bytearray(Bytes))

                self.uart.write(b"Bye!\n\0")
                self.uart.close()
                self.logger.info("Cube closed")
            except serial.SerialException as e:
                msg = "Disconnect(): {}\n\n" \
                      "Real cube was probably disconnected from PC!" \
                      "".format(e)
                self.logger.warn(msg)
                self.uart.close()
        else:
            msg = "Port is not opened, so it can not be closed!"
            self.logger.error(msg)
            raise RuntimeError(msg)

    ##########################################################################
    ##########################################################################

    def get_dimension(self):
        """Get cube dimension

        :returns: Cube dimension. For example 5 mean, that cube is 5x5x5
        :rtype: int
        """
        return (5)

    ##########################################################################

    def show_img(self, led_struct):
        """This function take LED structure and "print" it to real cube

        :param led_struct: Structure of LED in format
                           .w[wall_idx].c[column_idx].led[led_idx].
                           Every single LED have to contain: r,g,b,pwm.r,
                           pwm.g and pwm.b. Any other data are optional.
        :type led_struct: obj

        Raise RuntimeError exception when disconnection failed.
        """
        # Check if port is open
        if (not self.uart.isOpen()):
            msg = "Cube is not connected! Can not show image!"
            self.logger.error(msg)
            raise RuntimeError(msg)

        # Check if we're already in this mode.
        # 2DO: Now cube support only one mode, but in future we have to find
        # way how to close actual mode
        if (self.mode != "Fill buffer"):
            # Need to initialize this mode
            try:
                self.uart.write(b'Fill buffer\n\0')

                resp = self.uart.read(4)
            except serial.SerialException as e:
                msg = "show_img():fill buffer: {}\n\n" \
                      "Real cube was probably disconnected from PC!\n" \
                      " -> Can not initialize filling buffer." \
                      "".format(e)
                self.logger.warn(msg)
                self.uart.close()
                raise RuntimeError(msg)

            if (resp != b'OK\n\0'):
                msg = "Invalid response from cube ({}) !".format(resp)
                self.logger.error(msg)
                raise RuntimeError(msg)
            # And change mode
            self.mode = "Fill buffer"
        # /if mode != Fill buffer

        # If we get there, mode is already initialized

        self.logger.debug("Transferring image...")

        """
 * Data format (binary data for framebuffer)
 *     |MSB|           |LSB|MSB|           |LSB|MSB|           |LSB|
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 * | 0 | R | R | R | R | R | G | G | G | G | G | B | B | B | B | B |
 * +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
 *
 * Physical orientation in column:
 *
 *       C0         C1     ...
 *     +-----+   +-----+   ...
 * MSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 *     |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
 * LSB |R G B|   |R G B|   ...
 *     +-----+   +-----+   ...
        """

        # Prepare byte array for transmission
        Bytes = []

        for wall_idx in range(self.get_dimension()):
            for column_idx in range(self.get_dimension()):
                # Temporary 16 bit value
                B16 = 0

                for led_idx in range(self.get_dimension()):
                    # Check RGB
                    if (led_struct.w[wall_idx].c[column_idx].led[led_idx].r):
                        # Red is active. Add "1" bit to correct position
                        B16 = B16 + (1 << (led_idx + 10))

                    if (led_struct.w[wall_idx].c[column_idx].led[led_idx].g):
                        B16 = B16 + (1 << (led_idx + 5))
                    if (led_struct.w[wall_idx].c[column_idx].led[led_idx].b):
                        B16 = B16 + (1 << (led_idx + 0))
                # /for led_idx

                # Add 2 Bytes to Bytes[]. MSB first
                Bytes.append((B16 >> 8) & 0xff)
                Bytes.append(B16 & 0xff)
            # /for column
        # /for wall

        # Add PWM information (R, G, B). Since this cube support only one PWM
        # settings for whole cube, we take PWM value from wall 0, column 0 and
        # LED 0. Just in case retype value to integer
        Bytes.append(int(led_struct.pwm.r))
        Bytes.append(int(led_struct.pwm.g))
        Bytes.append(int(led_struct.pwm.b))

        try:
            self.uart.write(bytearray(Bytes))
            # And check response
            resp = self.uart.read(4)
        except serial.SerialException as e:
            msg = "show_img() :pwm: {}\n\n" \
                  "Real cube was probably disconnected from PC!" \
                  "".format(e)
            self.logger.warn(msg)
            self.uart.close()
            raise RuntimeError(msg)

        if (resp != b'OK\n\0'):
            msg = "Incorrect response from cube. Expected OK, but got {}" \
                  "".format(resp)
            if (resp == b''):
                msg = '{}\nMaybe is real cube physically disconnected.' \
                      ''.format(msg)
            self.logger.error(msg)
            raise RuntimeError(msg)

        self.logger.debug("Image transferred")

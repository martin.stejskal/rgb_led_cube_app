#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
====================================
API for Virtual cube
====================================

Sometimes is needed to test how application will behave at some conditions.
And not all the times we have real cube connected....
So for that testing I created this module. Based on RGB_LED_CUBE_5x5x5-00

``author`` Martin Stejskal

``version`` 0.4.1
"""

from lib.common import CmnLgrCls


class RealCubeCls:
    """Class for basic control of real cube"""

    def __init__(self):
        # logger initialization
        self.logger = CmnLgrCls("real_cube_hw")

        # Initial value - not connected
        self.connected = False

        # Parameters - can be changed on the fly ;)
        self.dimension = 5
        self.connection_fails = False
        self.disconnection_fails = False
        self.show_img_fails = False

    ##########################################################################
    def connect(self):
        """We will try to connect to HW cube

        Raise RuntimeError exception when disconnection failed.
        """
        if (self.connected):
            # Stupid programmer/bug
            msg = "Already connected -> can not connect again!"
            self.logger.error(msg)
            raise Exception(msg)

        if (self.connection_fails):
            self.connected = False
            msg = "Connection to real cube failed :("
            self.logger.error(msg)
            raise RuntimeError(msg)
        else:
            self.connected = True
            msg = "Connection successful :)"
            self.logger.info(msg)

    # /connect()
    ##########################################################################

    def disconnect(self):
        """We will try to disconnect to HW cube

        Raise RuntimeError exception when disconnection failed.
        """
        if (not self.connected):
            # Already disconnected -> stupid programmer/bug
            msg = "Cube is already disconnected"
            self.logger.error(msg)
            raise Exception(msg)
        # /if already disconnected

        if (self.disconnection_fails):
            # Quite strange, but it may happen
            msg = "Cube can not be disconnected :("
            self.logger.warn(msg)
            raise RuntimeError(msg)
        # /disconnect fails

        # Else disconnection is OK
        msg = "Disconnection successful :)"
        self.logger.info(msg)

    ##########################################################################
    ##########################################################################

    def get_dimension(self):
        """Get cube dimension

        :returns: Cube dimension. For example 5 mean, that cube is 5x5x5
        :rtype: int
        """
        return (self.dimension)

    ##########################################################################

    def show_img(self, led_struct):
        """This function take LED structure and "print" it to real cube

        :param led_struct: Structure of LED in format
                           .w[wall_idx].c[column_idx].led[led_idx].
                           Every single LED have to contain: r,g,b,pwm.r,
                           pwm.g and pwm.b. Any other data are optional.
        :type led_struct: obj

        Raise RuntimeError exception when projection failed.
        """
        # Check if port is open
        if (not self.connected):
            msg = "Cube is not connected! Can not show image!"
            self.logger.error(msg)
            raise RuntimeError(msg)
        # /if not connected

        if (self.show_img_fails):
            msg = "Projecting cube image failed :("
            self.logger.error(msg)

            self.disconnect()
            raise RuntimeError(msg)
        # /projection failed
        else:
            self.logger.debug("Image transferred")
        # /project image
    # /show_img()

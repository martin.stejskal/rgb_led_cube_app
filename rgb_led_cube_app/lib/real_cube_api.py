#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
====================================
General API for generic cube
====================================

This is only generic API, but it allow connect and work with "real" cube if it
 is supported.

``author`` Martin Stejskal

``version`` 0.6.2
"""
# Multiprocessing and sharing variables through process
from multiprocessing import Process

# Initial IPC require some pooling. Measuring time is needed due to timeouts
from time import sleep

# Network stack for interprocess communication
import zmq

# For listing "supported_subes" folder
from os import listdir

# tuned logger, application path
from lib.common import CmnLgrCls, app_path
from lib.cube_LED import CubeLEDCls
from lib.ca.ca_ipc import str2cmd_val

# Process naming is not something mandatory, but nice to have
try:
    from setproctitle import setproctitle
except Exception:
    pass


class RealCubeAPISettingsCls:
    """Real cube settings
    """

    def __init__(self):
        # IP for IPC should be local IP address.
        self.ip = "127.0.10.95"

        # Port through which will go mainly 3D cube image data.
        self.port_mstr2bg = 5520

        # Sending binary data is through different port
        self.port_mstr2bg_bin = self.port_mstr2bg + 1

        # Sometimes it is necessary asynchronous feedback channel from
        # background process.
        self.port_bg2mstr = self.port_mstr2bg_bin + 1

        # Special channel for reporting problems with show_img(). This actually
        # keep code cleaner and whole procedure is more easy to understand.
        self.port_bg2mstr_err = self.port_bg2mstr + 1

        # Define how many requests per second can be processed. Lower number
        # means lower CPU usage during idling, but slower response for the
        # "first" request. Recommended value is 50. 10 is quite slow, 100
        # use CPU more than needed
        self.req_per_sec = 50

        # Communication timeout
        # It might happen that some module crash during time that another
        # process is trying to communicate with crashed process. In that case
        # there is this fail-safe solution. After some time communication
        # should be interrupted and also other tasks should raise exception.
        # Value is in ms.
        self.timeout_ms = 3000
    # /__init__()


# /RealCubeAPISettingsCls


class RealCubeAPICls():
    """API for real cube"""

    def __init__(self, settings):
        """
        :param settings: Settings structure
        :type settings: RealCubeAPISettingsCls
        """
        # Save settings
        self.cfg = settings

        # logger initialization
        self.logger = CmnLgrCls("real_cube_api")

        # Run background process
        self.logger.debug('Starting background process')
        self.bg_proc = _RealCubeAPIBgCls(self.logger, self.cfg)
        self.bg_proc.start()

        self.logger.debug('Master: Preparing IPC')
        self._prepare_ipc()

        # In some cases, we do not need to ask background process for actual
        # value, since value can be pointless due to nature of background
        # process (Why ask periodically for status, when not sending commands?
        # It make sense to check status when somethings goes wrong or there
        # is connect/disconnect action)
        self.device_list = None
        self.status = None
        self.dimension = None

        # In case that there are some problems with IPC, you can enable this
        # debug output. But it can bring quite heavy CPU overhead.
        self.debug = False
        # In case you will enable debug, it is useful to know source of
        # messages
        self.name = 'Real Cube API - Master'

        self._update_device_list()
        self._update_status()
        # Updating dimension is pointless now. Let's wait for connection event

    # /__init__()
    # |RealCubeAPICls

    ##########################################################################

    def get_device_list(self):
        """Get list of supported devices

        :returns: List of supported devices
        :rtype: str[]
        """
        return (self.device_list)

    # /get_device_list()
    # |RealCubeAPICls

    def get_status(self):
        """Get status

        :returns: "Connected" or "Not connected"
        :rtype: str
        """
        return (self.status)

    # /get_status()
    # |RealCubeAPICls

    def get_dimension(self):
        """Get cube dimension

        :returns: Cube dimension. For example 5 mean, that cube is 5x5x5
        :rtype: int
        """
        # Kind of double check is at background. But let's check it here
        # to make it faster.
        if (self.status == 'Not connected'):
            msg = "Cube not connected yet. We can not know dimension"
            self.logger.warn(msg)
            return (0)
        else:
            return (self.dimension)

    # /get_dimension()
    # |RealCubeAPICls

    def connect(self,
                dev_name="RGB_LED_CUBE_8x8x8-00"):
        """This function will try connect with real cube

        Raise RuntimeError exception when connection failed.

        :param dev_name: device name. Please use one of names obtained from
                         get_device_list()
        :type dev_name: str
        """
        self._send_req(cmd="connect", val=dev_name)
        cmd, val = self._get_feedback(blocking=True)

        self._check_feedback(cmd, val, "connect")

        # When connected, update status and dimension
        self._update_status()
        self._update_dimension()

    # /connect()
    # |RealCubeAPICls

    def disconnect(self):
        """Simply disconnect from device
        """
        self._send_req(cmd="disconnect", val="")
        cmd, val = self._get_feedback(blocking=True)

        self._check_feedback(cmd, val, "disconnect")

        self._clean_up_error_messages_in_feedback()

        self._update_status()

    # /disconnect()
    # |RealCubeAPICls

    def show_img(self, led_struct):
        """This function take LED structure and "print" it to real cube

        Raise RuntimeError exception when uploading image failed.

        :param led_struct: Structure of LED
        :type led_struct: CubeLEDCls
        """
        # This is little bit different. Check if some error is registered
        cmd, val = self._get_err_feedback_non_blocking()
        if (cmd != ""):
            # If command is not empty -> some error
            try:
                self._check_feedback(cmd, val, "show_img")
            finally:
                # If any case we need to update status
                self._update_status()

                # Remove all remaining error messages in feedback queue, since
                # we'll update status accordingly
                self._clean_up_error_messages_in_feedback()
        # /when some error is reported

        # If not, send image
        pwm = led_struct.pwm
        val = [pwm.r, pwm.g, pwm.b]

        for wall in led_struct.w:
            for clmn in wall.c:
                for led in clmn.led:
                    # Assume that R/G/B are 1 or 0 -> put into one byte
                    val.extend([led.r + (2 * led.g) + (4 * led.b)])
        # /Generate structure
        self._send_req_bin(cmd="show_img", val=val)

    # /show_img()

    def close(self):
        """Properly close background process

        When closing application, we also want to close background
        process properly.
        """
        self._send_req(cmd="close", val="now")
        msg = 'Waiting for background process...'
        self.logger.debug(msg)
        self.bg_proc.join()

    # /close()
    # |RealCubeAPICls

    # ===========================| Internal functions |=======================
    def _update_device_list(self):
        """Update device list.

        Note that this make sense only during initialization.
        """
        self._send_req(cmd="get_device_list", val="")
        cmd, val = self._get_feedback(blocking=True)

        self._check_feedback(cmd, val, "get_device_list")

        self.device_list = val

    # /_update_device_list()
    # |RealCubeAPICls

    def _update_status(self):
        """Update real cube status

        It is good idea to update this value after connection, disconnection
        or some error.
        """
        self._send_req(cmd="get_status", val="")
        cmd, val = self._get_feedback(blocking=True)

        self._check_feedback(cmd, val, "get_status")

        self.status = val

    # /_update_status()

    def _update_dimension(self):
        """Update cube dimension

        This make sense only when connecting
        """
        self._send_req(cmd="get_dimension", val="")
        cmd, val = self._get_feedback(blocking=True)

        # Retype value to integer. This should be without any problems
        val = int(val)

        self._check_feedback(cmd, val, "get_dimension")

        self.dimension = val

    # /_update_dimension()

    def _check_feedback(self, cmd, val, function_name):
        """Check feedback response.

        Basically if cmd == function_name -> OK. Else fail
        """
        if (cmd == "error"):
            # Special case. In value is error message. Can be useful
            raise Exception(val)
        elif (cmd == "runtime_error"):
            raise RuntimeError(val)
        elif (cmd != function_name):
            msg = 'Internal error!' \
                  ' Expected "{}" command, but got "{}"' \
                  ''.format(function_name, cmd)
            self.logger.critical(msg)
            raise Exception(msg)
        # /if response is not as expected

    # /_check_feedback()
    # |RealCubeAPICls

    def _send_req(self, cmd="connect", val="RGB_LED_CUBE_8x8x8"):
        """Send request to background process

        :param cmd: Command.
        :type cmd: str

        :param val: Value
        :type val: str
        """
        self._req_send.send_string("{} {}".format(cmd, val))

    # /_send_req()
    # |RealCubeAPICls

    def _send_req_bin(self, cmd="show_img", val=b'\x36\x254\x33'):
        """Send binary request to the background process

        :param cmd: Command.
        :type cmd: str

        :param val: Value
        :type val: str
        """
        cmd = cmd + " "
        data = bytearray(cmd, encoding='utf-8') + bytearray(val)
        self._req_send_bin.send(data=data)

    # /_send_req_bin
    # |RealCubeAPICls

    def _get_feedback(self, blocking=False):
        """Get feedback from background process

        :param blocking: If set, then this function will wait until get
                         feedback from background process. Else just try to
                         get command and value. If there are no data, empty
                         command and value are returned.
        :type blocking: bool

        :returns: Command and value
        :rtype: str, str
        """
        cmd = ""
        val = ""

        if (blocking):
            data = self._feedback_get.recv_string()
        else:
            try:
                data = self._feedback_get.recv_string(flags=zmq.NOBLOCK)
            except zmq.Again:
                # No request -> it's also OK
                return (cmd, val)
            # /else some data were in queue
        # /else non-blocking function is required
        cmd, val = str2cmd_val(data=data, debug=self.debug, name=self.name)

        return (cmd, val)

    # /_get_feedback()
    # |RealCubeAPICls

    def _get_err_feedback_non_blocking(self):
        """Get error feedback from background process if any

        :returns: Command and value. If there was no error reported, both
                  values are empty strings.
        :rtype: str, str
        """
        cmd = ""
        val = ""

        try:
            data = self._feedback_err_get.recv_string(flags=zmq.NOBLOCK)
        except zmq.Again:
            # No command in queue
            return (cmd, val)
        else:
            # We got serious data. Deal with it!
            data = data.split(" ", maxsplit=1)
            return (data[0], data[1])
        # /else some data
        assert False, 'Program never ever goes here'

    # /_get_err_feedback_non_blocking()
    # |RealCubeAPICls

    def _clean_up_error_messages_in_feedback(self):
        """This should remove all error messages from feedback queue

        In some cases we need to make sure, that all error messages are out
        from feedback channel
        """
        # Also clean-up all reminding error messages
        msg = 'Waiting for all error messages in queue...'
        self.logger.debug(msg)
        cmd = 'something'
        while (cmd != ''):
            cmd, val = self._get_err_feedback_non_blocking()
        # Wait a bit and try to catch delayed messges
        sleep(1 / self.cfg.req_per_sec)

        cmd = 'something'
        while (cmd != ''):
            cmd, val = self._get_err_feedback_non_blocking()
        msg = 'OK, hopefully all error messages was already send'
        self.logger.debug(msg)

    # |RealCubeAPICls

    def _prepare_ipc(self):
        """Prepare all necessary parts for IPC with background process

        To increase performance, "show_img" command does not send back any
        ACK to confirm that everything is all right -> no need to wait ->
        increase performance. It sends back message only when something goes
        wrong. Technically it is possible detect failure only at next call of
        "show_img", but this will not limit user in any way.
        """

        # Start client - get data from background process
        # (sometimes if required)
        context = zmq.Context()
        self._feedback_get = context.socket(zmq.PULL)
        self._feedback_get.RCVTIMEO = self.cfg.timeout_ms
        self._feedback_get.connect("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_bg2mstr))

        # Run server - send commands to background process
        context = zmq.Context()
        self._req_send = context.socket(zmq.PUSH)
        self._req_send.RCVTIMEO = self.cfg.timeout_ms
        self._req_send.bind("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_mstr2bg))

        # Run server - send binary commands to background process
        context = zmq.Context()
        self._req_send_bin = context.socket(zmq.PUSH)
        self._req_send_bin.RCVTIMEO = self.cfg.timeout_ms
        self._req_send_bin.bind("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_mstr2bg_bin))

        # Run client which will listen for errors on show_img
        context = zmq.Context()
        self._feedback_err_get = context.socket(zmq.PULL)
        self._feedback_err_get.RCVTIMEO = self.cfg.timeout_ms
        self._feedback_err_get.connect("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_bg2mstr_err))
    # /_prepare_ipc()
    # |RealCubeAPICls


# /RealCubeAPICls


class _RealCubeAPIBgCls(Process):
    def __init__(self, logger, settings):
        # This will allow to run this class as single process. It basically
        # mark this class as base class for process. Due to this, it is not
        # recommended to have here too much "class" things. Therefore in
        # function "run()" is basically initialization
        super().__init__()

        # Take logger from parent class
        self.logger = logger

        try:
            # Set process name -> much easier to debug then
            setproctitle('Cube_Real_Cube_bg')
        except Exception:
            msg = 'Can not set process name. Nothing serious.'
            self.logger.info(msg)

        # Also pass settings
        self.cfg = settings

        # This will enable super verbose output. Only for debugging, since
        # CPU overhead is huge.
        self.debug = False

        # Just name used for debugging IPC
        self.name = 'Real Cube API - Bg'

    # /__init__()

    def run(self):
        # Waiting for command
        # Prepare IPC
        msg = 'BgProc: Preparing IPC'
        self.logger.debug(msg)
        self._prepare_ipc()

        # Calculate maximum waiting period when waiting for commands
        self._cmd_w8_period = 1 / self.cfg.req_per_sec

        self._create_list_of_devices()

        self._close = False
        while (not self._close):
            cmd, val = self._get_req()

            if (cmd == "close"):
                self._close = True
                msg = 'Got request to close process -> closing...'
                self.logger.debug(msg)
            elif (cmd == "get_device_list"):
                self._send_feedback(cmd=cmd, val=self.dev_lst_ipc)
            elif (cmd == "get_status"):
                self._send_feedback(cmd=cmd, val=self.cube.status)
            elif (cmd == "get_dimension"):
                # If not connected -> we do not know dimensions -> return 0
                if (self.cube.status == "Not connected"):
                    msg = "Cube not connected yet. We can not know dimension"
                    self.logger.warn(msg)
                    self._send_feedback(cmd=cmd, val=0)
                else:
                    self._send_feedback(
                        cmd=cmd, val=self.cube.real.get_dimension())
            elif (cmd == "connect"):
                cmd, val = self._connect(dev_name=val)
                self._send_feedback(cmd=cmd, val=val)
                # And create cube image. It will be modified later
                self.leds = CubeLEDCls(
                    dimension=self.cube.real.get_dimension())
            elif (cmd == "disconnect"):
                cmd, val = self._disconnect()
                self._send_feedback(cmd=cmd, val=val)
            elif (cmd == "show_img"):
                # Quite complex. Bette in function
                self._show_img(data=val)
            elif (cmd == ""):
                # No command here. Take a short nap
                sleep(self._cmd_w8_period)
            else:
                msg = 'Internal error: unknown command "{}"'.format(cmd)
                self._send_feedback(cmd="error", val=msg)
                raise Exception(msg)
        # /while receiving commands

    # /run()
    # |_RealCubeAPIBgCls

    # ===========================| Internal functions |=======================
    def _show_img(self, data):
        """De-serialize data and create CubeLEDCls

        LED structure will be passed to real LED cube module
        """
        # Check if cube is connected
        if (self.cube.status == "Not connected"):
            msg = "Cube is not connected. Can not show image!"
            self.logger.error(msg)
            self._send_err_feedback(cmd="error", val=msg)
            return
        # /check if not connected

        # De-serialize binary data into self.led. First is PWM, then data
        self.leds.pwm.r = data[0]
        self.leds.pwm.g = data[1]
        self.leds.pwm.b = data[2]

        idx = 3
        for wall in self.leds.w:
            for clmn in wall.c:
                for led in clmn.led:
                    led.r = (data[idx] & 1)
                    led.g = (data[idx] & 2) >> 1
                    led.b = (data[idx] & 4) >> 2
                    # And move to another LED
                    idx = idx + 1
                # /for LEDs
            # /for columns
        # /for walls

        # OK, so now data should be in place, so let's try to send image
        try:
            self.cube.real.show_img(self.leds)
        except RuntimeError as e:
            msg = 'Showing image failed!\n\n{}'.format(e)
            self.logger.error(msg)
            try:
                self._disconnect()
            except Exception:
                pass
            # send error via feedback channel
            self._send_err_feedback(cmd="runtime_error", val=msg)
            return
        # /if runtime exception
        except Exception as e:
            # This is not much expected, but who knows. We have to deal with
            # it as well
            msg = 'Showing image failed! Something bad happened.\n\n{}' \
                  ''.format(e)
            self.logger.error(msg)
            try:
                self._disconnect()
            except Exception:
                pass
            # send error via feedback channel
            self._send_err_feedback(cmd="error", val=msg)
            return
        # /Another error
        # Else everything is fine. No error message is needed to send.

    # /_show_img()
    # |_RealCubeAPIBgCls

    def _connect(self, dev_name="RGB_LED_CUBE_8x8x8-00"):
        """This function will try connect with real cube

        :param dev_name: device name
        :type dev_name: str

        :returns: Command and value for IPC
        :rtype: str, str
        """
        # =========================| Check input parameters |=================
        if (dev_name not in self.dev_lst):
            msg = 'Device "{}" not found in list of supported devices ({})!' \
                  "".format(dev_name, self.dev_lst)
            self.logger.error(msg)
            return ("error", msg)

        # ========================| Try load real cube module |===============
        # Device exist -> dynamically load device's class
        try:
            # Try import file
            real_module = __import__("lib.supported_cubes.{}"
                                     "".format(dev_name),
                                     fromlist=["RealCubeCls"])
            # And if it is OK, import "common" class (same function names)
            self.cube.real = getattr(real_module, "RealCubeCls")()
        except Exception as e:
            msg = 'Can not properly import "{}.py" module.' \
                  " Probably missing \n some library:\n\n" \
                  ' {}' \
                  "".format(dev_name, e)
            self.logger.error(msg)
            return ("error", msg)

        # =======================| Try real connection with HW |==============
        try:
            self.cube.real.connect()
        except RuntimeError as e:
            # Runtime errors are expected. For example hardware is not
            # connected
            self.logger.error(e)
            return ("runtime_error", e)
        except Exception as e:
            # Exception means that there is some problem in code or some
            # unexpected state.
            self.logger.critical(e)
            return ("error", e)
        # /try connect to real cube

        # Change cube status and name
        self.cube.dev_name = dev_name
        self.cube.status = "Connected"

        return ("connect", self.cube.status)

    # /_connect()
    # |_RealCubeAPIBgCls

    def _disconnect(self):
        """Simply disconnect from device
        """
        # Check if device is connected -> if not can not disconnect
        if (self.cube.status == "Not connected"):
            msg = 'Cube "{}" is not connected. Can not disconnect it!' \
                  "".format(self.cube.dev_name)
            self.logger.error(msg)
            return ("runtime_error", msg)
        else:
            # Anyway, consider cube as disconnected. If there is some
            # disconnect error, we're probably not able to communicate with
            # cube anyway.
            # Disconnection OK
            self.cube.status = "Not connected"

            # Device connected -> disconnect properly and check for error
            try:
                self.cube.real.disconnect()
            except RuntimeError as e:
                # It should not, but it may happen
                self.logger.error(e)
                return ("runtime_error", e)
            except Exception as e:
                self.logger.critical(e)
                return ("error", e)
            else:
                # No error
                return ("disconnect", self.cube.status)
            # /else no error
        # /else try to disconnect

    # /_disconnect()
    # |_RealCubeAPIBgCls

    def _create_list_of_devices(self):
        """Create list of supported devices

        This function need to be called only once. Ideally during
        initialization.
        """
        # List of supported devices
        self.logger.debug("Getting list of supported devices")

        # True filtered list of supported devices
        self.dev_lst = []

        all_files = listdir(path='{}/lib/supported_cubes'
                                 ''.format(app_path))
        for device in all_files:
            # Check if filename contain .py
            if (".py" in device):
                # So .py was found -> add device to list, but remove last ".py"
                # (3 chars)
                if (device != "__init__.py"):
                    # Well, we have to ignore __init__.py
                    self.dev_lst.append(device[:-3])
                # /if device name is not __init__.py - obviously wrong
            # /if found .py filename
        # /go through all filenames

        # /for all devices
        # Sort it according to ASCII table
        self.dev_lst = sorted(self.dev_lst, key=str.lower)

        # Also generate device list for IPC - no need to re-generate every time
        self.dev_lst_ipc = ""
        for dev in self.dev_lst:
            self.dev_lst_ipc = "{} {}".format(self.dev_lst_ipc, dev)
        # /create device list for IPC
        # Remove redundant space
        self.dev_lst_ipc = self.dev_lst_ipc[1:]

        self.logger.debug("Supported devices: {}".format(self.dev_lst))

        self.cube = self._StatusStructCls()

    # /_create_list_of_devices()
    # |_RealCubeAPIBgCls

    def _get_req(self):
        """Get request from master process (non blocking function)

        :returns: command and value as string. If no command is received, then
                  command value is empty string
        :rtype: str, str
        """
        # By default there is no command
        cmd = ""
        val = ""

        try:
            data = self._req_get.recv_string(flags=zmq.NOBLOCK)
        except zmq.Again:
            # No command -> get out. Try to receive binary data
            pass
        else:
            # Else some real deal - we need to process
            cmd, val = str2cmd_val(data, self.debug, self.name)
            return (cmd, val)
        # /else received some data

        # If we went here, no command was detected -> try luck with binary
        # data. Reason why binary data are tested later is that we can get
        # overwhelmed binary data. Once this happen, there have to be some
        # emergency break which allow to disconnect real cube and stop this
        # madness. That is why we check binary data secondly.
        #
        # It is expected, that this channel is used ONLY for show_img().
        # And sometimes may happen, that sensing images take actually more
        # time than real cube driver can handle/there is another bottleneck.
        # It that case it is better to skip some images to keep smooth GUI
        # response.
        bin_data_old = None
        # cnt = 0
        while (True):
            try:
                bin_data_old = self._req_get_bin.recv(flags=zmq.NOBLOCK)
            except zmq.Again:
                if (bin_data_old is None):
                    # No data -> return empty cmd and val
                    return (cmd, val)
                # /if there are not and was not any data -> just get out
                else:
                    # There were some data, but now not -> show them
                    break
            else:
                # Some data, store them for later use
                bin_data = bin_data_old
                # cnt = cnt + 1
                # It is possible to track how many images was dropped, but this
                # but it would significantly increased processing time and just
                # make this problem worse. And because it is better to silently
                # drop images to keep them synchronized at least
                # print(cnt)
                # /if one image is skipped
            # /else got some data
        # /while
        # OK, we got some data.

        # Find first space -> split command and value. Index counter is
        # shifted by 1, since we want to ignore space followed by command
        idx_cnt = 1
        for one_byte in bin_data:
            # If space found (ASCII 32)
            if (one_byte == 32):
                break
            else:
                # Slowly complete command. Byte by byte
                cmd = cmd + chr(one_byte)
                idx_cnt = idx_cnt + 1
            # /else still command
        # /for bin_data (bytes)
        val = bin_data[idx_cnt:]

        return (cmd, val)
        # /else there are some binary data
        assert False, 'This never should happen. Maybe universe is collapsing?'

    # /_get_req()
    # |_RealCubeAPIBgCls

    def _send_feedback(self, cmd="play_next_img", val=15):
        """Send feedback/request to master process

        :param cmd: Command
        :type cmd: str

        :param val: Command value/arguments.
        :type val: str/int/list/...
        """
        self._feedback_send.send_string("{} {}".format(cmd, val))

    # /_send_req()
    # |_RealCubeAPIBgCls

    def _send_err_feedback(self, cmd="runtime_error", val="description"):
        """Report error to master process

        Basically dedicated for show_img() errors

        :param cmd: Command/error type
        :type cmd: str

        :param val: Command value/error description
        :type val: str
        """
        self._feedback_err_send.send_string("{} {}"
                                            "".format(cmd, val))

    # /_send_err_feedback()
    # |_RealCubeAPIBgCls

    def _prepare_ipc(self):
        """Prepare all necessary parts for IPC with background process
        """
        # Start binary client - only for receiving binary messages
        # (show_img()).
        context = zmq.Context()
        self._req_get_bin = context.socket(zmq.PULL)
        self._req_get_bin.RCVTIMEO = self.cfg.timeout_ms
        self._req_get_bin.connect("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_mstr2bg_bin))

        # Start client - receive messages to the queue. Can reply later.
        context = zmq.Context()
        self._req_get = context.socket(zmq.PULL)
        self._req_get.RCVTIMEO = self.cfg.timeout_ms
        self._req_get.connect("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_mstr2bg))

        # Start our server. It may inform master process about some problems
        context = zmq.Context()
        self._feedback_send = context.socket(zmq.PUSH)
        self._feedback_send.RCVTIMEO = self.cfg.timeout_ms
        self._feedback_send.bind("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_bg2mstr))

        # Start server for reporting errors when calling show_img()
        context = zmq.Context()
        self._feedback_err_send = context.socket(zmq.PUSH)
        self._feedback_err_send.RCVTIMEO = self.cfg.timeout_ms
        self._feedback_err_send.bind("tcp://{}:{}".format(
            self.cfg.ip,
            self.cfg.port_bg2mstr_err))

    # /_prepare_ipc()
    # |_RealCubeAPIBgCls

    # =============================| Internal class |=========================
    # Cube status structure
    class _StatusStructCls():

        def __init__(self):
            self.status = "Not connected"
            self.dev_name = "unknown"
    # /StatusStructCls
# /_RealCubeAPIBgCls

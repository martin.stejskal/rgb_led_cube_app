#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
Library for loading configuration files
=======================================

This module handle all "control frames" for 3D led cube.

``author`` Martin Stejskal

``version`` 0.3.1
"""

# Playing with configuration files
import yaml

# Our local libraries
# tuned logger
from lib.common import CmnLgrCls

# Complete settings
from lib.gui_switcher import GUISwitcherSettingsCls


class LoadCfgCls:
    """
    Handle all operations with settings

    During initialization is loaded settings from file and is re-written
    factory default.
    """

    def __init__(self, cfg_file='cfg/app.yaml'):
        # Logger initialization
        self.logger = CmnLgrCls("load_cfg")

        # Load factory settings first (primary check for errors)
        self.load_factory()

        # Then do update
        self.reload(cfg_file=cfg_file)

        self.logger.debug("Initialization done")

    # /__init__()
    # |LoadCfgCls

    def get(self):
        """Get loaded settings

        This settings is kept unchanged until is called function "reload" or
        "load_factory" function.

        :returns: complete settings
        :rtype: struct
        """
        return (self.settings)

    # /get()
    # |LoadCfgCls

    def load_factory(self):
        """Load factory settings

        Local settings structure will be updated as well.

        :returns: complete settings
        :rtype: struct
        """
        self.settings = GUISwitcherSettingsCls()
        return (self.settings)

    # /load_factory()
    # |LoadCfgCls

    def reload(self, cfg_file='cfg/app.yaml'):
        """Reload all settings from configuration files.

        Local settings structure will be updated as well

        :returns: complete settings
        :rtype: struct
        """
        with open(cfg_file, 'r') as f:
            try:
                cfg = yaml.safe_load(f)
            except yaml.YAMLError as e:
                msg = 'Can not parse data in {} file:\n{}' \
                      ''.format(cfg_file, e)
                self.logger.error(msg)
                raise yaml.YAMLError(msg)
        # /Read configuration file

        self.logger.debug('Loaded configuration:\n{}'.format(cfg))

        # A lot of settings is related to cube 3D view -> shortcut
        cube_3D_view = self.settings.console_api.cube_3D.cube_3D_view
        # =================================| Basic |==========================
        section = 'basic'

        val = 'dimension'
        default = cube_3D_view.dimension
        new = self._update_value(cfg, [section, val], default, 'int')
        cube_3D_view.dimension = new

        val = 'default_mode'
        default = self.settings.console_api.mode
        new = self._update_value(cfg, [section, val], default)
        self.settings.console_api.mode = new

        # ==================================| GUI |===========================
        section = 'gui'

        val = 'core'
        default = self.settings.gui_core
        new = self._update_value(cfg, [section, val], default)
        self.settings.gui_core = new
        # ================================| 3D cube |=========================
        section = 'cube_3D'

        val = 'width'
        default = cube_3D_view.window.width
        new = self._update_value(cfg, [section, val], default, 'int')
        cube_3D_view.window.width = new

        val = 'height'
        default = cube_3D_view.window.height
        new = self._update_value(cfg, [section, val], default, 'int')
        cube_3D_view.window.height = new

        val = 'color_bg'
        default = cube_3D_view.window.bg_color
        new = self._update_value(cfg, [section, val], default, 'tuple_int')
        cube_3D_view.window.bg_color = new

        val = 'color_floor'
        default = cube_3D_view.floor.color
        new = self._update_value(cfg, [section, val], default, 'tuple_int')
        cube_3D_view.floor.color = new

        val = 'color_wireframe'
        default = cube_3D_view.wireframe.color
        new = self._update_value(cfg, [section, val], default, 'tuple_int')
        cube_3D_view.wireframe.color = new

        val = 'led_size'
        default = cube_3D_view.led.size
        new = self._update_value(cfg, [section, val], default, 'float')
        cube_3D_view.led.size = new

        val = 'led_quality'
        default = cube_3D_view.led.render_quality
        new = self._update_value(cfg, [section, val], default, 'int')
        cube_3D_view.led.render_quality = new

        self.logger.debug(self.settings)

    # /reload()
    # |LoadCfgCls

    # ===========================| Internal functions |=======================
    def _update_value(
            self, cfg_data, option, default_value, var_type='str'):
        """Update required option value

        Check if required value is define at configuration structure. If yes,
        then return value defined in configuration structure. Else use default
        value.

        :param cfg_data: Parsed YAML file as variable. Configuration structure
        :type cfg_data: dict

        :param option: The "path" to the requested option as array. Every item
                       symbolize one "level" in YAML hierarchy. For example
                       ['basic', 'dimension'] means basic: dimension: 5
        :type option: list (str)

        :param var_type: Expected type of variable. By default is everything
                        string, so no conversion is needed. However in some
                        cases conversion is needed. Here is list of supported
                        types:
                        str, int, float, bool, tuple_int
        :type var_type: str

        :returns: New value. Can be default if value is not defined at
                  configuration structure.
        :rtype: obj
        """
        loaded_option = self._is_option_set(cfg_data, option)

        if (loaded_option is None):
            msg = 'Option {} is not defined. Using default one: {}' \
                  ''.format(option, default_value)
            self.logger.debug(msg)
            return (default_value)
        else:
            try:
                if (var_type == 'str'):
                    # No need to modify value
                    pass
                elif (var_type == 'int'):
                    loaded_option = int(loaded_option)
                elif (var_type == 'float'):
                    loaded_option = float(loaded_option)
                elif (var_type == 'bool'):
                    # Make sure that it is treated as string
                    loaded_option = str(loaded_option)
                    if ((loaded_option.lower() == 'false') or
                            (loaded_option == '0') or
                            (loaded_option.lower() == 'n') or
                            (loaded_option.lower() == 'no')):
                        loaded_option = False
                    else:
                        loaded_option = True
                elif (var_type == 'tuple_int'):
                    # Tuple is little bit tricky. So let's make everything in
                    # array and then convert to tuple
                    new_tuple = []
                    # Remove "(", ")" from loaded option
                    loaded_option = loaded_option.replace('(', '')
                    loaded_option = loaded_option.replace(')', '')

                    loaded_option = loaded_option.split(',')

                    for val in loaded_option:
                        new_tuple.append(int(val))
                    # /for all items in loaded option
                    loaded_option = tuple(new_tuple)
                else:
                    msg = 'Internal error! Unexpected variable type "{}"' \
                          ''.format(var_type)
                    self.logger.critical(msg)
                    raise Exception(msg)
            except Exception as e:
                msg = 'Error occurred when casting {} type:\n{}' \
                      ''.format(option, e)
                self.logger.error(msg)
                raise Exception(msg)
            # /if casting failed

            if (loaded_option == default_value):
                msg = 'Option {} defined, however it is same as default' \
                      ' value: {}' \
                      ''.format(option, loaded_option)
                self.logger.info(msg)
            else:
                msg = 'Option {} defined. Value: {}\n(Default: {})' \
                      ''.format(option, loaded_option, default_value)
                self.logger.info(msg)
            return (loaded_option)

    # /_update_value()
    # |LoadCfgCls

    def _is_option_set(self, cfg_data, option=['basic', 'dimension']):
        """Check if given option is set or not at configuration file/structure

        :param cfg_data: Parsed YAML file as variable
        :type cfg_data: dict

        :param option: The "path" to the requested option as array. Every item
                       symbolize one "level" in YAML hierarchy. For example
                       ['basic', 'dimension'] means basic: dimension: 5
        :type option: list (str)

        :returns: Value if defined. Else return None
        :rtype: obj/None
        """
        # Algorythm will "move" through cfg_data via cfg_obj
        cfg_obj = cfg_data

        not_found = False

        for cfg in option:
            if (cfg in cfg_obj.keys()):
                # Option found -> move deeper
                cfg_obj = cfg_obj[cfg]
                if (cfg_obj is None):
                    # Empty variable -> break
                    not_found = True
                    break
            else:
                not_found = True
                break
        # /Try to find option

        if (not_found):
            return (None)
        else:
            return (cfg_obj)
    # /_is_option_set()
    # |LoadCfgCls
# /LoadCfgCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============
Control window
==============

This module handle all "control frames" for 3D led cube.
Also this module is responsible for reaction to user inputs
(keyboard, mouse).

``author`` Martin Stejskal

``version`` 0.7.2
"""
# Basic GUI for Python
import tkinter

# Common parts
from lib.common import DummyCls, CmnLgrCls, app_path, log_traceback

# Console API
from lib.console_api import ConsoleAPICls

# Common parts for GUI
from lib.gui.tkinter.lib.common_tk import PosStructCls

# GUI parts - static
from lib.gui.tkinter.lib.mode_switcher import ModeSwCls
from lib.gui.tkinter.lib.conn_dev import ConnDevCls

# GUI parts - dynamic
from lib.gui.tkinter.lib.pos import PosCls
from lib.gui.tkinter.lib.pwm import PWMCls
from lib.gui.tkinter.lib.wall import WallCls
from lib.gui.tkinter.lib.view import ViewCls
from lib.gui.tkinter.lib.op import OpCls
from lib.gui.tkinter.lib.btn_array import BtnArrayCls
from lib.gui.tkinter.lib.play import PlayCls
from lib.gui.tkinter.lib.load_save import LoadSaveCls
from lib.gui.tkinter.lib.rndm import RndmCls
from lib.gui.tkinter.lib.edit import EditCls
from lib.gui.tkinter.lib.effects import EffectsCls


class ControlWCls:
    """Control window"""

    def __init__(self, settings):
        """Initialization function

        :param settings: Expected class ConsoleAPISettingsCls
        :type settings: class
        """
        # Initialize logger
        self.logger = CmnLgrCls(logger_name="gui_control_w")

        # Save settings to instance
        self.settings = settings

        # Initialize runtime variables
        self.rt = _ControlWRtCls()

        # Initialize no GUI API and pass settings
        self.console_api = ConsoleAPICls(self.settings)
        self.logger.debug("Console API initialized. Initializing tkinter...")

        # Try to finish initialization. If something goes wrong, try at least
        # close all background process
        try:
            self._init_finish()
        except Exception as e:
            msg = "__init__: Internal error! Got exception when initializing" \
                  "\n\n{}".format(e)
            self.logger.critical(msg)
            log_traceback(self.logger)
            self.close()
            # and raise original exception
            raise
        # /try to finish initialization

    # /__init__()
    # |ControlWCls

    # ===================| Functions required by gui_switcher |===============
    def is_closing(self):
        """Is application got close request from user?"""
        return (self.rt.is_closing)

    # /is_closing()
    # |ControlWCls

    def update(self):
        """Update GUI"""
        # If there is close request, does not update window
        if (self.is_closing()):
            msg = "Close request from control window. Can not update."
            self.logger.debug(msg)
            return
        # /if close request at main window

        # To avoid sending periodically REQ/REP to console API background
        # process, notifications system was developed. It minimize data
        # communication and improve overall performance.
        self.console_api.notif.process_notifications()

        # Check if there was close request
        if (self.is_closing()):
            return

        # If it is animation mode, try to update "play" module
        if (self.get_mode() == "anim"):
            self.anim_lout.ply.c.update()

        # Update main window. This is necessary for checking pressing any
        # button or any action at GUI by user
        self.w_ctrl.update()

    # /update()
    # |ControlWCls

    def close(self):
        """Try to close all background process"""
        self.rt.is_closing = True

        try:
            self.console_api.close()
        except Exception as e:
            msg = 'Can not close "console_api"!\n\n{}'.format(e)
            self.logger.error(msg)
        # /try to close console_api

    # /close()
    # |ControlWCls

    # =================| Functions NOT required by gui_switcher |=============
    def set_dimension(self):
        """This callback function should be called when dimension is changed

        When dimension is changed, some GUI items need to be updated.
        """
        # Clear actual workspace
        self.clear_workspace()

        # Everything else should be piece of cake
        self.build_workspace()

        # Due to changes of dimension, real cube might get disconnected ->
        # -> update status
        self.f.conn_dev.func.update_status_according_to_console_api()

    # /set_dimension()
    # |ControlWCls

    def get_dimension(self):
        """Get dimension from console API

        :returns: Cube dimension
        :rtype: uint
        """
        return (self.console_api.get_dimension())

    # /get_dimension()
    # |ControlWCls

    def set_mode(self, mode="draw"):
        """Change mode

        :param mode: selected mode: draw,anim,random
        :type mode: str
        """
        self.clear_workspace()

        # Change mode at console API
        self.console_api.set_mode(mode)

        # Now it is time to rebuild GUI according to new mode
        self.build_workspace()

        # Workspace was build from scratch, but maybe we have some animation
        # in progress, so it is good idea to update play module if new mode
        # is "anim"
        if (mode == "anim"):
            self.anim_lout.ply.c.update(force_update=True)

    # /set_mode()
    # |ControlWCls

    def get_mode(self):
        """Return actual mode from GUI module

        :returns: actual mode
        :rtype: str
        """
        return (self.f.mode.func.get_mode())

    # /get_mode()
    # |ControlWCls

    def build_workspace(self):
        """This function re-build workspace

        Since a lot of things is dependent on dimension, these things may be
        needed to be refreshed sometimes (user change cube dimension by
        connecting real cube)
        """
        self.logger.debug("Building workspace...")
        self._prepare_workspace_frames()

        # ================================| Menu bar |========================
        # Start with menu bar (there can be only one)
        self.f.menu_bar = tkinter.Menu(self.w_ctrl)

        # Display menu bar. Menu bar is kind of exception. It can not be placed
        # anywhere. It simply have to belong root window. That is why it is
        # defined later.
        self.w_ctrl.config(menu=self.f.menu_bar)

        # Position of all modules depends on mode
        mode = self.get_mode()
        if (mode == "anim"):
            self._prepare_anim_workspace()
        elif (mode == "draw"):
            self._prepare_draw_workspace()
        elif (mode == "random"):
            self._prepare_random_workspace()
        else:
            raise Exception("Internal error: unknown mode {}".format(mode))

        # Set callbacks - need to be refreshed every time, since original
        # objects might changed
        self._set_callbacks()

        self.logger.debug("Building workspace done")

    # /build_workspace()
    # |ControlWCls

    def clear_workspace(self):
        """Clear actual workspace to make space for new one

        This function have to be called before
        "self.console_api.set_mode(new_mode)"
        """
        # Get old mode
        old_mode = self.console_api.get_mode()

        # According to old mode, properly close actual view
        if (old_mode == "anim"):
            self.logger.info("Closing all animation layout")
            # Button array -> as another window -> need to be closed properly
            self.anim_lout.btn_ary.c.close()

            # Clearing menu bar is more delicate operation
            self.f.menu_bar.delete("PWM")
            self.f.menu_bar.delete("Edit")
            self.f.menu_bar.delete("View")
            self.f.menu_bar.delete("Effects")

            # Destroy all tkinter frames
            al = self.anim_lout
            frames = [al.wll.f, al.pwm.f, al.op.f, al.btn_ary.f, al.ld_sve.f,
                      al.ply.f, al.edit.f, al.view.f, al.effects.f,
                      self.f.wrkspc_t, self.f.wrkspc_d,
                      self.f.wrkspc_l, self.f.wrkspc_r,
                      self.f.wrkspc_b]

            for frame in frames:
                frame.destroy()

            # And remove reference to class
            del self.anim_lout
        elif (old_mode == "draw"):
            self.draw_lout.btn_ary.c.close()

            # Clearing menu bar is more delicate operation
            self.f.menu_bar.delete("PWM")
            self.f.menu_bar.delete("Edit")
            self.f.menu_bar.delete("View")

            # Destroy all tkinter frames
            dl = self.draw_lout
            frames = [dl.wll.f, dl.pwm.f, dl.op.f, dl.btn_ary.f, dl.ld_sve.f,
                      dl.edit.f, dl.view.f,
                      self.f.menu_bar,
                      self.f.wrkspc_t, self.f.wrkspc_d,
                      self.f.wrkspc_l, self.f.wrkspc_r,
                      self.f.wrkspc_b]

            for frame in frames:
                frame.destroy()

            # Remove reference to class
            del self.draw_lout
        elif (old_mode == "random"):
            rl = self.rndm_lout

            self.f.menu_bar.delete("View")

            frames = [rl.rndm.f, rl.view.f,
                      self.f.wrkspc_t, self.f.wrkspc_d,
                      self.f.wrkspc_l, self.f.wrkspc_r,
                      self.f.wrkspc_b]

            for frame in frames:
                frame.destroy()

            del self.rndm_lout
        else:
            raise Exception(f"Internal error: unknown mode {old_mode}")

    # /clear_workspace()
    # |ControlWCls

    def disable_all_windows(self):
        """Disable all main windows

        This might be useful when another window pop-up and it is necessary
        to make sure that user will not use functions from other windows.
        """
        self.logger.debug("Disabling all windows")

        # Disable closing window
        self._disable_closing_window()

        # Now disable modules
        for module in self._get_list_of_active_modules():
            module.dis_widgets()

    # /disable_all_windows()
    # |ControlWCls

    def enable_all_windows(self):
        """Enable all main windows

        This function should be called after disable_all_windows(). It will
        re-enable all main windows again.
        """
        self.logger.debug("Enabling all windows")

        # Now enable modules
        for module in self._get_list_of_active_modules():
            module.en_widgets()

        # Enable closing window again
        self._enable_closing_window()

    # /enable_all_windows()
    # |ControlWCls

    # ===========================| Internal functions |=======================
    def _on_closing(self):
        """Called when main windows is closing"""

        """Just for reference
        if(messagebox.askokcancel("Quit", "Do you want quit application?")):
            self.rt.is_closing = True
            # And close console API
            self.close()
        """
        self.rt.is_closing = True
        # And close console API
        self.close()

    # /_on_closing()

    def _on_closing_dummy(self):
        """Called when closing button should be disabled
        """
        pass

    # /_on_closing_dummy()

    def _enable_closing_window(self):
        self.w_ctrl.protocol("WM_DELETE_WINDOW", self._on_closing)

    def _disable_closing_window(self):
        self.w_ctrl.protocol("WM_DELETE_WINDOW", self._on_closing_dummy)

    def _init_finish(self):
        """Finish initialization sequence"""
        # Initialize control window
        self.logger.debug("starting...")
        self.w_ctrl = tkinter.Tk()

        self.w_ctrl.title("Control")

        # This is not critical part and it is problematic on Pyinstaller
        try:
            # At Linux it have to be in xbm format and with "@" symbol.
            self.w_ctrl.iconbitmap('@{}/lib/app_icon.xbm'
                                   ''.format(app_path))
        except Exception:
            msg = "Can not load icon, but this is not crucial"
            self.logger.warn(msg)

        # When closing, callback on_closing()
        self._enable_closing_window()

        # Set binds
        self.w_ctrl.bind("<Key>", self._kbrd_event)

        # It seems that mouse events are pretty useless
        # self.w_ctrl.bind("<Button-1>", self._mouse_event)
        # Frame object
        self.f = DummyCls()

        # Prepare frames in control window for static GUI
        self._prepare_static_frames()

        # Setup static part of GUI (mode switcher, device connection, position)
        self._prepare_static_gui()

        # Build workspace GUI (dynamic - depend on mode, cube dimension
        # and so on)
        self.build_workspace()

        self.logger.debug("done")

    # /_init_finish()

    def _prepare_static_frames(self):
        """Prepare static frames on control window"""
        # Load class with frame positions (more easy to change GUI)
        f_pos = _FramesPosCls()

        # ==================================| Mode |==========================
        self.f.mode = tkinter.Frame(self.w_ctrl,
                                    borderwidth=2,
                                    relief=tkinter.RIDGE)
        self.f.mode.grid(column=f_pos.mode.column,
                         row=f_pos.mode.row,
                         columnspan=f_pos.mode.column_width,
                         rowspan=f_pos.mode.row_width)

        # =============================| Connect device |=====================
        self.f.conn_dev = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.conn_dev.grid(column=f_pos.conn_dev.column,
                             row=f_pos.conn_dev.row,
                             columnspan=f_pos.conn_dev.column_width,
                             rowspan=f_pos.conn_dev.row_width)

        # ================================| Position |========================
        self.f.pos = tkinter.Frame(self.w_ctrl,
                                   borderwidth=2,
                                   relief=tkinter.RIDGE)
        self.f.pos.grid(column=f_pos.pos.column,
                        row=f_pos.pos.row,
                        columnspan=f_pos.pos.column_width,
                        rowspan=f_pos.pos.row_width)

    # /_prepare_static_frames()

    def _prepare_workspace_frames(self):
        """Prepare workspace frames on control window"""
        # Load class with frame positions (more easy to change GUI)
        f_pos = _FramesPosCls()

        # Workspace - top
        self.f.wrkspc_t = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.wrkspc_t.grid(column=f_pos.wrkspc_t.column,
                             row=f_pos.wrkspc_t.row,
                             columnspan=f_pos.wrkspc_t.column_width,
                             rowspan=f_pos.wrkspc_t.row_width)

        # Workspace - down
        self.f.wrkspc_d = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.wrkspc_d.grid(column=f_pos.wrkspc_d.column,
                             row=f_pos.wrkspc_d.row,
                             columnspan=f_pos.wrkspc_d.column_width,
                             rowspan=f_pos.wrkspc_d.row_width)
        # Workspace  - left
        self.f.wrkspc_l = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.wrkspc_l.grid(column=f_pos.wrkspc_l.column,
                             row=f_pos.wrkspc_l.row,
                             columnspan=f_pos.wrkspc_l.column_width,
                             rowspan=f_pos.wrkspc_l.row_width)
        # Workspace  - right
        self.f.wrkspc_r = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.wrkspc_r.grid(column=f_pos.wrkspc_r.column,
                             row=f_pos.wrkspc_r.row,
                             columnspan=f_pos.wrkspc_r.column_width,
                             rowspan=f_pos.wrkspc_r.row_width)

        # Workspace - bottom
        self.f.wrkspc_b = tkinter.Frame(self.w_ctrl,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)
        self.f.wrkspc_b.grid(column=f_pos.wrkspc_b.column,
                             row=f_pos.wrkspc_b.row,
                             columnspan=f_pos.wrkspc_b.column_width,
                             rowspan=f_pos.wrkspc_b.row_width)

    # /_prepare_workspace_frames

    def _prepare_draw_workspace(self):
        # Draw layout
        self.draw_lout = DummyCls()

        # Prepare variables for drawing layout
        # Modules: PWM, wall, button array, ...
        self.draw_lout.wll = _SubModuleStruct()
        self.draw_lout.pwm = _SubModuleStruct()
        self.draw_lout.op = _SubModuleStruct()
        self.draw_lout.btn_ary = _SubModuleStruct()
        self.draw_lout.ld_sve = _SubModuleStruct()
        self.draw_lout.edit = _SubModuleStruct()
        self.draw_lout.view = _SubModuleStruct()

        # Add tkinter frames
        self.draw_lout.wll.f = tkinter.Frame(self.f.wrkspc_d,
                                             borderwidth=2,
                                             relief=tkinter.RIDGE)
        self.draw_lout.pwm.f = tkinter.Frame(self.f.wrkspc_d,
                                             borderwidth=2,
                                             relief=tkinter.RIDGE)

        self.draw_lout.op.f = tkinter.Frame(self.f.wrkspc_l,
                                            borderwidth=2,
                                            relief=tkinter.RIDGE)
        self.draw_lout.btn_ary.f = tkinter.Frame(self.f.wrkspc_l,
                                                 borderwidth=2,
                                                 relief=tkinter.RIDGE)
        self.draw_lout.ld_sve.f = tkinter.Frame(self.f.wrkspc_l,
                                                borderwidth=2,
                                                relief=tkinter.RIDGE)
        self.draw_lout.edit.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=1,
                                              relief=tkinter.RIDGE)
        self.draw_lout.view.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=1,
                                              relief=tkinter.RIDGE)

        # Put sub-frames to animation layout object. Let's hardcode it for
        # now, since GUI is not changed too often and still: everything will
        # be on this place, so it is easy to modify.

        self.draw_lout.wll.f.grid(column=0, row=0)
        self.draw_lout.pwm.f.grid(column=0, row=1)

        self.draw_lout.op.f.grid(column=0, row=1)
        self.draw_lout.ld_sve.f.grid(column=0, row=0)
        self.draw_lout.btn_ary.f.grid(column=9, row=9)

        # This is irrelevant. This is just item in menu bar. But in case that
        # in future this will not be true, here is this backup (so buttons
        # will be at least somewhere displayed)
        self.draw_lout.edit.f.grid(column=90, row=90)
        self.draw_lout.view.f.grid(column=91, row=91)

        # Now add sub-modules itself to "c" (class)
        self.draw_lout.pwm.c = PWMCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.pwm.f,
                            menu_bar=self.f.menu_bar))

        self.draw_lout.wll.c = WallCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.wll.f))

        self.draw_lout.op.c = OpCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.op.f))

        # Add "permissions" to handle keyboard shortcuts
        self.draw_lout.btn_ary.c = BtnArrayCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.btn_ary.f,
                            kbrd_event_fnc=self._kbrd_event),
            close_callback=self.close)

        # No play GUI here
        self.draw_lout.ld_sve.c = LoadSaveCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.ld_sve.f),
            dim_changed_f=self.set_dimension,
            play_gui=None)

        self.draw_lout.edit.c = EditCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.edit.f,
                            menu_bar=self.f.menu_bar),
            pwm_gui=self.draw_lout.pwm.c)

        self.draw_lout.view.c = ViewCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.draw_lout.view.f,
                            menu_bar=self.f.menu_bar))

    # /_prepare_draw_workspace()

    def _prepare_anim_workspace(self):
        # Animation layout
        self.anim_lout = DummyCls()

        # Prepare variables for animation layout
        # Modules: PWM, wall, button array, ...
        self.anim_lout.wll = _SubModuleStruct()
        self.anim_lout.pwm = _SubModuleStruct()
        self.anim_lout.op = _SubModuleStruct()
        self.anim_lout.ply = _SubModuleStruct()
        self.anim_lout.btn_ary = _SubModuleStruct()
        self.anim_lout.ld_sve = _SubModuleStruct()
        self.anim_lout.edit = _SubModuleStruct()
        self.anim_lout.view = _SubModuleStruct()
        self.anim_lout.effects = _SubModuleStruct()

        # Add tkinter frames
        self.anim_lout.wll.f = tkinter.Frame(self.f.wrkspc_d,
                                             borderwidth=2,
                                             relief=tkinter.RIDGE)
        self.anim_lout.pwm.f = tkinter.Frame(self.f.wrkspc_d,
                                             borderwidth=2,
                                             relief=tkinter.RIDGE)

        self.anim_lout.op.f = tkinter.Frame(self.f.wrkspc_l,
                                            borderwidth=2,
                                            relief=tkinter.RIDGE)
        self.anim_lout.btn_ary.f = tkinter.Frame(self.f.wrkspc_l,
                                                 borderwidth=2,
                                                 relief=tkinter.RIDGE)
        self.anim_lout.ld_sve.f = tkinter.Frame(self.f.wrkspc_l,
                                                borderwidth=2,
                                                relief=tkinter.RIDGE)

        self.anim_lout.ply.f = tkinter.Frame(self.f.wrkspc_b,
                                             borderwidth=2,
                                             relief=tkinter.RIDGE)
        self.anim_lout.edit.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=2,
                                              relief=tkinter.RIDGE)
        self.anim_lout.view.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=1,
                                              relief=tkinter.RIDGE)
        self.anim_lout.effects.f = tkinter.Frame(self.f.wrkspc_d,
                                                 borderwidth=1,
                                                 relief=tkinter.RIDGE)

        # Put sub-frames to animation layout object. Let's hardcode it for
        # now, since GUI is not changed too often and still: everything will
        # be on this place, so it is easy to modify.

        self.anim_lout.wll.f.grid(column=0, row=0)
        self.anim_lout.pwm.f.grid(column=0, row=1)

        self.anim_lout.op.f.grid(column=0, row=1)
        self.anim_lout.ld_sve.f.grid(column=0, row=0)
        self.anim_lout.btn_ary.f.grid(column=9, row=9)

        self.anim_lout.ply.f.grid(column=0, row=0)

        # This is irrelevant. This is just item in menu bar. But in case that
        # in future this will not be true, here is this backup (so buttons
        # will be at least somewhere displayed)
        self.anim_lout.edit.f.grid(column=90, row=90)
        self.anim_lout.view.f.grid(column=91, row=91)
        self.anim_lout.effects.f.grid(column=92, row=92)

        # Now add sub-modules itself to "c" (class)
        self.anim_lout.pwm.c = PWMCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.pwm.f,
                            menu_bar=self.f.menu_bar))

        self.anim_lout.wll.c = WallCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.wll.f))

        self.anim_lout.op.c = OpCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.op.f))

        # Add "permissions" to handle keyboard shortcuts
        self.anim_lout.btn_ary.c = BtnArrayCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.btn_ary.f,
                            kbrd_event_fnc=self._kbrd_event),
            close_callback=self.close)

        # Need access to PWM GUI
        self.anim_lout.ply.c = PlayCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.ply.f),
            pwm_gui=self.anim_lout.pwm.c)

        # Need access to "play" module to force update when loaded new
        # animation
        self.anim_lout.ld_sve.c = LoadSaveCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.ld_sve.f),
            dim_changed_f=self.set_dimension,
            play_gui=self.anim_lout.ply.c)

        self.anim_lout.edit.c = EditCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.edit.f,
                            menu_bar=self.f.menu_bar),
            pwm_gui=self.anim_lout.pwm.c)
        self.anim_lout.view.c = ViewCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.view.f,
                            menu_bar=self.f.menu_bar))
        self.anim_lout.effects.c = EffectsCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.anim_lout.effects.f,
                            menu_bar=self.f.menu_bar,
                            en_windows=self.enable_all_windows,
                            dis_windows=self.disable_all_windows),
            update_play_gui=self.anim_lout.ply.c.update)

    # /_prepare_anim_workspace()

    def _prepare_random_workspace(self):
        self.rndm_lout = DummyCls()

        self.rndm_lout.rndm = _SubModuleStruct()
        self.rndm_lout.view = _SubModuleStruct()

        self.rndm_lout.rndm.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=2,
                                              relief=tkinter.RIDGE)
        self.rndm_lout.view.f = tkinter.Frame(self.f.wrkspc_d,
                                              borderwidth=1,
                                              relief=tkinter.RIDGE)

        self.rndm_lout.rndm.f.grid(column=0, row=0)
        # View is using menubar - position is basically irrelevant
        self.rndm_lout.view.f.grid(column=91, row=91)

        self.rndm_lout.rndm.c = RndmCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.rndm_lout.rndm.f))
        self.rndm_lout.view.c = ViewCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.rndm_lout.view.f,
                            menu_bar=self.f.menu_bar,
                            mode=self.get_mode()))

    # /_prepare_random_workspace()

    def _prepare_static_gui(self):
        """This will render/add parts of GUI, which are same for all modes"""
        self.logger.debug("preparing static GUI...")

        # Most important is mode select
        # This module need to callback some function when mode is changed
        self.f.mode.func = ModeSwCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.f.mode),
            self.set_mode)

        # Connect/disconnect cube (if possible). Also need access to function
        # that refresh workspace due to dimension change
        self.f.conn_dev.func = ConnDevCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.f.conn_dev),
            dim_changed_f=self.set_dimension)

        self.f.pos.func = PosCls(
            _BasicParamsCls(logger=self.logger,
                            console_api=self.console_api,
                            parent_frame=self.f.pos))
        self.logger.debug("done")

    # /_prepare_static_gui()

    def _set_callbacks(self):
        """Set notification callbacks
        """
        # When user is dumb ass and disconnect real LED cube, we need to
        # update GUI
        self.console_api.notif.add_update_notification_callback(
            cmd="hw_cube_disconnected",
            callback_f=self.f.conn_dev.func.notification_hw_cube_disconnected)

        # Undo/redo notification callback is mode dependent
        mode = self.get_mode()
        if (mode == 'draw'):
            callback_f = self.draw_lout.edit.c.notification_can_undo_redo
        elif (mode == 'anim'):
            callback_f = self.anim_lout.edit.c.notification_can_undo_redo
        elif (mode == 'random'):
            # Callback function should not be ever called -> None
            callback_f = None
        else:
            msg = 'Internal error! Unknown mode "{}"'.format(mode)
            raise Exception(msg)

        self.console_api.notif.add_update_notification_callback(
            cmd="edit_can_undo_redo",
            callback_f=callback_f)

        if (mode == "anim"):
            self.console_api.notif.add_update_notification_callback(
                cmd="effect_progress",
                callback_f=self.anim_lout.effects.c.notification_progress)
            self.console_api.notif.add_update_notification_callback(
                cmd="effect_done",
                callback_f=self.anim_lout.effects.c.notification_done)
        # /Effects

        self.console_api.notif.add_update_notification_callback(
            cmd="closing",
            callback_f=self._close_req_from_console_api)

    # /_set_callbacks()

    def _close_req_from_console_api(self, reason):
        """Called when console API require to close application for some reason
        """
        msg = "Close request from console_api. Can not update. Reason:\n{}" \
              "".format(reason)
        self.logger.debug(msg)
        self.close()

    # /_close_req_from_console_api()

    def _get_list_of_active_modules(self):
        """Returns list of active modules as simple list

        Sometimes is needed to enable/disable all active modules -> putting
        all of them into one list makes it easier
        """
        # Main window
        modules = [self.f.mode.func, self.f.conn_dev.func, self.f.pos.func]

        # Mode specific windows
        mode = self.get_mode()

        if (mode == 'draw'):
            m = self.draw_lout
            modules.extend(
                [m.wll.c, m.pwm.c, m.op.c, m.btn_ary.c, m.edit.c, m.view.c])
        elif (mode == 'anim'):
            m = self.anim_lout
            modules.extend([
                m.wll.c, m.pwm.c, m.op.c, m.btn_ary.c, m.edit.c, m.view.c])
            modules.extend([
                m.ld_sve.c, m.ply.c, m.effects.c, ])
        elif (mode == 'random'):
            msg = 'Lazy programmer symptom. No use case for this mode while' \
                  ' programming this function'
            raise Exception(msg)
        else:
            msg = 'Internal error! Unknown mode "{}"'.format(mode)
            raise Exception(msg)

        return (modules)

    # /_get_list_of_active_modules()

    # ==========================| Keyboard/mouse events |=====================
    # Mouse events are most probably useless -> may be removed
    def _mouse_event(self, event):
        """Capture all mouse events at "control window"

        After event is captured, it can be decided what to do. However, in
        most of cases nothing happens.
        """
        self.logger.debug("clicked at {} {}".format(event.x, event.y))

    # /_mouse_event()

    def _kbrd_event(self, event):
        """Capture all keyboard events at "control window"

        After pressing key, key number is analyzed and if needed some action
        is done.
        """
        self.logger.debug("Pressed button #{} | ASCII: '{}' | State: {}\n"
                          "".format(event.keycode, event.char, event.state))

        # Modificators & event's state value
        # 16 - no modificators
        # Ctrl - 20
        # Alt - 24
        # Shift - +1 to previous value (Ctrl + Shift = 21, Alt + Shift = 25)
        # =================================| Global |=========================
        self._kbrd_e_global(event)
        # ================================| Set wall |========================
        self._kbrd_e_wall(event)
        # ==========================| Copy/paste/undo/redo |==================
        self._kbrd_e_copy_paste_undo_redo(event)
        # ====================| Next/previous image @ Animation |=============
        self._kbrd_e_nxt_prev_img_anim(event)

    # /_kbrd_event()
    # |ControlWCls

    def _kbrd_e_global(self, event):
        # Check if keys are without any modificator (shift, Ctrl, ...)
        if (not self._kbrd_is_pressed_special(event, shift=False, ctrl=False)):
            return

        # Shortcuts for all modes
        if (event.keycode == 25):
            self.f.pos.func.rot_up()
        elif (event.keycode == 39):
            self.f.pos.func.rot_down()
        elif (event.keycode == 38):
            self.f.pos.func.rot_left()
        elif (event.keycode == 40):
            self.f.pos.func.rot_right()
        elif (event.keycode == 20):
            self.f.pos.func.zoom_dec()
        elif (event.keycode == 21):
            self.f.pos.func.zoom_inc()

    # /_kbrd_e_global()
    # |ControlWCls

    def _kbrd_e_wall(self, event):
        # Check if keys are pressed without any modificator
        if (not self._kbrd_is_pressed_special(event, shift=False, ctrl=False)):
            return

        mode = self.get_mode()
        dim = self.console_api.get_dimension()

        if ((mode == "anim") or (mode == "draw")):
            # This is common for "anim" and "draw" mode, but objects are
            # different -> refer to actual function
            if (mode == "anim"):
                set_wall = self.anim_lout.wll.c.set_wall
            elif (mode == "draw"):
                set_wall = self.draw_lout.wll.c.set_wall
            else:
                msg = 'Internal error. Unexpected mode "{}"'.format(mode)
                self.logger.critical(msg)
                raise Exception(msg)

            if (event.keycode == 10):
                set_wall(0)
            elif ((event.keycode == 11) and (dim > 1)):
                set_wall(1)
            elif ((event.keycode == 12) and (dim > 2)):
                set_wall(2)
            elif ((event.keycode == 13) and (dim > 3)):
                set_wall(3)
            elif ((event.keycode == 14) and (dim > 4)):
                set_wall(4)
            elif ((event.keycode == 15) and (dim > 5)):
                set_wall(5)
            elif ((event.keycode == 16) and (dim > 6)):
                set_wall(6)
            elif ((event.keycode == 17) and (dim > 7)):
                set_wall(7)
            elif ((event.keycode == 18) and (dim > 8)):
                set_wall(8)
            elif ((event.keycode == 19) and (dim > 9)):
                set_wall(9)
            elif ((event.keycode == 11) and (dim > 1)):
                set_wall(1)
            elif ((event.keycode == 49) and (dim > 1)):
                set_wall("All")
        # /Set wall shortcurts

    # /_kbrd_e_wall()

    def _kbrd_e_copy_paste_undo_redo(self, event):
        # If Ctrl is not pressed -> return
        if (not self._kbrd_is_pressed_special(event, shift=False, ctrl=True)):
            return
        mode = self.get_mode()

        if ((mode == "anim") or (mode == "draw")):
            if (mode == "draw"):
                edit = self.draw_lout.edit.c
            elif (mode == "anim"):
                edit = self.anim_lout.edit.c
            else:
                msg = 'Internal error. Unexpected mode "{}"'.format(mode)
                self.logger.critical(msg)
                raise Exception(msg)

        if (event.keycode == 54):
            edit.copy()
        elif (event.keycode == 55):
            edit.paste()
        elif (event.keycode == 52):
            edit.undo()
        elif (event.keycode == 29):
            edit.redo()

    # /_kbrd_e_copy_paste_undo_redo()
    # |ControlWCls

    def _kbrd_e_nxt_prev_img_anim(self, event):
        # Check if keys are without any modificator (shift, Ctrl, ...)
        if (not self._kbrd_is_pressed_special(event, shift=False, ctrl=False)):
            return

        if (self.get_mode() != "anim"):
            return

        if (event.keycode == 24):
            self.anim_lout.ply.c.previous()
        elif (event.keycode == 26):
            self.anim_lout.ply.c.next()

    # /_kbrd_e_nxt_prev_img_anim
    # |ControlWCls

    def _kbrd_is_pressed_special(self, event, shift=False, ctrl=False):
        """Check if is pressed some special key
        """
        pressed_shift = False
        pressed_ctrl = False
        if (event.state & 5 == 5):
            pressed_ctrl = True
            pressed_shift = True
        elif (event.state & 4):
            pressed_ctrl = True
            pressed_shift = False
        elif (event.state & 1):
            pressed_ctrl = False
            pressed_shift = True
        else:
            pressed_ctrl = False
            pressed_shift = False

        if ((pressed_ctrl == ctrl) and (pressed_shift == shift)):
            return True
        else:
            return False
    # _kbrd_is_pressed_special()
    # |ControlWCls


# /ControlWCls


# ============================| Internal classes |============================
class _ControlWRtCls:
    """Runtime variables for control window"""

    def __init__(self):
        # Changed to True when there is request to close window
        self.is_closing = False

        # Main frame. Content will be created on the fly, because right now
        # is not easy to decide what will be actually here
        self.f = DummyCls()
    # /__init__()


# /_ControlWRtCls


class _BasicParamsCls:
    """Basic parameters, which are shared through functional frames
    """

    def __init__(self,
                 logger,
                 console_api,
                 parent_frame,
                 menu_bar=None,
                 kbrd_event_fnc=None,
                 mode=None,
                 en_windows=None,
                 dis_windows=None):
        self.logger = logger
        self.console_api = console_api
        self.parent_frame = parent_frame
        self.menu_bar = menu_bar
        self.kbrd_event_fnc = kbrd_event_fnc
        self.mode = mode
        self.en_windows = en_windows
        self.dis_windows = dis_windows
    # /__init__()


# /_BasicParamsCls


class _FramesPosCls:
    """Frame parameters"""

    def __init__(self):
        # Position - structure/class is part of common.py
        # Mode
        self.mode = PosStructCls(row=1, column=1)
        # Connect device
        self.conn_dev = PosStructCls(row=2, column=1)
        # Position
        self.pos = PosStructCls(row=3, column=1)

        # Workspace - top
        self.wrkspc_t = PosStructCls(row=0, column=0, column_width=3)

        # Workspace - down
        self.wrkspc_d = PosStructCls(row=4, column=1)
        # Workspace - left
        self.wrkspc_l = PosStructCls(row=1, column=0, row_width=4)
        # Workspace - right
        self.wrkspc_r = PosStructCls(row=1, column=2, row_width=4)

        # Workspace - bottom (through left, main, right & down)
        self.wrkspc_b = PosStructCls(row=5, column=0, column_width=4)
    # /__init__()


# /FrameParamCls


class _SubModuleStruct:
    """Structure of variables for sub-modules

    Every sub-module need own frame and store own instance of class
    """

    def __init__(self):
        # here will be stored frame
        self.f = None

        # And here instance sub-module
        self.c = None
    # /__init__()
# /_SubModuleStruct

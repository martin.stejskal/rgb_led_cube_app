#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===============================
Module for playing animation
===============================

Because static image is boring...

``author`` Martin Stejskal

``version`` 0.4.3
"""

# Basic GUI for Python
import tkinter

from lib.common import app_path


class PlayCls:
    """Module for operations with cube"""

    def __init__(self, basic_params_cls,
                 pwm_gui):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls

        :param pwm_gui: Object with tkinter sliders which show PWM value at GUI
        :type pwm_gui: obj (PWMCls)
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        # Make it easy to change
        self.pwd = "{}/lib/gui/tkinter".format(app_path)

        self.pwm_gui = pwm_gui

        # Runtime variables
        self.rt = _RtPlayCls(fldr="{}/lib/play_img/".format(self.pwd))

        # Finish initialization (mainly GUI)
        self._init_finish()

    # /__init__()

    def next(self):
        """Jump to next image"""
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.next()

        self._update()

    # /next()

    def previous(self):
        """Jump to previous image"""
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.previous()

        self._update()

    # /previous()

    def backward(self):
        """Move backward

        From technical point of view, it will go back by 10% of animation
        length.
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        max_idx = self.console_api.play.get_max_idx()
        actual_idx = self.console_api.play.get_actual_idx()

        # Calculate move backwards
        # If calculated value is zero, then move at least by 1
        move = int(max_idx * 0.1)
        if (move == 0):
            move = 1

        # Calculate new index and check if it will not be negative
        new_idx = actual_idx - move

        if (new_idx < 0):
            new_idx = 0

        self.console_api.play.set_actual_idx(new_idx)

        # And since we moved, we need to refresh GUI
        self._update()

    # /backward()

    def forward(self):
        """Move forward

        From technical point of view, it will go back by 10% of animation
        length.
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        max_idx = self.console_api.play.get_max_idx()
        actual_idx = self.console_api.play.get_actual_idx()

        # Calculate move. If calculated value is zero, then move at least by 1
        move = int(max_idx * 0.1)
        if (move == 0):
            move = 1

        # Calculate new index and check if it will not be negative
        new_idx = actual_idx + move

        if (new_idx > max_idx):
            new_idx = max_idx

        self.console_api.play.set_actual_idx(new_idx)

        # And since we moved, we need to refresh GUI
        self._update()

    # /forward()

    def play(self):
        """Start playing animation

        This will start playing animation no matter what. If actual image is
        last one it will start playing from begin (image 0)
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.play()
        self._update_status()

    # /play()

    def pause(self):
        """Pause animation

        This will pause animation no matter what.
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.pause()
        self._update_status()

    # /pause()

    def stop(self):
        """Stop (pause) animation and jump to the first image

        Only difference between "pause()" and this function is that this
        function will also set image index back to 0
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        # Following should set index position to 0
        self.console_api.play.stop()
        self._update_status()

        # Update GUI
        self._update()

    # /stop()

    def delete_actual_idx(self):
        """Delete actual cube frame.

        If actual index is set to 0 and maximum index is also 0,
        then do nothing
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.delete_actual_idx()
        self._update()

    # /delete_actual_idx()

    def insert_new_image_actual_idx(self):
        """Insert new image at actual index position
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        self.console_api.play.insert_new_image_actual_idx()
        self._update()

    # /insert_new_image_actual_idx()

    def repeat_changed(self):
        """This function is called when user click to "repeat" check button
        """
        assert self.rt.status != "disabled", \
            'Action is not allowed when disabled'

        repeat = self.rt.tk_var.repeat.get()

        self.console_api.play.set_repeat(repeat)

    # /repeat_changed()

    def update(self, force_update=False):
        """
        This function should be called by upper layer whenever playing
        animation

        :param force_update: Usually update is needed only if playing
                             animation. However in some cases (loaded
                             animation from file) update have to be done no
                             matter what. So this parameter bypass basic
                             "play" check
        :type force_update: bool
        """
        # If module is disabled, no update is possible, because it could
        # lead to trying communicate with console API, which would not response
        # in time when running severe task in background. Also it is not
        # purpose to have up-to-date module when disabled.
        if (self.rt.status == "disabled"):
            return

        if ((self.rt.status == "play") or (force_update)):
            self._update()

    # /update()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        # This disallow to perform any updates
        self.rt.status = "disabled"

        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

        self.rt.status = self.console_api.play.get_status()

    # /en_widgets()

    # ===========================| Internal functions |=======================

    def _init_finish(self):
        self.logger.debug("Preparing GUI components...")

        # Set default values
        self.rt.tk_var.timer.set(
            self.console_api.play.get_timer())
        self.rt.tk_var.actual_img_num.set(
            self.console_api.play.get_actual_idx())
        self.rt.tk_var.repeat.set(
            self.console_api.play.get_repeat())

        self.b_bckwrd = tkinter.Button(
            self.parent_frame,
            command=self.backward,
            image=self.rt.img.backward)
        self.b_prev = tkinter.Button(
            self.parent_frame,
            command=self.previous,
            image=self.rt.img.previous)
        self.b_next = tkinter.Button(
            self.parent_frame,
            command=self.next,
            image=self.rt.img.next)
        self.b_forward = tkinter.Button(
            self.parent_frame,
            command=self.forward,
            image=self.rt.img.forward)

        self.b_stop = tkinter.Button(
            self.parent_frame,
            command=self.stop,
            image=self.rt.img.stop)
        self.b_pause = tkinter.Button(
            self.parent_frame,
            command=self.pause,
            image=self.rt.img.pause)
        self.b_play = tkinter.Button(
            self.parent_frame,
            command=self.play,
            image=self.rt.img.play)

        self.l_timer = tkinter.Label(self.parent_frame,
                                     text="Timer [ms]")
        self.s_timer = tkinter.Scale(
            self.parent_frame,
            command=self._timer_slider_moved,
            variable=self.rt.tk_var.timer,
            from_=30,
            to=1000,
            length=180,
            orient=tkinter.HORIZONTAL,
            resolution=10,
            showvalue=1)

        self.l_img_num = tkinter.Label(self.parent_frame,
                                       text="Img: 1/1")
        self.l_tot_time = tkinter.Label(self.parent_frame,
                                        text="Tot time: 0.00s")
        self.s_tmln = tkinter.Scale(
            self.parent_frame,
            command=self._timeline_slider_moved,
            variable=self.rt.tk_var.actual_img_num,
            from_=0,
            to=self.console_api.play.get_max_idx(),
            length=500,
            orient=tkinter.HORIZONTAL,
            resolution=1,
            showvalue=0)

        self.b_del = tkinter.Button(
            self.parent_frame,
            command=self.delete_actual_idx,
            image=self.rt.img.del_img)
        self.b_new = tkinter.Button(
            self.parent_frame,
            command=self.insert_new_image_actual_idx,
            image=self.rt.img.new_img)

        self.cb_repeat = tkinter.Checkbutton(
            self.parent_frame,
            text="Repeat",
            variable=self.rt.tk_var.repeat,
            command=self.repeat_changed,
            onvalue=1,
            offvalue=0)

        # Define buttons position
        self.b_bckwrd.grid(column=0, row=1)
        self.b_prev.grid(column=1, row=1)
        self.b_next.grid(column=2, row=1)
        self.b_forward.grid(column=3, row=1)

        self.b_stop.grid(column=0, row=0)
        self.b_pause.grid(column=1, row=0, columnspan=2)
        self.b_play.grid(column=3, row=0)

        self.l_timer.grid(column=4, row=0)
        self.s_timer.grid(column=4, row=1)

        self.b_new.grid(column=6, row=0)
        self.b_del.grid(column=6, row=1)

        self.l_img_num.grid(column=5, row=0)
        self.l_tot_time.grid(column=5, row=1)
        self.s_tmln.grid(column=0, row=3, columnspan=10)

        self.cb_repeat.grid(column=0, row=2, columnspan=2)

        self._update_status()

        # Collect all widgets to one list, so they can be easily
        # disabled/enabled
        self.widgets = [
            self.b_bckwrd, self.b_prev, self.b_next, self.b_forward,

            self.b_stop, self.b_pause, self.b_play,

            self.l_timer, self.s_timer,

            self.b_new, self.b_del,

            self.l_img_num, self.l_tot_time, self.s_tmln,

            self.cb_repeat, ]

        self.logger.debug("done")

    # /_init_finish()

    def _timeline_slider_moved(self, value):
        """Change image index & update

        Called when user move by this slider
        """
        self.console_api.play.set_actual_idx(value)
        self._update()

    # /_timeline_slider_moved()

    def _timer_slider_moved(self, value):
        """Change timer value"""
        self.console_api.play.set_timer(value)

        self._update()

    # /_timer_slider_moved()

    def _update(self):
        """Update time line slider, image number & total time"""
        max_idx = self.console_api.play.get_max_idx()
        act_idx = self.console_api.play.get_actual_idx()

        # We can get timer value also from "self.s_timer", but this should
        # allow us to check this value
        timer = self.console_api.play.get_timer()
        # Check if timer value match. If not, update it. This may be caused
        # by loading new animation
        if (timer != self.rt.tk_var.timer.get()):
            self.rt.tk_var.timer.set(timer)

        # Set image number
        self.l_img_num.config(
            text="Img: {}/{}".format(act_idx + 1, max_idx + 1))

        # Calculate new total time & set it (also some rounding need to be
        # done)
        tot_time = (timer * 0.001) * (max_idx)
        self.l_tot_time.config(text="Tot time: {0:.2f}s".format(tot_time))

        # Set slider at time line
        self.s_tmln.config(to=max_idx)
        self.rt.tk_var.actual_img_num.set(act_idx)

        # Do not forget to update PWM GUI module
        self.pwm_gui.update_pwm_according_backend()

    # /_update()

    def _update_status(self):
        self.rt.status = self.console_api.play.get_status()
    # /_update_status()


# /PlayCls


# ============================| Internal classes |============================
class _RtPlayCls:

    def __init__(self, fldr="./lib/play_img/"):
        self.img = self.ImgCls(fldr)
        self.tk_var = self.TkVarCls()

        # Expected status, but it should be rewritten during initialization
        self.status = "pause"

    # /__init__()

    class TkVarCls:
        """Tkinter variables

        Since tkinter need special variable type, we can not use int or float
        for example
        """

        def __init__(self):
            # Timer value
            self.timer = tkinter.IntVar()

            # Actual image number
            self.actual_img_num = tkinter.IntVar()

            # Repeat
            self.repeat = tkinter.IntVar()
        # /__init__()

    # /TkVarCls

    class ImgCls:
        """Images for buttons"""

        def __init__(self, fldr):
            self.play = tkinter.PhotoImage(
                file="{}media-playback-start.png".format(fldr))
            self.pause = tkinter.PhotoImage(
                file="{}media-playback-pause.png".format(fldr))
            self.stop = tkinter.PhotoImage(
                file="{}media-playback-stop.png".format(fldr))

            self.next = tkinter.PhotoImage(
                file="{}go-next.png".format(fldr))
            self.previous = tkinter.PhotoImage(
                file="{}go-previous.png".format(fldr))
            self.forward = tkinter.PhotoImage(
                file="{}media-seek-forward.png".format(fldr))
            self.backward = tkinter.PhotoImage(
                file="{}media-seek-backward.png".format(fldr))

            self.del_img = tkinter.PhotoImage(
                file="{}tab-close.png".format(fldr))
            self.new_img = tkinter.PhotoImage(
                file="{}tab-new.png".format(fldr))
        # /__init__()
    # /ImgCls
# /_RtPlayCls

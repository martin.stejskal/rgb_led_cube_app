#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
View options
=================================================

Handle most of "view" options

``author`` Martin Stejskal

``version`` 0.2.1
"""
# Basic GUI for Python
import tkinter


class ViewCls:
    def __init__(self, basic_params_cls):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame
        self.menu_bar = basic_params_cls.menu_bar
        self.mode = basic_params_cls.mode

        if (self.menu_bar is None):
            msg = 'Internal error! Variable "menu_bar" have to be set!'
            self.logger.error(msg)
            raise Exception(msg)

        self._init_finish()

    # /__init__()
    # |ViewCls

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        self.menu_bar.entryconfig("View", state="disabled")

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        self.menu_bar.entryconfig("View", state="normal")

    # /en_widgets()

    # ===========================| Internal functions |=======================

    def _init_finish(self):
        # Initialize runtime variables
        self.rt = _ViewRtCls()

        # Load values from console API -> everything in sync
        self.rt.wall_highlight.set(self.console_api.view.get_wall_highlight())
        self.rt.show_disabled.set(self.console_api.view.get_show_disabled())
        self.rt.show_inactive.set(self.console_api.view.get_show_inactive())

        self.view_menu = tkinter.Menu(self.menu_bar, tearoff=0)

        # Add following options only when not in random mode
        if ((self.mode is None) or (self.mode != "random")):
            self.view_menu.add_checkbutton(
                label="Highlight actual wall", variable=self.rt.wall_highlight,
                command=self._set_wall_highlight, onvalue=True, offvalue=False)
            self.view_menu.add_checkbutton(
                label="Show inactive LEDs", variable=self.rt.show_inactive,
                command=self._set_show_inactive, onvalue=True, offvalue=False)
        # /only when not in random mode
        self.view_menu.add_checkbutton(
            label="Show disabled LEDs", variable=self.rt.show_disabled,
            command=self._set_show_disabled, onvalue=True, offvalue=False)

        self.menu_bar.add_cascade(label="View", menu=self.view_menu)

    # /_init_finish()
    # |ViewCls

    def _set_wall_highlight(self):
        self.console_api.view.set_wall_highlight(self.rt.wall_highlight.get())

    def _set_show_disabled(self):
        self.console_api.view.set_show_disabled(self.rt.show_disabled.get())

    def _set_show_inactive(self):
        self.console_api.view.set_show_inactive(self.rt.show_inactive.get())


# /ViewCls


class _ViewRtCls:
    """Runtime variables for ViewCls"""

    def __init__(self):
        self.wall_highlight = tkinter.BooleanVar()
        self.show_disabled = tkinter.BooleanVar()
        self.show_inactive = tkinter.BooleanVar()

    # /__init__()

    def __str__(self):
        msg = "== View Runtime ==\n" \
              "  wall highlight: {}\n" \
              "  show disabled: {}\n" \
              "  show inactive: {}\n" \
              "".format(self.wall_highlight.get(),
                        self.show_disabled.get(),
                        self.show_inactive.get())
        return (msg)
    # /__str__()
# /_ViewRtCls

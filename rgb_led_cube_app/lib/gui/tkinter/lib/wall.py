#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
Module for selecting wall
=================================================

Allow to select active wall

``author`` Martin Stejskal

``version`` 0.5.1
"""

# Basic GUI for Python
import tkinter


class WallCls:

    def __init__(self, basic_params_cls):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self._init_finish()

    # /__init__()

    def set_wall(self, wall_idx):
        """Select one of the wall. Or select all walls

        :param wall_idx: If value is "All", all walls are active. Else is
                         expected wall index as number but as string
        """
        self.rt.active_wall.set(wall_idx)

        # "translate" "All" for console API if needed
        if ((wall_idx == "All")):
            wall_idx = None

        self.console_api.wall.set_active(wall_idx=wall_idx)

    # /set_wall()

    def get_wall(self):
        return (self.rt.active_wall.get())

    # /get_wall()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        # Initialize runtime variables
        self.rt = _WallRtCls()

        # ==================| Set initial variables at console API |==========
        active_wall = self.console_api.wall.get_active()
        if (active_wall is None):
            active_wall = "All"
        self.rt.active_wall.set(active_wall)

        # ================================| Spinbox |=========================
        # Create list of values for spinbox. There will be "All" and then
        # numbers of walls in cube
        spinbox_list = ["All"]
        for wall_idx in range(self.console_api.get_dimension()):
            spinbox_list.append(wall_idx)
        # /fill spinbox list

        self.sp_box_sel_wall = tkinter.Spinbox(
            self.parent_frame,
            values=spinbox_list,
            command=self._select_wall_sp_box,
            width=5,
            wrap=True,
            state="readonly",
            textvariable=self.rt.active_wall)

        self.sp_box_lbl = tkinter.Label(self.parent_frame, text="Wall")

        self.sp_box_lbl.grid(column=0, row=0)
        self.sp_box_sel_wall.grid(column=1, row=0)

        # List of widgets
        self.widgets = [self.sp_box_sel_wall, self.sp_box_lbl]

    # /_init_finish()

    def _select_wall_sp_box(self):
        self.set_wall(wall_idx=self.rt.active_wall.get())
    # /_select_wall_sp_box()


# /WallCls


class _WallRtCls:
    """Runtime variables for WallCls"""

    def __init__(self):
        self.active_wall = tkinter.StringVar()
    # /__init__()
# /_WallRtCls

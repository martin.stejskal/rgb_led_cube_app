#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=============================================
Module for connecting/disconnecting LED cube
=============================================

Handle user requests for connecting and disconnecting cube

``author`` Martin Stejskal

``version`` 0.6.2
"""

# Basic GUI for Python
import tkinter
from tkinter import messagebox


class ConnDevCls:
    """Module for connecting/disconnecting real cube
    """

    def __init__(self, basic_params_cls, dim_changed_f):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls

        :param dim_changed_f: Pointer to function that handle changing
                              cube dimension at GUI level
        :type dim_changed_f: function
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self.dim_changed_f = dim_changed_f

        # Initialize runtime variables
        self.rt = ConnDevRtCls()

        self._init_finish()

    # /__init__()

    def update_status_according_to_console_api(self):
        """When cube is disconnected from other function, it is needed to
        update status also here

        This function will allow to re-check real cube status and
        update button status
        """
        status_ca = self.console_api.real_cube.get_status()
        if (status_ca == "Connected"):
            # Disable spinbox and update button text
            self._disable_spbox_txt_disconn()
        else:
            # Not connected -> allow to connect
            self._enable_spbox_txt_conn()

    # /update_status_according_to_console_api()

    def notification_hw_cube_disconnected(self, reason):
        """Executed when appropriate notification is received

        When "hw_cube_disconnected" notification is received, this function
        will be called (callback)
        """
        # Not connected -> allow to connect
        self._enable_spbox_txt_conn()

        messagebox.showwarning(title="Real cube disconnected",
                               message=reason)

    # /notification_hw_cube_disconnected()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        widgets = self.widgets_en_dis
        # Disable spinbox no matter what
        widgets.append(self.spinbox)

        for widget in self.widgets_en_dis:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets_en_dis:
            widget.config(state=tkinter.NORMAL)

        # Enable/disable spinbox according to real cube status
        self.update_status_according_to_console_api()

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        # Auto-counting y position (horizontal)
        y_pos = 0

        # Get list of supported devices
        self.rt.dev_list = self.console_api.real_cube.dev_list()

        # Check device list length - at least one device have to be supported
        if (len(self.rt.dev_list) < 1):
            msg = "Internal error! No supported device found!\n" \
                  "There must be at least supported device in order to\n" \
                  "make this module meaningful."
            self.logger.critical(msg)
            raise Exception(msg)
        # /check

        # =================================| Label |==========================
        self.lbl = tkinter.Label(self.parent_frame,
                                 text="Supported devices")
        self.lbl.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        # ================================| Spinbox |=========================
        # Set first supported cube as selected at spin box
        self.rt.sel_dev.set(self.rt.dev_list[0])
        # According to device_list (cube names) find longest cube name.
        # This value will be used for defining width of spinbox to guarantee
        # that cube name will be visible all
        max_len = 0
        for dev_name in self.rt.dev_list:
            dev_name_len = len(dev_name)

            if (dev_name_len > max_len):
                max_len = dev_name_len
            # /if found new maximum
        # /check all devices
        # Add student's constant
        max_len = max_len + 2
        self.logger.debug("Spinbox width: {}".format(max_len))

        self.spinbox = tkinter.Spinbox(self.parent_frame,
                                       values=self.rt.dev_list,
                                       wrap=True,
                                       state='readonly',
                                       textvariable=self.rt.sel_dev,
                                       width=max_len)
        self.spinbox.grid(column=0, row=y_pos)
        y_pos = y_pos + 1
        # =================================| Button |=========================
        self.btn = tkinter.Button(self.parent_frame,
                                  text="Connect",
                                  command=self._connect_action)
        self.btn.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        # Get actual status
        self.update_status_according_to_console_api()

        # List of widgets that can be simply enabled/disabled from outside
        self.widgets_en_dis = [self.lbl, self.btn]

    # /_init_finish()

    def _connect_action(self):
        # Get selected device
        sel_dev = self.rt.sel_dev.get()

        msg = "Trying to connecting to device {}".format(sel_dev)
        self.logger.info(msg)

        try:
            self.console_api.real_cube.connect(sel_dev)
        except RuntimeError:
            msg = "Connection to device {} failed. Make sure that device is" \
                  " connected.\nAlso check:\n * access rights\n" \
                  " * logs\n * google".format(sel_dev)
            messagebox.showerror("Connection to {} failed".format(sel_dev),
                                 msg)
            return
        except UserWarning as e:
            msg = f"Cube connected, but dimension had to be changed.\n{e}"
            messagebox.showwarning(title="Cube dimension changed",
                                   message=msg)
            # Callback function that handle dimension change at higher layer
            self.dim_changed_f()
        # /try connect

        # Now we should be connected - let's check few things
        self._disable_spbox_txt_disconn()

    # /_connect_action()

    def _disable_spbox_txt_disconn(self):
        # We have to freeze spinbox to avoid changing cube on the fly
        self.spinbox.config(state=tkinter.DISABLED)
        # And change text and command on button
        self.btn.config(text="Disconnect",
                        command=self._disconnect_action)

    # /_disable_spbox_txt_disconn()

    def _disconnect_action(self):
        sel_dev = self.rt.sel_dev.get()

        try:
            self.console_api.real_cube.disconnect()
        except RuntimeError as e:
            # Runtime error -> but basically there is nothing we can do.
            # Target cube is probably frozen/at some bug state, so we can not
            # consider it connected too. Just show warning
            msg = "Disconnection from {} failed!\n" \
                  "We recommend to physically disconnect cube.\n\n{}" \
                  "".format(sel_dev, e)
            self.logger.warn(msg)
            messagebox.showwarning("Disconnection from {} failed!"
                                   "".format(sel_dev), msg)
        # /disconnection failed

        self._enable_spbox_txt_conn()

    # /_disconnect_action()

    def _enable_spbox_txt_conn(self):
        # Un-freeze spinbox and change button text
        self.spinbox.config(state="readonly")
        self.btn.config(text="Connect",
                        command=self._connect_action)
    # /_enable_spbox_txt_conn()


# /ConnDevCls


class ConnDevRtCls:
    """Runtime variables for ConnDevCls"""

    def __init__(self):
        # List of supported devices
        self.dev_list = []

        # Selected device (for spinbox)
        self.sel_dev = tkinter.StringVar()
    # /__init__()
# /ConnDevRtCls

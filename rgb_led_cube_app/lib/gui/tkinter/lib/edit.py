#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===============================
Module for "edit" operations
===============================

Contains functions like "undo", "redo" and so on.

``author`` Martin Stejskal

``version`` 0.2.1
"""
# Basic GUI for Python
import tkinter


class EditCls:
    """Module for "edit" operations
    """

    def __init__(self, basic_params_cls, pwm_gui):
        """
            :param basic_params_cls: Basic parameters wrapped in one class
            :type basic_params_cls: _BasicParamsCls
            """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame
        self.menu_bar = basic_params_cls.menu_bar

        if (self.menu_bar is None):
            msg = 'Internal error! Variable "menu_bar" have to be set!'
            self.logger.error(msg)
            raise Exception(msg)

        self.pwm_gui = pwm_gui

        self._init_finish()

    # /__init__()
    # |EditCls

    def undo(self):
        """Undo last operation"""
        # Check status -> if disabled return
        if (self.edit_menu.entrycget("Undo", "state") == "disabled"):
            self.logger.debug("Nothing to undo -> ignore request")
            return
        # Still it can be possible, that user press "undo" shortcut while
        # we did not received notification event which disabled button.
        # Therefore errors are silently ignored. Of course there is possibility
        # to read status every time, but this would cost another extra time.
        self.console_api.edit.undo(fail_if_error=False)

        # Do not forget to update PWM GUI module (PWM value might change)
        self.pwm_gui.update_pwm_according_backend()

    # /undo()
    # |EditCls

    def redo(self):
        """Redo last operation"""
        if (self.edit_menu.entrycget("Redo", "state") == "disabled"):
            self.logger.debug("Nothing to redo -> ignore request")
            return
        self.console_api.edit.redo(fail_if_error=False)

        # Do not forget to update PWM GUI module (PWM value might change)
        self.pwm_gui.update_pwm_according_backend()

    # /redo()
    # |EditCls

    def copy(self):
        """Copy actual 3D cube to the temporary buffer"""
        self.console_api.edit.copy()

    # /copy()
    # |EditCls

    def paste(self):
        """Paste image from temporary buffer"""
        self.console_api.edit.paste()

        # Do not forget to update PWM GUI module (PWM value might change)
        self.pwm_gui.update_pwm_according_backend()

    # /paste()
    # |EditCls

    def notification_can_undo_redo(self, params):
        """This callback function which enable/disable undo/redo buttons
        """
        params = params.split()
        assert len(params) == 2, 'Expected 2 parameters (can undo, can redo)'

        # First check undo option
        if (params[0] == "True"):
            self.logger.debug('Notification: Enabling "undo"')
            self.edit_menu.entryconfig("Undo", state="normal")
        else:
            self.logger.debug(
                'Notification: Disabling "undo" ({})'.format(params[0]))
            self.edit_menu.entryconfig("Undo", state="disabled")

        # Now it is time for redo option
        if (params[1] == "True"):
            self.logger.debug('Notification: Enabling "redo"')
            self.edit_menu.entryconfig("Redo", state="normal")
        else:
            self.logger.debug(
                'Notification: Disabling "redo" ({})'.format(params[1]))
            self.edit_menu.entryconfig("Redo", state="disabled")

    # /notification_can_undo_redo()
    # |EditCls

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        self.menu_bar.entryconfig("Edit", state="disabled")

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        self.menu_bar.entryconfig("Edit", state="normal")

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        self.edit_menu = tkinter.Menu(self.menu_bar, tearoff=0)

        # Get can_undo/redo status, since user could switch to another mode,
        # so now we can not be sure if undo/redo is or is not possible
        if (self.console_api.edit.can_undo()):
            state_undo = "normal"
        else:
            state_undo = "disabled"
        if (self.console_api.edit.can_redo()):
            state_redo = "normal"
        else:
            state_redo = "disabled"

        self.edit_menu.add_command(
            label="Undo", command=self.undo, state=state_undo)
        self.edit_menu.add_command(
            label="Redo", command=self.redo, state=state_redo)

        self.edit_menu.add_separator()

        self.edit_menu.add_command(label="Copy", command=self.copy)
        self.edit_menu.add_command(label="Paste", command=self.paste)

        self.menu_bar.add_cascade(label="Edit", menu=self.edit_menu)
    # /_init_finish()
# /EditCls

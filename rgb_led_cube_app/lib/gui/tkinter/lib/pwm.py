#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
Module for changing PWM value on cube
=================================================

Handle RGB color channels on cube

``author`` Martin Stejskal

``version`` 0.6.1
"""

# Basic GUI for Python
import tkinter


class PWMCls:
    """Module for changing PWM value on cube"""

    def __init__(self, basic_params_cls):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame
        self.menu_bar = basic_params_cls.menu_bar

        self._init_finish()

    # /__init__()

    def set_r(self, r=255):
        # Check value and if not same, modify it.
        r = self._check_pwm_value(pwm_value=r)
        # Set value in 3D view as well
        # And save it to our structure for case that this was not called from
        # self (but from higher layer)
        if (not self._lock_pwm_if_merged(color='r', value=r)):
            self.rt.r.set(r)
            self.rt.r_old = self.get_r()
        self.console_api.pwm.set(self.get_r(), self.get_g(), self.get_b())

    def get_r(self):
        return (self.rt.r.get())

    def set_g(self, g=255):
        g = self._check_pwm_value(pwm_value=g)
        if (not self._lock_pwm_if_merged(color='g', value=g)):
            self.rt.g.set(g)
            self.rt.g_old = self.get_g()
        self.console_api.pwm.set(self.get_r(), self.get_g(), self.get_b())

    def get_g(self):
        return (self.rt.g.get())

    def set_b(self, b=255):
        b = self._check_pwm_value(pwm_value=b)
        if (not self._lock_pwm_if_merged(color='b', value=b)):
            self.rt.b.set(b)
            self.rt.b_old = self.get_b()
        self.console_api.pwm.set(self.get_r(), self.get_g(), self.get_b())

    def get_b(self):
        return (self.rt.b.get())

    def set_rgb(self, r=255, g=255, b=255):
        """Set RGB colors at once

        Since this function set all colors, option "merge_rgb" have to be
        implicitly ignored by nature.

        :param r: PWM value for red color
        :type r: int (0~255)

        :param g: PWM value for green color
        :type g: int (0~255)

        :param b: PWM value for blue color
        :type b: int (0~255)
        """
        r = self._check_pwm_value(pwm_value=r)
        g = self._check_pwm_value(pwm_value=g)
        b = self._check_pwm_value(pwm_value=b)

        self.rt.r.set(r)
        self.rt.r_old = self.get_r()
        self.rt.g.set(g)
        self.rt.g_old = self.get_g()
        self.rt.b.set(b)
        self.rt.b_old = self.get_b()

        self.console_api.pwm.set(self.get_r(), self.get_g(), self.get_b())

    # /set_rgb()

    def set_merge_rgb(self, merge_rgb=False):
        """Enable/disable merge RGB sliders

        :param merge_rgb: Enable/disable merge
        :type merge_rgb: bool
        """
        self.rt.merge_rgb.set(merge_rgb)

        if (self.rt.merge_rgb.get()):
            self.logger.debug("Merge SET")
        else:
            self.logger.debug("Merge CLEAR")

    # /set_merge_rgb()

    def set_pwm_all_img(self, r=255, g=255, b=255):
        """Set given PWM value at all images if in anim mode

        :param r: PWM value for red color
        :type r: int (0~255)

        :param g: PWM value for green color
        :type g: int (0~255)

        :param b: PWM value for blue color
        :type b: int (0~255)
        """
        self.console_api.pwm.set_all_img(r, g, b)

    # /set_pwm_all_img()

    def update_pwm_according_backend(self):
        """Loads PWM values from backend and update values in GUI
        """
        pwm = self.console_api.pwm.get()
        # Extra paranoia
        self._check_pwm_value(pwm_value=pwm[0])
        self._check_pwm_value(pwm_value=pwm[1])
        self._check_pwm_value(pwm_value=pwm[2])

        # Save old values
        self.rt.r_old = self.rt.r.get()
        self.rt.g_old = self.rt.g.get()
        self.rt.b_old = self.rt.b.get()

        # And replace with new one
        self.rt.r.set(pwm[0])
        self.rt.g.set(pwm[1])
        self.rt.b.set(pwm[2])

    # /update_pwm_according_backend()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

        self.menu_bar.entryconfig("PWM", state="disabled")

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

        self.menu_bar.entryconfig("PWM", state="normal")

    # /en_widgets()

    # ===========================| Internal functions |=======================

    def _init_finish(self):
        # Get PWM values from console API
        pwm = self.console_api.pwm.get()
        self.logger.debug("Loaded PWM values: R: {} G: {} B: {}"
                          "".format(pwm[0], pwm[1], pwm[2]))
        # Initialize runtime variables
        self.rt = _PWMRtCls(r=pwm[0],
                            g=pwm[1],
                            b=pwm[2])

        # =============================| RGB - R slider |=====================
        self.sldr_r = tkinter.Scale(self.parent_frame,
                                    command=self.set_r,
                                    variable=self.rt.r,
                                    from_=0,
                                    to=255,
                                    length=self.rt.slider_len,
                                    orient=tkinter.HORIZONTAL,
                                    resolution=1,
                                    showvalue=1)
        self.lbl_r = tkinter.Label(self.parent_frame, text="R")
        # =============================| RGB - G slider |=====================
        self.sldr_g = tkinter.Scale(self.parent_frame,
                                    command=self.set_g,
                                    variable=self.rt.g,
                                    from_=0,
                                    to=255,
                                    length=self.rt.slider_len,
                                    orient=tkinter.HORIZONTAL,
                                    resolution=1,
                                    showvalue=1)
        self.lbl_g = tkinter.Label(self.parent_frame, text="G")
        # =============================| RGB - B slider |=====================
        self.sldr_b = tkinter.Scale(self.parent_frame,
                                    command=self.set_b,
                                    variable=self.rt.b,
                                    from_=0,
                                    to=255,
                                    length=self.rt.slider_len,
                                    orient=tkinter.HORIZONTAL,
                                    resolution=1,
                                    showvalue=1)
        self.lbl_b = tkinter.Label(self.parent_frame, text="B")

        # ============================| Define placement |====================
        self.sldr_r.grid(column=1, row=0)
        self.lbl_r.grid(column=0, row=0)
        self.sldr_g.grid(column=1, row=1)
        self.lbl_g.grid(column=0, row=1)
        self.sldr_b.grid(column=1, row=2)
        self.lbl_b.grid(column=0, row=2)

        # ================================| Menu bar |========================
        # Putting all things at one module would be quite ugly and unorganized.
        # So there is menu bar.
        pwm_menu = tkinter.Menu(self.menu_bar, tearoff=0)

        pwm_menu.add_checkbutton(
            label="Merge RGB", variable=self.rt.merge_rgb,
            command=self._set_merge_rgb, onvalue=True, offvalue=False)

        mode = self.console_api.get_mode()
        if (mode == 'anim'):
            # Extra settings
            pwm_menu.add_command(
                label="Set at all images",
                command=self._set_pwm_all_img)
        # /if anim mode is enabled

        self.menu_bar.add_cascade(label="PWM", menu=pwm_menu)

        # Widget list
        self.widgets = [self.sldr_r, self.sldr_g, self.sldr_b,
                        self.lbl_r, self.lbl_g, self.lbl_b]

    # /_init_finish()

    def _check_pwm_value(self, pwm_value, auto_fix=False):
        """Check PWM value

        When value is out of range it throw exception unless parameter
        "auto_fix" is set

        :param pwm_value: PWM value
        :type pwm_value: int (0~255)

        :param auto_fix: If set, value will be automatically corrected. Also
                         exception will not be thrown.
        :type auto_fix: bool
        """
        # Retype value to integer
        pwm_value = int(pwm_value)

        if (pwm_value > 255):
            if (auto_fix):
                return (255)
            else:
                msg = "PWM value is higher than 255!"
                self.logger.error(msg)
                raise Exception(msg)

        if (pwm_value < 0):
            if (auto_fix):
                return (0)
            else:
                msg = "PWM value can not be negative!"
                self.logger.error(msg)
                raise Exception(msg)

        # And return it as integer
        return (pwm_value)

    # /_check_pwm_value()

    def _lock_pwm_if_merged(self, color='r', value=255):
        """Set all channels (RGB) to the same value if merge_rgb is set

        In some cases can be useful to move all sliders together.
        """
        if (self.rt.merge_rgb.get()):
            # User want to move all sliders together. Calculate difference
            if (color == 'r'):
                # The red is already changed - calculate difference
                diff = value - self.rt.r_old
                # And change others
                r = value
                g = self.rt.g.get() + diff
                b = self.rt.b.get() + diff
            elif (color == 'g'):
                diff = value - self.rt.g_old
                g = value
                r = self.rt.r.get() + diff
                b = self.rt.b.get() + diff
            elif (color == 'b'):
                diff = value - self.rt.b_old
                b = value
                r = self.rt.r.get() + diff
                g = self.rt.g.get() + diff
            else:
                msg = 'Internal error. Unknown color "{}"'.format(color)
                raise Exception(msg)

            self.rt.r.set(self._check_pwm_value(pwm_value=r, auto_fix=True))
            self.rt.g.set(self._check_pwm_value(pwm_value=g, auto_fix=True))
            self.rt.b.set(self._check_pwm_value(pwm_value=b, auto_fix=True))

            # Changed RGB -> modify old values at all colors
            self.rt.r_old = r
            self.rt.g_old = g
            self.rt.b_old = b
            return (True)
        else:
            return (False)

    # /_lock_pwm_if_merged()

    def _set_merge_rgb(self):
        """Since checkbox does not pass value, this function handle it
        """
        self.set_merge_rgb(merge_rgb=self.rt.merge_rgb.get())

    # /_set_merge_rgb()

    def _set_pwm_all_img(self):
        """Set actual PWM value at all images (if at anim mode)
        """
        self.set_pwm_all_img(r=self.get_r(), g=self.get_g(), b=self.get_b())
    # /_set_pwm_all_img()


# /PWMCls


class _PWMRtCls:
    """Runtime variables for PWMCls"""

    def __init__(self,
                 r=255,
                 g=255,
                 b=255,
                 merge_rgb=False):
        # Prepare variables for tkinter slider
        self.r = tkinter.IntVar()
        self.g = tkinter.IntVar()
        self.b = tkinter.IntVar()

        self.r.set(r)
        self.r_old = r
        self.g.set(g)
        self.g_old = g
        self.b.set(b)
        self.b_old = b

        self.merge_rgb = tkinter.BooleanVar()

        self.merge_rgb.set(merge_rgb)
        # And some other variables

        # Slider length
        self.slider_len = 170

    # /__init__()

    def __str__(self):
        msg = ('== Runtime variables ==\n'
               f' R: {self.r.get()} ({self.r_old})\n'
               f' G: {self.g.get()} ({self.g_old})\n'
               f' B: {self.b.get()} ({self.b_old})\n'
               f' Merge RGB: {self.merge_rgb.get()}\n')
        return (msg)
    # /__str__()
# /_PWMRtCls

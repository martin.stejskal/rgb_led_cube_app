#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
Animation effects
=================================================

In some cases user can use predefined effects.

``author`` Martin Stejskal

``version`` 0.0.3
"""
# Basic GUI for Python
import tkinter

from lib.common import app_path


class EffectsCls:
    def __init__(self, basic_params_cls, update_play_gui):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame
        self.menu_bar = basic_params_cls.menu_bar
        self.mode = basic_params_cls.mode
        # Callback for enabling/disabling other windows. This is used when
        # processing something to disallow user to perform any other actions
        self.en_windows = basic_params_cls.en_windows
        self.dis_windows = basic_params_cls.dis_windows

        assert not (self.menu_bar is None), \
            'Variable "menu_bar" have to be set!'
        assert not (self.en_windows is None), \
            'Callback "en_windows" have to be set!'
        assert not (self.dis_windows is None), \
            'Callback "dis_windows" have to be set!'

        # Callback for updating play GUI
        self.update_play_gui = update_play_gui

        assert not (self.update_play_gui is None), \
            'Callback "update_play_gui" have to be set!'

        self._init_finish()

    # /__init__()
    # |EffectsCls

    def notification_progress(self, params):
        """Callback function to update processing status
        """
        # If window does not exists, throw notification
        if (self.rt.win_opt is None):
            msg = ("Received notification for progress status, but no progress"
                   " window is not exists.")
            self.logger.warning(msg)
            return
        # /if callback function does not exists

        self.rt.win_opt.update_win_processing(params)

    # /notification_progress()
    # |EffectsCls

    def notification_done(self, params):
        # If window does not exists, throw notification
        if (self.rt.win_opt is None):
            msg = "Received notification for job done, progress window is" \
                  " already closed."
            self.logger.warning(msg)
            return
        # /if window is already closed/crashed

        self.rt.win_opt.close_win_processing()
        # And erase all references to that window -> garbage collector will
        # take care about that
        self.rt.win_opt = None

        # Need to update number of frames and other stuff
        self.update_play_gui(force_update=True)

    # /notification_done()
    # |EffectsCls

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        self.menu_bar.entryconfig("Effects", state="disabled")

    # /dis_widgets()
    # |EffectsCls

    def en_widgets(self):
        """Enable all widgets at this module
        """
        self.menu_bar.entryconfig("Effects", state="normal")

    # /en_widgets()
    # |EffectsCls

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        # Initialize runtime variables
        self.rt = _EffectsRtCls()

        self.effects_menu = tkinter.Menu(self.menu_bar, tearoff=0)

        # Add effects and callback functions
        self.effects_menu.add_command(
            label="Fade out", command=self._fade_out_show_menu)

        # Add it under "Effects" submenu
        self.menu_bar.add_cascade(label="Effects", menu=self.effects_menu)

    # /_init_finish()
    # |EffectsCls

    def _fade_out_show_menu(self):
        self.rt.win_opt = self._WindowOptionsCls(
            self.logger, self._fade_out_do,
            self._en_other_windows, self._dis_other_windows)

        # Add parameters to the window
        # Duration (2~10000)
        dur_f = tkinter.Frame(
            self.rt.win_opt.win, borderwidth=2, relief=tkinter.RIDGE)
        dur_f.grid(column=0, row=0)

        dur_lbl = tkinter.Label(dur_f, text="Duration: ")
        dur_lbl2 = tkinter.Label(dur_f, text=" images")
        dur_spbox = tkinter.Spinbox(
            dur_f,
            from_=self.rt.fade_out.dur_min, to=self.rt.fade_out.dur_max,
            width=5, wrap=True, textvariable=self.rt.fade_out.duration,
            validate='all', validatecommand=self._fade_out_check_dur_range)
        dur_lbl.grid(column=0, row=0)
        dur_spbox.grid(column=1, row=0)
        dur_lbl2.grid(column=2, row=0)

        # Effect type
        eff_f = tkinter.Frame(
            self.rt.win_opt.win, borderwidth=2, relief=tkinter.RIDGE)
        eff_f.grid(column=0, row=1)

        eff_lbl = tkinter.Label(eff_f, text="Effect type")
        eff_rb1 = tkinter.Radiobutton(
            eff_f, text="Linear", value="linear",
            variable=self.rt.fade_out.effect_type)
        eff_rb2 = tkinter.Radiobutton(
            eff_f, text="Exponential", value="exponential",
            variable=self.rt.fade_out.effect_type)

        eff_lbl.grid(column=0, row=0)
        eff_rb1.grid(column=0, row=1)
        eff_rb2.grid(column=0, row=2)

        # PWM channels
        # 2DO: Add PWM options

    # /_fade_out_show_menu()
    # |EffectsCls

    def _fade_out_check_dur_range(self):
        """Check value and if not in range, modify it

        :returns: Status value. True if in range, False otherwise
        :rtype: boot
        """
        val = self.rt.fade_out.duration.get()
        try:
            val = int(val)
        except Exception:
            self.rt.fade_out.duration.set(self.rt.fade_out.dur_min)
            return (False)
        if (val < self.rt.fade_out.dur_min):
            self.rt.fade_out.duration.set(self.rt.fade_out.dur_min)
            return (False)
        if (val > self.rt.fade_out.dur_max):
            self.rt.fade_out.duration.set(self.rt.fade_out.dur_max)
            return (False)
        return (True)

    # /_fade_out_check_dur_range()
    # |EffectsCls

    def _fade_out_do(self):
        """Perform fade out animation
        """
        # There have to be valid value at any cost. Spinbox itself does not
        # cove all cases
        self._fade_out_check_dur_range()

        self.console_api.effects.fade_out(
            duration=int(self.rt.fade_out.duration.get()),
            effect_type=self.rt.fade_out.effect_type.get())

    # /_fade_out_do()
    # |EffectsCls

    def _dis_other_windows(self):
        """Disable "Effects" submenu

        When processing effect, user should not run another effect or any
        other action
        """
        self.dis_windows()

    # /_dis_other_windows()
    # |EffectsCls

    def _en_other_windows(self):
        """Enable "Effects" submenu

        When effect processing is done, submenu and other windows should be
        re-enabled
        """
        self.en_windows()

    # /_en_other_windows()
    # |EffectsCls

    class _WindowOptionsCls:
        def __init__(self, logger, do_clbck, en_windows, dis_windows):
            # Store variables in to instance
            self.logger = logger
            # Call this function when user click to "OK" button
            self.do_clbck = do_clbck

            # Enable "Effects" submenu
            self.en_windows = en_windows

            # Disable other windows to avoid running other actions
            self.dis_windows = dis_windows
            self.dis_windows()

            self.win = tkinter.Toplevel()
            self.win.title("Options")
            # Need to handle situation when user click to "X"
            self.win.protocol("WM_DELETE_WINDOW", self.cancel)

            # Only this window will be active (others disabled)
            self.win.grab_set()

            # Status window (Processing) does not exists yet
            self.win_proc = None

            img_dir = "{}/lib/gui/tkinter/lib/effects_img/".format(app_path)
            self.ok_img = tkinter.PhotoImage(file="{}ok.png".format(img_dir))
            self.cancel_img = tkinter.PhotoImage(file="{}cancel.png"
                                                      "".format(img_dir))

            # This is not critical part and it is problematic on Pyinstaller
            try:
                # At Linux it have to be in xbm format and with "@" symbol.
                self.win.iconbitmap('@{}/lib/app_icon.xbm'
                                    ''.format(app_path))
            except Exception:
                msg = "Can not load icon, but this is not crucial"
                self.logger.warn(msg)

            # Set keyboard shortcuts for "Esc" and "Enter"
            self.win.bind("<Key>", self.kbrd_event)

            # Add OK & Cancel buttons to bottom frame
            btn_f = tkinter.Frame(
                self.win, borderwidth=2, relief=tkinter.RIDGE)
            btn_ok = tkinter.Button(
                btn_f, text="OK", command=self.ok, compound="right",
                image=self.ok_img)
            btn_cancl = tkinter.Button(
                btn_f, text="Cancel", command=self.cancel, compound="right",
                image=self.cancel_img)

            btn_f.grid(column=0, row=99, columnspan=99)
            btn_ok.grid(column=0, row=0)
            btn_cancl.grid(column=1, row=0)

        # /__init__()
        # |_WindowOptionsCls

        def ok(self):
            self.win.grab_release()
            self.win.destroy()
            # No time to wait for another update from upper layers
            self.win.update()
            self.create_win_processing()
            # Request to do effect. Now need to wait for notification that
            # effect is done
            self.do_clbck()

        # /ok()
        # |_WindowOptionsCls

        def cancel(self):
            self.win.grab_release()
            self.win.destroy()
            self.en_windows()

        # /cancel()
        # |_WindowOptionsCls

        def kbrd_event(self, event):
            """Keyboard shortcuts"""
            if (event.keycode == 9):
                # ESC button
                self.cancel()
            elif ((event.keycode == 36) or (event.keycode == 104)):
                self.ok()

        # /kbrd_event()
        # |_WindowOptionsCls

        def create_win_processing(self):
            self.win_proc = tkinter.Toplevel()
            self.win_proc.title("Processing...")
            self.win_proc.protocol("WM_DELETE_WINDOW", self._can_not_close_msg)

            # Only this window will be active (others disabled)
            self.win_proc.grab_set()

            lbl_proc = tkinter.Label(
                self.win_proc, text="Processing...", font=("Helvetica", 16))
            lbl_proc.grid(column=0, row=0)

            # This label can be modified
            self.win_proc.lbl = tkinter.Label(
                self.win_proc, text="", font=("Helvetica", 16))
            self.win_proc.lbl.grid(column=0, row=1)

            try:
                # At Linux it have to be in xbm format and with "@" symbol.
                self.win_proc.iconbitmap('@{}/lib/app_icon.xbm'
                                         ''.format(app_path))
            except Exception:
                msg = "Can not load icon, but this is not crucial"
                self.logger.warn(msg)

            self.win_proc.wait_visibility(lbl_proc)
            self.win_proc.update()

        # /create_win_processing()
        # |_WindowOptionsCls

        def update_win_processing(self, msg="0%"):
            """Update message for "processing" (status) window

            :param msg: Message which will be displayed
            :type msg: str
            """
            if (self.win_proc is None):
                msg = "Can not update status message, since window does not" \
                      " exists."
                self.logger.warning(msg)
                return
            # /if window does not exists yet

            self.win_proc.lbl.config(text=msg)
            self.win_proc.update()

        # /update_win_processing()
        # |_WindowOptionsCls

        def close_win_processing(self):
            # This window will not be only one active no more
            self.win_proc.grab_release()
            self.win_proc.destroy()
            self.win_proc.update()
            # Window can be closed, but all functions still can be there ->
            # -> need to make sure that update_win_processing() will not even
            # try to change label from now on
            self.win_proc = None

            self.en_windows()

        # /close_win_processing()
        # |_WindowOptionsCls

        def _can_not_close_msg(self):
            msg = 'Can not close window while processing'
            self.logger.info(msg)
    # /_WindowOptionsCls


# /EffectsCls


class _EffectsRtCls:
    """Runtime variabless for EffectsCls"""

    def __init__(self):
        # Keep Window options object. If none, window was closed/processing
        # done or job canceled
        self.win_opt = None
        self.fade_out = self._FadeOutCls()

    # /__init__()

    # ============================| Internal classes |========================
    class _FadeOutCls:
        def __init__(self, duration=30, effect_type="exponential",
                     pwm=[True, True, True]):
            self.duration = tkinter.StringVar()
            self.dur_min = 2
            self.dur_max = 10000
            self.effect_type = tkinter.StringVar()
            self.pwm = [tkinter.BooleanVar(),
                        tkinter.BooleanVar(),
                        tkinter.BooleanVar()]

            # Set default values
            self.duration.set(duration)
            self.effect_type.set(effect_type)
            self.pwm[0].set(pwm[0])
            self.pwm[1].set(pwm[1])
            self.pwm[2].set(pwm[2])
# /_EffectsRtCls

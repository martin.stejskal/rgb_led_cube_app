#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Module for handling "random" animations
===================================

Select active random mode

``author`` Martin Stejskal

``version`` 0.1.1
"""
# Basic GUI for Python
import tkinter


class RndmCls:
    """Module for operations with cube
    """

    def __init__(self, basic_params_cls):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self._init_finish()

    # /__init__()

    def get_animations(self):
        """Return list of supported random animations

        :returns: List of supported animations
        :rtype: list (str)
        """
        return (self.console_api.rndm.get_animations())

    # /get_animations

    def set_animation(self, anim_name):
        """Play selected random animation

        :param anim_name: Selected animation name
        :type anim_name: str
        """
        self.console_api.rndm.set_animation(anim_name)

    # /set_animation()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        # ==================================| Timer |=========================
        self.logger.debug("Preparing timer...")
        self.l_timer = tkinter.Label(self.parent_frame,
                                     text="Timer [ms]")
        self.scle_timer = tkinter.Scale(self.parent_frame,
                                        command=self._timer_slider_moved,
                                        from_=30,
                                        to=1000,
                                        length=180,
                                        orient=tkinter.HORIZONTAL,
                                        resolution=10,
                                        showvalue=1)
        # Set value according to console API
        self.scle_timer.set(value=self.console_api.rndm.get_timer())

        self.l_timer.grid(column=0, row=0, columnspan=2)
        self.scle_timer.grid(column=0, row=1, columnspan=2)

        # ===============================| Random list |======================
        self.logger.debug("Preparing list of supported animations...")
        # Create scroll bar, which will be attached to list box
        self.scrlb_lstbox = tkinter.Scrollbar(self.parent_frame)
        self.lstbx_sel_anim = tkinter.Listbox(
            self.parent_frame,
            yscrollcommand=self.scrlb_lstbox.set)
        self.scrlb_lstbox.config(command=self.lstbx_sel_anim.yview)

        # Get available animations
        self.available_anims = self.get_animations()
        self.logger.debug("List of available animations:\n{}"
                          "".format(self.available_anims))

        # Add them to list box
        for anim in self.available_anims:
            # Add them to list box
            self.lstbx_sel_anim.insert(tkinter.END, anim)

        # Generate event when user will click to list box
        self.lstbx_sel_anim.event_generate("<<ListboxSelect>>")
        # And add bindings to object -> when user click on item, change mode
        self.lstbx_sel_anim.bind('<<ListboxSelect>>',
                                 self._play_selected_anim)

        # Put list box on frame
        self.lstbx_sel_anim.grid(column=0, row=2,
                                 sticky=tkinter.W + tkinter.E)
        self.scrlb_lstbox.grid(column=1, row=2,
                               sticky=tkinter.N + tkinter.S)

        # Select first animation (this will activate playing)
        if (len(self.available_anims) > 0):
            self.logger.info("Selecting animation {}"
                             "".format(self.available_anims[0]))
            self.lstbx_sel_anim.select_set(0)
            self.set_animation(anim_name=self.available_anims[0])
        else:
            self.logger.warn("No available animation for this dimension :(")

    # /_init_finish()

    def _timer_slider_moved(self, value):
        """This function is called when timer slider moved"""
        self.console_api.play.set_timer(value)

    # /_timer_slider_moved()

    def _play_selected_anim(self, event):
        """Called when user clicked to any available animation"""
        current_selection = event.widget.curselection()
        self.logger.debug("Selected mode(s) >>{}<<"
                          "".format(current_selection))

        if (len(current_selection) < 1):
            # Nothing selected -> probably just void event from tkinter
            self.logger.debug(
                "Nothing selected in list box, but event occurred.\n"
                " Keeping current animation")
            return

        # Else some mode is really selected -> set it (only first one anyway)
        anim_name = self.available_anims[current_selection[0]]
        self.set_animation(anim_name=anim_name)
    # /_play_selected_anim()


# /RndmCls


# ============================| Internal classes |============================
class _RtRandomCls:
    """Runtime class for RndmCls"""

    def __init__(self):
        self.timer = tkinter.IntVar()
    # /__init__()
# /_RtRandomCls

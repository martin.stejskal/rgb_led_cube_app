#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===================================
Module for setting individual LEDs
===================================

Allow to set every single LED in actual wall

``author`` Martin Stejskal

``version`` 0.6.2
"""
# Basic GUI for Python
import tkinter

from lib.gui.tkinter.lib.common_tk import TkObjectCls
from lib.common import app_path


class BtnArrayCls:
    """Module for operations with cube

    """

    def __init__(self, basic_params_cls, close_callback):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls

        :param close_callback: Pointer to close function
        :type close_callback: Pointer to function
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self.close_callback = close_callback
        # Make it easy to change
        self.pwd = "{}/lib/gui/tkinter".format(app_path)

        # Need/want access to keyboard shortcuts, since there is another
        # window -> pass shortcut
        self.kbrd_event_fnc = basic_params_cls.kbrd_event_fnc

        # Runtime variables
        self.rt = _RtBtnArrayCls()

        self._init_finish()

    # /__init__()

    def set_column(self, column_idx):
        """Set colors (RGB) at specified column

        If column_idx is None, then all columns are selected

        :param column_idx: Column index
        :type column_idx: None, int (0 ~ (dimension -1))
        """
        self.set_led(column_idx=column_idx, row_idx=None)

    # /set_column()

    def set_row(self, row_idx):
        """Set colors (RGB) at specified row

        If row_idx is None, than all rows are selected

        :param row_idx: Row index
        :type row_idx: None, int (0 ~ (dimension -1))
        """
        self.set_led(column_idx=None, row_idx=row_idx)

    # /set_row()

    def set_led(self, column_idx, row_idx):
        """Set colors (RGB) at specified LED coordinates

        If column_idx is None, then all columns are selected.
        If row_idx is None, then all rows are selected.

        :param column_idx: Column index
        :type column_idx: None, int (0 ~ (dimension -1))
        :param row_idx: Row index
        :type row_idx: None, int (0 ~ (dimension -1))
        """
        auto_color = self.rt.color.o.auto.get()
        only_black_and_white = self.rt.color.o.black_and_white.get()
        self.logger.debug("Setting LED column: {} ; row: {} ; Auto color: {}"
                          " ; Only B&W: {}"
                          "".format(column_idx, row_idx, auto_color,
                                    only_black_and_white))

        # If auto color is not enabled, do not try to set "only b & w"
        if (not auto_color):
            only_black_and_white = False

        # Just copy/paste values and let console API to deal with it
        self.console_api.btn_array.set_led(
            column=column_idx, row=row_idx,
            r=self.rt.color.o.r.get(),
            g=self.rt.color.o.g.get(),
            b=self.rt.color.o.b.get(),
            auto_color=auto_color,
            auto_blck_and_whte=only_black_and_white)

        # Update R/G/B check buttons if "auto color" is enabled
        self._update_rgb_color_if_auto_color_enabled()

    # /set_led()

    def close(self):
        """Properly close button array window
        """
        self.rt.close_by_control_w = True

        self.w_btn_array.destroy()

    # /close()

    def dis_widgets(self):
        """Disable all widgets at this module

        Also disable option to close button array
        """
        self._disable_closing_window()
        # Disable all color related checkboxes. They would not
        # generate any traffic anyway, but when everything should be disabled,
        # let's disable truly everything.
        widgets = self.widgets_en_dis
        widgets.extend([
            self.rt.color.o.cb_auto, self.rt.color.o.cb_r,
            self.rt.color.o.cb_g, self.rt.color.o.cb_b,
            self.rt.color.o.cb_b_and_w])

        for widget in widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module

        Also allow user to use close ("X") button again
        """
        for widget in self.widgets_en_dis:
            widget.config(state=tkinter.NORMAL)

        # Restore state of color related checkboxes
        self._update_rgb_color_if_auto_color_enabled()

        self._enable_closing_window()

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        self.logger.debug("Preparing button array window & frames...")

        # Create new window. Note that "Toplevel" have to be used
        self.w_btn_array = tkinter.Toplevel()
        self.w_btn_array.title("Button array")

        # This is not critical part and it is problematic on Pyinstaller
        try:
            # At Linux it have to be in xbm format and with "@" symbol.
            self.w_btn_array.iconbitmap('@{}/lib/app_icon.xbm'
                                        ''.format(app_path))
        except Exception:
            msg = "Can not load icon, but this is not crucial"
            self.logger.warn(msg)

        # When closing, callback predefined function
        self._enable_closing_window()

        self.w_btn_array.bind("<Key>", self.kbrd_event_fnc)

        # Get dimension value
        dim = self.console_api.get_dimension()

        # Put frames inside window

        self.rt.btn_array.f = tkinter.Frame(self.w_btn_array,
                                            borderwidth=2,
                                            relief=tkinter.RIDGE)
        self.rt.clm_array.f = tkinter.Frame(self.w_btn_array,
                                            borderwidth=2,
                                            relief=tkinter.RIDGE)
        self.rt.row_array.f = tkinter.Frame(self.w_btn_array,
                                            borderwidth=2,
                                            relief=tkinter.RIDGE)
        self.rt.color.f = tkinter.Frame(self.w_btn_array,
                                        borderwidth=2,
                                        relief=tkinter.RIDGE)

        # Define position of frames
        self.rt.btn_array.f.grid(column=1, row=0, columnspan=dim, rowspan=dim)
        self.rt.clm_array.f.grid(column=1, row=dim, columnspan=dim)
        self.rt.row_array.f.grid(column=0, row=0, rowspan=dim)
        self.rt.color.f.grid(column=0, row=(dim + 1), columnspan=(dim + 1))

        # Now generate GUI itself. Little bit complicated, since this have to
        # be generated dynamically
        # ==============================| Button array |======================
        # Buttons will be stored at list
        self.rt.btn_array.o.btns = []

        # Make it simple
        btns = self.rt.btn_array.o.btns

        # Counter
        idx = 0

        for column_idx in range(dim):
            for row_idx in range(dim):
                # Save coordinates to one variable (object)
                coordinates = [column_idx, row_idx]

                # Add button - also with predefined arguments for function
                btns.append(
                    tkinter.Button(
                        self.rt.btn_array.f,
                        text="{}".format(idx),
                        command=lambda coordinates=coordinates:
                        self.set_led(column_idx=coordinates[0],
                                     row_idx=coordinates[1])))
                # And set button position. Since position system in Tkinter
                # and 3D LED cube is different, it is not so straightforward
                btns[idx].grid(column=column_idx,
                               row=((dim - 1) - row_idx))

                idx = idx + 1
            # /for row
        # /for column

        # ==============================| Column array |======================
        self.rt.clm_array.o.btns = []

        # Make it simple
        btns = self.rt.clm_array.o.btns

        for column_idx in range(dim):
            btns.append(
                tkinter.Button(
                    self.rt.clm_array.f,
                    text="{}".format(column_idx),
                    command=lambda column_idx=column_idx:
                    self.set_column(column_idx)))
            # Set button position
            btns[column_idx].grid(column=column_idx, row=0)
        # /for all columns

        # ===============================| Row array |========================
        self.rt.row_array.o.btns = []

        btns = self.rt.row_array.o.btns

        for row_idx in range(dim):
            btns.append(
                tkinter.Button(
                    self.rt.row_array.f,
                    text="{}".format(row_idx),
                    command=lambda row_idx=row_idx:
                    self.set_row(row_idx)))
            # Set button position
            btns[row_idx].grid(column=0, row=((dim - 1) - row_idx))
        # /for all rows

        # =================================| Color |==========================
        # By default, set "auto color" to enabled
        self.rt.color.o.auto.set(True)
        # Not only black and white
        self.rt.color.o.black_and_white.set(False)

        # Get settings from console API
        colors = self.console_api.btn_array.get_auto_color()
        self.rt.color.o.r.set(colors[0])
        self.rt.color.o.g.set(colors[1])
        self.rt.color.o.b.set(colors[2])

        # Auto color check box
        self.rt.color.o.cb_auto = tkinter.Checkbutton(
            self.rt.color.f,
            text="Auto",
            variable=self.rt.color.o.auto,
            command=self._update_rgb_color_if_auto_color_enabled)

        # R
        self.rt.color.o.cb_r = tkinter.Checkbutton(self.rt.color.f,
                                                   text="R",
                                                   variable=self.rt.color.o.r)
        # G
        self.rt.color.o.cb_g = tkinter.Checkbutton(self.rt.color.f,
                                                   text="G",
                                                   variable=self.rt.color.o.g)
        # B
        self.rt.color.o.cb_b = tkinter.Checkbutton(self.rt.color.f,
                                                   text="B",
                                                   variable=self.rt.color.o.b)

        # Only black and white auto color checkbox
        self.rt.color.o.cb_b_and_w = tkinter.Checkbutton(
            self.rt.color.f,
            text="Black & white",
            variable=self.rt.color.o.black_and_white)

        # Set coordinates
        self.rt.color.o.cb_auto.grid(column=0, row=0)
        self.rt.color.o.cb_r.grid(column=1, row=0)
        self.rt.color.o.cb_g.grid(column=2, row=0)
        self.rt.color.o.cb_b.grid(column=3, row=0)
        self.rt.color.o.cb_b_and_w.grid(column=0, row=1, columnspan=4)

        # Update enable/disable for R/G/B according to "auto color"
        self._update_rgb_color_if_auto_color_enabled()

        # List of widgets which can be directly enabled/disabled (stateless)
        self.widgets_en_dis = []
        self.widgets_en_dis.extend(self.rt.btn_array.o.btns)
        self.widgets_en_dis.extend(self.rt.clm_array.o.btns)
        self.widgets_en_dis.extend(self.rt.row_array.o.btns)

    # /_init_finish()

    def _on_closing(self):
        """When user click to "X" (close) button, this function is called"""

        # Sometimes control_w want close button array window without closing
        # whole application. And for this, following variable can be set, so
        # 3D view and console API keep running
        if (self.rt.close_by_control_w):
            pass
        else:
            self.logger.info('User clicked to "close" button')
            self.close_callback()

    # /_on_closing()

    def _on_closing_dummy(self):
        """When user click to "X" (close), but module is disabled
        """
        pass

    # /_on_closing_dummy()

    def _update_rgb_color_if_auto_color_enabled(self):
        """Updated R/G/B at checkboxes when auto color is enabled"""
        if (self.rt.color.o.auto.get()):
            # Auto color enabled -> get new R/G/B from console API and
            # update it. Also disable R/G/B check buttons
            self.rt.color.o.cb_r.config(state=tkinter.DISABLED)
            self.rt.color.o.cb_g.config(state=tkinter.DISABLED)
            self.rt.color.o.cb_b.config(state=tkinter.DISABLED)

            # Enable "only black and white" option
            self.rt.color.o.cb_b_and_w.config(state=tkinter.NORMAL)

            # Get new R/G/B
            color = self.console_api.btn_array.get_auto_color()

            # Set new R/G/B
            if (color[0]):
                self.rt.color.o.cb_r.select()
            else:
                self.rt.color.o.cb_r.deselect()

            if (color[1]):
                self.rt.color.o.cb_g.select()
            else:
                self.rt.color.o.cb_g.deselect()

            if (color[2]):
                self.rt.color.o.cb_b.select()
            else:
                self.rt.color.o.cb_b.deselect()
        else:
            # Auto color disabled -> enable R/G/B check boxes. Keep color
            self.rt.color.o.cb_r.config(state=tkinter.NORMAL)
            self.rt.color.o.cb_g.config(state=tkinter.NORMAL)
            self.rt.color.o.cb_b.config(state=tkinter.NORMAL)

            # Disable "only black and white" option
            self.rt.color.o.cb_b_and_w.config(state=tkinter.DISABLED)
        # /Auto color disabled

    # /_update_rgb_color_if_auto_color_enabled()

    def _enable_closing_window(self):
        self.w_btn_array.protocol("WM_DELETE_WINDOW", self._on_closing)

    def _disable_closing_window(self):
        self.w_btn_array.protocol("WM_DELETE_WINDOW", self._on_closing_dummy)


# /BtnArrayCls

# ============================| Internal classes |============================


class _RtBtnArrayCls:
    """Runtime variables for BtnArrayCls"""

    def __init__(self):
        # Color variables. This will add "f" (frame) and "o" (objects)
        self.color = TkObjectCls()
        # Add color variables to "o"
        self.color.o = self.ColorVarCls()

        self.btn_array = TkObjectCls()
        self.clm_array = TkObjectCls()
        self.row_array = TkObjectCls()

        # When button array is closed by control_w, closing procedure
        # differs a bit (does not close console_api)
        self.close_by_control_w = False

    # /__init__()

    class ColorVarCls:
        """Keep runtime variables for Tkinter and colors"""

        def __init__(self):
            self.auto = tkinter.BooleanVar()
            self.black_and_white = tkinter.BooleanVar()
            self.r = tkinter.BooleanVar()
            self.g = tkinter.BooleanVar()
            self.b = tkinter.BooleanVar()
        # /__init__()
    # /ColorVarCls
# /_RtBtnArrayCls

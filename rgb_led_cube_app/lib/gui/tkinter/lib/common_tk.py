#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===================================================
Common variables/classes for most of python modules
===================================================

  Some classes need to be shared and cross include is not too nice. So let's
  make one file with common data.

  ``author`` Martin Stejskal

  ``version`` 0.8.0

  ``Created``  2016.02.29

  ``Modified`` 2018.05.08
"""

# Else load that library
from lib.common import DummyCls


class PosStructCls:
    """Position structure

    Every element can be positioned by following values: column, row,
     column_width and row_width.
    """

    def __init__(self,
                 row=0,
                 column=0,
                 row_width=1,
                 column_width=1):
        self.row = row
        self.column = column
        self.row_width = row_width
        self.column_width = column_width
    # /__init__()
    # | PosStructCls
# /PosStructCls


class TkObjectCls:
    """Every Tk object have frame object and other Tk objects"""

    def __init__(self):
        # Frame
        self.f = None
        # Tk objects (labels, buttons, etc...)
        self.o = DummyCls()
    # /__init__()
# /TkObjectCls

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===============================
Module for operations with LEDs
===============================

If you want turn on all LEDs, make some rotations and so on, this is right
module. This module does not work with single LEDs. It is designed to work
with cube as whole (or maybe on wall level)

``author`` Martin Stejskal

``version`` 0.4.1
"""

# Basic GUI for Python
import tkinter

from lib.gui.tkinter.lib.common_tk import TkObjectCls
from lib.common import app_path


class OpCls:
    """Module for operations with cube

    """

    def __init__(self, basic_params_cls):
        """
            :param basic_params_cls: Basic parameters wrapped in one class
            :type basic_params_cls: _BasicParamsCls
            """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        # Make it more easy to change
        self.pwd = "{}/lib/gui/tkinter".format(app_path)
        self._init_finish()

    # /__init__()

    def cube_set_all(self):
        """Turn on every single LED in cube"""
        self.console_api.op.cube_set_all()

    def cube_clear_all(self):
        """Turn off every single LED in cube"""
        self.console_api.op.cube_clear_all()

    def cube_set_r(self):
        """Set all red LEDs

        If is checked option "additional R/G/B", then keep original color
        and just add red color
        """
        # Check if "additive" option is enabled
        if (self.rt.tk.cbe_clrs.o.add_clr.get()):
            self.console_api.op.cube_set_r(additive=True)
        else:
            self.console_api.op.cube_set_r(additive=False)

    # /cube_set_r()

    def cube_set_g(self):
        """Set all green LEDs

        If is checked option "additional R/G/B", then keep original color
        and just add green color
        """
        if (self.rt.tk.cbe_clrs.o.add_clr.get()):
            self.console_api.op.cube_set_g(additive=True)
        else:
            self.console_api.op.cube_set_g(additive=False)

    # /cube_set_g()

    def cube_set_b(self):
        """Set all blue LEDs

        If is checked option "additional R/G/B", then keep original color
        and just add blue color
        """
        if (self.rt.tk.cbe_clrs.o.add_clr.get()):
            self.console_api.op.cube_set_b(additive=True)
        else:
            self.console_api.op.cube_set_b(additive=False)

    # /cube_set_b()

    def wall_set_all(self):
        """Turn on all LED in active wall"""
        self.console_api.op.wall_set_all()

    # /set_wall_all()

    def wall_clear_all(self):
        """Turn off all LED in active wall"""
        self.console_api.op.wall_clear_all()

    # /wall_clear_all()

    def wall_set_r(self):
        """Turn all red LED in active wall

        If is checked option "additional R/G/B", then keep original color
        and just add red color
        """
        if (self.rt.tk.wll_clrs.o.add_clr.get()):
            self.console_api.op.wall_set_r(additive=True)
        else:
            self.console_api.op.wall_set_r(additive=False)

    # /wall_set_r()

    def wall_set_g(self):
        """Turn all green LED in active wall

        If is checked option "additional R/G/B", then keep original color
        and just add green color
        """
        if (self.rt.tk.wll_clrs.o.add_clr.get()):
            self.console_api.op.wall_set_g(additive=True)
        else:
            self.console_api.op.wall_set_g(additive=False)

    # /wall_set_g()

    def wall_set_b(self):
        """Turn all blue LED in active wall

        If is checked option "additional R/G/B", then keep original color
        and just add blue color
        """
        if (self.rt.tk.wll_clrs.o.add_clr.get()):
            self.console_api.op.wall_set_b(additive=True)
        else:
            self.console_api.op.wall_set_b(additive=False)

    # /wall_set_b()

    def t_bck2frnt(self):
        """Make transformation from BACK to FRONT wall"""
        self.console_api.op.t_bck2frnt(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_bck2frnt()

    def t_frnt2bck(self):
        """Make transformation from FRONT to BACK wall"""
        self.console_api.op.t_frnt2bck(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_frnt2bck()

    def t_lft2rght(self):
        """Make transformation from LEFT to RIGHT wall"""
        self.console_api.op.t_lft2rght(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_lft2rght()

    def t_rght2lft(self):
        """Make transformation from RIGHT to LEFT wall"""
        self.console_api.op.t_rght2lft(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_rght2lft()

    def t_btm2top(self):
        """Make transformation from BOTTOM to TOP wall"""
        self.console_api.op.t_btm2top(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_btm2top()

    def t_top2btm(self):
        """Make transformation from TOP to BOTTOM wall"""
        self.console_api.op.t_top2btm(
            shifts=1,
            repeat=bool(self.rt.tk.trans.o.repeat.get()))

    # /t_top2btm()

    def r_top2frnt(self):
        """Rotation from top to front"""
        self.console_api.op.r_top2frnt(shifts=1)

    # /r_top2frnt()

    def r_top2bck(self):
        """Rotation from top to back"""
        self.console_api.op.r_top2bck(shifts=1)

    # /r_top2bck()

    def r_frnt2lft(self):
        """Rotation from front to left"""
        self.console_api.op.r_frnt2lft(shifts=1)

    # /r_frnt2lft()

    def r_frnt2rght(self):
        """Rotation from front to right"""
        self.console_api.op.r_frnt2rght(shifts=1)

    # /r_frnt2rght()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

    # /en_widgets()

    # ===========================| Internal functions |=======================

    def _init_finish(self):
        self.logger.debug("Preparing frames...")

        self.rt = _OpRtCls(fldr="{}/lib/op_img/".format(self.pwd))

        # Inside this frame will be following sub-frames:
        #  * Cube colors
        #  * Wall colors
        #  * Transformations
        #  * Rotations
        self.rt.tk.cbe_clrs.f = tkinter.Frame(self.parent_frame,
                                              borderwidth=2,
                                              relief=tkinter.RIDGE)
        self.rt.tk.wll_clrs.f = tkinter.Frame(self.parent_frame,
                                              borderwidth=2,
                                              relief=tkinter.RIDGE)
        self.rt.tk.trans.f = tkinter.Frame(self.parent_frame,
                                           borderwidth=2,
                                           relief=tkinter.RIDGE)
        self.rt.tk.rot.f = tkinter.Frame(self.parent_frame,
                                         borderwidth=2,
                                         relief=tkinter.RIDGE)

        # Put sub-frames to grid
        self.rt.tk.cbe_clrs.f.grid(column=0, row=0)
        self.rt.tk.wll_clrs.f.grid(column=0, row=1)
        self.rt.tk.trans.f.grid(column=0, row=2)
        self.rt.tk.rot.f.grid(column=0, row=3)

        # ===============================| Cube color |=======================
        self.logger.debug("Preparing cube color buttons ...")
        # Make it simple
        cbe_clrs = self.rt.tk.cbe_clrs

        # Label
        cbe_clrs.o.l_title = tkinter.Label(cbe_clrs.f,
                                           text="Cube color")

        # Buttons with images
        cbe_clrs.o.b_all = tkinter.Button(cbe_clrs.f,
                                          bg="white",
                                          image=cbe_clrs.img.all,
                                          command=self.cube_set_all)
        cbe_clrs.o.b_none = tkinter.Button(cbe_clrs.f,
                                           bg="black",
                                           image=cbe_clrs.img.none,
                                           command=self.cube_clear_all)
        cbe_clrs.o.b_r = tkinter.Button(cbe_clrs.f,
                                        bg="red",
                                        image=cbe_clrs.img.r,
                                        command=self.cube_set_r)
        cbe_clrs.o.b_g = tkinter.Button(cbe_clrs.f,
                                        bg="green",
                                        image=cbe_clrs.img.g,
                                        command=self.cube_set_g)
        cbe_clrs.o.b_b = tkinter.Button(cbe_clrs.f,
                                        bg="blue",
                                        image=cbe_clrs.img.b,
                                        command=self.cube_set_b)

        # Checkbox - additive colors?
        cbe_clrs.o.cb_add_clr = tkinter.Checkbutton(
            cbe_clrs.f,
            text="Additive R/G/B",
            variable=cbe_clrs.o.add_clr,
            onvalue=1,
            offvalue=0)

        cbe_clrs.o.l_title.grid(column=0, row=0, columnspan=5)
        cbe_clrs.o.b_all.grid(column=0, row=1)
        cbe_clrs.o.b_none.grid(column=4, row=1)
        cbe_clrs.o.b_r.grid(column=1, row=1)
        cbe_clrs.o.b_g.grid(column=2, row=1)
        cbe_clrs.o.b_b.grid(column=3, row=1)
        cbe_clrs.o.cb_add_clr.grid(column=0, row=2, columnspan=5)

        # ===============================| Wall color |=======================
        self.logger.debug("Preparing wall color buttons ...")
        wll_clrs = self.rt.tk.wll_clrs

        # Label
        wll_clrs.o.l_title = tkinter.Label(wll_clrs.f,
                                           text="Wall color")

        # Buttons with images
        wll_clrs.o.b_all = tkinter.Button(wll_clrs.f,
                                          bg="white",
                                          image=wll_clrs.img.all,
                                          command=self.wall_set_all)
        wll_clrs.o.b_none = tkinter.Button(wll_clrs.f,
                                           bg="black",
                                           image=wll_clrs.img.none,
                                           command=self.wall_clear_all)
        wll_clrs.o.b_r = tkinter.Button(wll_clrs.f,
                                        bg="red",
                                        image=wll_clrs.img.r,
                                        command=self.wall_set_r)
        wll_clrs.o.b_g = tkinter.Button(wll_clrs.f,
                                        bg="green",
                                        image=wll_clrs.img.g,
                                        command=self.wall_set_g)
        wll_clrs.o.b_b = tkinter.Button(wll_clrs.f,
                                        bg="blue",
                                        image=wll_clrs.img.b,
                                        command=self.wall_set_b)

        wll_clrs.o.cb_add_clr = tkinter.Checkbutton(
            wll_clrs.f,
            text="Additive R/G/B",
            variable=wll_clrs.o.add_clr,
            onvalue=1,
            offvalue=0)

        wll_clrs.o.l_title.grid(column=0, row=0, columnspan=5)
        wll_clrs.o.b_all.grid(column=0, row=1)
        wll_clrs.o.b_none.grid(column=4, row=1)
        wll_clrs.o.b_r.grid(column=1, row=1)
        wll_clrs.o.b_g.grid(column=2, row=1)
        wll_clrs.o.b_b.grid(column=3, row=1)
        wll_clrs.o.cb_add_clr.grid(column=0, row=2, columnspan=5)
        # ============================| Transformations |=====================
        self.logger.debug("Preparing transformation buttons ...")

        # Make it simple
        trans = self.rt.tk.trans

        trans.o.l_title = tkinter.Label(trans.f,
                                        text="Transformations")

        trans.o.b_t_bck2frnt = tkinter.Button(trans.f,
                                              image=trans.img.bck2frnt,
                                              command=self.t_bck2frnt)
        trans.o.b_t_frnt2bck = tkinter.Button(trans.f,
                                              image=trans.img.frnt2bck,
                                              command=self.t_frnt2bck)
        trans.o.b_t_lft2rght = tkinter.Button(trans.f,
                                              image=trans.img.lft2rght,
                                              command=self.t_lft2rght)
        trans.o.b_t_rght2lft = tkinter.Button(trans.f,
                                              image=trans.img.rght2lft,
                                              command=self.t_rght2lft)
        trans.o.b_t_btm2top = tkinter.Button(trans.f,
                                             image=trans.img.btm2top,
                                             command=self.t_btm2top)
        trans.o.b_t_top2btm = tkinter.Button(trans.f,
                                             image=trans.img.top2btm,
                                             command=self.t_top2btm)

        trans.o.cb_repeat = tkinter.Checkbutton(trans.f,
                                                text="Repeat",
                                                variable=trans.o.repeat,
                                                onvalue=1,
                                                offvalue=0)

        trans.o.l_title.grid(column=0, row=0, columnspan=6)
        trans.o.b_t_bck2frnt.grid(column=0, row=1)
        trans.o.b_t_frnt2bck.grid(column=1, row=1)
        trans.o.b_t_lft2rght.grid(column=2, row=1)
        trans.o.b_t_rght2lft.grid(column=3, row=1)
        trans.o.b_t_btm2top.grid(column=4, row=1)
        trans.o.b_t_top2btm.grid(column=5, row=1)
        trans.o.cb_repeat.grid(column=0, row=2, columnspan=6)

        # ===============================| Rotations |========================
        self.logger.debug("Preparing rotation buttons")

        rot = self.rt.tk.rot

        rot.o.l_title = tkinter.Label(rot.f,
                                      text="Rotations")

        rot.o.b_r_top2frnt = tkinter.Button(rot.f,
                                            image=rot.img.top2frnt,
                                            command=self.r_top2frnt)
        rot.o.b_r_top2bck = tkinter.Button(rot.f,
                                           image=rot.img.top2bck,
                                           command=self.r_top2bck)

        rot.o.b_r_frnt2lft = tkinter.Button(rot.f,
                                            image=rot.img.frnt2lft,
                                            command=self.r_frnt2lft)
        rot.o.b_r_frnt2rght = tkinter.Button(rot.f,
                                             image=rot.img.frnt2rght,
                                             command=self.r_frnt2rght)

        rot.o.l_title.grid(column=0, row=0, columnspan=6)
        rot.o.b_r_top2frnt.grid(column=0, row=1)
        rot.o.b_r_frnt2lft.grid(column=1, row=1)
        rot.o.b_r_frnt2rght.grid(column=2, row=1)
        rot.o.b_r_top2bck.grid(column=3, row=1)

        # =================================| Finish |=========================
        self.widgets = [
            cbe_clrs.o.l_title, cbe_clrs.o.b_all, cbe_clrs.o.b_none,
            cbe_clrs.o.b_r, cbe_clrs.o.b_g, cbe_clrs.o.b_b,
            cbe_clrs.o.cb_add_clr,

            wll_clrs.o.l_title, wll_clrs.o.b_all, wll_clrs.o.b_none,
            wll_clrs.o.b_r, wll_clrs.o.b_g, wll_clrs.o.b_b,
            wll_clrs.o.cb_add_clr,

            trans.o.l_title, trans.o.b_t_bck2frnt, trans.o.b_t_frnt2bck,
            trans.o.b_t_lft2rght, trans.o.b_t_rght2lft, trans.o.b_t_btm2top,
            trans.o.b_t_top2btm, trans.o.cb_repeat,

            rot.o.l_title, rot.o.b_r_top2frnt, rot.o.b_r_top2bck,
            rot.o.b_r_frnt2lft, rot.o.b_r_frnt2rght, ]

        self.logger.debug("Init done")
    # /_init_finish()


# /OpCls


class _OpRtCls:
    """Runtime variables for OpCls"""

    def __init__(self, fldr="./lib/op_img/"):
        self.tk = self.TkObjectsCls(fldr)

    # /__init__()

    class TkObjectsCls:
        """This class keeps tkinter objects, which are used in OpCls"""

        def __init__(self, fldr):
            self.cbe_clrs = TkObjectCls()
            self.wll_clrs = TkObjectCls()
            self.trans = TkObjectCls()
            self.rot = TkObjectCls()

            # Add images
            self.cbe_clrs.img = self.ImgCubeCls(fldr)
            self.wll_clrs.img = self.ImgWallCls(fldr)
            self.trans.img = self.ImgTrnsCls(fldr)
            self.rot.img = self.ImgRotCls(fldr)

            # And runtime variables
            self.cbe_clrs.o.add_clr = tkinter.IntVar()
            self.cbe_clrs.o.add_clr.set(0)

            self.wll_clrs.o.add_clr = tkinter.IntVar()
            self.wll_clrs.o.add_clr.set(0)

            self.trans.o.repeat = tkinter.IntVar()
            self.trans.o.repeat.set(1)

        # /__init__()

        class ImgCubeCls:
            """Images for cube"""

            def __init__(self, fldr):
                self.r = tkinter.PhotoImage(
                    file="{}cube_r.png".format(fldr))
                self.g = tkinter.PhotoImage(
                    file="{}cube_g.png".format(fldr))
                self.b = tkinter.PhotoImage(
                    file="{}cube_b.png".format(fldr))
                self.all = tkinter.PhotoImage(
                    file="{}cube_all.png".format(fldr))
                self.none = tkinter.PhotoImage(
                    file="{}cube_none.png".format(fldr))

        # /ImgCubeCls

        class ImgWallCls:
            """Images for wall"""

            def __init__(self, fldr):
                self.r = tkinter.PhotoImage(
                    file="{}wall_r.png".format(fldr))
                self.g = tkinter.PhotoImage(
                    file="{}wall_g.png".format(fldr))
                self.b = tkinter.PhotoImage(
                    file="{}wall_b.png".format(fldr))
                self.all = tkinter.PhotoImage(
                    file="{}wall_all.png".format(fldr))
                self.none = tkinter.PhotoImage(
                    file="{}wall_none.png".format(fldr))

        # /ImgWallCls

        class ImgTrnsCls:
            """Images for transformations
            """

            def __init__(self, fldr):
                self.bck2frnt = tkinter.PhotoImage(
                    file="{}t_bck2frnt.png".format(fldr))
                self.frnt2bck = tkinter.PhotoImage(
                    file="{}t_frnt2bck.png".format(fldr))

                self.lft2rght = tkinter.PhotoImage(
                    file="{}t_lft2rght.png".format(fldr))
                self.rght2lft = tkinter.PhotoImage(
                    file="{}t_rght2lft.png".format(fldr))

                self.btm2top = tkinter.PhotoImage(
                    file="{}t_btm2top.png".format(fldr))
                self.top2btm = tkinter.PhotoImage(
                    file="{}t_top2btm.png".format(fldr))

        # /ImgTrnsCls

        class ImgRotCls:
            """Images for rotations
            """

            def __init__(self, fldr):
                self.top2frnt = tkinter.PhotoImage(
                    file="{}r_top2frnt.png".format(fldr))
                self.top2bck = tkinter.PhotoImage(
                    file="{}r_top2bck.png".format(fldr))
                self.frnt2lft = tkinter.PhotoImage(
                    file="{}r_frnt2lft.png".format(fldr))
                self.frnt2rght = tkinter.PhotoImage(
                    file="{}r_frnt2rght.png".format(fldr))
            # /__init__()
    # /TkObjectsCls
# /_OpRtCls

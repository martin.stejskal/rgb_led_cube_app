#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
===========================================
Module for loading/saving animation/images
===========================================

``author`` Martin Stejskal

``version`` 0.6.1
"""
# Basic GUI for Python
import tkinter
from tkinter import filedialog
from tkinter import messagebox

from lib.common import app_path


class LoadSaveCls:
    """Module for operations with cube"""

    def __init__(self, basic_params_cls,
                 dim_changed_f,
                 play_gui=None):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        # Make it easy to change
        self.pwd = "{}/lib/gui/tkinter".format(app_path)

        self.dim_changed_f = dim_changed_f
        self.play_gui = play_gui

        self._init_finish()

    # /__init__()

    def save(self):
        filename = filedialog.asksaveasfilename(
            title="Save animation to file",
            filetypes=(("Animation files", "*.3da.gz"),))
        self.logger.debug("Save as {}".format(filename))

        # Check if user pressed "Cancel"
        if (len(filename) == 0):
            return

        try:
            self.console_api.load_save.save_animation(filename)
        except Exception as e:
            self.logger.error('Saving animation to "{}" failed! Reason: {}'
                              ''.format(filename, e))
            messagebox.showerror(message="Saving animation file failed!")
        # /if error

    # /save()

    def load(self):
        filename = filedialog.askopenfilename(
            title="Load animation from file",
            filetypes=(("Animation files", "*.3da.gz"),))
        self.logger.debug("Open {}".format(filename))

        # Check if user pressed "Cancel"
        if (len(filename) == 0):
            return

        try:
            self.console_api.load_save.load_animation(filename)
        except UserWarning as e:
            msg = "Dimension had to be changed due to different dimension at" \
                  f" animation file.\n{e}"
            self.logger.warn(msg)
            # Callback function that handle dimension change at higher layer
            self.dim_changed_f()

            messagebox.showwarning(title="Cube dimension changed",
                                   message=msg)
        except Exception as e:
            self.logger.error('Loading animation from "{}" failed! Reason: {}'
                              ''.format(filename, e))
            messagebox.showerror(message="Loading animation file failed!")
            return
        # /if error

        # Timer value might changed -> need to get updated values
        if (self.play_gui is not None):
            self.play_gui.update(force_update=True)

    # /load()

    def append(self):
        filename = filedialog.askopenfilename(
            title="Add animation from file",
            filetypes=(("Animation files", "*.3da.gz"),))
        self.logger.debug("Open {}".format(filename))

        # Check if user pressed "Cancel"
        if (len(filename) == 0):
            return

        status = self.console_api.load_save.append_animation(filename)

        if (status != "OK"):
            self.logger.error('Adding animation from "{}" failed! Reason: {}'
                              ''.format(filename, status))
            messagebox.showerror(message="Adding animation file failed!")
            return
        # /if error

        # Timer value might changed -> need to get updated values
        if (self.play_gui is not None):
            self.play_gui.update(force_update=True)

    # /append()

    def new(self):
        self.console_api.load_save.new()

        # Get actual mode - if "anim" -> update anim module
        if (self.console_api.get_mode() == "anim"):
            assert self.play_gui is not None, (
                'At animation mode need access to "play" GUI module to update'
                ' GUI if necessary')
            self.play_gui.update(force_update=True)
        # /if anim mode

    # /new()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        # Runtime variables
        self.rt = _LoadSaveRtCls("{}/lib/load_save_img/".format(self.pwd))

        self.b_save = tkinter.Button(self.parent_frame,
                                     image=self.rt.img.save,
                                     command=self.save)
        self.b_load = tkinter.Button(self.parent_frame,
                                     image=self.rt.img.load,
                                     command=self.load)
        self.b_append = tkinter.Button(self.parent_frame,
                                       image=self.rt.img.append,
                                       command=self.append)
        self.b_new = tkinter.Button(self.parent_frame,
                                    image=self.rt.img.new,
                                    command=self.new)

        # Define button positions
        self.b_new.grid(column=0, row=0)
        self.b_load.grid(column=1, row=0)
        self.b_append.grid(column=2, row=0)
        self.b_save.grid(column=3, row=0)

        # Collect all widgets to one list, so they can be easily
        # disabled/enabled
        self.widgets = [self.b_save, self.b_load, self.b_append, self.b_new]
    # /_init_finish()


# /LoadSaveCls


# ============================| Internal classes |============================
class _LoadSaveRtCls:

    def __init__(self, fldr="./lib/load_save_img/"):
        self.img = self.ImagesCls(fldr)

    # /__init__()

    class ImagesCls:

        def __init__(self, fldr):
            self.save = tkinter.PhotoImage(
                file="{}media-floppy.png".format(fldr))
            self.load = tkinter.PhotoImage(
                file="{}document-open.png".format(fldr))
            self.append = tkinter.PhotoImage(
                file="{}document-append.png".format(fldr))
            self.new = tkinter.PhotoImage(
                file="{}document-new.png".format(fldr))
# /_LoadSaveRtCls

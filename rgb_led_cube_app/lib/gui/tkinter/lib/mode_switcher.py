#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=============================================
Module for switching modes
=============================================

Handle user requests for switching mode

``author`` Martin Stejskal

``version`` 0.3.1
"""

# Basic GUI for Python
import tkinter


class ModeSwCls:
    """Module for switching modes
    """

    def __init__(self, basic_params_cls, callback_f):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls

        :param callback_f: Function which will be called if mode will be
                           changed
        :type callback_f: function
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self.callback_f = callback_f

        self._init_finish()

    # /__init__()

    def get_mode(self):
        return (self.rt.get_selected_option())

    # /get_mode()
    # |ModeSwCls

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()
    # |ModeSwCls

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

    # /en_widgets()
    # |ModeSwCls

    # ===========================| Internal functions |=======================
    def _set_mode(self):
        """This function is called when user change mode

        Callback predefined function if there is mode change
        """
        new_mode = self.rt.get_selected_option()

        # Check if there is mode change
        if (self.rt.previous_selected_option == new_mode):
            msg = "Previous option ({}) and current selection are same -> " \
                  "nothing to do".format(self.rt.previous_selected_option)
            self.logger.debug(msg)
        else:
            # else callback function & update previous option
            self.logger.debug("Switching mode to {}".format(new_mode))

            self.rt.previous_selected_option = new_mode
            self.callback_f(new_mode)
        # /else callback function

    # /_set_mode()

    def _init_finish(self):
        """Finish initialize procedure
        """
        # Initialize runtime variables
        self.rt = _ModeSwRtCls()

        # Auto-counting y position (horizontal)
        y_pos = 0

        # =================================| Label |==========================
        self.lbl = tkinter.Label(self.parent_frame, text="Mode")
        self.lbl.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        # ==============================| Radio button |======================
        self.rb_draw = tkinter.Radiobutton(self.parent_frame,
                                           text="Draw",
                                           variable=self.rt.selected_option,
                                           value="draw",
                                           command=self._set_mode)
        self.rb_draw.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        self.rb_anim = tkinter.Radiobutton(self.parent_frame,
                                           text="Anim",
                                           variable=self.rt.selected_option,
                                           value="anim",
                                           command=self._set_mode)
        self.rb_anim.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        self.rb_rndm = tkinter.Radiobutton(self.parent_frame,
                                           text="Random",
                                           variable=self.rt.selected_option,
                                           value="random",
                                           command=self._set_mode)
        self.rb_rndm.grid(column=0, row=y_pos)
        y_pos = y_pos + 1

        # According to settings, select default mode
        # Set initial value for previous option
        self.rt.previous_selected_option = self.console_api.get_mode()

        # Select correct radio button
        if (self.rt.previous_selected_option == "draw"):
            self.rb_draw.select()
        elif (self.rt.previous_selected_option == "anim"):
            self.rb_anim.select()
        elif (self.rt.previous_selected_option == "random"):
            self.rb_rndm.select()
        else:
            msg = 'Unknown default mode "{}"' \
                  ''.format(self.rt.previous_selected_option)
            self.logger.critical(msg)
            raise Exception(msg)
        # else unsupported mode

        # Define list of GUI widgets -> easier to enable/disable them
        self.widgets = [self.lbl, self.rb_draw, self.rb_anim, self.rb_rndm]
    # /_init_finish()


# /ModeSwCls


class _ModeSwRtCls:
    """Runtime variables for ModeSwCls"""

    def __init__(self):
        self.selected_option = tkinter.StringVar()
        # Keep track about previous option
        self.previous_selected_option = None

    # /__init__()

    def get_selected_option(self):
        return (self.selected_option.get())
    # /get_selected_option()
# /_ModeSwRtCls

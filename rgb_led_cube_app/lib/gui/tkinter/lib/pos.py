#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
Module for changing position at LED cube 3D view
=================================================

Handle cube rotations and zoom

``author`` Martin Stejskal

``version`` 0.4.1
"""

# Basic GUI for Python
import tkinter

from lib.common import app_path


class PosCls:
    """Module for changing position at LED cube 3D view"""

    def __init__(self, basic_params_cls):
        """
        :param basic_params_cls: Basic parameters wrapped in one class
        :type basic_params_cls: _BasicParamsCls
        """
        # Save variables to instance
        self.logger = basic_params_cls.logger
        self.console_api = basic_params_cls.console_api
        self.parent_frame = basic_params_cls.parent_frame

        self._init_finish()

    # /__init__()

    def rot_left(self):
        """Rotate cube left"""
        self.console_api.pos.rot_left(self.rt.rot.step)

    # /rot_left()

    def rot_right(self):
        """Rotate cube right"""
        self.console_api.pos.rot_right(self.rt.rot.step)

    # /rot_right()

    def rot_up(self):
        """Rotate cube up"""
        self.console_api.pos.rot_up(self.rt.rot.step)

    # /rot_up()

    def rot_down(self):
        """Rotate cube down"""
        self.console_api.pos.rot_down(self.rt.rot.step)

    # /rot_down()

    def reset_view(self):
        """Reset 3D cube view"""
        self.console_api.pos.reset_cube_view()

        # Do not forget to update zoom value
        self.rt.zoom.actual.set(self.console_api.pos.get_zoom())

    # /reset_view()

    def zoom_inc(self):
        """Increase zoom

        It is up to this module "how much" will zoom be incremented.
        """
        self.rt.zoom.inc_zoom()

        # Set zoom also at console API
        self._set_zoom(new_zoom=self.rt.zoom.actual.get())

    # /zoom_inc()

    def zoom_dec(self):
        """Decrease zoom

        It is up to this module "how much" will zoom be decremented.
        """
        self.rt.zoom.dec_zoom()

        # Set zoom also at console API
        self._set_zoom(new_zoom=self.rt.zoom.actual.get())

    # /zoom_dec()

    def dis_widgets(self):
        """Disable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.DISABLED)

    # /dis_widgets()

    def en_widgets(self):
        """Enable all widgets at this module
        """
        for widget in self.widgets:
            widget.config(state=tkinter.NORMAL)

    # /en_widgets()

    # ===========================| Internal functions |=======================
    def _init_finish(self):
        """Generate GUI"""
        # Initialize runtime variables
        # Make it easy to change
        self.pwd = "{}/lib/gui/tkinter".format(app_path)

        self.rt = _PosRtCls(fldr="{}/lib/pos_img/".format(self.pwd))

        self._init_zoom()

        # ==================================| Zoom |==========================
        self.zoom_scale = tkinter.Scale(self.parent_frame,
                                        command=self._set_zoom,
                                        variable=self.rt.zoom.actual,
                                        from_=self.rt.zoom.min,
                                        to=self.rt.zoom.max,
                                        length=130,
                                        orient=tkinter.HORIZONTAL,
                                        resolution=self.rt.zoom.step,
                                        showvalue=1)
        self.zoom_scale.grid(column=0, row=0, columnspan=3)

        # =============================| Arrow buttons |======================
        self.btn_up = tkinter.Button(self.parent_frame,
                                     image=self.rt.img.up,
                                     command=self.rot_up)
        self.btn_up.grid(column=1, row=1)

        self.btn_down = tkinter.Button(self.parent_frame,
                                       image=self.rt.img.down,
                                       command=self.rot_down)
        self.btn_down.grid(column=1, row=2)

        self.btn_left = tkinter.Button(self.parent_frame,
                                       image=self.rt.img.left,
                                       command=self.rot_left)
        self.btn_left.grid(column=0, row=2)

        self.btn_right = tkinter.Button(self.parent_frame,
                                        image=self.rt.img.right,
                                        command=self.rot_right)
        self.btn_right.grid(column=2, row=2)

        self.btn_reset = tkinter.Button(self.parent_frame,
                                        image=self.rt.img.reset,
                                        command=self.reset_view)
        self.btn_reset.grid(column=2, row=1)

        # Create list of widgets
        self.widgets = [self.zoom_scale, self.btn_up, self.btn_down,
                        self.btn_left, self.btn_right, self.btn_reset]

    # /_init_finish()

    def _init_zoom(self):
        """Initialize and check zoom value"""
        # Get actual zoom
        self.rt.zoom.actual.set(self.console_api.pos.get_zoom())

        # Check boundaries
        if (self.rt.zoom.actual.get() > self.rt.zoom.max):
            self.rt.zoom.actual.set(self.rt.zoom.max)
            msg = "Actual zoom is higher than maximum allowed!\nChanging " \
                  "zoom value to {}".format(self.rt.zoom.actual.get())
            self.logger.warn(msg)

            self.console_api.pos.set_zoom(self.rt.zoom.actual.get())
        # /if zoom value is too high

        if (self.rt.zoom.actual.get() < self.rt.zoom.min):
            self.rt.zoom.actual.set(self.rt.zoom.min)
            msg = "Actual zoom is lower than minimum allowed!\nChanging " \
                  "zoom value to {}".format(self.rt.zoom.actual.get())
            self.logger.warn(msg)

            self.console_api.pos.set_zoom(self.rt.zoom.actual.get())
        # /if zoom value is too low

    # /_init_zoom()

    def _set_zoom(self, new_zoom):
        """This function is called, when zoom value is changed by user"""
        # new_zoom = self.rt.zoom.actual.get()

        self.console_api.pos.set_zoom(new_zoom)
    # /_set_zoom()


# /PosCls


class _PosRtCls:
    """Runtime variables for PosCls"""

    def __init__(self, fldr="./lib/pos_img/"):
        self.zoom = self.ZoomCls()
        self.rot = self.RotationCls()
        self.img = self.ImgCls(fldr)

    # /__init__()

    class ZoomCls:

        def __init__(self):
            # For actual value we need to use special variable, since it is
            # used by tkinter
            self.actual = tkinter.DoubleVar()
            self.actual.set(1)

            self.min = 0.5
            self.max = 2
            self.step = 0.1

        # /__init__()

        def inc_zoom(self):
            """Increase zoom value"""
            new_val = self.actual.get() + self.step

            # Check new value
            if (new_val > self.max):
                new_val = self.max

            self.actual.set(new_val)

        # /inc_zoom()

        def dec_zoom(self):
            """Decrease zoom value"""
            new_val = self.actual.get() - self.step

            # Check new value
            if (new_val < self.min):
                new_val = self.min

            self.actual.set(new_val)
        # /dec_zoom()

    # /ZoomCls

    class RotationCls:

        def __init__(self):
            self.step = 5
        # /__init__()

    # /RotationCls

    class ImgCls:
        """Images for buttons"""

        def __init__(self, fldr):
            self.left = tkinter.PhotoImage(
                file="{}left.png".format(fldr))
            self.right = tkinter.PhotoImage(
                file="{}right.png".format(fldr))
            self.up = tkinter.PhotoImage(
                file="{}up.png".format(fldr))
            self.down = tkinter.PhotoImage(
                file="{}down.png".format(fldr))
            self.reset = tkinter.PhotoImage(
                file="{}reset.png".format(fldr))
        # /__init__()
    # /ImgCls
# /_PosRtCls

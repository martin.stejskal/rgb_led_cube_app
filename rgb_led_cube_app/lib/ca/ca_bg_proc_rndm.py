#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================================
Module that deal with playing "random" animations
===================================================

This is part of "ca_bg_proc" module. But due to code size it was put here

``author`` Martin Stejskal

``version`` 0.4.3
"""
from lib.cube_LED import CubeLEDOpCls

# =========================| All random animations |======================
# Every random animation have to have following:
#  * name (static) : Any human readable, but short string
#  * dim_min (static) : 0 does not make any sense. Integer should be used
#  * dim_max (static) : 0 means no limit. Else integer value should be used
#  * get_img() (can change of course :) : Function that returns new image
#    as "CubeLEDCls"
#
from lib.ca.rndm_anim.brthng_cbe_1 import BrthngCbe1Cls
from lib.ca.rndm_anim.brthng_cbe_2 import BrthngCbe2Cls
from lib.ca.rndm_anim.brthng_cbe_3 import BrthngCbe3Cls
from lib.ca.rndm_anim.lighting import LightningCls
from lib.ca.rndm_anim.electron import ElectronCls
from lib.ca.rndm_anim.matrix import MatrixCls
from lib.ca.rndm_anim.snow import SnowCls
from lib.ca.rndm_anim.build_cube_1 import BuildCube1Cls
from lib.ca.rndm_anim.build_cube_2 import BuildCube2Cls


class ConsoleAPIBgRndmCls:

    def __init__(self, dimension, logger):
        # Actual cube dimension
        self.dim = dimension

        # Thanks to this bad boy logging is easy
        self.logger = logger

        # Operations with cube
        self.op = CubeLEDOpCls()

        # Keeps all animations here
        self._all_anims = []

        # But not all of them might be available
        self._anims = []

        self.logger.debug("Preparing random animations...")
        # ==========================| Fill _all_anims list |==================
        self._all_anims.append(BrthngCbe1Cls(self.dim, self.op))
        self._all_anims.append(BrthngCbe2Cls(self.dim, self.op))
        self._all_anims.append(BrthngCbe3Cls(self.dim, self.op))
        self._all_anims.append(LightningCls(self.dim, self.op))
        self._all_anims.append(ElectronCls(self.dim, self.op))
        self._all_anims.append(MatrixCls(self.dim, self.op))
        self._all_anims.append(SnowCls(self.dim, self.op))
        self._all_anims.append(BuildCube1Cls(self.dim, self.op))
        self._all_anims.append(BuildCube2Cls(self.dim, self.op))

        # ============================| Fill _anims list |====================
        self.logger.debug("Creating available animation list...")

        for anim in self._all_anims:
            # Check minimal dimension
            if (self.dim < anim.dim_min):
                msg = "Animation {} require cube dimension at least {}\n" \
                      "".format(anim.name, anim.dim_min)
                self.logger.debug(msg)
                # Continue with another animation
                continue
            # /check minimal dimension

            if ((self.dim > anim.dim_max) and (anim.dim_max != 0)):
                msg = "Animation {} require smaller dimension. At maximum {}" \
                      "".format(anim.name, anim.dim_max)
                self.logger.debug(msg)
                # Continue with another animation
                continue
            # /check maximal dimension

            self._anims.append(anim)
        # /for all animations

        # ===================| Create list of animations (string) |===========
        self._anims_names = []
        for anim in self._anims:
            self._anims_names.append(anim.name)
        # /for all animations

        self._actual_anim = None
        self.logger.debug("Initialization done")

    # /__init__()

    def get_animations(self):
        """Get list of all available animations

        :returns: List of available animations as animation names
        :rtype: list (str)
        """
        return (self._anims_names)

    # /get_animations()

    def set_animation(self, animation_name):
        """Set actual animation name

        :param animation_name: One of the animation name from get_animations()
        :type animation_name: str
        """
        # Check if actual animation can be found at available list of
        # animations
        found = False
        for anim in self._anims:
            if (anim.name == animation_name):
                found = True
                # Get pointer to function get_img()
                anim_get_img = anim.get_img
                break
            # /if animation found
        # /for all available animations

        if (not found):
            msg = 'Animation "{}" was not found in list of available' \
                  ' animations'.format(animation_name)
            self.logger.error(msg)
            raise Exception(msg)
        # /if not found
        else:
            # else found -> write
            self._actual_anim = animation_name
            self._actual_get_img = anim_get_img

    # /set_animation()

    def get_animation(self):
        """Get actual animation name

        :returns: Actual animation name. If no animation is set, None is
                  returned
        :rtype: str, None
        """
        return (self._actual_anim)

    # /get_animation()

    def get_img(self):
        """Get next image for actual animation

        If no animation is set, exception will be raised.

        :returns: New image. If no animation was selected,, exception will be
                  raised.
        :rtype: CubeLEDCls
        """
        if (self._actual_anim is None):
            msg = "No mode is not selected!\nCan not get any new image"
            self.logger.error(msg)
            raise Exception(msg)

        # OK, so actual image should be somewhere...
        return (self._actual_get_img())
    # /get_img()
# /ConsoleAPIBgRndmCls

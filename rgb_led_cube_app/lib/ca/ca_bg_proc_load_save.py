#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===========================================
Module that loads and save animation files
===========================================

This is part of "ca_bg_proc" module. But due to complexity code for loading
and saving animations is here.

``author`` Martin Stejskal

``version`` 0.5.1
"""
import gzip

from lib.cube_LED import CubeLEDCls


class ConsoleAPIBgLoadSaveCls:
    version = 0.1

    def __init__(self, logger):
        self.logger = logger

    # /__init__()

    def save_animation(self, filename="animation.3da", data=[],
                       timer=50):
        """Save animation to file

        :param filename: File name - obviously
        :type filename: str

        :param data: Array of CubeLEDCls objects
        :type data: list (CubeLEDCls)

        :param timer: Timer value in ms
        :type timer: int
        """
        if (len(data) > 200):
            # This may take a while -> show message
            self.logger.info("Saving animation to {} ...".format(filename))
        # /show message when animation is long

        # Write compressed data directly
        self.f_w = gzip.open(filename, "wb")

        self._write_header(data, timer)
        self._write_data(data)

        self.f_w.close()

        self.logger.info("Animation saved to {}".format(filename))

    # /save_animation()

    def load_animation(self, filename="animation.3da"):
        """Load animation from file

        :param filename: File name - obviously
        :type filename: str

        :returns: Array of CubeLEDCls objects, timer value
        :rtype: list (CubeLEDCls), int
        """
        self.logger.info("Opening animation {} ...".format(filename))
        self.f_r = gzip.open(filename, "rb")

        header = self._read_header()
        data = self._read_data(header)

        self.f_r.close()

        self.logger.info("Animation {} loaded".format(filename))

        return (data, header.timer)

    # /load_animation()

    # ===========================| Internal functions |=======================
    def _read_data(self, header):
        """Read RGB data from file"""
        data = []

        # We will show status every 5% of loading
        show_stat_period = header.frames / 20

        # counter for showing status
        show_stat_cnt = 0

        for img_idx in range(header.frames):
            # Sometimes show statistics
            show_stat_cnt = show_stat_cnt + 1

            if (show_stat_cnt == 1):
                self.logger.debug("Loading frame {}/{}".format(img_idx,
                                                               header.frames))
            elif (show_stat_cnt >= show_stat_period):
                show_stat_cnt = 0

            data.append(CubeLEDCls(header.dimension))

            # Get PWM value
            pwm_r, pwm_g, pwm_b = self._read_pwm()
            data[img_idx].pwm.r = pwm_r
            data[img_idx].pwm.g = pwm_g
            data[img_idx].pwm.b = pwm_b

            for wall in data[img_idx].w:
                for column in wall.c:
                    for led in column.led:
                        r, g, b = self._read_led()
                        led.r = r
                        led.g = g
                        led.b = b
                    # /for led
                # /for column
            # /for wall
        # /for all frames
        return (data)

    # /_read_data()

    def _read_led(self):
        """Read R/G/B value of LED

        :returns: R/G/B values
        :rtype: list (int 0~1)
        """
        line = self._read_line()

        # Check length
        if (len(line) != 3):
            msg = 'Expected 3 variables, but found {}'.format(len(line))
            self.logger.error(msg)
            raise Exception(msg)
        # /length check

        r = int(line[0])
        g = int(line[1])
        b = int(line[2])

        for color in [r, g, b]:
            if (color < 0):
                msg = "Color can not be negative. Only 0 and 1 are expected"
                self.logger.error(msg)
                raise Exception(msg)
            if (color > 1):
                msg = "Expected 0 and 1 values, but found {}".format(color)
                self.logger.error(msg)
                raise Exception(msg)
        # /check colors

        return (r, g, b)

    # /_read_led()

    def _read_pwm(self):
        """Read PWM values from line

        :returns: R/G/B values
        :rtype: list (int 0~255)
        """
        line = self._read_line()

        # Check length
        if (len(line) != 4):
            msg = "Expected 4 values on line, but found {}".format(len(line))
            self.logger.error(msg)
            raise Exception(msg)
        # /length check

        if (line[0] != "pwm"):
            msg = 'Expected "pwm" variable, but got "{}"'.format(line[0])
            self.logger.error(msg)
            raise Exception(msg)
        # /check PWM string

        # OK, now need to get integer for R/G/B
        r = int(line[1])
        g = int(line[2])
        b = int(line[3])

        # Check values
        for value in [r, g, b]:
            if ((value < 0) or (value > 255)):
                msg = 'Expected PWM value from 0~255, but got {}' \
                      ''.format(value)
                self.logger.error(msg)
                raise Exception(msg)
            # /if value out of range
        # /for R/G/B

        return (r, g, b)

    # /_read_pwm()

    def _read_header(self):
        """Read header from file"""
        hdr = self._HeaderCls()

        hdr.version = self._read_version()
        hdr.dimension = self._read_dimension()
        hdr.frames = self._read_frames()
        hdr.timer = self._read_timer()

        return (hdr)

    # /_read_header()

    def _read_timer(self):
        """Read timer value"""
        variable, value = self._read_line_get_str_int()

        # Check variable name
        if (variable != "timer"):
            msg = 'Expected "timer", but got "{}"'.format(variable)
            self.logger.error(msg)
            raise Exception(msg)

        assert value > 10, "Timer value should be higher than 10 ms"
        return (value)

    # /_read_frames()

    def _read_frames(self):
        """Read number of frames"""
        variable, value = self._read_line_get_str_int()

        # Check variable name
        if (variable != "frames"):
            msg = 'Expected "frames", but got "{}"'.format(variable)
            self.logger.error(msg)
            raise Exception(msg)

        assert value > 0, "At least one image is expected."
        return (value)

    # /_read_frames()

    def _read_dimension(self):
        """Read dimension value"""
        variable, value = self._read_line_get_str_int()

        # Check variable name
        if (variable != "dimension"):
            msg = 'Expected "dimension", but got "{}"'.format(variable)
            self.logger.error(msg)
            raise Exception(msg)

        assert value > 0, "Dimension can not be zero or negative"
        return (value)

    # /_read_dimension()

    def _read_line_get_str_int(self):
        """Read one line and return string and int values

        Since some parameters are "variable value", this function will get
        that data from configuration file and simply prepare everything
        """
        line = self._read_line()

        # Check length
        if (len(line) != 2):
            msg = "Unexpected line length. Expected 2, but got {}" \
                  "".format(len(line))
            self.logger.error(msg)
            raise Exception(msg)

        # String is string. Easy. But we need to get integer from string
        variable = line[0]
        value = int(line[1])

        return (variable, value)

    # /_read_line_get_str_int()

    def _read_version(self):
        # Check version
        line = self._read_line()

        if (line[0] != "version"):
            msg = 'Expected "version", but got "{}"'.format(line[0])
            self.logger.error(msg)
            raise Exception(msg)
        # /string check

        if (len(line) != 2):
            msg = 'Unexpected data on line. Expected version number, but got' \
                  ' "{}"'.format(line)
            self.logger.error(msg)
            raise Exception(msg)
        # /if length is not as expected

        version = line[1]
        self.logger.debug("Detected version: {}".format(version))
        version = float(version)

        if (version != self.version):
            msg = 'Version miss match. Expected version {}, but got {}.' \
                  ' This may cause problems later' \
                  ''.format(self.version, version)
            self.logger.warn(msg)
        # /version check

        return (version)

    # /_read_version()

    def _read_line(self):
        """Read one line from file

        :returns: Line as list of string (split by spaces)
        :rtype: list (str)
        """
        # Read raw data
        line = self.f_r.readline()
        # convert to UTF8
        line = line.decode("utf-8")

        # And split by spaces
        line = line.split(" ")

        return (line)

    # /_read_line()

    def _write_header(self, data, timer):
        """Write header to file"""

        # Version of animation file. Since there is chance that in future
        # syntax of animation file will be different
        self._write_str("version {}\n".format(self.version))

        # Get cube dimension from first 3D image
        dim = data[0].get_dimension()
        self._write_str("dimension {}\n".format(dim))

        # Number of frames
        self._write_str("frames {}\n".format(len(data)))

        # Timer value
        self._write_str("timer {}\n".format(timer))

    # /_write_header()

    def _write_data(self, data):
        """Write whole animation to file"""
        # Counter - just for statistics
        cnt = 0

        for cube in data:
            # Write PWM value
            self._write_str("pwm {} {} {}\n".format(cube.pwm.r,
                                                    cube.pwm.g,
                                                    cube.pwm.b))
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        # convert LED values to 0 and 1
                        # (shorter than True/False)
                        if (led.r):
                            r = 1
                        else:
                            r = 0

                        if (led.g):
                            g = 1
                        else:
                            g = 0

                        if (led.b):
                            b = 1
                        else:
                            b = 0

                        self._write_str("{} {} {}\n".format(
                            r, g, b))
                        cnt = cnt + 1
                    # /for LED
                # /for column
            # /for wall
        # /for cubes

        self.logger.debug("Processed {} LEDs".format(cnt))

    # /_write_data()

    def _write_str(self, str):
        """Write string to file"""
        self.f_w.write(bytearray(str, 'utf-8'))

    # /_write_str()

    class _HeaderCls:

        def __init__(self):
            self.version = 0.0
            self.dimension = 0
            self.frames = 0
            self.timer = 0

        # /__init__()

        def __str__(self):
            msg = "version: {}\n" \
                  "dimension: {}\n" \
                  "frames: {}\n" \
                  "timer: {}\n" \
                  "".format(self.version,
                            self.dimension,
                            self.frames,
                            self.timer)
            return (msg)
        # /__str__()
    # /_HeaderCls
# /ConsoleAPIBgLoadSaveCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=================================================
Animation effects
=================================================

In some cases user can use predefined effects.

``author`` Martin Stejskal

``version`` 0.0.2
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIEffectsCls:

    def __init__(self, ipc, logger, get_mode_fnc,
                 ca_ping_f):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # get mode callback function
        self.get_mode = get_mode_fnc

        # Ping function
        self.ca_ping_f = ca_ping_f

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def fade_out(self, duration=10, effect_type="linear",
                 pwm=[True, True, True]):
        """Fade image out by decreasing PWM values

        :param duration: Effect duration in frames.
        :type duration: int

        :param effect_type: Type of fade out effect.
        :type effect_type: str (linear, exponential)

        :param pwm: Define with which PWM channels should effect work (R,G,B).
        :type pwm: array (bool, bool, bool)
        """
        self._check_mode()
        # ============================| Parameter check |=====================
        if (duration < 2):
            msg = "Duration can not be lower than 2"
            raise ValueError(msg)
        if ((effect_type != "linear") and (effect_type != "exponential")):
            msg = 'Unexpected effect type. Expected values:' \
                  ' linear, exponential'
            raise ValueError(msg)
        if ((not pwm[0]) and (not pwm[1]) and (not pwm[2])):
            msg = 'At least one PWM channel have to be enabled'
            raise Exception(msg)

        self.ipc.send_req(
            cmd="effect_fade_out",
            val="{} {} {} {} {}".format(
                duration, effect_type, pwm[0], pwm[1], pwm[2]))

    # /fade_out()

    # ===========================| Internal functions |=======================
    def _check_mode(self):
        if (self.get_mode() != "anim"):
            raise Exception('Effects can be done only for "anim" mode')
    # /_check_mode()
# /ConAPIEffectsCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==========================================
Common functions/classes for console API
==========================================


``author`` Martin Stejskal

``version`` 0.4.2
"""


class ConAPICommonCls:
    """Common functions mainly for console API modules
    """

    def __init__(self, logger=None):
        """Common functions mainly for console API modules

        :param logger: Optional parameter. If desired, logger can be used in
                       advance to show some messages.
        :type logger: Obj (CmnLgrCls/None)
        """
        self.logger = logger

    def check_variable_name(self, received_var, expected_var,
                            function_name=""):
        """Check variable name

        If does not match, exception is threw
        """
        if (received_var != expected_var):
            # If function name is defined, add it
            if (function_name != ""):
                func_name = "{}: ".format(function_name)
            else:
                func_name = ""
            # /if function name is defined

            msg = "Internal error! {}Got unexpected variable: {}\n" \
                  "Expected variable: {}".format(func_name,
                                                 received_var,
                                                 expected_var)
            if (not (self.logger is None)):
                self.logger.error(msg)
            raise Exception(msg)
        # /if variables are not correct

    # /check_variable_name()

    def str2bool(self, var):
        if ((var == "False") or (var == "0")):
            return (False)
        else:
            return (True)

    # /str2bool()

    def restore_msg(self, msg):
        """Restore message parsed by IPC
        """
        res = ""
        for item in msg:
            res = "{}{} ".format(res, item)

        # Remove last space (redundant)
        res = res[:-1]

        return (res)

    # /restore_msg()

    def rplc_spc_wth_undrscr_in_str_lst(self, string_list):
        """Replace space symbol with underscore at list of strings

        :param string_list: List of strings
        :type string_list: list (str)

        :returns: Modified string list
        :rtype: list (str)
        """
        res = []
        for text in string_list:
            res.append(text.replace(" ", "_"))

        return (res)

    # /rplc_spc_wth_undrscr_in_str_lst()

    def rplc_undrscr_wth_spc_in_str_lst(self, string_list):
        """Replace underscore symbol with space at list of strings

        :param string_list: List of strings
        :type string_list: list (str)

        :returns: Modified string list
        :rtype: list (str)
        """
        res = []
        for text in string_list:
            res.append(text.replace("_", " "))

        return (res)
    # /rplc_undrscr_wth_spc_in_str_lst()
# /ConAPICommonCls

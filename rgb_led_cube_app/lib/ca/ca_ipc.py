#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
====================
IPC for console API
====================

This module connects GUI with low-level cube control system. Target is to
distinguish GUI and "console" interface.

``author`` Martin Stejskal

``version`` 0.5.7
"""
# Network stack for interprocess communication
import zmq
from time import sleep


class ConAPIIPCSettingsCls:
    """Structure for interprocess communication settings
    """

    def __init__(self):
        # IP should be on local machine
        self.ip = "127.0.10.95"
        # Port for sending requests to background process
        self.port_send_req = 5510
        # Port for sending requests from background process here (to GUI
        # eventually)
        self.port_get_req = self.port_send_req + 1

        # Sometimes we just need to ask background process for something
        # and we need response which is tight to question. Problem of
        # PUSH/PULL is that messages are in queue, so in meantime we could
        # receive incorrect response
        self.port_req_rply = self.port_get_req + 1

        # On this port will background process sending requests to timer
        # (timer need to connect to this port)
        self.port_timer_get_req = self.port_req_rply + 1

        # Port on which background process will listen for requests
        # (timer will send to this port)
        self.port_timer_send_req = self.port_timer_get_req + 1

        # On this port will be used for sending asynchronous notifications
        # from console API (background) to console API (foreground), where
        # GUI callback function can be called
        self.port_notifications = self.port_timer_send_req + 1
        # Same as above, but this time notification will be sent from
        # foreground API to foreground. Something like loop. This is useful
        # for sending notifications when foreground functions need to handle
        # notifications (no need to interact with background).
        self.port_notifications_mstr = self.port_notifications + 1

        # On this port will came notifications from 3D view. Quite useful is
        # notification "on_close", so we do not need to poll 3D view
        self.port_3D_view_notif = self.port_notifications_mstr + 1

        # Communication timeout
        # It might happen that some module crash during time that another
        # process is trying to communicate with crashed process. In that case
        # there is this fail-safe solution. After some time communication
        # should be interrupted and also other tasks should raise exception.
        # Value is in ms.
        self.timeout_ms = 3000

    # /__init__()

    def __str__(self):
        msg = "= Console API IPC ({}) ==\n" \
              " IP: {}\n" \
              " Port master -> background: {}\n" \
              " Port background -> master: {}\n" \
              " Port REQ/REP master <---> background: {}\n" \
              " Port background -> timer: {}\n" \
              " Port timer -> background: {}\n" \
              " Port master -> GUI (notifications): {}\n" \
              " Port 3D view -> background (notifications): {}\n" \
              "".format(self.__class__.__name__,
                        self.ip, self.port_send_req, self.port_get_req,
                        self.port_req_rply, self.port_timer_get_req,
                        self.port_timer_send_req, self.port_notifications,
                        self.port_3D_view_notif)
        return (msg)
    # /__str__()


# /ConAPIIPCSettingsCls


class ConAPIIPCMasterCls():
    """IPC for master process.

    Probably used by console_api module
    """

    def __init__(self, settings, logger):
        """
        :param settings: Structure with port and IP address settings
        :type settings: ConAPIIPCSettingsCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.settings = settings
        self.logger = logger

        # String used in debug messages
        self.name = 'CA IPC - Master'

        # Enable/disable debug. Since it will produce a lot of output it is
        # hard coded here. Enable if you need
        self.debug = False

        # Server for sending requests to background process
        context = zmq.Context()
        self._req_send = context.socket(zmq.PUSH)
        self._req_send.RCVTIMEO = self.settings.timeout_ms
        self._req_send.bind("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_send_req))

        # Client for receiving requests from background process (to inform
        # GUI if needed)
        context = zmq.Context()
        self._req_get = context.socket(zmq.PULL)
        self._req_get.RCVTIMEO = self.settings.timeout_ms
        self._req_get.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_get_req))

        # Also prepare request/reply client (send request to background
        # process)
        context = zmq.Context()
        self._req_rply = context.socket(zmq.REQ)
        self._req_rply.RCVTIMEO = self.settings.timeout_ms
        self._req_rply.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_req_rply))

        # Notification server (send notifications to GUI/self)
        context = zmq.Context()
        self._notification_send = context.socket(zmq.PUSH)
        self._notification_send.RCVTIMEO = self.settings.timeout_ms
        self._notification_send.bind("tcp://{}:{}".format(
            self.settings.ip, self.settings.port_notifications_mstr))

        # Receive notifications from background & foreground process
        context = zmq.Context()
        self._notification_get = context.socket(zmq.PULL)
        self._notification_get.RCVTIMEO = self.settings.timeout_ms
        self._notification_get.connect("tcp://{}:{}".format(
            self.settings.ip, self.settings.port_notifications))
        self._notification_get.connect("tcp://{}:{}".format(
            self.settings.ip, self.settings.port_notifications_mstr))

        # For sending request/reply scheme - it should be called only once
        # per time -> no parallel access -> need to use locks
        self.lock_send_req_get_rply = False

    # /__init__()
    # |ConAPIIPCMasterCls

    def send_req(self, cmd="command_xyz", val=""):
        """Send request to background process

        :param cmd: Command
        :type cmd: str

        :param val: Command value
        :type val: str
        """
        self._req_send.send_string("{} {}".format(cmd, val))

        if (self.debug):
            # Following can be handy for debug, but it might take too much CPU
            # resources
            print("{} sending: {} {}".format(self.name, cmd, val))

    # /send_req()
    # |ConAPIIPCMasterCls

    def get_req(self):
        """Get request from background process

        :returns: command and value as string. If no command is received, then
                  command value is empty string
        :rtype: str, str
        """
        cmd = ""
        val = ""

        try:
            data = self._req_get.recv_string(flags=zmq.NOBLOCK)
        except zmq.Again:
            # No request received -> return void command and value
            pass
        else:
            cmd, val = str2cmd_val(data, self.debug, self.name)
        # /try to get command and value
        return (cmd, val)

    # /get_req()
    # |ConAPIIPCMasterCls

    def send_req_get_rply(self, cmd="get_dimension", val="", timeout=None):
        """Send request and waiting for response.

        In some cases we need also need feedback channel. Therefore we're
        using it as blocking function

        :param cmd: Command
        :type cmd: str

        :param val: Command value
        :type val: str

        :param timeout: Some commands might take a lot of time such as huge
                        animations. In that case it is worth to modify
                        timeout value temporary. Value is in ms.
        :rtype timeout: int

        :returns: command and value as string. If no command is received, then
                  command value is empty string
        :rtype: str, str
        """
        # Wait for lock
        while (self.lock_send_req_get_rply):
            sleep(0.02)
        # Lock it
        self.lock_send_req_get_rply = True

        # Since background thread is probably waiting for command through
        # PULL, we need to tell background process, that we want to
        # process request/reply
        self.send_req(cmd="process_req_rply_TX")

        if (self.debug):
            msg = "{} sending (REQ/rep): {} | {}".format(self.name, cmd, val)
            print(msg)

        self._req_rply.send_string("{} {}".format(cmd, val))

        if (self.debug):
            msg = "{} receiving req/REP data...".format(self.name)
            print(msg)
        # If timeout is defined -> set it
        if (not (timeout is None)):
            self._req_rply.RCVTIMEO = timeout

        reply = self._req_rply.recv_string()

        # Set default timeout back
        if (not (timeout is None)):
            self._req_rply.RCVTIMEO = self.settings.timeout_ms

        variable, value = str2cmd_val(reply, self.debug, self.name)

        # Unlock it
        self.lock_send_req_get_rply = False
        return (variable, value)

    # /send_req_get_rply()
    # |ConAPIIPCMasterCls

    def get_notification(self):
        """Get notification from background process

        :returns: command and value as string. If no command is received, then
                  command and value is empty string
        :rtype: str, str
        """
        cmd = val = ""

        try:
            data = self._notification_get.recv_string(flags=zmq.NOBLOCK)
        except zmq.Again:
            # No notification received
            pass
        else:
            data = data.split(" ", maxsplit=1)
            cmd = data[0]
            val = data[1]

            # It is expected, that notifications will not be so often, so
            # that is why they are logged for now. It might change in future
            self.logger.debug('Received notification: {}'.format(data))
        # /try to get notification
        return (cmd, val)

    # /get_notification()

    def send_notification(self, cmd="effect_progress", val="5% (50/1000)"):
        """Send notification to foreground

        :param cmd: Command
        :type cmd: str

        :param val: Command value
        :type val: str
        """
        self._notification_send.send_string("{} {}".format(cmd, val))

        if (self.debug):
            print("{} sending notification: {} {}".format(self.name, cmd, val))
    # /send_notification()

    # ===========================| Internal functions |=======================


# /ConAPIIPCMasterCls


class ConAPIIPCBgProcCls():
    """IPC for background process

    Probably used by ca_bg_proc
    """

    def __init__(self, settings, logger):
        """
        :param settings: Structure with port and IP address settings
        :type settings: ConAPIIPCSettingsCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.settings = settings
        self.logger = logger

        # Following is used in debug messages
        self.name = 'CA IPC - Bg'

        # Since this will produce a lot of messages, it is disabled by default
        # Enabling can help you with debug, but performance cost can be
        # high. Think about it and set it wisely
        self.debug = False

        # Setup client (it will handle requests in queue)
        context = zmq.Context()
        self._req_get = context.socket(zmq.PULL)
        # Here can not be used timeout - whole function is based on
        # asynchronous waiting for any event
        # self._req_get.RCVTIMEO = self.settings.timeout_ms
        self._req_get.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_send_req))

        # Setup server (will send requests from here to console API)
        context = zmq.Context()
        self._req_send = context.socket(zmq.PUSH)
        self._req_send.RCVTIMEO = self.settings.timeout_ms
        self._req_send.bind("tcp://{}:{}".format(self.settings.ip,
                                                 self.settings.port_get_req))

        # Setup ZMQ REQ/REP server
        context = zmq.Context()
        self._req_rply = context.socket(zmq.REP)
        self._req_rply.RCVTIMEO = self.settings.timeout_ms
        self._req_rply.bind("tcp://{}:{}".format(self.settings.ip,
                                                 self.settings.port_req_rply))

        # Notification server (send notifications to GUI)
        context = zmq.Context()
        self._notification_send = context.socket(zmq.PUSH)
        self._notification_send.RCVTIMEO = self.settings.timeout_ms
        self._notification_send.bind("tcp://{}:{}".format(
            self.settings.ip, self.settings.port_notifications))

        # Timer stuff
        # Start server, so we can send command(s) to timer's IPC later
        context = zmq.Context()
        self._tmr_req_send = context.socket(zmq.PUSH)
        self._tmr_req_send.RCVTIMEO = self.settings.timeout_ms
        self._tmr_req_send.bind("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_timer_get_req))

        # Connect to timer IPC, so background process can get requests
        # from timer
        self._req_get.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_timer_send_req))

        # Connect cube 3D view to IPC, so we can receive notifications/commands
        # from 3D view if needed
        self._req_get.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_3D_view_notif))

    # /__init__()
    # |ConAPIIPCMasterCls

    def send_req(self, cmd="move_time_slider", val="50"):
        """Send requests to foreground (master)

        :param cmd: Command
        :type cmd: str

        :param val: Command value
        :type val: str
        """
        self._req_send.send_string("{} {}".format(cmd, val))

        if (self.debug):
            print("{} sending: {} {}".format(self.name, cmd, val))

    # /send_req()
    # |ConAPIIPCMasterCls

    def get_req(self):
        """Get request from console_api

        :returns: command and value as string. Waiting for command
        :rtype: str, str
        """
        if (self.debug):
            msg = "{} receiving data...".format(self.name)
            print(msg)
        data = self._req_get.recv_string()

        cmd, val = str2cmd_val(data, self.debug, self.name)

        return (cmd, val)

    # /get_req()
    # |ConAPIIPCMasterCls

    def send_notification(self, cmd="hw_cube_disconnected", val="reason"):
        """Send notification to foreground, where it can be passed to GUI

        :param cmd: Command
        :type cmd: str

        :param val: Command value
        :type val: str
        """
        self._notification_send.send_string("{} {}".format(cmd, val))

        if (self.debug):
            print("{} sending notification: {} {}".format(self.name, cmd, val))

    # /send_notification()
    # |ConAPIIPCMasterCls

    # ============================| Request/reply IPC |=======================
    def req_rply_get_cmd_val(self):
        """Get request command and values (arguments)

        :returns: Command and values/arguments. If values contain
                  multiple arguments, they are automatically split to list.
                  If value is empty, string "" will be set to value. If there
                  is only one value, it is returned as simple string.
        :rtype: str (command), str/list of strings (values)
        """
        # Get request - blocking function
        if (self.debug):
            msg = "{} receiving req/REP data...".format(self.name)
            print(msg)
        req = self._req_rply.recv_string()

        if (self.debug):
            # Only for debug
            print("{} Got req/rply request ({})".format(self.name, req))

        cmd, val = str2cmd_val(req, self.debug, self.name)

        return (cmd, val)

    # /req_rply_get_cmd_val()
    # |ConAPIIPCMasterCls

    def req_rply_send_response(self, variable, value):
        """Send response to request"""
        if (self.debug):
            # print is only for debug
            print("{} req/REP - sending {} | {}"
                  "".format(self.name, variable, value))
        self._req_rply.send_string("{} {}"
                                   "".format(variable, value))

    # /req_rply_send_response()
    # |ConAPIIPCMasterCls

    # ===============================| Timer IPC |============================
    def timer_send_req(self, cmd="close", val=25):
        """This function will send request to timer IPC"""
        self._tmr_req_send.send_string("{} {}".format(cmd, val))
    # /timer_send_req()
    # |ConAPIIPCMasterCls
    # ===========================| Internal functions |=======================


# /ConAPIIPCBgProcCls


class ConAPIIPCBgProcTimerCls():

    def __init__(self, settings, logger):
        """
        :param settings: Structure with port and IP address settings
        :type settings: ConAPIIPCSettingsCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.settings = settings
        self.logger = logger

        # String used in debug messages
        self.name = 'CA IPC - Timer'

        # If you need to debug IPC. But set it only for devel/debug purpose.
        # It may overwhelm your logs...
        self.debug = False

        # Initialize client (accept requests from background process)
        context = zmq.Context()
        self._req_get = context.socket(zmq.PULL)
        # Here can not be used timeout - whole function is based on
        # asynchronous waiting for any event
        # self._req_get.RCVTIMEO = self.settings.timeout_ms
        self._req_get.connect("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_timer_get_req))

        # Initialize server (send requests/interrupts to background
        # process)
        context = zmq.Context()
        self._req_send = context.socket(zmq.PUSH)
        self._req_send.RCVTIMEO = self.settings.timeout_ms
        self._req_send.bind("tcp://{}:{}".format(
            self.settings.ip,
            self.settings.port_timer_send_req))

        # Init is not finished yet. We need to wait for pair side
        self._ready = False

    # /__init__()
    # |ConAPIIPCBgProcTimerCls

    def send_req(self, cmd="play_next_img", val=15):
        """Send request to background process

        :param cmd: Command
        :type cmd: str

        :param val: Command value/arguments.
        :type val: str/int/list/...
        """
        self._req_send.send_string("{} {}".format(cmd, val))

    # /send_req()
    # |ConAPIIPCBgProcTimerCls

    def get_req(self):
        """Get request from background process

        :returns: command and value as string. If no command is received, then
                  command value is empty string
        :rtype: str, str
        """
        cmd = ""
        val = ""

        try:
            data = self._req_get.recv_string(flags=zmq.NOBLOCK)
        except zmq.Again:
            # No request received -> return void command and value
            pass
        else:
            # Received command -> parse it
            cmd, val = str2cmd_val(data, self.debug, self.name)
        # /try to get command and value
        return (cmd, val)

    # /get_req()
    # |ConAPIIPCBgProcTimerCls

    def get_req_blocking(self):
        """Get request from background process (blocking function)

        :returns: command and value as string. If no command is received, then
                  command value is empty string
        :rtype: str, str
        """
        if (self.debug):
            msg = "{} receiving data...".format(self.name)
            print(msg)

        data = self._req_get.recv_string()
        cmd, val = str2cmd_val(data, self.debug, self.name)

        return (cmd, val)
    # /get_req_blocking()
    # |ConAPIIPCBgProcTimerCls
    # ===========================| Internal functions |=======================


# /ConAPIIPCBgProcTimerCls


def str2cmd_val(data, debug=False, name=None):
    """Convert string to command and value(s)

    :param data: String to parse. It should be exact as string as received
                over network
    :type str: str

    :param debug: This will show all` the data that are processed by this
                  function. Enabling this option will have negative impact
                  to performance
    :type debug: Bool

    :param name: When debug parameter is enabled, optional name can be added
                 to auto-generated message
    :type name: str
    """
    if (debug):
        if (name is None):
            print("RX request: {}".format(data))
        else:
            print("{} RX request: {}".format(name, data))

    data = data.split(" ")

    # Command is simple
    cmd = data[0]

    # Check if value is empty, only one value or array
    if (len(data) < 2):
        val = ""
    elif (len(data) > 2):
        # Array -> Value is everything after 2nd index
        val = data[1:]
    else:
        val = data[1]
    # /modify value type if needed
    return (cmd, val)
# /str2cmd_val()

# =======================| Internal functions/classes |=======================

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
========================================
Module for handling "random" animations
========================================

Sometimes is easier to write pattern than whole animation and put little bit
random factor inside.

``author`` Martin Stejskal

``version`` 0.2.2
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIRndmCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def get_animations(self):
        """Return list of supported random animations

        :returns: List of supported animations
        :rtype: list (str)
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="rndm_get_animations", val="ca_rndm")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="animations",
                                        function_name="get_animations()")

        anim_list = self.ca_cmn.rplc_undrscr_wth_spc_in_str_lst(value)
        return (anim_list)

    # /get_animations()

    def set_animation(self, anim_name):
        """Play selected random animation

        :param anim_name: Selected animation name
        :type anim_name: str
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="rndm_set_animation", val=anim_name)

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="set_animation()")
        if (value != "OK"):
            msg = 'Can not set animation "{}"\n\n{}' \
                  ''.format(anim_name, self.ca_cmn.restore_msg(value))
            self.logger.error(msg)
            raise Exception(msg)

    # /set_animation()

    def set_timer(self, time_ms=100):
        """Set timer value in ms

        :param time_ms: Time in ms
        :type time_ms: int
        """
        self.ipc.send_req(cmd="set_timer",
                          val=time_ms)

    # /set_timer()

    def get_timer(self):
        """Returns timer value

        :returns: Timer value in ms
        :rtype: int
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_timer")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="timer",
                                        function_name="get_timer")

        return (int(value))
    # /get_timer()
# /ConAPIRandomCls

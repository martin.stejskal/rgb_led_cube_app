#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Operations with cube
===================================

Do you want to enable all LED? Or turn on only red ones? Anything else?

``author`` Martin Stejskal

``version`` 0.3.2

``Created``  2017.12.17

``Modified`` 2018.10.28
"""


class ConAPIOpCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj

        """
        self.ipc = ipc
        self.logger = logger
    # /__init__()

    # ==============================| Cube colors |===========================
    def cube_set_all(self):
        """Turn on every single LED in cube"""
        self.ipc.send_req(cmd="op_cube_set_all", val="")

    def cube_clear_all(self):
        """Turn off every single LED in cube"""
        self.ipc.send_req(cmd="op_cube_clear_all", val="")

    def cube_set_r(self, additive=True):
        """Set all red LEDs

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_cube_set_r", val=additive)

    def cube_set_g(self, additive=True):
        """Set all green LEDs

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_cube_set_g", val=additive)

    def cube_set_b(self, additive=True):
        """Set all blue LEDs

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_cube_set_b", val=additive)

    # ==============================| Wall colors |===========================
    def wall_set_all(self):
        """Turn on all LED in active wall"""
        self.ipc.send_req(cmd="op_wall_set_all", val="")

    def wall_clear_all(self):
        """Turn off all LED in active wall"""
        self.ipc.send_req(cmd="op_wall_clear_all", val="")

    def wall_set_r(self, additive=True):
        """Turn on all red LED in active wall

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_wall_set_r", val=additive)

    def wall_set_g(self, additive=True):
        """Turn on all green LED in active wall

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_wall_set_g", val=additive)

    def wall_set_b(self, additive=True):
        """Turn on all blue LED in active wall

        :param additive: Add color (true) or set selected color and wipe
                         other colors (false).
        :type additive: bool
        """
        self.ipc.send_req(cmd="op_wall_set_b", val=additive)

    # ============================| Transformations |=========================
    def t_bck2frnt(self, shifts=1, repeat=True):
        """Make transformation from BACK to FRONT wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from front will appear in the
                       back. This will happen, when repeat parameter is
                       true. Else void data will appear in back.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_bck2frnt",
                          val="{} {}".format(shifts, repeat))
    # /t_bck2frnt()

    def t_frnt2bck(self, shifts=1, repeat=True):
        """Make transformation from FRONT to BACK wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from back will appear in the
                       front. This will happen, when repeat parameter is
                       true. Else void data will appear in front.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_frnt2bck",
                          val="{} {}".format(shifts, repeat))
    # /t_frnt2bck()

    def t_lft2rght(self, shifts=1, repeat=True):
        """Make transformation from LEFT to RIGHT wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from right will appear in the
                       left. This will happen, when repeat parameter is
                       true. Else void data will appear in left.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_lft2rght",
                          val="{} {}".format(shifts, repeat))
    # /t_lft2rght()

    def t_rght2lft(self, shifts=1, repeat=True):
        """Make transformation from RIGHT to LEFT wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from right will appear in the
                       right. This will happen, when repeat parameter is
                       true. Else void data will appear at right.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_rght2lft",
                          val="{} {}".format(shifts, repeat))
    # /t_rght2lft()

    def t_btm2top(self, shifts=1, repeat=True):
        """Make transformation from BOTTOM to TOP wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from top will appear in the
                       bottom. This will happen, when repeat parameter is
                       true. Else void data will appear in bottom.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_btm2top",
                          val="{} {}".format(shifts, repeat))
    # /t_btm2top()

    def t_top2btm(self, shifts=1, repeat=True):
        """Make transformation from TOP to BOTTOM wall

        :param shifts: Define how many times shift should be performed.
                       Every shift is "1px" (1 LED) distance.
        :type shifts: int

        :param repeat: During transformation there are 2 options: image
                       is repeated and data from bottom will appear in the
                       top. This will happen, when repeat parameter is
                       true. Else void data will appear in top.
        :type repeat: bool
        """
        self.ipc.send_req(cmd="t_top2btm",
                          val="{} {}".format(shifts, repeat))
    # /t_top2btm()

    # ===============================| Rotations |============================
    def r_top2frnt(self, shifts=1):
        """Rotate top floor (0) to front wall (0)

        :param shifts: Define how many times shift should be performed.
                       Every shift is rotation by 90 deg.
        :type shifts: int
        """
        self.ipc.send_req(cmd="r_top2frnt",
                          val=shifts)
    # /r_top2frnt()

    def r_top2bck(self, shifts=1):
        """Rotate top floor (dimension) to back wall (dimension)

        :param cube: LED cube
        :type cube: obj (CubeLEDCls)

        :param shifts: Define how many times shift should be performed.
                       Every shift is rotation by 90 deg.
        :type shifts: int
        """
        self.ipc.send_req(cmd="r_top2bck",
                          val=shifts)
    # /r_top2bck()

    def r_frnt2lft(self, shifts=1):
        """Rotate front wall (0) to the left side (wall 0~dimension)

        :param shifts: Define how many times shift should be performed.
                       Every shift is rotation by 90 deg.
        :type shifts: int
        """
        self.ipc.send_req(cmd="r_frnt2lft",
                          val=shifts)
    # /r_frnt2lft()

    def r_frnt2rght(self, shifts=1):
        """Rotate front wall (0) to the right side (wall 0~dimension)

        :param shifts: Define how many times shift should be performed.
                       Every shift is rotation by 90 deg.
        :type shifts: int
        """
        self.ipc.send_req(cmd="r_frnt2rght",
                          val=shifts)
    # /r_frnt2rght()
# /ConAPIOpCls

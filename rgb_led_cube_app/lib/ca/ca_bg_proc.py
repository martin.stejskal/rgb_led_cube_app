#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Background process for console API
===================================

This module works with class which is background process for console_api

``author`` Martin Stejskal

``version`` 1.0.5
"""
# Multiprocessing and sharing variables through process
from multiprocessing import Process

# Process naming is not something mandatory, but nice to have
try:
    from setproctitle import setproctitle
except Exception:
    pass

# Our library - LED cube (basic, no GUI, but support cube operations)
from lib.cube_LED import CubeLEDOpCls

# Our 3D LED cube window
from lib.cube_3D import Cube3DWCls

# IPC for communication with console API
from lib.ca.ca_ipc import ConAPIIPCBgProcCls

# For controlling real cube
from lib.real_cube_api import RealCubeAPICls, RealCubeAPISettingsCls

# Tuned logger
from lib.common import CmnLgrCls, log_traceback

# Common functions for console API
from lib.ca.ca_common import ConAPICommonCls

# Handle saving/loading animations
from lib.ca.ca_bg_proc_load_save import ConsoleAPIBgLoadSaveCls

# Random animations
from lib.ca.ca_bg_proc_rndm import ConsoleAPIBgRndmCls

# Effects
from lib.ca.ca_bg_proc_effects import ConsoleAPIBgEffectsCls

# Internal classes, functions
from lib.ca.ca_bg_proc_internal import _ConAPIBgRtCls, _ConsoleAPIBgTimerCls


class ConsoleAPIBgCls(Process):

    def __init__(self, settings):
        """
        :param settings: Settings structure
        :type settings: ConsoleAPISettingsCls
        """
        # This will allow to run this class as single process. It basically
        # mark this class as base class for process. Due to this, it is not
        # recommended to have here too much "class" things. Therefore in
        # function "run()" is basically initialization
        super().__init__()

        # Save settings to instance
        self.settings = settings

        # Initialize logger
        self.logger = CmnLgrCls("console_api_bg")

        try:
            # Set process name -> much easier to debug then
            setproctitle('Cube_console_API_bg')
        except Exception:
            msg = 'Can not set process name. Nothing serious.'
            self.logger.info(msg)

    # /__init__()

    def run(self):
        """This function is running at background"""
        # Finish initialization (well, try it)
        self.logger.debug("Finishing initialization of background process...")
        try:
            self._init_finish()
        except Exception as e:
            msg = "Initialization failed :(\n\n{}".format(e)
            log_traceback(logger=self.logger, type="critical")
            # Try to close background process properly
            self.close(reason=msg)
            raise Exception(msg)
        # /try to finish initialization sequence

        self.logger.debug("Background process is running in loop...")

        # Keep running...
        while (self.rt.keep_running):
            # Try to process incoming request - at the end of the day, this
            # is blocking function, so whole loop is kind a event based
            try:
                self._process_req()
            except Exception as e:
                msg = "Processing request failed!\n\n{}".format(e)
                self.logger.error(msg)

                log_traceback(logger=self.logger, type='error')
                self.close(reason=msg)
                raise Exception(msg)
            # /try to process request
        # /while keep running

        # Close 3D view (background process is there as well)
        self.close(reason="Request from upper layer")

        self.logger.debug("Terminating background process")

    # /run()

    def close(self, reason="unknown"):
        """Properly close all background process"""
        self.logger.debug("Closing... reason: {}".format(reason))

        self.logger.info("Closing real cube API")
        try:
            self.real_cube.close()
        except Exception:
            self.logger.warn("Can not close real cube API. Maybe it crashed?")
        # /try to close real cube

        self.logger.info("Closing cube 3D view...")
        try:
            self.cube_3D_w.close()
        except Exception:
            self.logger.warn("Can not close 3D window. Maybe it crashed?")
        # /try to close 3D view

        self.logger.info("Closing timer process...")
        try:
            self.ipc.timer_send_req(cmd="close")
            self.timer.join()
        except Exception:
            self.logger.warn("Can not close timer process. Maybe it crashed?")
        # /try to close background process

        # Try to notify upper layer if possible. However upper layer might
        # waiting for REQ/REP, so it does not have to help
        if (not self.rt.sent_close_notification):
            self.ipc.send_notification(
                cmd="closing",
                val=reason)
            self.rt.sent_close_notification = True

        self.logger.debug("Close done")

    # /close()

    def update_cube(self):
        """Update 3D view

        In some cases is needed to update cube image. To make it simple,
        this function will rewrite cube_3D LED structure
        """
        # Check if 3D view is still running or not
        if (self.cube_3D_w.get_err_code_3D_view() is None):
            self.cube_3D_w.set_base_led_struct(self.leds)
        else:
            msg = "3D view already closed. Can not update."
            self.logger.warning(msg)

        self._update_real_cube_image_if_connected()

    # /update_cube()

    def set_dimension(self, dimension):
        # Set dimension value(s)
        # Main dimension variable. But it is at more places. We should not
        # take this information from other places, but due to sub-classes,
        # dimension parameter was copied to many places. That is true reason
        # why it have to be set at multiple places

        # Tell new dimension also 3D view
        self.logger.debug('Setting new dimension ({}) at 3D view...'
                          ''.format(dimension))
        self.cube_3D_w.set_dimension(dimension=dimension)

        mode = self.get_mode()
        self.logger.debug('Updating mode ({}) variables...'.format(mode))

        # Common stuff - need to be done regardless on mode
        self.rt.set_dimension(dimension)

        self.rndm = ConsoleAPIBgRndmCls(dimension=dimension,
                                        logger=self.logger)
        # Since animation list could change due to dimension change,
        # simply choose first animation from list
        anim_name = self.rndm.get_animations()
        self.rndm.set_animation(anim_name[0])

        # If in animation mode, actual animation should be wiped
        if (mode == "anim"):
            self.leds = self.rt.modes.draw.get_cube()
        elif (mode == "draw"):
            self.leds = self.rt.modes.anim.get_actual_cube()
        elif (mode == "random"):
            self.leds = self.rndm.get_img()

        self.logger.debug('Checking if actual dimension is same at '
                          '"Real cube"...')
        # If real cube is connected and dimension does not match -> disconnect
        real_cube_dim = self.real_cube.get_dimension()
        if ((dimension != real_cube_dim) and
                (self.real_cube.get_status() == "Connected")):
            msg = "Cube was connected, but new dimension ({})\n" \
                  " does not match with real cube dimension ({})" \
                  "".format(dimension, real_cube_dim)
            self.logger.warn(msg)
            self.real_cube.disconnect()
        # /if real cube dimension and new dimension does not match

        self.logger.debug('Setting dimension done')

    # /set_dimension()

    def get_dimension(self):
        return (self.leds.get_dimension())

    # /get_dimension()

    def get_active_wall(self):
        """Tells you which wall is active right now

        :returns: Index of active wall or None if all walls are active
        :rtype: None, int
        """
        return (self.rt.active_wall)

    # /get_active_wall()

    def get_active_wall_list(self):
        """It will return list of active walls

        :retruns: List of active walls
        :rtype: list (int)
        """
        active_walls = []

        active_wall = self.get_active_wall()

        if (active_wall is None):
            # All walls are active
            for i in range(self.get_dimension()):
                active_walls.append(i)
            # /for whole cube
        # /if all walls are active
        else:
            active_walls.append(active_wall)
        # /else only one is active

        return (active_walls)

    # /get_active_wall_list()

    def set_mode(self, mode):
        """Change mode

        Every mode have some specific functions, so when is changed, obviously
        a lot of things need to be changes as well

        :param mode: selected mode: draw, anim, random
        :type mode: str
        """
        msg = 'set_mode: Setting mode "{}"'.format(mode)
        self.logger.debug(msg)

        old_mode = self.get_mode()

        # Check if change is pointless
        if (mode == old_mode):
            # It is pointless to change mode to the same one. Ignoring....
            msg = "Selected mode ({}) and actual ({}) are same -> " \
                  "ignoring...".format(mode, old_mode)
            self.logger.debug(msg)
            return
        # /if mode is same, return

        # Check previous mode & clean up
        if (old_mode == "anim"):
            # Pause animation if running
            # Check actual status
            if (self.rt.modes.anim.get_status() == "play"):
                self.rt.modes.anim.pause()
                self.ipc.timer_send_req(cmd="pause", val="set_mode (old)")
            # /pause if playing animation
        elif (old_mode == "draw"):
            pass
        elif (old_mode == "random"):
            # Pause timer again (if any animation was selected)
            if (self.rndm.get_animation() is not None):
                self.ipc.timer_send_req(cmd="pause", val="set_mode (old)")
        elif (old_mode is None):
            # No previous mode -> initialization -> pass
            pass
        else:
            msg = "Internal error!\nUnknown old mode {}! Can not change to" \
                  " this mode".format(old_mode)
            self.logger.critical(msg)
            raise Exception(msg)
        # /clean up old modes

        # Check mode
        if (mode == "draw"):
            self._set_mode_draw()
        elif (mode == "anim"):
            self._set_mode_anim()
        elif (mode == "random"):
            self._set_mode_random()
        else:
            msg = "Internal error: Unknown mode ({})!".format(mode)
            self.logger.error(msg)
            raise Exception(msg)
        # /else unknown mode

        msg = 'Mode switched from "{}" to "{}"'.format(old_mode, mode)
        self.logger.info(msg)

        # Everything seems to be OK -> save new mode
        self.rt.mode = mode

    # /set_mode()

    def get_mode(self):
        """Get actual mode
        """
        return (self.rt.mode)

    # /get_mode()

    def get_runtime_mode_obj(self):
        """Return undo/redo object for actual mode

        :returns: Undo/redo object
        :rtype: obj (DrawModeCls/AnimModeCls)
        """
        mode = self.get_mode()
        if (mode == 'anim'):
            return (self.rt.modes.anim)
        elif (mode == 'draw'):
            return (self.rt.modes.draw)
        else:
            msg = 'Internal error!\nCan not runtime mode object at "{}" mode' \
                  ''.format(mode)
            self.logger.error(msg)
            raise Exception(msg)

    # /get_runtime_mode_obj()
    # |ConsoleAPIBgCls

    def commit_cube(self):
        """Commit actual cube, so undo/redo can be used later
        """

    def str2idx_list(self, var):
        """Function that convert string to list of index for addressing LED

        If parameter is not "None", it will just convert string to integer
        and put it to list. If parameter is "None", it will create list
        of integers in rage of cube dimension

        :param var: Input string. It should contain "None" or number as string
        :type var: str (None, 0 ~ (cube dimension -1))
        """
        # For now is output empty
        output = []
        if (var == "None"):
            # Create list in range of dimension
            for i in range(self.get_dimension()):
                output.append(i)
            # /for size of cube dimension
        # /if None
        else:
            # Simple integer is expected
            output.append(int(var))
        # /Else simple integer

        return (output)

    # /str2idx_list()
    # ===========================| Internal functions |=======================

    def _init_finish(self):
        # =============================| 3D cube window |=====================
        # Since we know, that we want to receive notifications about
        # "on_close", simply enable them. Later connect to notification
        # server
        self.settings.cube_3D.cube_3D_view.notif.on_close = True
        self.settings.cube_3D.cube_3D_view.notif.ip = \
            self.settings.ipc.ip
        self.settings.cube_3D.cube_3D_view.notif.port = \
            self.settings.ipc.port_3D_view_notif

        # Init 3D cube window. It have to be there, since at __init__()
        # is initialized this task
        self.cube_3D_w = Cube3DWCls(settings=self.settings.cube_3D)

        # Get dimension - will be used quite a lot
        # Dimension is also stored in settings, but this way we can verify it
        dimension = self.cube_3D_w.get_dimension()
        assert dimension == self.settings.cube_3D.cube_3D_view.dimension, \
            'During initialization dimension in settings should be equal to ' \
            'dimension at cube 3D view'

        # ======================| Console API common functions |==============
        self.ca_cmn = ConAPICommonCls(self.logger)

        # ======================| Interprocess initialization |===============
        self.ipc = ConAPIIPCBgProcCls(self.settings.ipc,
                                      self.logger)

        # ==========================| Initialize variables |==================
        # Initialize runtime variables (structure/class/whatever)
        self.rt = _ConAPIBgRtCls(
            dimension=dimension, send_notification=self.ipc.send_notification)

        # =============================| Real cube API |======================
        # Initialize real cube API. Pass settings structure
        self.real_cube = RealCubeAPICls(RealCubeAPISettingsCls())

        # ==========================| Load/save animations |==================
        # Handle loading and saving to/from files
        self.load_save = ConsoleAPIBgLoadSaveCls(logger=self.logger)

        # ===========================| Random animations |====================
        self.rndm = ConsoleAPIBgRndmCls(dimension=dimension,
                                        logger=self.logger)

        # ============================| Initialize timer |====================
        # Since timer can be common for multiple modes, it is not so bad idea
        # to keep it running at the background most of the time
        self.timer = _ConsoleAPIBgTimerCls(self.settings,
                                           self.rt.get_timer())
        self.timer.start()

        # LED structure - basically only pointer to actual LED structure at
        # dependence of used mode
        self.leds = None

        # Class for cube operations
        self.op = CubeLEDOpCls()

        # Change mode according to settings
        assert self.rt.mode is None, "It is expected, that mode has not been" \
                                     " set yet."
        self.set_mode(mode=self.settings.mode)

        assert self.leds is not None, "After mode change self.leds should be" \
                                      " already ready to use"
        assert self.leds.get_dimension() == dimension, (
            'Since this is  initialization, it is expected that dimension is'
            ' not changed yet')

        # ================================| Effects |=========================
        # Need to be initialized when object self.leds already exists
        self.effects = ConsoleAPIBgEffectsCls(
            settings=self.settings.effects,
            logger=self.logger, anim_obj=self.rt.modes.anim,
            get_mode_fnc=self.get_mode,
            send_notification_fnc=self.ipc.send_notification,
            update_cube_fnc=self.update_cube,
            timer_send_req_fnc=self.ipc.timer_send_req)

        # LEDs are set, set them also in 3D view
        self.update_cube()

    # /_init_finish()

    # ============================| Process request |=========================
    def _process_req(self):
        """Try to process request (if any) from buffer"""
        cmd, val = self.ipc.get_req()

        if (self._proc_req_main(cmd, val) or
                self._proc_req_pos(cmd, val) or
                self._proc_req_wall(cmd, val) or
                self._proc_req_view(cmd, val) or
                self._proc_req_pwm(cmd, val) or
                self._proc_req_op(cmd, val) or
                self._proc_req_btn_array(cmd, val) or
                self._proc_req_play(cmd, val) or
                self._proc_req_load_save(cmd, val) or
                self._proc_req_effects(cmd, val) or
                self._proc_req_edit(cmd, val) or
                self._proc_req_3D_view_notif(cmd, val)):
            pass
        else:
            msg = "Internal error. Unknown request ({}) from console_api" \
                  " layer!".format(cmd)
            self.logger.critical(msg)
            raise Exception(msg)
        # /else unknown command

    # /_process_req()

    def _proc_req_3D_view_notif(self, cmd, val):
        if (cmd == "cube_3D"):
            # For sure notification from 3D view
            if (val == "closing"):
                # Request to close. Send notification
                self.ipc.send_notification(
                    cmd="closing",
                    val="Request from 3D view")
                self.rt.sent_close_notification = True
                # ONE BIG NOTE: Process should not set "self.rt.keep_running"
                # to False, since we can not be 100% sure that upper layers
                # (GUI) right now know about close status. It can do some
                # processing and in meantime it can sent many requests and
                # it can still keep sending. If we would just cut off this
                # process, IPC would freeze, since it would wait for response,
                # which would not happen. So that is why we need to handle
                # the rest of requests until GUI "realize" that 3D view is
                # already closed.
            else:
                msg = 'Internal error. Unexpected notification from 3D' \
                      ' view: "{}"'.format(val)
                self.logger.critical(msg)
                raise Exception(msg)
        else:
            # Unknown command
            return (False)
        # Known command
        return (True)

    # /_proc_req_3D_view_notif()

    def _proc_req_load_save(self, cmd, val):
        if (cmd == "load_save_new"):
            # This is mode dependent
            mode = self.get_mode()
            if (mode == "anim"):
                self.leds = self.rt.modes.anim.new()
                self.update_cube()
            elif (mode == "draw"):
                self.leds = self.rt.modes.draw.new()
                self.update_cube()
            else:
                msg = 'Can not create new 3D cube for mode "{}"'.format(mode)
                self.logger.error(msg)
        # /load_save_new
        else:
            # Unknown command -> False
            return (False)
        # Command known -> True
        return (True)

    # /_proc_req_load_save()

    def _proc_req_effects(self, cmd, val):
        if (cmd == "effect_fade_out"):
            self.effects.fade_out(
                duration=int(val[0]),
                effect_type=val[1],
                pwm=[self.ca_cmn.str2bool(val[2]),
                     self.ca_cmn.str2bool(val[3]),
                     self.ca_cmn.str2bool(val[4])])
        else:
            return (False)

        # Need to always update cube
        self.leds = self.rt.modes.anim.get_actual_cube()
        self.update_cube()
        return (True)

    # /_proc_req_effects()

    def _proc_req_edit(self, cmd, val):
        """Process edit requests
        """
        update_view = True

        if (cmd == "edit_undo"):
            rt_obj = self.get_runtime_mode_obj()
            val = self.ca_cmn.str2bool(var=val)
            leds = rt_obj.undo(fail_if_error=val)
            # Save some CPU time if needed
            if (leds is None):
                update_view = False
            else:
                self.leds = leds
        elif (cmd == "edit_redo"):
            rt_obj = self.get_runtime_mode_obj()
            val = self.ca_cmn.str2bool(var=val)
            leds = rt_obj.redo(fail_if_error=val)
            if (leds is None):
                update_view = False
            else:
                self.leds = leds
        elif (cmd == "edit_copy"):
            self.rt.copy_paste.copy(cube=self.leds)
        elif (cmd == "edit_paste"):
            self.leds = self.rt.copy_paste.paste()
            # And commit cube
            rt_obj = self.get_runtime_mode_obj()
            rt_obj.commit(old_cube=self.leds)

            # And update link to actual cube
            mode = self.get_mode()
            if (mode == 'draw'):
                rt_obj.set_cube(self.leds)
            elif (mode == 'anim'):
                rt_obj.set_actual_cube(self.leds)
            assert mode != 'random', 'Internal error! Paste operation not' \
                                     ' expected for random mode'
        # /paste
        else:
            # Not known command
            return (False)

        if (update_view):
            self.update_cube()
        # Known command
        return (True)

    # /_proc_req_edit()

    def _proc_req_main(self, cmd, val):
        """Process main requests

        Some requests are simply general and they can not be put to any other
        function. So for this is there this function
        """
        if (cmd == "close"):
            # Process should be closed -> set variable
            self.rt.keep_running = False
        elif (cmd == "process_req_rply_TX"):
            # Some request/reply is needed
            self._process_req_rply()
        elif (cmd == "set_timer"):
            val = int(val)
            self.rt.set_timer(time_ms=val)
            self.ipc.timer_send_req(cmd="set_timer", val=val)
        elif (cmd == "set_mode"):
            self.set_mode(mode=val)

            # Mode changed -> need to update cube
            self.update_cube()
        elif (cmd == "timer_interrupt"):
            # Timer interrupt -> this is mode dependent
            mode = self.get_mode()

            if (mode == "anim"):
                # Get status and if needed, send signal to timer
                status = self.rt.modes.anim.get_status()

                if (status == "pause"):
                    # Status is "pause", but timer is still running -> send
                    # "pause" command to timer
                    self.ipc.timer_send_req(
                        cmd="pause",
                        val="_proc_req_main (status pause but got interrupt)")
                elif (status == "play"):
                    # We got interrupt -> timer is running -> next image

                    self.rt.modes.anim.next_no_new_img()
                    # Check status. If this was "last image", status was
                    # changed to "pause" -> check it
                    if (self.rt.modes.anim.get_status() == "pause"):
                        self.ipc.timer_send_req(
                            cmd="pause",
                            val="_proc_req_main (last image -> pause)")

                    # Anyway, we need to update image
                    self.leds = self.rt.modes.anim.get_actual_cube()
                    self.update_cube()
                else:
                    msg = "Internal error!\n" \
                          " Unknown status {}. Expected play/pause" \
                          "".format(status)
                    self.logger.critical(msg)
                    raise Exception(msg)
            # /if mode anim
            elif (mode == "random"):
                # Update image only if some animation is selected.
                # This may happened in case we switched mode, but timer did
                # not read "pause" request "in time" and in meantime it
                # sent interrupt. It may happen, but chance is low.
                if (self.rndm.get_animation is None):
                    msg = 'No random animation selected, but got interrupt. ' \
                          'Background timer probably did not read "pause" ' \
                          'command in time. Nothing serious, but it should ' \
                          'not be often'
                    self.logger.warn(msg)
                else:
                    # Just update image (timer should run only if animation is
                    # selected)
                    self.leds = self.rndm.get_img()
                    self.update_cube()
            # /if mode random
            elif (mode == "draw"):
                # Due to asynchronous behavior, we can get interrupt when
                # previous mode is using timer and we switched to "draw" mode.
                # It can happen once, or twice. Better to report it to the log
                msg = 'Got interrupt in "{}" mode, which should not happen.' \
                      'This may rarely happen when switching from different ' \
                      'mode which is using timer. Anyway, nothing serious.' \
                      ''.format(mode)
                self.logger.warn(msg)
            else:
                msg = 'Internal error!\n' \
                      ' Got timer interrupt at mode {}, which is unexpected' \
                      ''.format(mode)
                self.logger.critical(msg)
                raise Exception(msg)
            # /mode check/if/switch
        # /timer interrupt
        else:
            return (False)
        return (True)

    # /_proc_req_main()

    def _proc_req_play(self, cmd, val):
        """Check if command is related to play request

        :returns: True if cmd is one of the wall command
        :rtype: bool
        """
        update = False

        if (cmd == "play_set_actual_idx"):
            self.rt.modes.anim.set_actual_idx(actual_idx=int(val))
            update = True
        elif (cmd == "play_next"):
            self.rt.modes.anim.next()
            update = True
        elif (cmd == "play_previous"):
            self.rt.modes.anim.previous()
            update = True
        elif (cmd == "play_play"):
            # Check actual status first
            if (self.rt.modes.anim.get_status() == "play"):
                # Already playing -> do nothing
                self.logger.debug('Got "play" request, but already playing')
            else:
                # Set status and activate timer
                self.rt.modes.anim.play()
                self.ipc.timer_send_req(cmd="play", val="_proc_req_play")

                # Update image
                update = True
        # /play_play
        elif (cmd == "play_pause"):
            # Check actual status
            if (self.rt.modes.anim.get_status() == "pause"):
                # Already paused -> do nothing
                self.logger.debug('Got "pause" request, but already paused')
            else:
                self.rt.modes.anim.pause()
                self.ipc.timer_send_req(cmd="pause", val="_proc_req_play")
        # /play_pause
        elif (cmd == "play_stop"):
            if (self.rt.modes.anim.get_status() == "play"):
                self.ipc.timer_send_req(cmd="pause", val="_proc_req_play")
            # and set image index and update status
            self.rt.modes.anim.stop()
            update = True
        elif (cmd == "play_del_act_idx"):
            self.rt.modes.anim.delete_actual_idx()
            update = True
        elif (cmd == "play_new_img_act_idx"):
            self.rt.modes.anim.insert_new_image_actual_idx()
            update = True
        elif (cmd == "play_set_repeat"):
            value = self.ca_cmn.str2bool(var=val)
            self.rt.modes.anim.set_repeat(value)
        else:
            # Unknown command
            return (False)

        if (update):
            self.leds = self.rt.modes.anim.get_actual_cube()
            self.update_cube()
        return (True)

    # /_proc_req_play()

    def _proc_req_pwm(self, cmd, val):
        """Check if command is related to PWM

        :returns: True if cmd is one of the wall command
        :rtype: bool
        """
        if (cmd == "set_pwm"):
            self.leds.pwm.r = int(val[0])
            self.leds.pwm.g = int(val[1])
            self.leds.pwm.b = int(val[2])

            # We changed PWM value -> need to update 3D view
            self.update_cube()
        elif (cmd == "set_pwm_all_img"):
            # Check mode. This is totally pointless in draw or random mode
            mode = self.get_mode()
            assert mode == 'anim', 'It is pointless to call "{}" at mode' \
                                   ' "{}"!'.format(cmd, mode)
            # Parse value, check PWM values and set it at all images
            self.rt.modes.anim.set_pwm_all_cubes(
                r=int(val[0]), g=int(val[1]), b=int(val[2]))
        else:
            # Unknown command
            return (False)
        return (True)

    # /_proc_req_pwm()

    def _proc_req_wall(self, cmd, val):
        """Check if command is related to wall operation

        :returns: True if cmd is one of the wall command
        :rtype: bool
        """
        if (cmd == "set_active_wall"):
            # We can not TX "None" variable -> modification
            if (val == "None"):
                val = None
            else:
                val = int(val)

            self._set_active_wall(wall_idx=val)
        # /set active wall
        else:
            return (False)
        return (True)

    # /_proc_req_wall()

    def _proc_req_view(self, cmd, val):
        """Check if command is related to view operations

        :returns True if cmd is on of the view command
        :rtype: bool
        """
        if (cmd == "view_set_wall_highlight"):
            self.rt.highlight_active_wall = self.ca_cmn.str2bool(var=val)
            self._set_active_wall(wall_idx=self.get_active_wall())
        elif (cmd == "view_set_show_disabled"):
            self.cube_3D_w.set_show_disabled(self.ca_cmn.str2bool(var=val))
        elif (cmd == "view_set_show_inactive"):
            self.cube_3D_w.set_show_inactive(self.ca_cmn.str2bool(var=val))
        else:
            return (False)
        return (True)

    # /_proc_req_view()

    def _proc_req_op(self, cmd, val):
        """Check if command is related to cube operation

        :returns: True if cmd is one of the operation command
        :rtype: bool
        """
        # Get actual wall index (might be used later)
        wall_idx = self.get_active_wall()

        if (cmd == "op_cube_set_all"):
            self.op.cube.set_all(self.leds)
        elif (cmd == "op_cube_clear_all"):
            self.op.cube.clear_all(self.leds)
        elif (cmd == "op_cube_set_r"):
            # If additive option is enabled
            if (self.ca_cmn.str2bool(var=val)):
                self.op.cube.set_r(self.leds)
            else:
                # Else set desired color and wipe (turn off) other colors
                self.op.cube.set_rgb(cube=self.leds, red=1, green=0, blue=0)
        elif (cmd == "op_cube_set_g"):
            if (self.ca_cmn.str2bool(var=val)):
                self.op.cube.set_g(self.leds)
            else:
                self.op.cube.set_rgb(cube=self.leds, red=0, green=1, blue=0)
        elif (cmd == "op_cube_set_b"):
            if (self.ca_cmn.str2bool(var=val)):
                self.op.cube.set_b(self.leds)
            else:
                self.op.cube.set_rgb(cube=self.leds, red=0, green=0, blue=1)

        elif (cmd == "op_wall_set_all"):
            if (wall_idx is None):
                # All walls -> whole cube
                self.op.cube.set_all(self.leds)
            else:
                # Only selected wall
                self.op.wall.set_all(self.leds, wall_idx)
        elif (cmd == "op_wall_clear_all"):
            if (wall_idx is None):
                self.op.cube.clear_all(self.leds)
            else:
                self.op.wall.clear_all(self.leds, wall_idx)
        elif (cmd == "op_wall_set_r"):
            if (wall_idx is None):
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.cube.set_r(self.leds)
                else:
                    self.op.cube.set_rgb(self.leds, 1, 0, 0)
            else:
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.wall.set_r(self.leds, wall_idx)
                else:
                    self.op.wall.set_rgb(self.leds, wall_idx, 1, 0, 0)
        elif (cmd == "op_wall_set_g"):
            if (wall_idx is None):
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.cube.set_g(self.leds)
                else:
                    self.op.cube.set_rgb(self.leds, 0, 1, 0)
            else:
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.wall.set_g(self.leds, wall_idx)
                else:
                    self.op.wall.set_rgb(self.leds, wall_idx, 0, 1, 0)
        elif (cmd == "op_wall_set_b"):
            if (wall_idx is None):
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.cube.set_b(self.leds)
                else:
                    self.op.cube.set_rgb(self.leds, 0, 0, 1)
            else:
                if (self.ca_cmn.str2bool(var=val)):
                    self.op.wall.set_b(self.leds, wall_idx)
                else:
                    self.op.wall.set_rgb(self.leds, wall_idx, 0, 0, 1)
        elif (cmd == "t_bck2frnt"):
            self.op.cube.t_bck2frnt(cube=self.leds,
                                    shifts=int(val[0]),
                                    repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "t_frnt2bck"):
            self.op.cube.t_frnt2bck(cube=self.leds,
                                    shifts=int(val[0]),
                                    repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "t_lft2rght"):
            self.op.cube.t_lft2rght(cube=self.leds,
                                    shifts=int(val[0]),
                                    repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "t_rght2lft"):
            self.op.cube.t_rght2lft(cube=self.leds,
                                    shifts=int(val[0]),
                                    repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "t_btm2top"):
            self.op.cube.t_btm2top(cube=self.leds,
                                   shifts=int(val[0]),
                                   repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "t_top2btm"):
            self.op.cube.t_top2btm(cube=self.leds,
                                   shifts=int(val[0]),
                                   repeat=self.ca_cmn.str2bool(val[1]))
        elif (cmd == "r_top2frnt"):
            self.op.cube.r_top2frnt(cube=self.leds,
                                    shifts=int(val[0]))
        elif (cmd == "r_top2bck"):
            self.op.cube.r_top2bck(cube=self.leds,
                                   shifts=int(val[0]))
        elif (cmd == "r_frnt2lft"):
            self.op.cube.r_frnt2lft(cube=self.leds,
                                    shifts=int(val[0]))
        elif (cmd == "r_frnt2rght"):
            self.op.cube.r_frnt2rght(cube=self.leds,
                                     shifts=int(val[0]))
        else:
            # Unknown command
            return (False)
        # /else unknown command

        # Command was known, but it means, that image need to be updated
        undo_redo = self.get_runtime_mode_obj()
        undo_redo.commit(self.leds)
        self.update_cube()
        return (True)

    # /_proc_req_op()

    def _proc_req_btn_array(self, cmd, val):
        """Function that check if cmd is related to button array operation

        :returns: True if cmd is one of the button array commands
        :rtype: bool
        """
        # Developer's note: it can set one led, or more thanks to possibility
        # to set column or row (or wall) to None. So name is not exactly
        # describing, however it can make more than expected :)
        if (cmd == "set_one_led_ish"):

            column_lst = self.str2idx_list(var=val[0])
            row_lst = self.str2idx_list(var=val[1])
            r = self.ca_cmn.str2bool(var=val[2])
            g = self.ca_cmn.str2bool(var=val[3])
            b = self.ca_cmn.str2bool(var=val[4])

            # Get list of active walls (one or all)
            active_walls = self.get_active_wall_list()

            for wall in active_walls:
                for column in column_lst:
                    for row in row_lst:
                        led = self.leds.w[
                            wall].c[
                            column].led[
                            row]

                        led.r = r
                        led.g = g
                        led.b = b
                    # /for rows
                # /for columns
            # /for walls
        else:
            # Unknown command
            return (False)

        undo_redo = self.get_runtime_mode_obj()
        undo_redo.commit(self.leds)
        self.update_cube()
        return (True)

    # /_proc_req_btn_array()

    def _proc_req_pos(self, cmd, val):
        """Function that check if cmd is related to position

        :returns: True if cmd is one of the position commands
        :rtype: bool
        """
        if (cmd == "set_zoom"):
            self.cube_3D_w.set_zoom(val)
        elif (cmd == "set_angle_x"):
            self.cube_3D_w.set_angle_x(val)
        elif (cmd == "set_angle_y"):
            self.cube_3D_w.set_angle_y(val)
        elif (cmd == "set_angle_z"):
            self.cube_3D_w.set_angle_z(val)
        elif (cmd == "reset_cube_view"):
            self.cube_3D_w.reset_cube_view()
        else:
            # unknown command
            return (False)
        return (True)

    # /_proc_req_pos()

    # ============================| /Process request |========================

    # =========================| Process request reply |======================
    def _process_req_rply(self):
        """Process request/reply thing
        """
        # Get parsed command and value
        cmd, val = self.ipc.req_rply_get_cmd_val()

        if (self._proc_req_rply_main(cmd, val) or
                self._proc_req_rply_pos(cmd, val) or
                self._proc_req_rply_pwm(cmd, val) or
                self._proc_req_rply_wall(cmd, val) or
                self._proc_req_rply_view(cmd, val) or
                self._proc_req_rply_real_cube(cmd, val) or
                self._proc_req_rply_play(cmd, val) or
                self._proc_req_rply_load_save(cmd, val) or
                self._proc_req_rply_rndm(cmd, val) or
                self._proc_req_rply_edit(cmd, val) or
                self._proc_req_rply_notif(cmd, val)):
            pass
        else:
            msg = "Internal error! Unknown request: {} {}".format(cmd, val)
            self.logger.error(msg)
            raise Exception(msg)
        # /else unknown reply

    # /_process_req_rply()

    def _proc_req_rply_notif(self, cmd, val):
        if (cmd == "get_list_of_notifications"):
            notif_list = self.rt.list_of_notifications
            # Compose message for IPC
            value = ""
            for notif in notif_list:
                value = "{}{} ".format(value, notif)
            # /for all notifications
            # Remove redundant space
            value = value[:-1]

            variable = "list_of_notifications"
        else:
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_notif()

    def _proc_req_rply_rndm(self, cmd, val):
        if (cmd == "rndm_get_animations"):
            anim_list = self.rndm.get_animations()
            # Since result will be automatically split by spaces, let's replace
            # spaces in every animation name to "_" character
            anim_list = self.ca_cmn.rplc_spc_wth_undrscr_in_str_lst(anim_list)

            # Now put it into one string
            value = ""
            for anim in anim_list:
                value = "{}{} ".format(value, anim)
            # And remove last space
            value = value[:-1]

            variable = "animations"
        elif (cmd == "rndm_get_animation"):
            value = self.rndm.get_animation()
            variable = "animation"
        elif (cmd == "rndm_set_animation"):
            variable = "status"
            # If there was space in name, need to merge it. Else don't.
            # If there was space, list is returned
            if (type(val) is list):
                anim_name = self.ca_cmn.restore_msg(val)
            else:
                anim_name = val
            # Modify string message

            try:
                self.rndm.set_animation(animation_name=anim_name)
            except Exception as e:
                # If something goes wrong, report it
                value = e
            else:
                # Else animation found -> update image
                self.leds = self.rndm.get_img()

                # Check timer -> if paused, play
                if (self.rndm.get_animation() is not None):
                    # Paused (not played anything yet -> play)
                    self.ipc.timer_send_req(cmd="play",
                                            val="_proc_req_rply_rndm")
                value = "OK"
        else:
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_rndm()
    # |ConsoleAPIBgCls

    def _proc_req_rply_edit(self, cmd, val):
        if (cmd == "edit_can_undo"):
            variable = "can_undo"
            undo_redo = self.get_runtime_mode_obj()
            value = undo_redo.can_undo()
        elif (cmd == "edit_can_redo"):
            variable = "can_redo"
            undo_redo = self.get_runtime_mode_obj()
            value = undo_redo.can_redo()
        else:
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_edit()
    # |ConsoleAPIBgCls

    def _proc_req_rply_load_save(self, cmd, val):
        """Check if command is related to load/save request

        :returns: True if cmd is one of the wall command
        :rtype: bool
        """
        if (cmd == "save_anim"):
            # Set data source according to mode
            mode = self.get_mode()
            if (mode == "anim"):
                data = self.rt.modes.anim.get_all_cubes()
            elif (mode == "draw"):
                data = [self.leds]
            else:
                msg = "Unsupported mode {}".format(mode)
                self.logger.error(msg)
                raise Exception(msg)

            try:
                self.load_save.save_animation(filename=val, data=data,
                                              timer=self.rt.get_timer())
            except Exception as e:
                msg = "Saving to file {} failed!\n\n{}".format(val, e)
                self.logger.error(msg)
                value = msg
            else:
                # Else no exception -> OK
                value = "OK"

            variable = "status"
        elif (cmd == "load_anim"):
            try:
                data, timer = self.load_save.load_animation(filename=val)
            except Exception as e:
                msg = "Loading from file {} failed\n\n{}".format(val, e)
                self.logger.error(msg)
                value = msg
            else:
                value = "OK"
                # Stop animation, update dimension (if needed) & set timer
                self.rt.modes.anim.stop()
                new_dim = data[0].get_dimension()
                old_dim = self.get_dimension()
                if (old_dim != new_dim):
                    msg = 'Dimension changed from {} to {}' \
                          ''.format(old_dim, new_dim)
                    self.logger.warn(msg)
                    self.set_dimension(dimension=new_dim)
                    # Let console API know
                    value = "Dimension_changed"
                # /dimension check

                self.rt.set_timer(time_ms=timer)
                self.ipc.timer_send_req(cmd="set_timer", val=timer)

                # According to mode push data to correct structure
                mode = self.get_mode()
                if (mode == "anim"):
                    data = self.rt.modes.anim.set_all_cubes(data)
                    self.leds = self.rt.modes.anim.get_actual_cube()
                elif (mode == "draw"):
                    self.leds = data[0]
                    if (len(data) > 1):
                        msg = "Since this is only image mode, only first" \
                              " image will be shown (total {} images)" \
                              "".format(len(data))
                        self.logger.warn(msg)
                    # /check data length
                else:
                    msg = "Unsupported mode {}".format(mode)
                    self.logger.error(msg)
                    raise Exception(msg)

                # Loaded new image anyway -> update needed
                self.update_cube()
            # /load data
            variable = "status"
        # /load anim
        elif (cmd == "append_anim"):
            try:
                data, timer = self.load_save.load_animation(filename=val)
            except Exception as e:
                msg = "Loading from file {} failed\n\n{}".format(val, e)
                self.logger.error(msg)
                value = msg
            else:
                # Check cube dimension (have to match)
                if (data[0].get_dimension() != self.get_dimension()):
                    msg = "Dimensions are not match! Can not merge animations"
                    self.logger.error(msg)
                    value = msg
                else:
                    # Dimension match
                    value = "OK"
                    # Stop animation. No need to change timer (keep actual)
                    self.rt.modes.anim.stop()

                    # According to mode add data to correct structure
                    mode = self.get_mode()
                    if (mode == "anim"):
                        data = self.rt.modes.anim.add_cubes(data)
                        self.leds = self.rt.modes.anim.get_actual_cube()
                    elif (mode == "draw"):
                        self.leds = data[0]
                        if (len(data) > 1):
                            msg = "Since this is only image mode, only first" \
                                  " image will be shown"
                            self.logger.warn(msg)
                        # /check data length
                    else:
                        msg = "Unsupported mode {}".format(mode)
                        self.logger.error(msg)
                        raise Exception(msg)

                    # Loaded new image anyway -> update needed
                    self.update_cube()
                # /else dimension match
            # /load data
            variable = "status"
        # /append animation
        else:
            # Unknown command
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_load_save()

    def _proc_req_rply_play(self, cmd, val):
        """Check if command is related to play request

        :returns: True if cmd is one of the wall command
        :rtype: bool
        """
        if (cmd == "play_get_act_idx"):
            variable = "idx"
            value = self.rt.modes.anim.get_actual_idx()
        elif (cmd == "play_get_max_idx"):
            variable = "idx"
            value = self.rt.modes.anim.get_max_idx()
        elif (cmd == "play_get_status"):
            variable = "status"
            value = self.rt.modes.anim.get_status()
        elif (cmd == "play_get_repeat"):
            variable = "repeat"
            value = self.rt.modes.anim.get_repeat()
        else:
            # Unknown command -> return False
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_play()

    def _proc_req_rply_main(self, cmd, val):
        """Process main requests

        Some requests are simply general and they can not be put to any other
        function. So for this is there this function
        """
        if (cmd == "get_dimension"):
            variable = "dimension"
            value = self.get_dimension()
        elif (cmd == "get_timer"):
            variable = "timer"
            value = self.rt.get_timer()
        elif (cmd == "get_mode"):
            variable = "mode"
            value = self.rt.mode
        elif (cmd == "ping"):
            variable = "pong"
            value = "Alive!"
        else:
            # Unknown command -> return False
            return (False)
        # Command known -> send response
        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_main()

    def _proc_req_rply_pos(self, cmd, val):
        """Request for some position function"""
        if (cmd == "get_zoom"):
            variable = "zoom"
            value = self.cube_3D_w.get_zoom()
        elif (cmd == "get_angle_x"):
            variable = "angle_x"
            value = self.cube_3D_w.get_angle_x()
        elif (cmd == "get_angle_y"):
            variable = "angle_y"
            value = self.cube_3D_w.get_angle_y()
        elif (cmd == "get_angle_z"):
            variable = "angle_z"
            value = self.cube_3D_w.get_angle_z()
        else:
            return (False)

        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_pos()

    def _proc_req_rply_pwm(self, cmd, val):
        """PWM request"""
        if (cmd == "get_pwm"):
            variable = "pwm"
            value = "{} {} {}".format(self.leds.pwm.r,
                                      self.leds.pwm.g,
                                      self.leds.pwm.b)
        else:
            return (False)

        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_pwm()

    def _proc_req_rply_wall(self, cmd, val):
        """Wall requests"""
        if (cmd == "get_active_wall"):
            variable = "wall_idx"
            value = self.get_active_wall()
        else:
            return (False)

        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_wall()

    def _proc_req_rply_view(self, cmd, val):
        """View requests
        """
        if (cmd == "view_get_wall_highlight"):
            variable = "highlight"
            value = self.rt.highlight_active_wall
        elif (cmd == "view_get_show_disabled"):
            variable = "show_disabled"
            value = self.cube_3D_w.get_show_disabled()
        elif (cmd == "view_get_show_inactive"):
            variable = "show_inactive"
            value = self.cube_3D_w.get_show_inactive()
        else:
            return (False)

        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_view()

    def _proc_req_rply_real_cube(self, cmd, val):
        """Real cube related requests"""
        if (cmd == "real_cube_get_dev_list"):
            variable = "dev_list"

            # Create device list as string separated by spaces
            value = ""
            for device in self.real_cube.get_device_list():
                value = "{} {}".format(value, device)
            # /for all supported devices
            # Remove first space, since it is redundant
            value = value[1:]
        # /if get real cube device list
        elif (cmd == "real_cube_connect"):
            variable = "real_cube_connect_status"

            try:
                self.real_cube.connect(dev_name=val)
            except RuntimeError:
                # Runtime error -> respond NG
                value = "NG"

                # Just a warning. Not reason to crash
                msg = "real_cube_connect to {} FAILED :/".format(val)
                self.logger.warn(msg)
            except Exception as e:
                # Another exception occurs -> probably internal error
                value = "Internal_error!"

                msg = "real_cube_conect to {} raised exception!\n\n{}" \
                      "".format(val, e)
                self.close(reason=msg)
                self.logger.critical(msg)
            else:
                # No exception -> connected
                value = "OK"

                msg = "real_cube_connect: cube {} connected :)".format(val)
                self.logger.info(msg)

                # Check dimension
                new_dim = self.real_cube.get_dimension()
                if (new_dim != self.get_dimension()):
                    # Dimension need to be changed
                    self.set_dimension(dimension=new_dim)
                    value = "Dimension_changed"
                # /if dimension does not match
            # /try connect

            # And automatically update image
            self.update_cube()
        # /if real cube connect
        elif (cmd == "get_real_cube_status"):
            variable = "status"
            value = self.real_cube.get_status()
        # /if real cube get status
        elif (cmd == "real_cube_disconnect"):
            variable = "real_cube_disconnect_status"

            try:
                self.real_cube.disconnect()
            except RuntimeError:
                # Runtime error -> respond NG
                value = "NG"

                # Just a warning. Not reason to crash
                msg = "real_cube_disconnect FAILED :/"
                self.logger.warn(msg)
            except Exception as e:
                # Another exception occurs -> probably internal error
                value = "Internal error!"

                msg = "real_cube_disconect raised exception!\n\n{}" \
                      "".format(e)
                self.close(reason=msg)
                self.logger.critical(msg)
            else:
                # No exception -> connected
                value = "OK"

                msg = "real_cube_disconnect: cube disconnected :)"
                self.logger.info(msg)
            # /try disconnect
        else:
            return (False)

        self.ipc.req_rply_send_response(variable, value)
        return (True)

    # /_proc_req_rply_real_cube()

    # =========================| /Process request reply |=====================

    # =================| Mostly common functions for functions |==============

    def _set_active_wall(self, wall_idx):
        self.rt.active_wall = wall_idx

        # If all walls are selected or if highlight is disabled
        if ((self.get_active_wall() is None) or
                (not self.rt.highlight_active_wall)):
            # No need to highlight
            self.cube_3D_w.set_active_wall(wall_idx=None)
        else:
            # User want to highlight selected wall
            self.cube_3D_w.set_active_wall(wall_idx=wall_idx)
        # /else user want to highlight certain wall

        self.update_cube()

    # /_set_active_wall()

    def _set_mode_draw(self):
        """Prepare variables for draw mode"""
        self.logger.info('Setting "Draw" mode...')

        # Fill pointed to LED structure
        self.leds = self.rt.modes.draw.get_cube()

    # /_set_mode_draw()

    def _set_mode_anim(self):
        """Prepare variables for anim mode"""
        self.logger.info('Setting "Anim" mode...')
        # Fill pointer to LED structure
        self.leds = self.rt.modes.anim.get_actual_cube()

    # /_set_mode_anim()

    def _set_mode_random(self):
        """Prepare variables for random mode"""
        # Active wall is None, highlight settings does not matter
        self._set_active_wall(wall_idx=None)

        # Basically everything is handled by self.rndm, so not much to do here

        # If some animation is selected -> play timer
        if (self.rndm.get_animation() is not None):
            # Set pointer to "leds"
            self.leds = self.rndm.get_img()
            self.ipc.timer_send_req(cmd="play", val="set_mode")

    # /_set_mode_random()

    def _update_real_cube_image_if_connected(self):
        """Update cube image in real cube, if real cube is connected"""
        if (self.real_cube.get_status() == "Connected"):
            try:
                self.real_cube.show_img(self.leds)
            except RuntimeError as e:
                # Runtime error can occur when for example user disconnect
                # real cube
                msg = "Real cube reported runtime error: {}".format(e)
                self.logger.error(msg)

                if (self.real_cube.get_status() != "Connected"):
                    self.ipc.send_notification(
                        cmd="hw_cube_disconnected",
                        val="User probably disconnected cube from computer")
        # /if cube is connected
    # /_update_real_cube_image_if_connected()
# /ConsoleAPIBgCls

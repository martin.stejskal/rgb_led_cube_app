#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Handle position of 3D view
===================================

This module works with positioning of 3D model view

``author`` Martin Stejskal

``version`` 0.3.1
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIPosCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def set_zoom(self, zoom_value=1):
        """Set zoom value

        :param zoom_value: Zoom value
        :type zoom_value: float
        """
        self.ipc.send_req(cmd="set_zoom", val=zoom_value)

    # /set_zoom()

    def get_zoom(self):
        """Get zoom value

        :returns: Zoom value
        :rtype: float
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_zoom")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="zoom",
                                        function_name="get_zoom()")
        return (float(value))

    # /get_zoom()

    def set_angle_x(self, deg=0):
        """Set camera angle in X axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self.ipc.send_req(cmd="set_angle_x", val=deg)

    # /set_angle_x()

    def get_angle_x(self):
        """Get camera angle in X axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._get_angle("x"))

    # /get_angle_x()

    def set_angle_y(self, deg=0):
        """Set camera angle in Y axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self.ipc.send_req(cmd="set_angle_y", val=deg)

    # /set_angle_x()

    def get_angle_y(self):
        """Get camera angle in Y axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._get_angle("y"))

    # /get_angle_y()

    def set_angle_z(self, deg=0):
        """Set camera angle in Z axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self.ipc.send_req(cmd="set_angle_z", val=deg)

    # /set_angle_z()

    def get_angle_z(self):
        """Get camera angle in Z axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._get_angle("z"))

    # /get_angle_x()

    def rot_left(self, deg=5):
        """Rotate cube left

        :param deg: Define rotation angle
        :type deg: int
        """
        self.set_angle_y(self.get_angle_y() + deg)

    # /rot_left()

    def rot_right(self, deg=5):
        """Rotate cube right

        :param deg: Define rotation angle
        :type deg: int
        """
        self.set_angle_y(self.get_angle_y() - deg)

    # /rot_right()

    def rot_up(self, deg=5):
        """Rotate cube up

        :param deg: Define rotation angle
        :type deg: int
        """
        self.set_angle_x(self.get_angle_x() + deg)

    # /rot_up()

    def rot_down(self, deg=5):
        """Rotate cube down

        :param deg: Define rotation angle
        :type deg: int
        """
        self.set_angle_x(self.get_angle_x() - deg)

    # /rot_down()

    def reset_cube_view(self):
        """Set all rotation angles to 0 and reset zoom"""
        self.ipc.send_req(cmd="reset_cube_view")

    # /reset_cube_view()

    # ===========================| Internal functions |=======================
    def _get_angle(self, axe="x"):
        """Function that simplify getting angle

        :param axe: Define axe: x, y, z
        :type axe: str
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_angle_{}"
                                                         "".format(axe))

        self.ca_cmn.check_variable_name(
            received_var=variable,
            expected_var="angle_{}".format(axe),
            function_name="get_angle_{}()".format(axe))
        return (int(value))
    # /_get_angle()
# /ConAPIPosCls

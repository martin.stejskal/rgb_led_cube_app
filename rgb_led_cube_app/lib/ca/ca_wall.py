#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Select active wall
===================================

This module purpose is selecting actual wall. It also can highlight actual
wall at 3D view.

``author`` Martin Stejskal

``version`` 0.6.1
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIWallCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def set_active(self, wall_idx=None):
        """Set active wall

        Information about active wall can be useful for GUI and
        transformations with cube

        :param wall_idx: Wall index or None if all walls are active
        :type wall_idx: int, None

        ToDo: Parameter check (optional parameter)
        """
        self.ipc.send_req(cmd="set_active_wall", val=wall_idx)

    # /set_active()

    def get_active(self):
        """Returns active wall index

        :returns: Index of active wall. If whole cube is active (all walls),
                  None is returned
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_active_wall")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="wall_idx",
                                        function_name="get_active")

        if (value == "None"):
            return (None)
        else:
            return (int(value))
    # /get_active()
# /ConAPIWallCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Matrix animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
# For generating random numbers
from random import randrange


class MatrixCls:

    def __init__(self, dimension, op):
        self.name = "Matrix"
        self.dim_min = 2
        self.dim_max = 0

        self.dim = dimension
        self.op = op

        self._cube = CubeLEDCls(dimension)

        # We want only green PWM channel. Others are pointless
        self._cube.pwm.r = 0
        self._cube.pwm.g = 255
        self._cube.pwm.b = 0

        # Turn off all LEDs
        self.op.cube.clear_all(self._cube)

        # Keep all lines informations
        self.lines = []

        # Define maximum number of lines at whole cube
        self.max_num_lines = int(self.dim * 2.3)

        # Define minimum number of lines at whole cube
        self.min_num_lines = int(self.dim / 2)

        # Maximum line length
        self.max_line_lngth = self.dim * 1.2

        # Add random number of lines
        for i in range(randrange(1, self.max_num_lines, 1)):
            self.lines.append(self.LineCls(self.dim, self.max_line_lngth))

    # /__init__()

    def get_img(self):
        # Rotation down
        self.op.cube.t_top2btm(cube=self._cube, shifts=1, repeat=False)

        # It is expected that by time some lines became pointless (already
        # showed), so we will need to clear actual list of lines. Actually
        # it is easier to create new one
        actual_lines = []

        # Go through every line and turn on specified LEDs. If needed
        # mark actual line for removal
        for idx in range(len(self.lines)):
            wall_idx = self.lines[idx].wall
            column_idx = self.lines[idx].column

            # Enable LED
            self._cube.w[wall_idx].c[column_idx].led[self.dim - 1].g = 1

            # Decrease line length
            self.lines[idx].len = self.lines[idx].len - 1

            # Is actual line still valid?
            if (self.lines[idx].len > 0):
                actual_lines.append(self.lines[idx])
            # /if this was last point
        # /for lines

        # Now update list of lines
        self.lines = actual_lines

        # Generate new list if there is "not much" lines. Following numbers
        # are simply only subject values
        if (len(self.lines) < (self.min_num_lines)):
            # We need to add few more lines
            for i in range(randrange(1,
                                     int(self.max_num_lines -
                                         len(self.lines)),
                                     1)):
                self.lines.append(self.LineCls(self.dim,
                                               self.max_line_lngth))
            # /add few lines. Not too much, but at least one
        # /if not much cool lines

        return (self._cube)

    # /get_img()

    class LineCls:

        def __init__(self, dim, max_len):
            # Generate line position and length
            self.wall = randrange(0, dim, 1)
            self.column = randrange(0, dim, 1)

            # Length
            self.len = randrange(1, int(max_len), 1)
        # /__init__()
    # /LineCls
# /matrix

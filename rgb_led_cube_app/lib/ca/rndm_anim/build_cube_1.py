#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Building cube 1 animation
==============================================

``author`` Martin Stejskal

``version`` 0.0.2
"""
from lib.cube_LED import CubeLEDCls
from random import randrange


class BuildCube1Cls:
    def __init__(self, dimension, op):
        self.name = "Build white cube"
        self.dim_min = 2
        self.dim_max = 0

        self.op = op

        self.dim = dimension
        # Total number of LEDs
        self.led_total = self.dim * self.dim * self.dim

        # Generate cube, turn off all LEDs
        self._cube = CubeLEDCls(dimension)
        self.op.cube.clear_all(self._cube)

        # First we want to build cube until all LEDs are on
        self.led_on_cnt = 0
        self.build = True

    # /__init__()

    def get_img(self):
        # Pick random coordinates -> if already on, re-pick
        if (self.build):
            # Turn on random LED, which is off right now
            led_found = False
            while (not led_found):
                wall_idx = randrange(0, self.dim, 1)
                clmn_idx = randrange(0, self.dim, 1)
                led_idx = randrange(0, self.dim, 1)

                led = self._cube.w[wall_idx].c[clmn_idx].led[led_idx]
                if (not led.r):
                    # LED is still off -> on
                    led.r = led.g = led.b = 1
                    led_found = True
                # /if LED is off
            # /while not found off LED

            self.led_on_cnt = self.led_on_cnt + 1
            if (self.led_on_cnt >= self.led_total):
                # Next time disable LEDs
                self.build = False
        else:
            # Turn off random LED - LED have to be already on
            led_found = False
            while (not led_found):
                wall_idx = randrange(0, self.dim, 1)
                clmn_idx = randrange(0, self.dim, 1)
                led_idx = randrange(0, self.dim, 1)

                led = self._cube.w[wall_idx].c[clmn_idx].led[led_idx]
                if (led.r):
                    # LED is on -> can off
                    led.r = led.g = led.b = 0
                    led_found = True
                # /if LED is on
            # /while not found ON LED

            self.led_on_cnt = self.led_on_cnt - 1
            if (self.led_on_cnt <= 0):
                # Next time enable LEDs
                self.build = True
        # /disassemble cube

        return (self._cube)
    # /get_img()
# /BuildCube1Cls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Breathing cube 3 animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
# For generating random numbers
from random import randrange


class BrthngCbe3Cls:

    def __init__(self, dimension, op):
        # Following variables are mandatory for ALL modes
        self.name = "Breathing cube 3"
        # Minimal and maximal dimension, which is supported by this
        # mode 0 means no limit, but for minimal it does not make
        # too much sense
        self.dim_min = 1
        self.dim_max = 0

        # Operations with cube
        self.op = op

        # Create base LED cube object which contains only LED (no GUI
        # references).
        self._cube = CubeLEDCls(dimension)

        # Turn off all LED - simple
        self.op.cube.clear_all(self._cube)

        # Internal values
        class StateStruct():

            def __init__(self):
                self.pwm = 0

                self.up_dir = True

                self.r = 0
                self.g = 0
                self.b = 0
            # /__init__()

        # /StateStruct

        self.state = StateStruct()

    def get_img(self):
        # Check if we start from begin
        if (self.state.pwm == 0):

            self.state.r = 0
            self.state.g = 0
            self.state.b = 0

            self.op.cube.clear_all(self._cube)

            # Randomly pick RGB. But at least one have to be active
            while ((self.state.r == 0) and
                   (self.state.g == 0) and
                   (self.state.b == 0)):
                self.state.r = randrange(0, 2, 1)
                self.state.g = randrange(0, 2, 1)
                self.state.b = randrange(0, 2, 1)
            # /while picking randomly RGB

            self.op.cube.set_rgb(self._cube,
                                 self.state.r,
                                 self.state.g,
                                 self.state.b)
        # /if state.pwm == 0

        # Update PWM value on cube
        self._cube.pwm.r = self._cube.pwm.g = self._cube.pwm.b = \
            self.state.pwm
        # Prepare values for next round
        if (self.state.up_dir):
            # Increase PWM
            self.state.pwm = self.state.pwm + 1

            # Check value
            if (self.state.pwm >= 255):
                self.state.up_dir = False
        else:
            # Decrease PWM
            self.state.pwm = self.state.pwm - 1

            # Check value
            if (self.state.pwm <= 0):
                self.state.up_dir = True
        # /else decrease PWM
        return (self._cube)
    # /get_img()
# /brthng_cbe_3

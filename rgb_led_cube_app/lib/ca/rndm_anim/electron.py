#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Electron animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
# For generating random numbers
from random import randrange


class ElectronCls:

    def __init__(self, dimension, op):
        self.name = "Electron"
        self.dim_min = 2
        self.dim_max = 0

        self.dimension = dimension

        self.op = op

        self._cube = CubeLEDCls(dimension)
        self._cube.pwm.r = self._cube.pwm.g = self._cube.pwm.b = 255

        # Timers
        #  * Simple time counter
        self.time_cnt = 0
        #  * Define when electron will be "launched"
        #    There is negative number to be sure, that first condition
        #    will be true
        self.time_trigger_el = -2 * (self.dimension)

        # Keep information about electron "wall", "column" and "led"
        # position
        self.el_wll = None
        self.el_clm = None
        self.el_led = None

    def get_img(self):
        # Check if this is last iteration
        if ((self.time_cnt - self.time_trigger_el) >= self.dimension):
            self.time_cnt = 0

            # If time is 0, need to set time trigger
            self.time_trigger_el = randrange(0, 12, 1)

            # Prepare image
            self.op.cube.clear_all(self._cube)

            # Bottom layer
            self.op.cube.set_floor_b(self._cube, 0)
            # Top layer
            self.op.cube.set_floor_r(self._cube, self.dimension - 1)
        # /if time_cnt == 0

        # If Launch electron
        if (self.time_cnt == self.time_trigger_el):
            # Launch electron -> generate random coordinates
            self.el_wll = randrange(0, self.dimension, 1)
            self.el_clm = randrange(0, self.dimension, 1)

            # Electron starts from bottom
            self.el_led = 0

            led = self._cube.w[
                self.el_wll].c[
                self.el_clm].led[
                self.el_led]
            led.r = led.g = 1
            led.b = 0
            self.el_led = self.el_led + 1
        # /if launch electron

        # Check if "electron was launched"
        if (self.time_cnt > self.time_trigger_el):
            # Electron is launched -> need to re-draw image
            self.op.cube.clear_all(self._cube)
            self.op.cube.set_floor_b(self._cube, 0)
            self.op.cube.set_floor_r(self._cube, self.dimension - 1)

            led = self._cube.w[
                self.el_wll].c[
                self.el_clm].led[
                self.el_led]
            led.r = led.g = 1
            led.b = 0
            self.el_led = self.el_led + 1
        # /if electron was launched

        self.time_cnt = self.time_cnt + 1

        return (self._cube)
    # /get_img()
# /electron

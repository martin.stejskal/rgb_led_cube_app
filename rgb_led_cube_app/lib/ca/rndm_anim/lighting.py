#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Lighting animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
# For generating random numbers
from random import randrange


class LightningCls:

    def __init__(self, dimension, op):
        self.name = "Lightning"
        self.dim_min = 3
        self.dim_max = 0

        self.dimension = dimension

        # Cube operation functions
        self.op = op

        self._cube = CubeLEDCls(dimension)

        self.op.cube.clear_all(self._cube)
        self._cube.pwm.r = 255
        self._cube.pwm.g = 255
        self._cube.pwm.b = 255

        self.cnt = 0
        self.turn_off_time = 10

    def get_img(self):
        if (self.cnt == 0):
            # Pick a random wall, column and LED and turn it on
            wall = randrange(0, self.dimension, 1)
            column = randrange(0, self.dimension, 1)
            led = randrange(0, self.dimension, 1)

            self._cube.w[wall].c[column].led[led].r = \
                self._cube.w[wall].c[column].led[led].g = \
                self._cube.w[wall].c[column].led[led].b = 1
        elif (self.cnt == 1):
            self.op.cube.clear_all(self._cube)
        else:
            self.cnt = self.cnt + 1
            if (self.cnt >= self.turn_off_time):
                self.cnt = -1

        self.cnt = self.cnt + 1

        return (self._cube)
    # /get_img()
# /lightning

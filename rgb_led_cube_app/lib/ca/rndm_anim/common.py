#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Common functions/classes for random animations
==============================================


``author`` Martin Stejskal

``version`` 0.0.2
"""
from random import randrange


class RandomPWMFluentCls:
    """Fluently change PWM value
    """

    def __init__(self, min_pwm=30, max_pwm=255, step=3, buff_size=15,
                 init_r=127, init_g=127, init_b=127):
        # Store parameters in the instance
        self.min_pwm = min_pwm
        self.max_pwm = max_pwm
        self.step = step
        self.buff_size = buff_size
        self.r = init_r
        self.g = init_g
        self.b = init_b

        # Create circular buffer & load initial values
        self.v_r = []
        self.v_g = []
        self.v_b = []

        for i in range(buff_size):
            self.v_r.append(randrange(0, 255, 1))
            self.v_g.append(randrange(0, 255, 1))
            self.v_b.append(randrange(0, 255, 1))

    # /__init__()

    def get(self):
        """Returns pwm values

        :returns: PWM values for R, G & B
        :rtype: int (0~255), int (0~255), int (0~255)
        """
        # Remove first item from buffer
        self.v_r = self.v_r[1:]
        self.v_g = self.v_g[1:]
        self.v_b = self.v_b[1:]

        # Get new random values
        self.v_r.append(randrange(0, 255, 1))
        self.v_g.append(randrange(0, 255, 1))
        self.v_b.append(randrange(0, 255, 1))

        # Get average value
        ar = ag = ab = 0
        for val in self.v_r:
            ar = ar + val
        ar = ar / (len(self.v_r))

        for val in self.v_g:
            ag = ag + val
        ag = ag / (len(self.v_g))

        for val in self.v_b:
            ab = ab + val
        ab = ab / (len(self.v_b))

        # Now decide about destiny of RGB. Also threshold is not fixed
        if (ar > randrange(120, 134)):
            mul_r = 1
        else:
            mul_r = -1

        if (ag > randrange(120, 134)):
            mul_g = 1
        else:
            mul_g = -1

        if (ab > randrange(120, 134)):
            mul_b = 1
        else:
            mul_b = -1

        # Change PWM value (randrange(start, stop, step)
        self.r = self.r + (self.step * mul_r)
        self.g = self.g + (self.step * mul_g)
        self.b = self.b + (self.step * mul_b)

        # Check PWM value limits
        if (self.r > self.max_pwm):
            self.r = self.max_pwm - self.step
        if (self.r < self.min_pwm):
            self.r = self.min_pwm + self.step
        if (self.g > self.max_pwm):
            self.g = self.max_pwm - self.step
        if (self.g < self.min_pwm):
            self.g = self.min_pwm + self.step
        if (self.b > self.max_pwm):
            self.b = self.max_pwm - self.step
        if (self.b < self.min_pwm):
            self.b = self.min_pwm + self.step

        # Round numbers
        self.r = int(self.r)
        self.g = int(self.g)
        self.b = int(self.b)

        return (self.r, self.g, self.b)
# /RandomPWMFluentCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Breathing cube 1 animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
from lib.ca.rndm_anim.common import RandomPWMFluentCls


class BrthngCbe1Cls:

    def __init__(self, dimension, op):
        self.name = "Breathing cube 1"
        self.dim_min = 1
        self.dim_max = 0

        # Functions for operations with cube
        self.op = op

        # Prepare cube
        self._cube = CubeLEDCls(dimension)

        # Turn all LED - simple
        self.op.cube.set_all(self._cube)

        self.pwm = RandomPWMFluentCls()

    # /__init__()

    def get_img(self):
        # Get new random values & set PWM values
        self._cube.pwm.r, self._cube.pwm.g, self._cube.pwm.b = self.pwm.get()

        return (self._cube)
# /brthng_cbe_1

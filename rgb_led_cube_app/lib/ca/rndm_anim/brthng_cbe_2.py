#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Breathing cube 2 animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
from lib.ca.rndm_anim.common import RandomPWMFluentCls


class BrthngCbe2Cls:

    def __init__(self, dimension, op):
        # Following variables are mandatory for ALL modes
        self.name = "Breathing cube 2"
        # Minimal and maximal dimension, which is supported by this
        # mode 0 means no limit, but for minimal it does not make
        # too much sense
        self.dim_min = 3
        self.dim_max = 0

        # Operations with cube
        self.op = op

        # Create base LED cube object which contains only LED (no GUI
        # references).
        self._cube = CubeLEDCls(dimension)

        # Turn on only LED on edges
        self.op.cube.clear_all(self._cube)

        # Now we turn on only selected LED. It is kind a algorithmic
        for wall in [0, dimension - 1]:
            # Front and back "pillars"
            for clmn in [0, dimension - 1]:
                self.op.column.set_all(self._cube, wall, clmn)

            # Front and back horizontal lines
            for clmn in range(1, dimension - 1):
                for led in [0, dimension - 1]:
                    self._cube.w[wall].c[clmn].led[led].r = 1
                    self._cube.w[wall].c[clmn].led[led].g = 1
                    self._cube.w[wall].c[clmn].led[led].b = 1

        # lines between front and back 2D cube
        for wall in range(1, dimension - 1):
            for clmn in [0, dimension - 1]:
                for led in [0, dimension - 1]:
                    self._cube.w[wall].c[clmn].led[led].r = 1
                    self._cube.w[wall].c[clmn].led[led].g = 1
                    self._cube.w[wall].c[clmn].led[led].b = 1

        self.pwm = RandomPWMFluentCls()

    # /__init__()

    def get_img(self):
        # Get new random values & set PWM values
        self._cube.pwm.r, self._cube.pwm.g, self._cube.pwm.b = self.pwm.get()

        return (self._cube)
# /brthng_cbe_2

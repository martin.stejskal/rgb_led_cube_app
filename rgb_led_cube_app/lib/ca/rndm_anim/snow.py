#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============================================
Snow animation
==============================================

``author`` Martin Stejskal

``version`` 1.0.1
"""
from lib.cube_LED import CubeLEDCls
# For generating random numbers
from random import randrange


class SnowCls:
    def __init__(self, dimension, op):
        self.name = "Snow"
        self.dim_min = 2
        self.dim_max = 0

        self.dim = dimension
        self.op = op

        # Initialize cube
        self._cube = CubeLEDCls(dimension)
        self._cube.pwm.r = 255
        self._cube.pwm.g = 255
        self._cube.pwm.b = 255

        # Turn off all LEDs
        self.op.cube.clear_all(self._cube)

        self.snowflakes = []

        self.min_snowflakes = int(self.dim * self.dim * 0.7)
        self.max_spawn = int(self.dim * self.dim / 3)

        self.snowflakes.append(self._SnowflakeCls(self.dim))

        # And render snowflakes
        for snowflake in self.snowflakes:
            led = self._cube.w[
                snowflake.wall].c[
                snowflake.column].led[
                self.dim - 1]
            led.r = 1
            led.g = 1
            led.b = 1

    # /__init__()

    def get_img(self):
        # Rotation down
        self.op.cube.t_top2btm(cube=self._cube, shifts=1, repeat=False)

        # It is expected that by time some snowflakes became pointless
        # (already showed), so we will need to clear actual list. But
        # easier is creating new list
        actual_snowflakes = []

        # Remove unused "snowflakes"
        for snowflake in self.snowflakes:
            snowflake.lifetime = snowflake.lifetime - 1
            if (snowflake.lifetime != 0):
                actual_snowflakes.append(snowflake)
        # /for all snowflakes

        self.snowflakes = actual_snowflakes

        # Need to add some more?
        if (len(self.snowflakes) < (self.min_snowflakes)):
            for i in range(
                    randrange(
                        1,
                        self.max_spawn,
                        1)):
                sf = self._SnowflakeCls(self.dim)
                # Render snowflake
                self._cube.w[sf.wall].c[sf.column].led[self.dim - 1].r = 1
                self._cube.w[sf.wall].c[sf.column].led[self.dim - 1].g = 1
                self._cube.w[sf.wall].c[sf.column].led[self.dim - 1].b = 1
                self.snowflakes.append(sf)
            # /generate snowflake
        # /if need generate more snowflakes
        return (self._cube)

    # /get_img()

    class _SnowflakeCls:
        def __init__(self, dim):
            self.wall = randrange(0, dim, 1)
            self.column = randrange(0, dim, 1)
            self.lifetime = dim
    # /_SnowflakeCls
# /SnowCls

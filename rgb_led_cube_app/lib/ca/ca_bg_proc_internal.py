#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
======================================
Internal things for background process
======================================

Some internal classes were moved from ca_bg_proc here to split code into
smaller logic parts.

``author`` Martin Stejskal

``version`` 0.2.3
"""
# Multiprocessing and sharing variables through process
from multiprocessing import Process

# Process naming is not something mandatory, but nice to have
try:
    from setproctitle import setproctitle
except Exception:
    pass

from time import sleep, perf_counter

from lib.cube_LED import CubeLEDCls, copy_cube_led
# IPC for communication with console API
from lib.ca.ca_ipc import ConAPIIPCBgProcTimerCls
# Tuned logger
from lib.common import CmnLgrCls


class _ConAPIBgRtCls:
    """Keep runtime variables/functions/classes for background process

    Mainly for ConsoleAPIBgCls.
    """

    # ================================| Classes |=============================
    class ModesCls:

        def __init__(self, dimension, send_notification=None, debug=False):
            """
            :param dimension: Cube dimension. For example 5 means cube 5x5x5.
            :type dimension: int (>0)

            :param send_notification: Function which sends notification to GUI.
                                      In some cases it can be beneficial to
                                      just send asynchronous message instead of
                                      polling.
            :type send_notification: function/None
            """

            self.draw = self.DrawModeCls(
                dimension=dimension, send_notification=send_notification,
                debug=debug)
            self.anim = self.AnimModeCls(
                dimension=dimension, send_notification=send_notification,
                debug=debug)

        # /__init__()

        class DrawModeCls:

            def __init__(self, dimension, send_notification=None, debug=False):
                """
                :param dimension: Cube dimension. For example 5 means cube
                                  5x5x5.
                :type dimension: int (>0)

                :param debug: Enable chatty debug.
                :type debug: bool
                """
                self.dim = dimension
                self.debug = debug
                self.send_notification = send_notification

                self.new()
                self._init_undo_redo()

            # /__init__()
            # |DrawModeCls

            def new(self):
                """New image
                """
                # we need only one cube for drawing
                self._cube = CubeLEDCls(dimension=self.dim)
                return (self._cube)

            # /new()
            # |DrawModeCls

            def get_cube(self):
                """Get actual LED cube structure

                :returns: LED cube
                :rtype: obj (CubeLEDCls)
                """
                return (self._cube)

            # /get_cube()
            # |DrawModeCls

            def set_cube(self, cube):
                """Set actual cube(image)

                :param cube: One LED cube structure
                :type cube: obj (CubeLEDCls)
                """
                self._cube = cube

            # /set_cube()
            # |DrawModeCls

            # ===========================| undo/redo functions |==============
            # Draw class need to keep track about "latest" cube, so that is
            # why everything is passed via this class.
            # |DrawModeCls

            def commit(self, old_cube):
                """Store old cube content to buffer

                When some operation change cube, this function should be
                called. Function will automatically copy old cube to buffer,
                so undo() and redo() can be simply used.
                """
                self._cube = self._undo_redo.commit(old_cube)
                assert old_cube == self._cube
                return (self._cube)

            # /commit()
            # |DrawModeCls

            def undo(self, fail_if_error=True):
                """Undo lost operation

                It basically returns previous LED cube.

                :param fail_if_error: Optional parameter which decide about
                                      failing behavior. If set and operation
                                      can not be done, an exception is raised.
                                      Otherwise fails is ignored and cube in
                                      not changed.
                :type fail_if_error: bool

                :returns: Previous cube image.
                :rtype: obj (CubeLEDCls)
                """
                self._cube = self._undo_redo.undo(fail_if_error)
                return (self._cube)

            # /undo()
            # |DrawModeCls

            def redo(self, fail_if_error=True):
                """Redo last operation

                :param fail_if_error: Optional parameter which decide about
                                      failing behavior. If set and operation
                                      can not be done, an exception is raised.
                                      Otherwise fails is ignored and cube in
                                      not changed.
                :type fail_if_error: bool

                :returns: Next cube image
                :rtype: obj (CubeLEDCls)
                """
                self._cube = self._undo_redo.redo(fail_if_error)
                return (self._cube)

            # /redo()
            # |DrawModeCls

            def can_undo(self):
                """Check if undo operation can be safely performed

                Simply chceck index and respond accordingly

                :returns: Availability of undo command
                :rtype: bool
                """
                return (self._undo_redo.can_undo())

            # /can_undo()
            # |DrawModeCls

            def can_redo(self):
                """Check if redo operation can be safely performed

                Check actual index and maximum index and then decide

                :returns: Availability of redo command
                :rtype: bool
                """
                return (self._undo_redo.can_redo())

            # /can_redo()
            # |DrawModeCls

            # ===========================| Internal functions |===============

            def _init_undo_redo(self):
                if (self.debug):
                    self._undo_redo = _ConAPIBgUndoRedoCls(
                        logger=CmnLgrCls("console_api_bg_internal"),
                        send_notification=self.send_notification)
                else:
                    self._undo_redo = _ConAPIBgUndoRedoCls(
                        send_notification=self.send_notification)
                # Commit initial image -> so user can go back
                self._undo_redo.commit(old_cube=self.get_cube())
            # /_init_undo_redo()
            # |DrawModeCls

        # /DrawModeCls

        class AnimModeCls:

            def __init__(self, dimension, send_notification=None, debug=False):
                """
                :param dimension: Cube dimension. For example 5 means cube
                                  5x5x5.
                :type dimension: int (>0)

                :param send_notification: Function which sends notification to
                                          GUI. In some cases it can be
                                          beneficial to just send asynchronous
                                          message instead of polling.
                :type send_notification: function/None

                :param debug: Enable chatty debug.
                :type debug: bool
                """
                self.dim = dimension
                self.debug = debug
                self.send_notification = send_notification
                # Animation need to keep multiple 3D images (_cubes) in memory
                self.new()

                # Status - play/pause. Better to set pause and then play
                # animation
                self._status = "pause"

                # Repeat status
                self._repeat = False

                self._init_undo_redo()

            # /__init__()
            # |AnimModeCls

            def new(self):
                """New animation/image.

                Basically it will wipe all your work with default LED cube

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                self._cubes = []

                self._cubes.append(CubeLEDCls(dimension=self.dim))

                # Actual image index. This is needed for informing GUI to move
                # slidebar every time when _timer sends request to change image
                self._img_idx = 0

                return (self.get_actual_cube())

            # /new()

            def play(self):
                """Change status to "play" and return actual cube

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                # Check actual image number. If we are on last image, we
                # should jump to image 0 and start again. At least this is
                # what I would expect
                if (self.get_actual_idx() >= self.get_max_idx()):
                    self.set_actual_idx(0)

                # And then set status
                self._status = "play"

                return (self.get_actual_cube())

            # /play()

            def pause(self):
                """Change status to "pause" and return actual cube

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                self._status = "pause"

                return (self.get_actual_cube())

            # /pause()

            def stop(self):
                """Change status to "stop" and return actual cube (first one)

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                self.pause()
                self.set_actual_idx(0)

                return (self.get_actual_cube())

            # /stop()

            def get_status(self):
                """Return actual status

                :returns: Play status
                :rtype: str (play/pause)
                """
                return (self._status)

            # |AnimModeCls
            def get_actual_cube(self):
                """Returns actual LED cube

                :returns: Actual cube
                :rtype: obj (CubeLEDCls)
                """
                return (self._cubes[self.get_actual_idx()])

            # /get_actual_cube()

            def set_actual_cube(self, cube):
                """Set actual cube(image)

                :param cube: One LED cube structure
                :type cube: obj (CubeLEDCls)
                """
                self._cubes[self.get_actual_idx()] = cube

            # /set_actual_cube()

            def get_all_cubes(self):
                """Returns all LED cubes

                :returns: List of all LED cubes
                :rtype: list (CubeLEDCls)
                """
                return (self._cubes)

            # /get_all_cubes()

            def set_all_cubes(self, cubes):
                """Set all LED cubes (images)

                :param cubes: List of all LED cubes
                :type cubes: list (CubeLEDCls)
                """
                self._cubes = cubes

            # /set_all_cubes()

            def add_cubes(self, cubes):
                """Add list of cubes to animation

                :param cubes: List of new LED cubes
                :type cubes: list (CubeLEDCls)
                """
                self._cubes.extend(cubes)

            # /append_cubes()

            # |AnimModeCls
            def cube_append(self):
                """This function will add new cube image to "_cubes" variable
                """
                self._cubes.append(CubeLEDCls(dimension=self.dim))

                self.copy_cube(idx_src=(self.get_max_idx() - 1),
                               idx_dst=self.get_max_idx())

            # /cube_append()

            def copy_cube(self, idx_src, idx_dst):
                """Copy content of one cube to another.

                :param idx_src: Index of source cube
                :type idx_src: int

                :param idx_dst: Index of destination cube
                :type idx_dst: int
                """
                copy_cube_led(self._cubes[idx_src], self._cubes[idx_dst])

            # /copy_cube()
            # |AnimModeCls

            def delete_actual_idx(self):
                """Delete actual cube frame.

                If actual index is set to 0 and maximum index is also 0,
                then do nothing
                """
                idx = self.get_actual_idx()
                if ((idx == 0) and (self.get_max_idx() == 0)):
                    return

                # Else delete actual image
                del self._cubes[idx]

                # And decrease actual index (only if not already set to 0)
                if (idx > 0):
                    self.set_actual_idx(idx - 1)

            # /delete_actual_idx()

            def insert_new_image(self, idx):
                """Insert new image at given position

                :param idx: Image index value
                :type idx: int
                """
                # Check input value
                if (idx < 0):
                    raise Exception("Index value can not be lower than 0")
                if (idx > len(self._cubes)):
                    raise Exception("Index value can not be high than {}"
                                    "".format(len(self._cubes)))

                self._cubes.insert(idx,
                                   CubeLEDCls(dimension=self.dim))
                if (self.get_actual_idx() == idx):
                    # Copy image
                    self.copy_cube(idx_src=(idx + 1), idx_dst=idx)

            # /insert_new_image()

            def insert_new_image_actual_idx(self):
                """Insert new image at actual index position
                """
                self.insert_new_image(idx=self.get_actual_idx())

            # /insert_new_image_actual_idx()

            # |AnimModeCls
            def set_actual_idx(self, actual_idx):
                """Set actual index value, which is basically image number

                :param actual_idx: New actual index value
                :type actual_idx: int
                """
                if (actual_idx < 0):
                    raise Exception("Actual index can not be negative")
                if (actual_idx > self.get_max_idx()):
                    raise Exception("Actual index can not be higher than "
                                    "{}".format(self.get_max_idx()))
                self._img_idx = actual_idx

            # /set_actual_idx()

            def get_actual_idx(self):
                """Returns number of actual cube image

                :returns: Index of actual cube
                :rtype: int
                """
                return (self._img_idx)

            # /get_actual_idx()
            # |AnimModeCls

            def get_max_idx(self):
                """Return index of last valid cube.

                This may be useful for knowing how much frames remaining
                during playing animation.
                Maximum value is automatically updated, since it is every
                time "calculated"

                :returns: Maximum index which can be used for cube.
                :rtype: int
                """
                return (len(self._cubes) - 1)

            # /get_max_idx()

            # |AnimModeCls
            def next(self):
                """Move to next image. If image does not exist, create it

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                # Get actual index
                idx = self.get_actual_idx()

                # Check if we are at the last image or not
                if (idx >= self.get_max_idx()):
                    self.cube_append()

                # In both bases, move to another cube
                idx = idx + 1
                self.set_actual_idx(idx)

                return (self.get_actual_cube())

            # /next()
            # |AnimModeCls

            def next_no_new_img(self):
                """Move to next image (no new image is created)

                If image does not exist, change status to "pause" and keep
                maximum possible image number.

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                idx = self.get_actual_idx()
                max_idx = self.get_max_idx()

                if (idx >= max_idx):
                    # Should not be higher than max_idx
                    assert idx == max_idx, \
                        'idx ({}) should be equal to max_idx ({})' \
                        ''.format(idx, max_idx)
                    # Check status - it should be pause. If not, set it
                    # (if repeat is not enabled)
                    if (not self.get_repeat()):
                        if (self.get_status() != "pause"):
                            self.pause()
                    else:
                        # Repeat enabled -> set index to 0
                        self.set_actual_idx(0)
                # /if last image
                else:
                    # Not last image yet -> increase index
                    idx = idx + 1
                    self.set_actual_idx(idx)
                # /not last image -> increase image index

                return (self.get_actual_cube())

            # /next_no_new_img()

            # |AnimModeCls
            def previous(self):
                """Move to previous image

                If it is already at image index 0, it will stay here

                :returns: Actual LED cube
                :rtype: obj (CubeLEDCls)
                """
                idx = self.get_actual_idx()

                # If already at beginning, nothing to do
                if (idx == 0):
                    return

                # Else move back
                idx = idx - 1

                # Set new actual index
                self.set_actual_idx(idx)

                return (self.get_actual_cube())

            # /previous()
            # |AnimModeCls

            def get_repeat(self):
                """Get repeat value

                :returns: Repeat value
                :rtype: bool
                """
                return (self._repeat)

            # /get_repeat()
            # |AnimModeCls

            def set_repeat(self, repeat_enable=False):
                """Set repeat value

                :param repeat_enable: Enable/disable repeat parameter
                :type repeat_enable: bool
                """
                self._repeat = repeat_enable

            # /set_repeat()
            # |AnimModeCls

            def set_pwm_all_cubes(self, r=255, g=255, b=255):
                """Set PWM at all cubes

                :param r: PWM value for red color
                :type r: int (0~255)

                :param g: PWM value for green color
                :type g: int (0~255)

                :param b: PWM value for blue color
                :type b: int (0~255)
                """
                for cube in self._cubes:
                    cube.pwm.r = r
                    cube.pwm.g = g
                    cube.pwm.b = b
                # /for all cubes

            # /set_pwm_all_cubes()
            # |AnimModeCls

            # ===========================| undo/redo functions |==============
            # Anim class need to keep track about "latest" cube, so that is
            # why everything is passed via this class.
            # |AnimModeCls

            def commit(self, old_cube):
                """Store old cube content to buffer

                When some operation change cube, this function should be
                called. Function will automatically copy old cube to buffer,
                so undo() and redo() can be simply used.
                """
                cube = self._undo_redo.commit(self.get_actual_cube())
                assert cube == self.get_actual_cube()
                return (cube)

            # /commit()
            # |AnimModeCls

            def undo(self, fail_if_error=True):
                """Undo lost operation

                It basically returns previous LED cube.

                :param fail_if_error: Optional parameter which decide about
                                      failing behavior. If set and operation
                                      can not be done, an exception is raised.
                                      Otherwise fails is ignored and cube in
                                      not changed.
                :type fail_if_error: bool

                :returns: Previous cube image.
                :rtype: obj (CubeLEDCls)
                """
                self._cubes[self.get_actual_idx()] = \
                    self._undo_redo.undo(fail_if_error)
                return (self.get_actual_cube())

            # /undo()
            # |AnimModeCls

            def redo(self, fail_if_error=True):
                """Redo last operation

                :param fail_if_error: Optional parameter which decide about
                                      failing behavior. If set and operation
                                      can not be done, an exception is raised.
                                      Otherwise fails is ignored and cube in
                                      not changed.
                :type fail_if_error: bool

                :returns: Next cube image
                :rtype: obj (CubeLEDCls)
                """
                self._cubes[self.get_actual_idx()] = \
                    self._undo_redo.redo(fail_if_error)
                return (self.get_actual_cube())

            # /redo()
            # |AnimModeCls

            def can_undo(self):
                """Check if undo operation can be safely performed

                Simply chceck index and respond accordingly

                :returns: Availability of undo command
                :rtype: bool
                """
                return (self._undo_redo.can_undo())

            # /can_undo()
            # |AnimModeCls

            def can_redo(self):
                """Check if redo operation can be safely performed

                Check actual index and maximum index and then decide

                :returns: Availability of redo command
                :rtype: bool
                """
                return (self._undo_redo.can_redo())

            # /can_redo()
            # |AnimModeCls

            # ===========================| Internal functions |===============

            def _init_undo_redo(self):
                if (self.debug):
                    self._undo_redo = _ConAPIBgUndoRedoCls(
                        logger=CmnLgrCls("console_api_bg_internal"),
                        send_notification=self.send_notification)
                else:
                    self._undo_redo = _ConAPIBgUndoRedoCls(
                        send_notification=self.send_notification)
                # Commit initial image -> so user can go back
                self._undo_redo.commit(old_cube=self.get_actual_cube())
            # /_init_undo_redo()
            # |AnimModeCls
        # /AnimModeCls

    # /ModesCls
    # |_ConAPIBgRtCls

    class CopyPasteCls:
        def __init__(self, dimension):
            """
            :param dimension: Cube dimension. For example 5 means cube
                              5x5x5.
            :type dimension: int (>0)
            """
            self._cube = CubeLEDCls(dimension=dimension)

        # /__init__()
        # |CopyPasteCls

        def copy(self, cube):
            """Copy actual cube to buffer

            :param cube: Cube to copy to buffer
            :type cube: obj (CubeLEDCls)
            """
            copy_cube_led(src=cube, dst=self._cube)

        # /copy()

        def paste(self):
            """Paste cube from buffer

            :returns: Cube from buffer
            :rtype: obj (CubeLEDCls)
            """
            cube = CubeLEDCls(dimension=self._cube.get_dimension())
            copy_cube_led(src=self._cube, dst=cube)
            return (cube)
        # /paste()
        # |CopyPasteCls

    # /CopyPasteCls

    def __init__(self, dimension=5, send_notification=None):
        """
        :param dimension: Cube dimension. For example 5 means cube 5x5x5.
        :type dimension: int (>0)

        :param send_notification: Function which sends notification to GUI.
                                  In some cases it can be beneficial to just
                                  send asynchronous message instead of polling.
        :type send_notification: function/None
        """
        # Keep background process running?
        self.keep_running = True

        # Close notification send to upper layer?
        # Normally it is expected, that close request can occur from 3D view
        # (need to notify upper layers) or from upper layer (not need to
        # notify obviously). However in case of crash, we need to make sure
        # that upper layer will know about close event.
        self.sent_close_notification = False

        # Keep information about actual mode
        self.mode = None

        # Enable/disable print messages - good only for develop at special
        # cases -> not worth for logging (too verbose)
        self.debug = True

        # Variables/functions for modes
        self.modes = self.ModesCls(
            dimension=dimension, send_notification=send_notification,
            debug=self.debug)

        # Define which wall is actually selected. None means all/no wall is
        # actually selected
        self.active_wall = None

        # Highlight active wall ?
        self.highlight_active_wall = True

        # Buffer for copy/paste operation
        self.copy_paste = self.CopyPasteCls(dimension=dimension)

        # Timer default value
        # Decent value for binaries (there are performance issues)
        self._timer = 100

        # List of available notifications. This is just double check for
        # developer to avoid typos when sending notifications.
        self.list_of_notifications = [
            "hw_cube_disconnected", "closing", "edit_can_undo_redo",
            "effect_progress", "effect_done"
        ]

    # /__init__()
    # |_ConAPIBgRtCls

    # ===============================| Functions |============================
    def set_timer(self, time_ms=50):
        """Set timer value in ms

        :param time_ms: Time in ms
        :type time_ms: int
        """
        # Very basic check
        if (time_ms < 0):
            raise Exception("Can not work with negative number")

        self._timer = time_ms

    # /set_timer()
    # |_ConAPIBgRtCls

    def get_timer(self):
        """Returns timer value

        :returns: Timer value in ms
        :rtype: int
        """
        return (self._timer)

    # /get_timer()
    # |_ConAPIBgRtCls

    def set_dimension(self, dimension):
        """When there is dimension change, this function should be called

        Point is that dimension value is tight to many things, so changing it
        is not so straightforward.
        """
        self.modes = self.ModesCls(dimension, self.debug)
        self.copy_paste = self.CopyPasteCls(dimension=dimension)
    # /set_dimension()
    # |_ConAPIBgRtCls


# /_ConAPIBgRtCls


class _ConAPIBgUndoRedoCls:
    def __init__(self, buff_size=50, logger=None, send_notification=None):
        """
        :param buff_size: Buffer size. Number of CubeLEDCls which will be
                          kept in memory.
        :type buff_size: int

        :param logger: If programmer need to debug, it is worth to pass
                       logger object, so critical parts will be logged. If
                       logging is not wanted, simply keep this variable empty.
        :type logger: logger/None
        """
        # Parameter check
        if (buff_size < 1):
            msg = 'Buffer size can not be lower than 1!'
            raise ValueError(msg)
        if (buff_size > 10000):
            msg = 'Buffer size exceeded. Lower buffer size.'
            raise ValueError(msg)

        # Limit buffer size. It will be filled eventually by time anyway,
        # so it should be set wisely. Technically it says maximum index used
        # for addressing self._cubes
        self.buff_size = buff_size - 1

        # Keep all cubes at one place
        self._cubes = []

        # User can "jump" between commits, so we need to know which is actual
        self._idx = -1

        # Since from beginning self._cubes is empty, program need to know
        # which record is last one.
        self._idx_max = -1

        # Logger
        if (logger is None):
            self._show_debug_msg = False
        else:
            self._show_debug_msg = True
            self.logger = logger

        # Sending notification
        if (send_notification is None):
            self._send_notification = False
        else:
            self._send_notification = True
            self.send_notification = send_notification
            # For deciding if notification should be send or not, we need to
            # know previous can_undo/redo value. Since now there is no image
            # stored yet, undo/redo can not be performed.
            self._old_can_undo = False
            self._old_can_redo = False

    # /__init__()
    # |_ConAPIBgUndoRedoCls
    # ==========================| High level functions |======================

    def commit(self, old_cube):
        """Store old cube content to buffer

        When some operation change cube, this function should be called.
        Function will automatically copy old cube to buffer, so undo() and
        redo() can be simply used.

        :returns: Old cube image, so it can be passed to other functions
        :rtype: obj (CubeLEDCls)
        """
        if (self._show_debug_msg):
            self.logger.debug(
                'Committing cube: orig idx: {} ; idx max: {} ; buff size: {}'
                ' ; len cubes: {}'
                ''.format(self._idx, self._idx_max, self.buff_size,
                          len(self._cubes)))

        # Copy cube - create brand new and copy old cube here
        cube_copy = CubeLEDCls(dimension=old_cube.get_dimension())
        copy_cube_led(src=old_cube, dst=cube_copy)

        # Index have to increased with every single commit, unless value is
        # same as buffer size
        if (self._idx < self.buff_size):
            self._idx = self._idx + 1

        # Procedure is different for case that buffer is already filled and
        # for case that buffer is just filling
        if (self._idx_max < self.buff_size):

            # Filling buffer
            if (self._show_debug_msg):
                self.logger.debug(
                    'Filling buffer: self._idx_max < self.buff_size')

            # Decide whether committed image will be just added to the end
            # or if have to be inserted somewhere in buffer
            if (self._idx > self._idx_max):
                # Add to end of buffer
                self._cubes.append(cube_copy)
            else:
                # Insert image in between. We want to insert actual image to
                # the "idx" position
                begin = self._cubes[:self._idx]
                end = self._cubes[self._idx:]

                # Add copied cube in between
                begin.append(cube_copy)
                begin.extend(end)
                self._cubes = begin
            # /else insert image in between

            # In both cases maximum index was increased by 1
            self._idx_max = self._idx_max + 1
        # /Filling buffer
        else:
            # Already filled
            if (self._show_debug_msg):
                self.logger.debug('Buffer already filled')

            assert self._idx_max == self.buff_size, (
                "It is expected that max index is equal to buffer size.")
            assert self._idx != 0, (
                "Expecting non zero index due to increasing index before")

            # There are 2 possible cases:
            # 1) Add commit to the end of buffer
            # 2) Add commit somewhere in the middle of the buffer
            # In both cases oldest backup have to be erased to keep buffer
            # size in limit
            if (self._idx == self.buff_size):
                if (self._show_debug_msg):
                    self.logger.debug("Adding cube to end of buffer. Removing"
                                      " oldest backup.")
                # Delete oldest backup, add to end of buffer
                self._cubes = self._cubes[1:]
                self._cubes.append(cube_copy)

                # Since index is at it's own maximum, no need to increase index
            # If index is 1 (previously 0, but there is +1)
            elif (self._idx == 1):
                if (self._show_debug_msg):
                    self.logger.debug(
                        "Adding cube to index {}, removing  newest backup"
                        " (index: {})".format(self._idx, self.buff_size))
                # /if debug message on

                # Remove "newest" backup
                self._cubes = self._cubes[:-1]
                # And inject copy to index 1
                cubes = []
                cubes.append(self._cubes[0])
                cubes.append(cube_copy)  # On index 1
                cubes.extend(self._cubes[1:])  # Add others

                self._cubes = cubes
            # /if index == 1
            else:
                assert self._idx < self._idx_max, (
                    "It is expected that index is lower than maximum"
                    " index/buffer size")

                begin = self._cubes[:self._idx]
                end = self._cubes[self._idx:]

                # Add copied cube in between
                begin.append(cube_copy)
                begin.extend(end)
                self._cubes = begin

                # One record need to be discarded. Problem is to decide which
                # one, since we're "in the middle" of the buffer. So let's
                # remove most "distant" backup (at begin or end of buffer)
                if (self._idx > (self.buff_size / 2)):
                    # More distant is backup with index 0
                    self._cubes = self._cubes[1:]
                    if (self._show_debug_msg):
                        self.logger.debug('Removing cube 0')
                    # /if debug
                    # Since this will shift relative positions of cube in
                    # relation to the index, index have to be decreased
                    self._idx = self._idx - 1
                    assert self._idx > 1, (
                        "At this 'else' branch is expected that index is at"
                        " least 2")
                else:
                    # Remove more distant is backup
                    self._cubes = self._cubes[:-1]
                    if (self._show_debug_msg):
                        self.logger.debug(
                            'Removing cube {}'.format(self.buff_size))
            # /add commit in the middle of buffer
        # /filled buffer

        # ============================| Extra checking  |=====================
        # Mainly checks for developers. Cost is low compare to benefits
        assert self._idx <= self.buff_size, (
            'Index should not be bigger than buffer size')
        assert self._idx <= self._idx_max, (
            'Index should not be bigger than maximum index value')
        assert self._idx_max <= self.buff_size, (
            'Maximum index should not be bigger than buffer size')
        # Following assert is little bit tricky. buff_size is technically last
        # possible index, while len(self._cubes) giving maximum index +1.
        assert (len(self._cubes) - 1) <= self.buff_size, (
            'Number of cube commits should not be bigger than buffer size')
        assert cube_copy == self._cubes[self._idx], (
            "It is expected that committed cube should be on current index.\n"
            f"Committed copy: {cube_copy} ; Cube[{self._idx}]: "
            f"{self._cubes[self._idx]}\nCubes:\n{self._get_cubes_address()}")

        if (self._show_debug_msg):
            msg = 'Cube committed with index {} ; len cubes: {}' \
                  ''.format(self._idx, len(self._cubes))
            self.logger.debug(msg)

        # If sending notifications is enabled
        self._send_notification_if_needed()

        return (old_cube)

    # /commit()
    # |_ConAPIBgUndoRedoCls

    def undo(self, fail_if_error=True):
        """Undo lost operation

        It basically returns previous LED cube.

        :param fail_if_error: Optional parameter which decide about failing
                              behavior. If set and operation can not be done,
                              an exception is raised. Otherwise fails is
                              ignored and cube in not changed.
        :type fail_if_error: bool

        :returns: Previous cube image.
        :rtype: obj (CubeLEDCls)
        """
        # Inc/dec idx, check value and raise exception if needed
        if (self._idx == 0):
            if (fail_if_error):
                raise RuntimeError('There is nothing to undo!')
            else:
                return (None)
        else:
            self._idx = self._idx - 1

        if (self._show_debug_msg):
            msg = 'Undo. Changing index to {} (len cubes: {})' \
                  ''.format(self._idx, len(self._cubes))
            self.logger.debug(msg)

        # Need to work with copy to not modify committed content
        old_cube = self._cubes[self._idx]
        cube_copy = CubeLEDCls(dimension=old_cube.get_dimension())
        copy_cube_led(src=old_cube, dst=cube_copy)

        # If sending notifications is enabled
        self._send_notification_if_needed()

        return (cube_copy)

    # /undo()
    # |_ConAPIBgUndoRedoCls

    def redo(self, fail_if_error=True):
        """Redo last operation

        :param fail_if_error: Optional parameter which decide about failing
                              behavior. If set and operation can not be done,
                              an exception is raised. Otherwise fails is
                              ignored and cube in not changed.
        :type fail_if_error: bool

        :returns: Next cube image
        :rtype: obj (CubeLEDCls)
        """
        if (self._idx == self._idx_max):
            if (fail_if_error):
                raise RuntimeError('There is nothing to redo!')
            else:
                return (None)
        else:
            self._idx = self._idx + 1

        if (self._show_debug_msg):
            msg = 'Redo. Changing index to {} (len cubes: {})' \
                  ''.format(self._idx, len(self._cubes))
            self.logger.debug(msg)

        # Need to work with copy to not modify committed content
        new_cube = self._cubes[self._idx]
        cube_copy = CubeLEDCls(dimension=new_cube.get_dimension())
        copy_cube_led(src=new_cube, dst=cube_copy)

        # If sending notifications is enabled
        self._send_notification_if_needed()

        return (cube_copy)

    # /redo()
    # |_ConAPIBgUndoRedoCls

    def can_undo(self):
        """Check if undo operation can be safely performed

        Simply chceck index and respond accordingly

        :returns: Availability of undo command
        :rtype: bool
        """
        if (self._idx <= 0):
            return (False)
        else:
            return (True)

    # /can_undo()
    # |_ConAPIBgUndoRedoCls

    def can_redo(self):
        """Check if redo operation can be safely performed

        Check actual index and maximum index and then decide

        :returns: Availability of redo command
        :rtype: bool
        """
        if (self._idx >= self._idx_max):
            return (False)
        else:
            return (True)

    # /can_redo()
    # |_ConAPIBgUndoRedoCls
    # ==========================| Low level functions |========================

    def get_idx(self):
        """Get actual index, so upper layer can know if undo can be done or not

        :returns: Index value. If negative, then no image was committed yet.
        :rtype: int
        """
        return (self._idx)

    # /get_idx()
    # |_ConAPIBgUndoRedoCls

    def get_max_idx(self):
        """Get maximum index value

        Buffer does not need to be full all the times, since it take few
        commits to fill buffer. This function can help upper layers to know
        buffer usage.

        :returns: Maximum index value. Negative number means that there
                  is nothing in buffer (since index starts count from 0)
        :rtype: int
        """
        return (self._idx_max)

    # /get_max_idx()
    # \_ConAPIBgUndoRedoCls

    def get_buffer_size(self):
        """Return maximum buffer size

        Maximum buffer size corresponds with maximum index -1
        """
        return (self.buff_size)

    # /get_buffer_size()
    # |_ConAPIBgUndoRedoCls
    # ===========================| Internal functions |=======================

    def _send_notification_if_needed(self):
        """Send notification to upper layer if needed
        """
        if (self._send_notification):
            new_can_undo = self.can_undo()
            new_can_redo = self.can_redo()

            if ((new_can_undo != self._old_can_undo) or
                    (new_can_redo != self._old_can_redo)):
                self.send_notification(
                    cmd="edit_can_undo_redo",
                    val="{} {}".format(new_can_undo, new_can_redo))
                self._old_can_undo = new_can_undo
                self._old_can_redo = new_can_redo
            # /if can_undo/redo are not same as last time
        # /if notifications are enabled

    # /_send_notification_if_needed()
    # |_ConAPIBgUndoRedoCls

    def _get_cubes_address(self):
        out_str = ""
        for cube in self._cubes:
            out_str = "{}{}\n".format(out_str, cube)
        return (out_str)
    # /_get_cubes_address()
    # |_ConAPIBgUndoRedoCls


# /_ConAPIBgUndoRedoCls


class _ConsoleAPIBgTimerCls(Process):
    """Background timer

    Purpose of this timer is sending interrupts to ca_bg_proc when playing
    animation or when running some random animation.
    """

    def __init__(self, settings, init_timer_value_ms=1000):
        """
        :param settings: Structure of settings
        :type settings: ConsoleAPISettingsCls

        :param init_timer_value_ms: Initial timer value in ms. This value can
                                    be changed at any time later by sending
                                    command through IPC.
        :type init_timer_value_ms: int
        """
        super().__init__()

        self.settings = settings

        # Set timer value
        self.timer = init_timer_value_ms * 0.001

        self.logger = CmnLgrCls("console_api_bg")

        try:
            # Set process name -> much easier to debug then
            setproctitle('Cube_main_and_console_API_bg_timer')
        except Exception:
            msg = 'Can not set process name. Nothing serious.'
            self.logger.info(msg)

    # __init__()

    def run(self):
        """Main function for this class"""
        # Initialize IPC
        self.ipc = ConAPIIPCBgProcTimerCls(self.settings.ipc, self.logger)

        # Close process?
        close = False

        # Keep status - pause/play
        status = "pause"

        # Get time in seconds
        check_period = self.settings.timer_check_period_ms * 0.001

        # Calculate "deadline" from self.timer and actual time
        self._calculate_end_time()

        while (not close):
            if (status == "pause"):
                # Just wait for command - blocking command
                cmd, val = self.ipc.get_req_blocking()
            elif (status == "play"):
                # Try to get command. But not blocking function.
                cmd, val = self.ipc.get_req()
            else:
                msg = "Unknown status: {}\n Expected play or pause" \
                      "".format(status)
                raise Exception(msg)
            # /get command

            # Check command (if any)
            if (cmd == "close"):
                close = True
                break
            elif (cmd == "play"):
                # Status was set to play -> update set timer
                status = "play"
                self._calculate_end_time()
            elif (cmd == "pause"):
                status = "pause"
            elif (cmd == "set_timer"):
                self.timer = int(val) * 0.001
                # Refresh end time
                self._calculate_end_time()
            elif (cmd == ""):
                # No command -> pass
                pass
            else:
                msg = "Unknown command: {}".format(cmd)
                raise Exception(msg)
            # /check commands

            if (status == "pause"):
                # Wait for new command
                continue
            elif (status == "play"):
                actual_time = perf_counter()
                if (actual_time >= self.time_end):
                    # Calculate new time
                    self.time_end = self.time_end + self.timer

                    self.ipc.send_req(cmd="timer_interrupt", val=self.timer)
                # if is time to send interrupt to background process
            # /if status is play

            sleep(check_period)
        # /while not close

    # /run()

    def _calculate_end_time(self):
        self.time_begin = perf_counter()
        self.time_end = self.time_begin + self.timer
    # /_calculate_end_time()

# /_ConsoleAPIBgTimerCls

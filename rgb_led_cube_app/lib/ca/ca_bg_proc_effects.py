#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=========================================
Processing effects at background process
=========================================

Effect algorithms are quite big, so let's put them to another file

``author`` Martin Stejskal

``version`` 0.0.3
"""
# Measure time
from time import perf_counter


class ConsoleAPIBgEffectsSettingsCls:
    def __init__(self):
        # When processing, it may take a while and user can became nervous.
        # So it is good idea to send status notifications time to time. And
        # This value define this time in seconds.
        self.processing_status_period = 0.1

        # While processing, it might be nice to see progress also visually.
        # It is not necessary render every single image, but at least time
        # to time show "what is going on". Lower value means higher CPU usage
        # and longer processing, but more user friendly. Set this value wisely.
        self.update_cube_period = 0.3
    # /__init__()


# /ConsoleAPIBgEffectsSettingsCls


class ConsoleAPIBgEffectsCls:
    def __init__(self, settings, logger, anim_obj, get_mode_fnc,
                 send_notification_fnc, update_cube_fnc, timer_send_req_fnc):
        self.settings = settings

        self.logger = logger

        self.anim = anim_obj

        self.get_mode = get_mode_fnc
        self.send_notification = send_notification_fnc
        self.update_cube = update_cube_fnc
        self.timer_send_req = timer_send_req_fnc

    # /__init__()

    def fade_out(self, duration=10, effect_type="linear",
                 pwm=[True, True, True]):
        """Fade image out by decreasing PWM values

        :param duration: Effect duration in frames.
        :type duration: int

        :param effect_type: Type of fade out effect.
        :type effect_type: str (linear, exponential)

        :param pwm: Define with which PWM channels should effect work (R,G,B).
        :type pwm: array (bool, bool, bool)
        """
        self._pre_checks()

        # Always work with updated cube
        leds = self.anim.get_actual_cube()

        # Get PWM RGB - maximum values (will be only decreasing)
        pwm_max = [leds.pwm.r, leds.pwm.g, leds.pwm.b]

        # PWM values will be stored there
        pwm_vals = []

        # Get PWM values for used channels
        for frame in range(duration):
            # For R, G, B
            new_pwm = [0, 0, 0]
            for clr in (0, 1, 2):
                if (pwm[clr]):
                    # Color is active. Get new PWM - but in reversed order
                    if (effect_type == "linear"):
                        new_pwm[clr] = self._get_linear_val(
                            duration - 1 - frame, duration - 1, pwm_max[clr])
                    elif (effect_type == "exponential"):
                        if (frame == (duration - 1)):
                            # Small hack for last value
                            new_pwm[clr] = 0
                        else:
                            new_pwm[clr] = self._get_exp_val(
                                duration - 1 - frame, duration - 1,
                                pwm_max[clr])
                    else:
                        msg = 'Internal error. Unexpected effect type {}' \
                              ''.format(effect_type)
                        raise Exception(msg)
                else:
                    # Color is inactive -> keep value
                    new_pwm[clr] = pwm_max[clr]
                # /Inactive color
            # /for RGB
            pwm_vals.append(new_pwm)
        # /for all frames

        # Now change PWM values accordingly
        time_begin = perf_counter()
        tout_proc_stat = time_begin + self.settings.processing_status_period
        tout_updt_cube = time_begin + self.settings.update_cube_period
        for idx in range(duration):
            # Insert new image, move to new image and set PWM
            self.anim.insert_new_image_actual_idx()
            self.anim.next()
            leds.pwm.r = pwm_vals[idx][0]
            leds.pwm.g = pwm_vals[idx][1]
            leds.pwm.b = pwm_vals[idx][2]

            time_now = perf_counter()
            if (time_now > tout_proc_stat):
                self.send_notification(
                    cmd="effect_progress",
                    val="{:.1f} % ({}/{})".format(
                        (100 * idx) / duration, idx, duration))
                tout_proc_stat = (time_now +
                                  self.settings.processing_status_period)
            # /if time to send notification about progress

            if (time_now > tout_updt_cube):
                self.update_cube()
                tout_updt_cube = (time_now +
                                  self.settings.update_cube_period)
            # /if cube should be updated
        # /while processing

        # Send notification about finished operation
        self.send_notification(
            cmd="effect_done",
            val="{}".format(perf_counter() - time_begin))

    # /fade_out()
    # |ConsoleAPIBgEffectsCls

    # ===========================| Internal functions |=======================
    def _get_linear_val(self, frame, max_frame, max_val):
        """Return Y value as linear function of X (frame)
        """
        return (int((max_val / max_frame) * frame))

    # /_get_linear_val()
    # |ConsoleAPIBgEffectsCls

    def _get_exp_val(self, frame, frame_max, max_val):
        """Return Y value as exponential function of X (frame)"""
        # Ideal curve is for 2^x from range 0~5.
        # Idea is use this curve within this range, while step (X resolution)
        # will be changed according to the maximum number of frames.
        step = 5 / frame_max
        x_val = step * frame

        # 2^5 = 32
        val = (max_val / 32) * pow(2, x_val)

        # Few hacks to make it more realistic
        # At x=0, we're expecting value 0. But we're working with integer, so
        # every error is accumulate and cause not nice "values" -> put some
        # correction (y value when x=0 -> if non zero use it as correction)
        corr = (max_val / 32)
        val = val - corr

        assert val >= 0, 'Value have to be positive'
        assert val <= max_val, 'Value can not be higher than maximum'

        return (int(val))

    # /_get_exp_val()
    # |ConsoleAPIBgEffectsCls

    def _pre_checks(self):
        """Set of checks that should be tested before every run
        """
        assert self.get_mode() == 'anim', "Effects are only for animation mode"

        if (self.anim.get_status() == "play"):
            self.anim.pause()
            self.timer_send_req(cmd="pause", val="generate effect")
    # /_pre_checks()
    # |ConsoleAPIBgEffectsCls
# /ConsoleAPIBgEffectsCls

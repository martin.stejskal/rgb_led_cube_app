#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Load/save animation/image
===================================

``author`` Martin Stejskal

``version`` 0.3.3
"""

# Some common part
from lib.ca.ca_common import ConAPICommonCls


class ConAPILoadSaveCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj

        """
        self.ipc = ipc
        self.logger = logger

        # Loading and saving animations can take a while. So let's set this
        # timer to some reasonable value
        self.timeout_ms = 70000

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def save_animation(self, filename="animation.3da.gz", add_extension=True):
        """Save animation to file

        :param filename: Path with file name
        :type filename: str
        """
        # Add extension if needed
        if (add_extension):
            # Check if there is extension ".3da.gz". If, not automatically
            # add it.
            if (not (".3da.gz" in filename)):
                filename = '{}.3da.gz'.format(filename)
                msg = 'Used did not added extension. Adding automatically.'
                self.logger.debug(msg)
            else:
                # Check if 3da.gz is at the end of the filename. If not,
                # add it.
                if (not (filename[-7:] == '.3da.gz')):
                    filename = '{}.3da.gz'.format(filename)
                    msg = 'Used did not added extension. But the ".3da.gz" ' \
                          'is in filename. Adding extension automatically.'
                    self.logger.debug(msg)
                # /if string found, but not as extension
            # /extension string in filename
        # /if we want to add extension automatically

        variable, value = self.ipc.send_req_get_rply(
            cmd="save_anim", val=filename, timeout=self.timeout_ms)

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="save_animation()")

        if (value != "OK"):
            msg = self.ca_cmn.restore_msg(msg)
            self.logger.error(msg)
            raise Exception(msg)

    # /save_animation()

    def load_animation(self, filename="animation.3da.gz"):
        """Load animation from file

        :param filename: Path with file name
        :type filename: str
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="load_anim", val=filename, timeout=self.timeout_ms)

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="load_animation()")

        if (value == "Dimension_changed"):
            msg = "Dimension changed"
            self.logger.warn(msg)
            raise UserWarning(msg)
        elif (value != "OK"):
            # Recover message
            msg = self.ca_cmn.restore_msg(msg)
            self.logger.error()
            raise Exception(msg)

    # /load_animation()

    def append_animation(self, filename="animation_2.3da.gz"):
        """Append another animation from file

        This will allow to create quite long animations assembled from shorter
        ones.

        :param filename: Path with file name
        :type filename: str
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="append_anim", val=filename, timeout=self.timeout_ms)

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="append_animation()")

        return (value)

    # /append_animation()

    def new(self):
        """New animation/image.

        Basically it will wipe all your work with default LED cube
        """
        self.ipc.send_req(cmd="load_save_new", val="ca_load_save")
    # /new()
# /ConAPILoadSaveCls

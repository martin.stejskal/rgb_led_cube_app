#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Handle PWM values
===================================

Allow to set PWM values for whole cube

``author`` Martin Stejskal

``version`` 0.4.2
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIPWMCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def set(self, r, g, b):
        """Set PWM for whole cube

        :param r: PWM value for red color
        :type r: int (0~255)

        :param g: PWM value for green color
        :type g: int (0~255)

        :param b: PWM value for blue color
        :type b: int (0~255)
        """
        self.ipc.send_req(cmd="set_pwm",
                          val="{} {} {}".format(int(r), int(g), int(b)))

    # /set_pwm()

    def get(self):
        """Returns actual PWM settings in RGB format

        :returns: PWM value for RGB channels
        :rtype: array (int: 0~255, 0~255, 0~255)
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_pwm")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="pwm",
                                        function_name="get_pwm()")
        # Retype values to integer. For this we need to create new array
        res = []
        for item in value:
            res.append(int(item))
        # Return as list integers
        return (res)

    # /get_pwm()

    def set_all_img(self, r, g, b):
        """Set PWM value for all images

        Typically this can be used in "anim" mode, when is needed to change
        PWM through whole animation.

        :param r: PWM value for red color
        :type r: int (0~255)

        :param g: PWM value for green color
        :type g: int (0~255)

        :param b: PWM value for blue color
        :type b: int (0~255)
        """
        self.ipc.send_req(cmd="set_pwm_all_img",
                          val="{} {} {}".format(int(r), int(g), int(b)))
    # /set_all_img()

# /ConAPIPWMCls

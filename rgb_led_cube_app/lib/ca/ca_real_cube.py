#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Basic interface for real cube
===================================

Sometimes user want to connect real cube...

``author`` Martin Stejskal

``version`` 0.2.1
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIRealCubeCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def dev_list(self):
        """Get list of supported cubes

        :returns: List of supported devices
        :rtype: str array
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="real_cube_get_dev_list")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="dev_list",
                                        function_name="real_cube_dev_list()")

        # Make sure that we will return list -> in case that there is only
        # one supported cube, we need to convert string to list
        if (isinstance(value, str)):
            value = [value]
        # /if string, create list
        return (value)

    # /dev_list()

    def get_status(self):
        """Get real cube status

        :returns: "Connected" or "Not connected"
        :rtype: str
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="get_real_cube_status")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="real_cube_get_status")
        return (value)

    # /get_status()

    def connect(self, dev_name):
        """This function will try connect with real cube

        Raise RuntimeError exception when connection failed.
        Raise UserWarning when cube dimension is changed (it is needed)

        :param dev_name: device name. Please use one of names obtained from
                         get_device_list()
        :type dev_name: str
        """
        variable, value = self.ipc.send_req_get_rply(cmd="real_cube_connect",
                                                     val=dev_name)
        self.ca_cmn.check_variable_name(
            received_var=variable,
            expected_var="real_cube_connect_status",
            function_name="real_cube_connect")
        # Now parse
        if (value == "OK"):
            msg = "Cube {} connected".format(dev_name)
            self.logger.info(msg)
        elif (value == "NG"):
            msg = "Cube {} NOT connected!".format(dev_name)
            self.logger.warn(msg)

            raise RuntimeError(msg)
        elif (value == "Dimension_changed"):
            msg = "Cube dimension have to be changed!"
            self.logger.warn(msg)

            raise UserWarning(msg)
        else:
            # Something went wrong
            msg = "Unexpected response when connecting to" \
                  " cube ({})".format(value)
            self.logger.critical(msg)
            raise Exception(msg)
        # /parse value

    # /connect()

    def disconnect(self):
        variable, value = self.ipc.send_req_get_rply(
            cmd="real_cube_disconnect")

        self.ca_cmn.check_variable_name(
            received_var=variable,
            expected_var="real_cube_disconnect_status",
            function_name="real_cube_disconnect")

        # Now parse
        if (value == "OK"):
            msg = "Cube disconnected"
            self.logger.info(msg)
        elif (value == "NG"):
            msg = "Cube NOT disconnected!"
            self.logger.warn(msg)

            raise RuntimeError(msg)
        else:
            # Something went wrong
            msg = "Unexpected response when connecting" \
                  " to cube ({})".format(value)
            self.logger.critical(msg)
            raise Exception(msg)
        # /parse value
    # /disconnect()
# /ConAPIRealCubeCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Edit options
===================================

Most important functions like "undo" and "redo" are here.

``author`` Martin Stejskal

``version`` 0.0.4
"""
from lib.ca.ca_common import ConAPICommonCls


class ConAPIEditCls:
    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj

        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def undo(self, fail_if_error=True):
        """Undo last operation if possible

        :param fail_if_error: Optional parameter which decide about failing
                              behaviour. If set and operation can not be done,
                              an exception is raised. Otherwise fails is
                              ignored and cube in not changed.
        :type fail_if_error: bool
        """
        self.ipc.send_req(cmd="edit_undo", val=fail_if_error)

    # /undo()
    # |ConAPIEditCls

    def redo(self, fail_if_error=True):
        """Go forward to the newer operation

        :param fail_if_error: Optional parameter which decide about failing
                              behaviour. If set and operation can not be done,
                              an exception is raised. Otherwise fails is
                              ignored and cube in not changed.
        :type fail_if_error: bool
        """
        self.ipc.send_req(cmd="edit_redo", val=fail_if_error)

    # /redo()
    # |ConAPIEditCls

    def can_undo(self):
        """Check if undo operation can be safely performed

        Simply chceck index and respond accordingly

        :returns: Availability of undo command
        :rtype: bool
        """
        variable, value = self.ipc.send_req_get_rply(cmd="edit_can_undo")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="can_undo",
                                        function_name="can_undo()")
        value = self.ca_cmn.str2bool(var=value)
        return (value)

    # /can_undo()
    # |ConAPIEditCls

    def can_redo(self):
        """Check if redo operation can be safely performed

        Check actual index and maximum index and then decide

        :returns: Availability of redo command
        :rtype: bool
        """
        variable, value = self.ipc.send_req_get_rply(cmd="edit_can_redo")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="can_redo",
                                        function_name="can_redo()")
        value = self.ca_cmn.str2bool(var=value)
        return (value)

    # /can_redo()
    # |ConAPIEditCls

    def copy(self):
        """Copy actual image to buffer

        Cube can be pasted later whenever user want.
        """
        self.ipc.send_req(cmd="edit_copy")

    # /copy()
    # |ConAPIEditCls

    def paste(self):
        """Paste image from buffer

        Paste previously copied image. If image was not copied before, warning
        will be sent via notification channel.
        """
        self.ipc.send_req(cmd="edit_paste")
    # /paste()
    # |ConAPIEditCls
# /ConAPIEditCls

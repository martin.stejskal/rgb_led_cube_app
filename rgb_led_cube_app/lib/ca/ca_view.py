#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
View operations
===================================

This module should handle most of "view" operations.

``author`` Martin Stejskal

``version`` 0.1.1
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIViewCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()
    # |ConAPIWallCls

    def set_wall_highlight(self, highlight=True):
        """Enable/disable highlighting actual wall at cube at 3D view

        :param highlight: Enable/disable highlighting actual wall at 3D view
        :type highlight: bool
        """
        self.ipc.send_req(cmd="view_set_wall_highlight", val=highlight)

    # /set_wall_highlight()
    # |ConAPIWallCls

    def get_wall_highlight(self):
        """Do we want to highlight actual wall?

        :returns: Index of highlighted wall or None if all walls are active
        :rtype: int (0~(dimension -1))/None
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="view_get_wall_highlight")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="highlight",
                                        function_name="get_wall_highlight")
        return (self.ca_cmn.str2bool(value))

    # /get_wall_highlight()
    # |ConAPIWallCls

    def set_show_disabled(self, show_disabled=True):
        """Show/hide disabled LEDs

        When LED is off (color 0,0,0) it can be rendered as black LED or not
        displayed at all.

        :param show_disabled: When disabled, LEDs which are off (0,0,0) will
                              not be rendered
        :type show_disabled: bool
        """
        self.ipc.send_req(cmd="view_set_show_disabled", val=show_disabled)

    # /set_show_disabled()
    # |ConAPIWallCls

    def get_show_disabled(self):
        """Get "show disabled" option

        :returns: Show disabled value
        :rtype: bool
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="view_get_show_disabled")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="show_disabled",
                                        function_name="get_show_disabled")
        return (self.ca_cmn.str2bool(value))

    # /get_show_disabled()
    # |ConAPIWallCls

    def set_show_inactive(self, show_inactive=False):
        """Show inactive LEDs when wall is selected?

        By default, actual wall is highlighted by special grid. However in
        some cases other "inactive" (deselected) LEDs can be disruptive. This
        option allow to not render them, so actual wall is pretty visible.

        :param show_inactive: Enable/disable rendering inactive LEDs.
        :type show_inactive: bool
        """
        self.ipc.send_req(cmd="view_set_show_inactive", val=show_inactive)

    # /set_show_inactive()
    # |ConAPIWallCls

    def get_show_inactive(self):
        """Get "show inactive" option

        :returns: Show inactive LEDs when wall is selected
        :rtype: bool
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="view_get_show_inactive")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="show_inactive",
                                        function_name="get_show_inactive")
        return (self.ca_cmn.str2bool(value))
    # /get_show_inactive()
    # |ConAPIWallCls
# /ConAPIWallCls

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=============================================
Handle notifications from background process
=============================================

This module register callbacks from GUI and when appropriate notification is
received, it callback function is executed.

``author`` Martin Stejskal

``version`` 0.1.2
"""
# Common things for some functions
from lib.ca.ca_common import ConAPICommonCls


class ConAPINotificationsCls:
    def __init__(self, ipc, logger):
        # Store parameters to instance
        self.ipc = ipc
        self.logger = logger

        # Common stuff
        self.ca_cmn = ConAPICommonCls(self.logger)

        # Get list of available notifications. This is more for developer
        # than for user, but it might be handy
        variable, value = self.ipc.send_req_get_rply(
            cmd="get_list_of_notifications")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="list_of_notifications",
                                        function_name="__init__()")
        self._lst_of_avail_ntf = value
        self.logger.debug("List of notifications: {}".format(value))

        # Keep list of events (commands) and callback functions.
        # It is dictionary in format: command: callback function
        self._lst_evnts_clbcks = {}

    # /__init__()

    def get_list_of_notifications(self):
        """Returns list of notifications

        :returns: List of notifications
        :rtype: list (str)
        """
        return (self._lst_of_avail_ntf)

    # /get_list_of_nofitications()
    # |ConAPINotificationsCls

    def add_notification_callback(self, cmd="hw_cube_disconnected",
                                  callback_f=None):
        """Register notification and callback function

        If functions is already registered, raise exception.

        :param cmd: Event (command) name. It should be one of the events
                    obtained by get_list_of_nofitications()
        :type cmd: str

        :param callback_f: Pointer to function which will be called when
                           event will be received.
        :type callback_f: pointer to function
        """
        # Check if event (command) is at list of available notifications
        self._check_if_cmd_in_avail_ntf(cmd)

        # Check if already registered
        if (cmd in self._lst_evnts_clbcks.keys()):
            msg = 'Event "{}" is already registered.'.format(cmd)
            self.logger.error(msg)
            raise RuntimeError(msg)
        # /if already registered

        # Register it
        self._lst_evnts_clbcks[cmd] = callback_f

    # /add_notification_callback()
    # |ConAPINotificationsCls

    def add_update_notification_callback(self, cmd="hw_cube_disconnected",
                                         callback_f=None):
        """Register notification and callback function

        If notification is already registered, update pointer to callback
        function.

        :param cmd: Event (command) name. It should be one of the events
                    obtained by get_list_of_nofitications()
        :type cmd: str

        :param callback_f: Pointer to function which will be called when
                           event will be received.
        :type callback_f: pointer to function
        """
        # Check if event (command) is at list of available notifications
        self._check_if_cmd_in_avail_ntf(cmd)

        # Check if already registered
        if (cmd in self._lst_evnts_clbcks.keys()):
            msg = 'Event "{}" is already registered. Updating callback' \
                  ' function'.format(cmd)
            self.logger.debug(msg)
        # /if already registered just show debug message

        # Register it
        self._lst_evnts_clbcks[cmd] = callback_f

    # /add_update_notification_callback()
    # |ConAPINotificationsCls

    def del_notification_callback(self, cmd="hw_cube_disconnected"):
        """Unregister notification event

        :param cmd: Notification (command) name which will be unregistered.
        :type cmd: str
        """
        # Check if event (command) is at list of available notifications
        self._check_if_cmd_in_avail_ntf(cmd)

        # Check if command is registered
        if (not (cmd in self._lst_evnts_clbcks.keys())):
            msg = 'Event "{}" was not registered, so it can not be removed' \
                  ''.format(cmd)
            self.logger.error(msg)
            raise RuntimeError(msg)

        # If yes, then remove it
        del self._lst_evnts_clbcks[cmd]

    # /del_notification_callback()
    # |ConAPINotificationsCls

    def process_notifications(self):
        """Process notifications events

        If some registered event is received, appropriate callback function
        will be called. This function should be called by GUI periodically
        to ensure that all notifications are properly processed.
        """
        cmd, val = self.ipc.get_notification()

        # If no command, no callback
        if (cmd == ''):
            return

        # Check if event (command) is at list of available notifications
        # Only good for developing.
        self._check_if_cmd_in_avail_ntf(cmd)

        # Check if callback for "cmd" is set.
        if (not (cmd in self._lst_evnts_clbcks.keys())):
            msg = 'No callback set for "{}" notification!'.format(cmd)
            self.logger.warn(msg)
            return

        # So far so good, let's call callback function with value as parameter
        self._lst_evnts_clbcks[cmd](val)

    # /process_notifications()
    # |ConAPINotificationsCls

    # ===========================| Internal functions |=======================
    def _check_if_cmd_in_avail_ntf(self, cmd):
        """Check if command is in list of available notifications

        :param cmd: Command/notification
        :type cmd: str
        """
        if (not (cmd in self._lst_of_avail_ntf)):
            msg = 'Event "{}" can not be registered, because it is not\n' \
                  'in list of available events ({})' \
                  ''.format(cmd, self._lst_of_avail_ntf)
            self.logger.error(msg)
            raise RuntimeError(msg)
    # /_check_if_cmd_in_avail_ntf()
# /ConAPINotificationsCls

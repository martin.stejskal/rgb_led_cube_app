#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
============================================
Play operations (play, pause, FW, FF, ...)
============================================

``author`` Martin Stejskal

``version`` 0.3.1
"""

# Some common part
from lib.ca.ca_common import ConAPICommonCls


class ConAPIPlayCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj

        """
        self.ipc = ipc
        self.logger = logger

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def next(self):
        """Jump to next image

        If next image does not exist yet, last image is copied and new image
        is created
        """
        # Value is not mandatory, but it helps with debugging (to show source
        # of this command)
        self.ipc.send_req(cmd="play_next", val="{}".format(__file__))

    # /next()

    def previous(self):
        """Move to previous image

        If it is already at image index 0, it will stay here
        """
        # Value is not mandatory, but it helps with debugging (to show source
        # of this command)
        self.ipc.send_req(cmd="play_previous", val="{}".format(__file__))

    # /previous()

    def play(self):
        """Start playing animation

        This will start playing animation no matter what. If actual image is
        last one it will start playing from begin (image 0)
        """
        self.ipc.send_req(cmd="play_play", val="{}".format(__file__))

    # /play()

    def pause(self):
        """Pause animation

        This will pause animation no matter what.
        """
        self.ipc.send_req(cmd="play_pause", val="{}".format(__file__))

    # /play()

    def stop(self):
        """Stop (pause) animation and jump to the first image

        Only difference between "pause()" and this function is that this
        function will also set image index back to 0
        """
        self.ipc.send_req(cmd="play_stop", val="{}".format(__file__))

    # /stop()

    def set_timer(self, time_ms=40):
        """Set timer value in ms

        :param time_ms: Time in ms
        :type time_ms: int
        """
        self.ipc.send_req(cmd="set_timer",
                          val=time_ms)

    # /set_timer()

    def get_timer(self):
        """Returns timer value

        :returns: Timer value in ms
        :rtype: int
        """
        variable, value = self.ipc.send_req_get_rply(cmd="get_timer")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="timer",
                                        function_name="get_timer")

        return (int(value))

    # /get_timer()

    def set_actual_idx(self, actual_idx=0):
        """Set actual index value, which is basically image number

        :param actual_idx: New actual index value
        :type actual_idx: int
        """
        self.ipc.send_req(cmd="play_set_actual_idx",
                          val=actual_idx)

    # /set_actual_idx()

    def get_actual_idx(self):
        """Returns number of actual cube image

        :returns: Index of actual cube
        :rtype: int
        """
        variable, value = self.ipc.send_req_get_rply(cmd="play_get_act_idx")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="idx",
                                        function_name="get_actual_idx")

        return (int(value))

    # /get_actual_idx()

    def get_max_idx(self):
        """Return index of last valid cube.

        This may be useful for knowing how much frames remaining
        during playing animation.

        :returns: Maximum index which can be used for cube.
        :rtype: int
        """
        variable, value = self.ipc.send_req_get_rply(cmd="play_get_max_idx")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="idx",
                                        function_name="get_max_idx")

        return (int(value))

    # /get_max_idx()

    def get_status(self):
        """Return actual status

        Status may change during playing itself (last frame for example), so
        higher layer should check frame number or watch this status.
        Recommended is to watch this status.

        :returns: Play status
        :rtype: str (play/pause)
        """
        variable, value = self.ipc.send_req_get_rply(cmd="play_get_status")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="status",
                                        function_name="get_status")

        return (value)

    # /get_status()

    def delete_actual_idx(self):
        """Delete actual cube frame.

        If actual index is set to 0 and maximum index is also 0,
        then do nothing
        """
        self.ipc.send_req(cmd="play_del_act_idx", val="{}".format(__file__))

    # /delete_actual_idx()

    def insert_new_image_actual_idx(self):
        """Insert new image at actual index position
        """
        self.ipc.send_req(cmd="play_new_img_act_idx",
                          val="{}".format(__file__))

    # /insert_new_image_actual_idx()

    def get_repeat(self):
        """Get repeat value

        :returns: Repeat value
        :rtype: bool
        """
        variable, value = self.ipc.send_req_get_rply(cmd="play_get_repeat")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="repeat",
                                        function_name="get_repeat")

        value = self.ca_cmn.str2bool(var=value)
        return (value)

    # /get_repeat()

    def set_repeat(self, repeat_enable=False):
        """Set repeat value

        :param repeat_enable: Enable/disable repeat parameter
        :type repeat_enable: bool
        """
        self.ipc.send_req(cmd="play_set_repeat", val=repeat_enable)
    # /set_repeat()
# /ConAPIPlayCls

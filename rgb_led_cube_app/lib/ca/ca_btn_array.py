#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
===================================
Button array module
===================================

Module that handle actions from button array

``author`` Martin Stejskal

``version`` 0.2.2
"""

from lib.ca.ca_common import ConAPICommonCls


class ConAPIBtnArrayCls:

    def __init__(self, ipc, logger):
        """
        :param ipc: Object for IPC communication.
        :type ipc: ConAPIIPCMasterCls

        :param logger: Object with logger. For example CmnLgrCls
        :type logger. Obj
        """
        self.ipc = ipc
        self.logger = logger

        # Runtime variables
        self.rt = _RtConAPIBtnArrayCls()

        # Common parts for console API
        self.ca_cmn = ConAPICommonCls(self.logger)

    # /__init__()

    def set_led(self, column=0, row=0,
                r=True, g=True, b=True, auto_color=False,
                auto_blck_and_whte=False):
        """Set one defined LED

        Since user selected some wall, we just need to know column and row

        :param column: Column index. If parameter is None, then all rows are
                       selected
        :type column: None, int (0 ~ (cube dimension -1))

        :param row: Row index. If parameter is None, then all rows are
                    selected
        :type row: None, int (0 ~ (cube dimension -1))

        :param r: Enable or disable red LED?
                  When auto_color is enabled, this variable is ignored.
        :type r: bool
        :param g: Enable or disable green LED?
                  When auto_color is enabled, this variable is ignored.
        :type g: bool
        :param b: Enable or disable blue LED?
                  When auto_color is enabled, this variable is ignored.
        :type b: bool

        :param auto_color: Automatically change R/G/B color? If enabled,
                           it will ignore parameters r, g and b
        :type auto_color: bool

        :param auto_blck_and_whte: When automatic color is enabled, this option
                                   allow to simply switch between white
                                   (RGB enabled) and black (RGB disabled)
                                   states. This can be useful for cases where
                                   user does not play with colors or cube does
                                   not support colors.
        :type auto_blck_and_whte: bool
        """
        if ((not auto_color) and auto_blck_and_whte):
            self.logger.warn(
                "Auto color is disabled, but auto Black & white option\n"
                "is enabled!\nAuto Black & white will be ignored!")
        # /if not auto color, but only black and white enabled

        if (auto_color):
            # Get new color (automatic change)
            self.rt.a_clr.change_color(auto_blck_and_whte)
        else:
            # Auto color is disabled
            self.rt.a_clr.r = r
            self.rt.a_clr.g = g
            self.rt.a_clr.b = b
        # /else user defined R/G/B

        self.ipc.send_req(cmd="set_one_led_ish",
                          val="{} {} {} {} {}".format(column, row,
                                                      self.rt.a_clr.r,
                                                      self.rt.a_clr.g,
                                                      self.rt.a_clr.b))

    # /set_led()

    def set_auto_color(self, r=True, g=True, b=True):
        """Rewrite auto changed value of R, G and B

        :param r: Enable red LED(s)
        :type r: bool

        :param g: Enable green LED(s)
        :type g: bool

        :param b: Enable blue LED(s)
        :type b: bool
        """
        self.rt.a_clr.r = r
        self.rt.a_clr.g = g
        self.rt.a_clr.b = b

    # /set_auto_color()

    def get_auto_color(self):
        """Returns automatically changed colors

        :returns: red, green, blue
        :rtype: bool, bool, bool
        """
        return (self.rt.a_clr.r, self.rt.a_clr.g, self.rt.a_clr.b)
    # /get_auto_color()

    # ===========================| Internal functions |=======================


# /ConAPIBtnArrayCls

# ============================| Internal classes |============================


class _RtConAPIBtnArrayCls:
    """Runtime variables for ConAPIBtnArrayCls"""

    def __init__(self):
        self.a_clr = self.AutoColorCls()

    # /__init__()

    class AutoColorCls:

        def __init__(self):
            # Enabled red?
            self.r = 1

            # Enabled green?
            self.g = 1

            # Enabled blue?
            self.b = 1

        # /__init__()

        def change_color(self, only_black_and_white=False):
            """Automatically change RGB

            This function help with automatic changing LED color, which can be
            used for buttons or it can actually set LED in cube.

            :param only_black_and_white: Basically just enabled or disable all
                                         RGB LEDs. No colors, just black and
                                         white.
            :type only_black_and_white: bool
            """
            if (only_black_and_white):
                # If any color enabled -> disable
                if (self.r or self.g or self.b):
                    # R enabled -> disable all
                    self.r = self.g = self.b = 0
                else:
                    # All colors were disabled -> enable
                    self.r = self.g = self.b = 1
                return
            # /if just black and white

            # Since there is only 8 cases and we (I) want pre-defined order,
            # there is only one way out -> if madness

            led_color = [self.r, self.g, self.b]

            # R -> RG
            if (led_color == [1, 0, 0]):
                new_color = [1, 1, 0]
            # RG -> G
            elif (led_color == [1, 1, 0]):
                new_color = [0, 1, 0]
            # G -> GB
            elif (led_color == [0, 1, 0]):
                new_color = [0, 1, 1]
            # GB -> B
            elif (led_color == [0, 1, 1]):
                new_color = [0, 0, 1]
            # B -> RB
            elif (led_color == [0, 0, 1]):
                new_color = [1, 0, 1]
            # RB -> RGB
            elif (led_color == [1, 0, 1]):
                new_color = [1, 1, 1]
            # RGB -> none
            elif (led_color == [1, 1, 1]):
                new_color = [0, 0, 0]
            # none -> R (this close loop)
            else:
                new_color = [1, 0, 0]

            # and save new colors
            self.r = new_color[0]
            self.g = new_color[1]
            self.b = new_color[2]
        # /change_color()
    # /AutoColorCls
# /_RtConAPIBtnArrayCls

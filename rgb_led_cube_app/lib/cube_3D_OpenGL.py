#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
======================================
Module for rendering 3D cube by OpenGL
======================================

Based on tutorial at
https://pythonprogramming.net/opengl-rotating-cube-example-pyopengl-tutorial/

Thanks for sharing!


This module simply just show 3D cube according to input parameters

``author`` Martin Stejskal

``version`` 0.7.3
"""
# Using OpenGL libraries -> no dependency to pygame or tkinter
from OpenGL.GL import glDepthFunc, \
    GL_LESS, \
    glEnable, \
    GL_DEPTH_TEST, \
    glClearColor, glLoadMatrixf, glClear, GL_COLOR_BUFFER_BIT, \
    GL_DEPTH_BUFFER_BIT, glLineWidth, glBegin, GL_LINES, glColor3f, \
    glVertex3fv, glEnd, glPushMatrix, glTranslatef, GL_QUADS, glPopMatrix

# OpenGL windows (Multi-platform)
from OpenGL.GLUT import glutInit, glutInitDisplayMode, GLUT_DOUBLE, GLUT_RGB, \
    GLUT_DEPTH, glutInitWindowSize, glutCreateWindow, glutCloseFunc, \
    glutDisplayFunc, glutMainLoopEvent, glutSolidSphere, glutSwapBuffers

import math

# Sometimes we need sleep and know system time
from time import sleep, perf_counter

# Multiprocessing and sharing variables through process
from multiprocessing import Process

# Network stack for interprocess communication
import zmq

# Process naming is not something mandatory, but nice to have
try:
    from setproctitle import setproctitle
except Exception:
    pass


class Cube3DOpenGLSettingsCls:
    """Settings for 3D cube view
    """

    def __init__(self):
        # Cube dimension (eg. 5 -> LED cube 5x5x5 LED)
        self.dimension = 5

        self.window = self.WindowStruct()
        self.floor = self.FloorStruct()
        self.wireframe = self.WireframeStruct()
        self.wall = self.WallStruct()
        self.led = self.LEDStruct()

        # Set IP and port for interprocess communication
        self.interproc = self.InterProcStruct()

        # Notifications
        self.notif = self.NotificationsStruct()

    # /__init__()
    # |Cube3DOpenGLSettingsCls

    def __str__(self):
        msg = "= Cube 3D OpenGL settings ({}) =\n" \
              " Dimension: {}\n\n" \
              "{}\n{}\n{}\n{}\n{}\n{}" \
              "".format(self.__class__.__name__,
                        self.dimension,
                        self.window,
                        self.floor,
                        self.wireframe,
                        self.led,
                        self.interproc,
                        self.notif)
        return (msg)

    # /__str__()
    # |Cube3DOpenGLSettingsCls

    class WindowStruct:
        """Parameters for window with 3D image
        """

        def __init__(self):
            self.width = 600
            self.height = 600

            # background color (RGB)
            self.bg_color = (5, 6, 10)

            # Maximum FPS
            self.max_fps = 30

            # If max FPS can not be reached, show warning message?
            # This should be enabled only for debug, because it also cause
            # delay, which is leads to higher delay and message will be shown
            # probably again
            self.not_max_fps_wrn = False

        # /__init__()

        def __str__(self):
            msg = "== Window ==\n" \
                  " Width: {} px\n" \
                  " Height: {} px\n" \
                  " Background color: {}\n" \
                  " Max FPS: {}\n" \
                  " Warn when max FPS not reached? {}\n" \
                  "".format(self.width, self.height, self.bg_color,
                            self.max_fps, self.not_max_fps_wrn)
            return (msg)
        # /__str__()

    # /WindowStruct
    # |Cube3DOpenGLSettingsCls

    class FloorStruct:
        """Virtual floor for 3D cube
        """

        def __init__(self):
            # Color of color in RGB format
            self.color = (40, 40, 50)

            # Relative height compare to dimension
            self.rel_height = 0.2

            # Relative size compare to dimension size
            self.rel_size = 1.3

            # Define relative Y (height) position of "floor". Negative number
            # means "down". Relative value is referenced to dimension
            self.rel_y_pos = -1.2

        # /__init__()

        def __str__(self):
            msg = "== Floor ==\n" \
                  " Color: {}\n" \
                  " Relative height to the cube: {}\n" \
                  " Relative size to the cube: {}\n" \
                  " Relative horizontal position: {}\n" \
                  "".format(self.color, self.rel_height, self.rel_size,
                            self.rel_y_pos)
            return (msg)
        # /__str__()

    # /FloorStruct
    # |Cube3DOpenGLSettingsCls

    class WireframeStruct:
        """Parameters of wireframe
        """

        def __init__(self):
            self.color = (60, 30, 0)
            self.width = 1

        # /__init__()

        def __str__(self):
            msg = "== Wireframe ==\n" \
                  " Color: {}\n" \
                  "".format(self.color)
            return (msg)
        # /__str__()

    # /WireframeStruct
    # |Cube3DOpenGLSettingsCls

    class WallStruct:
        """Parameters of "wall"

        Wall is displayed to highlight actual selected wall when drawing.
        """

        def __init__(self):
            self.color = (150, 0, 0)
            self.width = 3

        # /__init__()

        def __str__(self):
            msg = "== Wall ==\n" \
                  " Color: {}\n" \
                  "".format(self.color)
            return (msg)
        # /__str__()

    # /WallStruct
    # |Cube3DOpenGLSettingsCls

    class LEDStruct:
        """Parameters of LED
        """

        def __init__(self):
            self.size = 0.17

            # Initial RGB color for LED
            self.init_color = [0, 0, 0]

            # Render quality - practical values are 3~15
            self.render_quality = 10

            # LED darken factor (in case that some LED need to be highlighted,
            # others will be darken).
            # 2 means 1/2 of original intensity, 3 means 1/3 of original
            # intensity and so on.
            self.darken_factor = 2

        # /__init__()

        def __str__(self):
            msg = "== LED ==\n" \
                  " Size: {}\n" \
                  " Initial color: {}\n" \
                  " Render quality: {}\n" \
                  "".format(self.size, self.init_color, self.render_quality)
            return (msg)
        # /__str__()

    # /LEDStruct
    # |Cube3DOpenGLSettingsCls

    class InterProcStruct:
        """Structure for interprocess communication
        """

        def __init__(self):
            self.ip = "127.0.10.95"
            # Port for string commands
            self.port_str = 5501

            # Port for binary commands
            self.port_bin = self.port_str + 1

            # Communication timeout
            # It might happen that some module crash during time that another
            # process is trying to communicate with crashed process. In that
            # case there is this fail-safe solution. After some time
            # communication should be interrupted and also other tasks should
            # raise exception.
            # Value is in ms.
            self.timeout_ms = 3000

        # /__init__()

        def __str__(self):
            msg = "== Interprocess communication ==\n" \
                  " IP: {}\n" \
                  " Port strings: {}\n" \
                  " Port binaries: {}\n" \
                  "".format(self.ip, self.port_str, self.port_bin)
            return (msg)
        # __str__()

    # /InterProcStruct
    # |Cube3DOpenGLSettingsCls

    class NotificationsStruct:
        """Parameters for notification server

        Through this server can be send notifications to upper layers.
        For example: "closing" -> user clicked to "X" and want to close
        application. Can be modified some higher layer, so beware! Here is just
        default settings.
        """

        def __init__(self):
            self.ip = "127.0.10.95"
            self.port = 5505

            # There is list of events when notification will be send. Note
            # that sending notification is blocking process, so if you enable
            # any, make sure some process will pick the message
            # Want to notify on close?
            self.on_close = False

        # /__init__()

        def __str__(self):
            msg = '== Notification server ==\n' \
                  ' IP: {}\n' \
                  ' Port: {}\n' \
                  ''.format(self.ip, self.port)
            return (msg)
    # /NotificationsStruct
    # |Cube3DOpenGLSettingsCls


# /Cube3DOpenGLSettingsCls


class Cube3DOpenGLLEDStructCls:
    """Structure which keep necessary information about LEDs

    Since we trying to keep this structure light as much as possible, there
    are no functions. Only RGB information.
    """

    class DummyCls:
        """Just dummy class

        Will be used for creating empty "slots" when creating LED structure
        """
        pass

    class ColorCls:

        def __init__(self, r=0, g=0, b=0):
            self.r = r
            self.g = g
            self.b = b

        def __str__(self):
            msg = "R: {} G: {} B: {}\n".format(self.r, self.g, self.b)
            return (msg)

    # /ColorCls

    def __init__(self, dimension=5,
                 init_color=[127, 127, 127]):
        """Initialization function for LED structure

        :param dimension: Cube dimension. For example 3 means cube 3x3x3 LEDs
        :type dimension: int

        :param init_color: Initial LED color in RGB, where 255 is full bright.
        :type init_color: tuple of int
        """
        self.dimension = dimension
        self.w = []

        for wall_idx in range(dimension):
            # Create new wall dimension ...
            self.w.append(self.DummyCls())
            # ... and inside create new column array
            self.w[wall_idx].c = []

            for column_idx in range(dimension):
                # Create new column dimension ....
                self.w[wall_idx].c.append(self.DummyCls())
                # .. and inside create new led array
                self.w[wall_idx].c[column_idx].led = []

                for led_idx in range(dimension):
                    led = self.w[wall_idx].c[column_idx].led

                    # Kinda trap here. We can not simply add array, because
                    # Python would just create pointer to array, so all LED
                    # would be same.... boring. So we need to clearly say,
                    # that we want every LED special. That's why we using
                    # ColorCls
                    led.append(self.ColorCls(r=init_color[0],
                                             g=init_color[1],
                                             b=init_color[2]))
                # /led
            # /column
        # /wall

    # /__init__()

    def __str__(self):
        line = "<==============================================>\n"
        msg = ""

        for wall_idx in range(self.dimension):
            msg = "{}{}\n".format(msg, line)

            for column_idx in range(self.dimension):
                msg = "{}{}".format(msg, line)

                for led_idx in range(self.dimension):
                    color = self.w[wall_idx].c[column_idx].led[led_idx]

                    msg = "{}w[{}].c[{}].led[{}] = {}" \
                          "".format(msg, wall_idx, column_idx, led_idx,
                                    color)
                # /led
            # /column
        # /wall
        return (msg)
    # /__str__()


# /Cube3DOpenGLLEDStructCls


class Cube3DOpenGLCls:
    """Class for rendering 3D image of cube

    Because of performance, only "set_*" functions are defined. Point is, that
    ZMQ simply queue requests and render them "later". Sending request takes
    basically no time, so we can send many many various request and we do not
    need to care.
    """

    def __init__(self, settings):
        """Initialization function

        :param settings: Settings structure
        :type settings: Struct (Cube3DOpenGLSettingsCls)
        """
        # Save settings to this instance
        self.settings = settings

        # Prepare background process
        self.bg_proc_3D_view = Process(target=self._bg_proc_3D_view_fnc,
                                       args=(self.settings,))

        interproc = self.settings.interproc

        # Prepare request server
        context = zmq.Context()
        self.req_srvr = context.socket(zmq.PUSH)
        self.req_srvr.RCVTIMEO = interproc.timeout_ms
        self.req_srvr.bind("tcp://{}:{}"
                           "".format(interproc.ip, interproc.port_str))

        # Prepare request server for binary data
        context = zmq.Context()
        self.req_srvr_bin = context.socket(zmq.PUSH)
        self.req_srvr_bin.RCVTIMEO = interproc.timeout_ms
        self.req_srvr_bin.bind("tcp://{}:{}"
                               "".format(interproc.ip, interproc.port_bin))

        # Start background process
        self.bg_proc_3D_view.start()

    # =============================| User functions |=========================

    def set_dimension(self, dimension):
        """Set cube dimension

        Since changing cube dimension require to recalculate everything
        from scratch, we need to close and open 3D view. Problematic is
        closing ZMQ socket and it's re-opening so that is why there is this
        function.

        :param dimension: Cube dimension
        :type dimension: int
        """
        # Check dimension
        if (dimension < 2):
            msg = "Can not render cube smaller than dimension=2\n" \
                  " Please increase dimension size!"
            raise Exception(msg)
        # /if dimension too small

        if (dimension == self.get_dimension()):
            # Nothing to do -> exit
            return
        # /if new dimension is same

        # Close 3D window, change settings, re-generate all structures,
        # open new 3D window
        self.close()

        self.settings.dimension = dimension
        self.bg_proc_3D_view = Process(target=self._bg_proc_3D_view_fnc,
                                       args=(self.settings,))
        self.bg_proc_3D_view.start()

    # /set_dimension()

    def get_dimension(self):
        """Get cube dimension

        :returns: Cube dimension
        :rtype: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return (-1)
        return (self.settings.dimension)

    # /get_dimension()

    def close(self):
        """Close background process properly

        This function should be called when it is intended to close 3D view
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self.bg_proc_3D_view.terminate()
        # Wait until process is closed
        self.bg_proc_3D_view.join()

    # /close()

    def get_err_code(self):
        """Get error code of 3D view

        You can check status of 3D window by getting error code

        :returns: None (still running), integer (finished -> error code)
        :rtype: None/int
        """
        return (self.bg_proc_3D_view.exitcode)

    # /get_err_code()

    def set_base_led_struct(self, bse_led_strct):
        """Set whole LED structure (RGB)

        :param bse_led_strct: Structure
        :type bse_led_strct: Cube3DOpenGLLEDStructCls
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return
        # Generate value variable
        val = []
        for wall in bse_led_strct.w:
            for clmn in wall.c:
                for led in clmn.led:
                    # R,G,B - extend solution should be better than 3x append
                    val.extend([led.r, led.g, led.b])
                # /for LED
            # /for column
        # /for wall

        # Create command
        self._send_req_bin(cmd="base_led_strcut", val=val)

    # /set_base_led_struct()

    def set_one_led(self, wall_idx=0, column_idx=0, led_idx=0,
                    r=0, g=0, b=0,
                    check_param=True):
        """Set RGB value for one LED

        If only one LED need to be changed, it is more effective to set
        directly this LED instead of setting all LED on cube

        :param wall_idx: Wall index
        :type wall_idx: int

        :param column_idx: Column index
        :type column_idx: int

        :param led_idx: LED index
        :type led_idx: int

        :param r: Intensity of red spectrum
        :type r: int (0~255)

        :param g: Intensity of green spectrum
        :type g: int (0~255)

        :param b: Intensity of blue spectrum
        :type b: int (0~255)

        :param check_param: Check input parameters? This may consume some
                            CPU time, but you can be sure, that no invalid
                            parameters are given to background process.
                            But if you want performance, you might want to
                            skip this testing
        :type check_param: bool
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        if (check_param):
            dim = self.get_dimension()

            # Check for negative numbers
            if ((wall_idx < 0) or (column_idx < 0) or (led_idx < 0)):
                msg = "Wall ({}), column ({}) and LED index ({}) need" \
                      " to be positive number! Be positive ;)" \
                      "".format(wall_idx, column_idx, led_idx)
                raise Exception(msg)
            if ((wall_idx >= dim) or (column_idx >= dim) or (led_idx >= dim)):
                msg = "Wall ({}), column ({}) and LED index ({}) need to be" \
                      " smaller than cube dimension ({})" \
                      "".format(wall_idx, column_idx, led_idx, dim)
                raise Exception(msg)

            if ((r < 0) or (g < 0) or (b < 0)):
                msg = "R ({}), G ({}) and B ({}) values can not be negative!" \
                      " Think more positively :)".format(r, g, b)
                raise Exception(msg)
            if ((r > 255) or (g > 255) or (b > 255)):
                msg = "R ({}), G ({}) and B ({}) values can not be higher" \
                      " than 255!".format(r, g, b)
                raise Exception(msg)
        # /check_param

        # Generate value (parameter) for command
        val = "{},{},{},{},{},{}".format(wall_idx, column_idx, led_idx,
                                         r, g, b)

        # Send request
        self._send_req(cmd="one_led", val=val)

    # /set_one_led()

    def set_active_wall(self, wall_idx=None):
        """Set active wall

        :param wall_idx: Wall index. If set to none, no wall will be
                         highlighted (equal to disabled)
        :type wall_idx: int (0~(dimension-1))/None
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="wall", val=wall_idx)

    # /set_active_wall

    def set_show_disabled(self, show_disabled=True):
        """Show disabled (off) LEDs?

        :param show_disabled: When disabled, LEDs which are off (0,0,0) will
                              not be rendered
        :type show_disabled: bool
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="show_disabled", val=show_disabled)

    # /set_show_disabled()

    def set_show_inactive(self, show_inactive=False):
        """Show inactive LEDs when wall is selected?

        By default, actual wall is highlighted by special grid. However in
        some cases other "inactive" (deselected) LEDs can be disruptive. This
        option allow to not render them, so actual wall is pretty visible.

        :param show_inactive: Enable/disable rendering inactive LEDs.
        :type show_inactive: bool
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="show_inactive", val=show_inactive)

    # /set_show_inactive()

    def set_zoom(self, zoom_val=1):
        """Set camera zoom

        :param zoom_val: Zoom value
        :type zoom_val: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="zoom", val=zoom_val)

    # /set_zoom()

    def set_angle_x(self, deg=0):
        """Set camera angle in X axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="angleX", val=deg)

    # /set_angle_x()

    def set_angle_y(self, deg=0):
        """Set camera angle in Y axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="angleY", val=deg)

    # /set_angle_y()

    def set_angle_z(self, deg=0):
        """Set camera angle in Z axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="angleZ", val=deg)

    # /set_angle_z()

    def reset_cube_view(self):
        """Reset all rotations and zoom
        """
        # Check if window is not already closed
        if (not (self.get_err_code() is None)):
            # Process terminated
            return

        self._send_req(cmd="reset_cube_view")

    # /reset_cube_view()

    # ==========================| End of user functions |=====================
    def _send_req(self, cmd="zoom", val="3"):
        """Send request to background process

        :param cmd: Command
        :type cmd: str

        :param val: value
        :type val: str
        """
        self.req_srvr.send_string("{} {}".format(cmd, val))

        # Following is handy for debug, but performance cost can be high
        # print("sending.... {} {}".format(cmd, val))

    # /send_cmd()

    def _send_req_bin(self, cmd="set_base_led_struct", val=b"\x35\x75\x33"):
        """Send request to background process as binary data

        Sending binary data can improve performance in some cases. For example
        when sending raw data of whole RGB cube, it worth to transmit RGB
        information as 3 binary bytes instead of 3 characters, which might be
        at worst case 9 bytes (+ 3x separator -> +3 bytes). In this case is
        performance cost quite visible especially for "bigger cubes"

        :param cmd: Command
        :type cmd: str

        :param val: value
        :type val: list (byte)
        """
        # Add specific string before command and space behind command as usual
        cmd = "{} ".format(cmd)

        # Merge command and values.
        data = bytearray(cmd, encoding='utf-8') + bytearray(val)

        self.req_srvr_bin.send(data=data)
        # Following is handy for debug, but performance cost can be high
        # print("sending bin.... {} {}".format(cmd, val))

    # /_send_req_bin()

    def _bg_proc_3D_view_fnc(self, settings):
        """Process which render 3D view

        :param settings: Settings structure
        :type settings: struct
        """
        # ======================| Interprocess initialization |===============
        # Request queue for string commands
        context = zmq.Context()
        req_que = context.socket(zmq.PULL)
        req_que.RCVTIMEO = settings.interproc.timeout_ms
        req_que.connect("tcp://{}:{}".format(settings.interproc.ip,
                                             settings.interproc.port_str))

        # Request queue for binary commands
        context = zmq.Context()
        req_que_bin = context.socket(zmq.PULL)
        req_que_bin.RCVTIMEO = settings.interproc.timeout_ms
        req_que_bin.connect("tcp://{}:{}".format(settings.interproc.ip,
                                                 settings.interproc.port_bin))

        # For sending notifications
        context = zmq.Context()
        ntf_send = context.socket(zmq.PUSH)
        ntf_send.RCVTIMEO = settings.interproc.timeout_ms
        ntf_send.bind("tcp://{}:{}"
                      "".format(settings.notif.ip, settings.notif.port))
        # ========================| Initialize other classes |================
        # 3D elements
        elements_3D = _Elements3DStructCls(settings)

        # RGB value for every single LED
        leds = Cube3DOpenGLLEDStructCls(settings.dimension,
                                        settings.led.init_color)

        # Used in debug messages
        id = "cube_3D_OpenGL: _bg_proc_3D_view_fnc: "

        # ==========================| Function definitions |==================
        def _init_OpenGL():
            # ==========================| OpenGL initialization |=============
            # Initialize Window
            try:
                glutInit()
            except Exception as e:
                msg = "It seems that you have PyOpenGL installed, but there " \
                      "is some problem with GLUT :/\n" \
                      'You might want to install "freeglut3" package\n' \
                      f'since at pip installation can be bug\n{e}'
                raise Exception(msg)

            glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
            glutInitWindowSize(settings.window.width,
                               settings.window.height)
            glutCreateWindow("3D cube")

            # Also set callback when user will clock to "close" button
            glutCloseFunc(_on_close)

            # GLUT need to know draw function in case that some event would
            # interact with 3D image (maybe keyboard, mouse?)
            glutDisplayFunc(_draw)

            # We want to show objects not by their draw sequence, but
            # according their position as in real life
            # specify the value used for depth buffer comparisons
            glDepthFunc(GL_LESS)
            glEnable(GL_DEPTH_TEST)

            glClearColor(settings.window.bg_color[0] / 255,
                         settings.window.bg_color[1] / 255,
                         settings.window.bg_color[2] / 255,
                         0)

        # /_init_OpenGL()

        def _on_close():
            print("{}User clicked to 'X' button. Closing...".format(id))
            rt.keep_running = False

            # If notification is set, send it
            if (settings.notif.on_close):
                ntf_send.send_string("cube_3D closing")

        # /_on_close()

        def _draw_wireframe():
            # glBegin and glEnd define "group" of vertexes
            glLineWidth(settings.wireframe.width)
            glBegin(GL_LINES)
            glColor3f(settings.wireframe.color[0] / 255,
                      settings.wireframe.color[1] / 255,
                      settings.wireframe.color[2] / 255)
            for edge in elements_3D.wireframe.e:
                for vertex in edge:
                    # Pass vertex coordinates to OpenGL engine
                    glVertex3fv(elements_3D.wireframe.v[vertex])
            glEnd()

        # /_draw_wireframe()

        def _draw_wall():
            # Draw wall only if some is selected
            if (not (rt.selected_wall is None)):
                # Move object in Z plane according to selected wall
                glPushMatrix()
                # There is another small offset to get highlight in front of
                # wireframe
                z_coord = (((settings.dimension - 1) / 2) - rt.selected_wall
                           + 0.01)
                glTranslatef(0, 0, z_coord)

                glLineWidth(settings.wall.width)
                glBegin(GL_LINES)
                glColor3f(settings.wall.color[0] / 255,
                          settings.wall.color[1] / 255,
                          settings.wall.color[2] / 255)

                for edge in elements_3D.wall.e:
                    for vertex in edge:
                        glVertex3fv(elements_3D.wall.v[vertex])
                glEnd()
                glPopMatrix()
            # /if wall selected

        # /_draw_wall()

        def _draw_floor():
            glPushMatrix()
            # Move floor object "down"
            glTranslatef(0,
                         settings.floor.rel_y_pos *
                         ((settings.dimension - 1) / 2),
                         0)

            # Draw quad
            glBegin(GL_QUADS)
            glColor3f(settings.floor.color[0] / 255,
                      settings.floor.color[1] / 255,
                      settings.floor.color[2] / 255)
            for face in elements_3D.floor.v:
                for v in face:
                    glVertex3fv(v)
            glEnd()

            glPopMatrix()

        # /_draw_floor()

        def _draw_leds():
            # Wall index, column index and LED index are basically
            # coordinates -> take advantage of this and place them directly
            dim = settings.dimension
            cube_offset = (dim - 1) / 2

            # This is performance killer. However should not this run on GPU
            # instead of CPU?
            for wall_idx in range(dim):
                for column_idx in range(dim):
                    for led_idx in range(dim):
                        color = leds.w[wall_idx].c[column_idx].led[led_idx]
                        if ((not rt.show_disabled) and
                                ((color.r == 0) and (color.g == 0) and
                                 (color.b == 0))):
                            # If LED is "off" and option is enabled, continue
                            # on next LED (do not render)
                            continue
                        # If actual LED is not at active wall and should not
                        # be rendered
                        elif ((rt.selected_wall is not None) and
                              (rt.selected_wall != wall_idx) and
                              (not rt.show_inactive)):
                            continue

                        # Work on local matrix -> backup "upper"
                        glPushMatrix()

                        # Move it! (x is "column", y is "led", z is "wall"
                        # but not literally since zero is at center of cube
                        # and not at left bottom corner)
                        glTranslatef(column_idx - cube_offset,
                                     led_idx - cube_offset,
                                     (-1 * wall_idx) + cube_offset)

                        # If LED should be shown regularly
                        if ((rt.selected_wall is None) or
                                (rt.selected_wall == wall_idx)):
                            glColor3f(color.r, color.g, color.b)
                        else:
                            r = color.r / settings.led.darken_factor
                            g = color.g / settings.led.darken_factor
                            b = color.b / settings.led.darken_factor
                            if (r < 0):
                                r = 0
                            if (g < 0):
                                g = 0
                            if (b < 0):
                                b = 0
                            glColor3f(r, g, b)
                        # R, quality, quality
                        glutSolidSphere(settings.led.size,
                                        settings.led.render_quality,
                                        settings.led.render_quality)

                        glPopMatrix()
                    # /for LED
                # /for column
            # /for wall

        # /_draw_leds()

        def _draw():
            # Clear buffers
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            _draw_wireframe()
            _draw_wall()
            _draw_floor()
            _draw_leds()

            # Show image (since image is double buffered, switch current
            # buffer with previous one -> remove flickering)
            glutSwapBuffers()

        # /_draw()

        def _proc_req(cmd="zoom", val="1"):
            """Process request
            """
            if (rt.debug):
                print("CMD: {}\nVAL: {}\n\n".format(cmd, val))

            if (cmd == "wall"):
                if (val == "None"):
                    rt.selected_wall = None
                else:
                    rt.selected_wall = int(val)
            elif (cmd == "show_disabled"):
                if ((val == "False") or (val == "0")):
                    rt.show_disabled = False
                else:
                    rt.show_disabled = True
            elif (cmd == "show_inactive"):
                if ((val == "False") or (val == "0")):
                    rt.show_inactive = False
                else:
                    rt.show_inactive = True
            elif (cmd == "zoom"):
                rt.set_zoom(zoom=float(val))

            elif (cmd == "angleX"):
                rt.set_angle_x(deg=float(val))
            elif (cmd == "angleY"):
                rt.set_angle_y(deg=float(val))
            elif (cmd == "angleZ"):
                rt.set_angle_z(deg=float(val))
            elif (cmd == "reset_cube_view"):
                rt.reset_cube_view()

            elif (cmd == "base_led_strcut"):
                # Values are just list of int
                idx = 0

                for wall in leds.w:
                    for clmn in wall.c:
                        for led in clmn.led:
                            # Save new values. But OpenGL works with float,
                            # so we need to divide. It is better to send
                            # RGB information (short message) and then
                            # divide, although it will cost some CPU time
                            # here
                            led.r = val[idx] / 255
                            led.g = val[idx + 1] / 255
                            led.b = val[idx + 2] / 255

                            # Next time next LED...
                            idx = idx + 3
                            # /for color
                        # /for LED
                    # /for column
                # /for wall
            elif (cmd == "one_led"):
                # Parse string to list
                values = val.split(",")

                # Empty array
                val_int = []

                # Get integer from string
                for one_val in values:
                    val_int.append(int(one_val))

                # We expect, that higher layer already checked parameters.
                # So no check here. Only dummy parse. If some exception
                # occurs, it is problem of layer, which sent command

                # To make it simple, get selected LED
                led = leds.w[val_int[0]].c[val_int[1]].led[val_int[2]]

                # ... and write RGB values
                led.r = val_int[3] / 255
                led.g = val_int[4] / 255
                led.b = val_int[5] / 255

                if (rt.debug):
                    print(leds.w[val_int[0]].c[val_int[1]].led[val_int[2]])
            else:
                print("{}Unknown command: {}".format(id, cmd))

        # /_parse_req()

        def _get_lits_of_cmds():
            """Get list of commands in queue

            :returns: List of commands. Every item in array have "str" array
                      which contains command and value
            """

            list_of_cmds = []

            # Try to get binary commands
            while (True):
                try:
                    bin_data = req_que_bin.recv(flags=zmq.NOBLOCK)
                except zmq.Again:
                    # No command -> get out from loop
                    break
                else:
                    # Go through array -> when found \x32 (space) -> stop
                    # -> determine command -> then process (_proc_req)
                    cmd = ""
                    val = []

                    # Index counter (do not want space after cmd -> 0+1)
                    idx_cnt = 1
                    for byte in bin_data:
                        # If found space -> break
                        if (byte == 32):
                            break
                        else:
                            # Else add character to cmd string
                            cmd = "{}{}".format(cmd, chr(byte))
                            idx_cnt = idx_cnt + 1
                        # /else still command
                    # /for bin_data
                    val = bin_data[idx_cnt:]
                    # Add to list of commands
                    list_of_cmds.append([cmd, val])
                # /else binary command
            # /while getting all binary commands

            # Try to get string commands
            while (True):
                try:
                    # Format is: <command> <value>
                    data = req_que.recv_string(flags=zmq.NOBLOCK)
                except zmq.Again:
                    # No command -> get out from loop
                    break
                else:
                    # Add command to list of commands.
                    data = data.split(sep=" ", maxsplit=1)
                    list_of_cmds.append([data[0], data[1]])
            # /while getting all string commands

            # if there is none or 1 command, do not optimize
            if (len(list_of_cmds) < 2):
                # No optimizations needed
                return (list_of_cmds)

            # Remove duplicity. This is quite complex. Expecting maximally
            # few commands in queue, which simplify some cases
            # ============================| Remove duplicity |================
            # If there is command "one_led", then we can expect more of these
            # and it is important their order relative to possible command
            # "base_led_strcut" -> if found command "one_led", skip
            # optimizations, since processing would be quite complex

            # Index counter
            idx_cnt = 0

            # When some command is found, only latest index is recorded. So
            # far nothing found
            zoom_idx = None
            angleX_idx = None
            angleY_idx = None
            angleZ_idx = None
            rst_cbe_v_idx = None
            bse_led_idx = None
            wall_idx = None
            show_dis_idx = None
            show_inact_idx = None

            for data_str in list_of_cmds:
                cmd = data_str[0]
                if (cmd == "one_led"):
                    # Command found - no optimizations
                    return (list_of_cmds)
                # /if command "one_led" found
                elif (cmd == "base_led_strcut"):
                    bse_led_idx = idx_cnt
                elif (cmd == "wall"):
                    wall_idx = idx_cnt
                elif (cmd == "show_disabled"):
                    show_dis_idx = idx_cnt
                elif (cmd == "show_inactive"):
                    show_inact_idx = idx_cnt
                elif (cmd == "zoom"):
                    zoom_idx = idx_cnt
                elif (cmd == "angleX"):
                    angleX_idx = idx_cnt
                elif (cmd == "angleY"):
                    angleY_idx = idx_cnt
                elif (cmd == "angleZ"):
                    angleZ_idx = idx_cnt
                elif (cmd == "reset_cube_view"):
                    rst_cbe_v_idx = idx_cnt
                else:
                    raise Exception('Unexpected command "{}"'.format(cmd))

                idx_cnt = idx_cnt + 1
            # /for all commands in list

            # Check if there is some angle settings before "reset_cube_view".
            # If so, all "angle" commands before this command are pointless
            if (rst_cbe_v_idx):
                # OK, reset view command was found -> check if some angles
                # are affected
                if (angleX_idx):
                    if (angleX_idx < rst_cbe_v_idx):
                        angleX_idx = None
                # /angleX
                if (angleY_idx):
                    if (angleY_idx < rst_cbe_v_idx):
                        angleY_idx = None
                # /angleY
                if (angleZ_idx):
                    if (angleZ_idx < rst_cbe_v_idx):
                        angleZ_idx = None
                # /angleZ
            # /if reset view found

            # Now we can create new queue. Point is, that does not matter on
            # order, since it will be rendered at one moment
            list_of_opt_cmds = []
            if (wall_idx):
                list_of_opt_cmds.append(list_of_cmds[wall_idx])
            if (show_dis_idx):
                list_of_opt_cmds.append(list_of_cmds[show_dis_idx])
            if (show_inact_idx):
                list_of_opt_cmds.append(list_of_cmds[show_inact_idx])
            if (zoom_idx):
                list_of_opt_cmds.append(list_of_cmds[zoom_idx])
            if (angleX_idx):
                list_of_opt_cmds.append(list_of_cmds[angleX_idx])
            if (angleY_idx):
                list_of_opt_cmds.append(list_of_cmds[angleY_idx])
            if (angleZ_idx):
                list_of_opt_cmds.append(list_of_cmds[angleZ_idx])
            if (rst_cbe_v_idx):
                list_of_opt_cmds.append(list_of_cmds[rst_cbe_v_idx])
            if (bse_led_idx):
                list_of_opt_cmds.append(list_of_cmds[bse_led_idx])
            # ============================| /Remove duplicity |===============

            return (list_of_opt_cmds)

        # /_get_lits_of_cmds()

        # /==========================| Function definitions |=================
        try:
            # Set process name -> much easier to debug then
            setproctitle('Cube_OpenGL_bg')
        except Exception:
            msg = 'Can not set process name. Nothing serious.'
            print(msg)

        # Run initialization routine
        _init_OpenGL()

        # Runtime variables/functions/class
        # This will also prepare matrix for OpenGL view
        rt = _RuntimeVarCls(settings)

        # Recalculate FPS to time
        time_fps = 1 / settings.window.max_fps

        # Loop routine - run while keep_running is set to True
        while (rt.keep_running):
            # Get actual time
            time_begin = perf_counter()

            # Check events. Callback were already set at the _init_OpenGL(),
            # so basically there is nothing more needed
            glutMainLoopEvent()

            """
            # Try to get command(s). For cycle define maximum number of
            # attempts for performing command requests
            redraw = False
            """

            list_of_cmds = _get_lits_of_cmds()
            if (len(list_of_cmds) > 0):
                # Process commands
                for data_str in list_of_cmds:
                    _proc_req(cmd=data_str[0], val=data_str[1])

                # And redraw
                _draw()
            # /if there are some commands

            # Since processing also took some time, let's calculate how long
            # process can sleep -> if negative -> no sleep

            # Get actual time
            time_end = perf_counter()

            # Calculate sleep interval
            time_sleep = (time_begin + time_fps) - time_end
            if (time_sleep > 0):
                sleep(time_sleep)
            # /if enough time -> sleep
            else:
                if (settings.window.not_max_fps_wrn):
                    # Too high GPU load
                    print("{}"
                          " too high CPU/GPU load!\n"
                          "Delta time: {} (cmds: {})\n\n"
                          "".format(id, time_sleep, len(list_of_cmds)))
            # /else can not reach max FPS
        # /while window running

    # /_bg_proc_3D_view_fnc()


# /Cube3DOpenGL


class _Elements3DStructCls:
    """Structure that keep vertexes/edges of 3D elements

    This structure keep data of wireframe, floor and 3D LED on cube
    """

    def __init__(self, settings):
        self.wireframe = self.WireframeCls(settings)
        self.wall = self.WallCls(settings)
        self.floor = self.FloorCls(settings)

    # /__init__()
    # |_Elements3DStructCls

    class WireframeCls:
        """Wireframe vertexes and edges
        """

        def __init__(self, settings):
            # Keep it short - settings dimension in dim -> code will be
            # easy to read.
            # And since distance from -1 to +1 is 2, we need to take half of
            # distance

            dim = (settings.dimension - 1) / 2
            # Verticles
            self.v = (
                (1 * dim, -1 * dim, -1 * dim),
                (1 * dim, 1 * dim, -1 * dim),
                (-1 * dim, 1 * dim, -1 * dim),
                (-1 * dim, -1 * dim, -1 * dim),
                (1 * dim, -1 * dim, 1 * dim),
                (1 * dim, 1 * dim, 1 * dim),
                (-1 * dim, -1 * dim, 1 * dim),
                (-1 * dim, 1 * dim, 1 * dim)
            )

            # Edges
            self.e = (
                (0, 1),
                (0, 3),
                (0, 4),
                (2, 1),
                (2, 3),
                (2, 7),
                (6, 3),
                (6, 4),
                (6, 7),
                (5, 1),
                (5, 4),
                (5, 7)
            )
        # /__init__()

    # /WireframeCls
    # |_Elements3DStructCls

    class WallCls:
        """Vertexes for wall
        """

        def __init__(self, settings):
            dim = (settings.dimension)
            # Since zero is in the middle of the cube, we need to calculate
            # with offset
            cube_offset = (dim - 1) / 2

            # We need to make grid.
            self.v = []
            self.e = []

            # Generate vertexes for grid's "lines"
            for cnt in range(dim):
                self.v.extend([(- cube_offset, cnt - cube_offset, 0),
                               (cube_offset, cnt - cube_offset, 0)])

            # Generate vertexes for grid's "columns". First and last are
            # already there -> do not need to redefine
            for cnt in range(1, dim - 1):
                self.v.extend([(-cube_offset + cnt, -cube_offset, 0),
                               (-cube_offset + cnt, cube_offset, 0)])

            # Now it is time for edges
            #  * Edge lines
            for cnt in range(dim):
                self.e.append((2 * cnt, 2 * cnt + 1))
            # * Edge columns - first & last
            self.e.extend([(0, 2 * dim - 2), (1, 2 * dim - 1)])

            for cnt in range(2 * dim, 2 * dim + 2 * (dim - 2), 2):
                self.e.append((cnt, cnt + 1))
        # /__init__()

    # /WallCls
    # |_Elements3DStructCls

    class FloorCls:
        """Vertexes for floor
        """

        def __init__(self, settings):
            # Keep it short - settings dimension in dim -> code will be
            # easy to read.
            # And since distance from -1 to +1 is 2, we need to take half of
            # distance
            dim = ((settings.dimension - 1) / 2) * settings.floor.rel_size

            # Define Y coordinates ("height") for bottom and top plane
            btm_y = (-1 * ((settings.dimension - 1) / 2) *
                     settings.floor.rel_height)

            # Floor will be generated through "quads" (4 coordinates)
            # However it really depends on sequence of vertexes!!!
            self._bottom = ((-1 * dim, btm_y, 1 * dim),
                            (1 * dim, btm_y, 1 * dim),
                            (1 * dim, btm_y, -1 * dim),
                            (-1 * dim, btm_y, -1 * dim),)
            self._up = ((-1 * dim, 0, 1 * dim),
                        (1 * dim, 0, 1 * dim),
                        (1 * dim, 0, -1 * dim),
                        (-1 * dim, 0, -1 * dim),)

            self._front = ((-1 * dim, btm_y, 1 * dim),
                           (1 * dim, btm_y, 1 * dim),
                           (1 * dim, 0, 1 * dim),
                           (-1 * dim, 0, 1 * dim))
            self._back = ((-1 * dim, btm_y, -1 * dim),
                          (1 * dim, btm_y, -1 * dim),
                          (1 * dim, 0, -1 * dim),
                          (-1 * dim, 0, -1 * dim))

            self._left = ((-1 * dim, btm_y, 1 * dim),
                          (-1 * dim, 0, 1 * dim),
                          (-1 * dim, 0, -1 * dim),
                          (-1 * dim, btm_y, -1 * dim))

            self._right = ((1 * dim, btm_y, 1 * dim),
                           (1 * dim, 0, 1 * dim),
                           (1 * dim, 0, -1 * dim),
                           (1 * dim, btm_y, -1 * dim))

            self.v = (self._bottom,
                      self._up,
                      self._front,
                      self._back,
                      self._left,
                      self._right,
                      )
        # /__init__()
    # /FloorCls
    # |_Elements3DStructCls


# /class _Elements3DStructCls


class _RuntimeVarCls:
    """Keep runtime variables for background process (3D view) and allow to
       set them
    """
    # ============================| Static variables |========================
    # Constant used for calculating radians from degrees
    rad_coef = math.pi / 180

    # This constant is taken from C headers and it is calculated as pi/360
    pi_over_360 = math.pi / 360

    def __init__(self, settings):
        # Save settings to instance
        self.settings = settings

        # We want to run this function. When user click to "close" button,
        # callback function will change this variable
        self.keep_running = True

        # If enabled, prints some extra outputs
        self.debug = False

        # Angle of camera in X,Y and Z
        self._angle_x = 0
        self._angle_y = 0
        self._angle_z = 0

        # This is approximately zoom value 1x
        self._zoom = 1.0

        # Selected wall
        self.selected_wall = None

        # Will disabled LED (RGB - off) be rendered?
        self.show_disabled = True
        # When some wall is selected - render inactive LEDs?
        self.show_inactive = False

        self._prepare_default_view_matrix()

        # View matrix - this matrix is alpha and omega when displaying LED
        # cube. Since here are kept all rotations and zoom, I decided to keep
        # that view matrix here, so zoom functions and rotation functions
        # can work with it.
        # self._vm
        self._reset_view_matrix()

        glLoadMatrixf(self._vm)

    # /__init__()

    # ==================================| Zoom |==============================
    def set_zoom(self, zoom=1.0):
        """Set user zoom value

        :param zoom: User defined zoom. Recommended values are from 0.1 to 2
        :type zoom: int
        """
        # save zoom value for get function
        self._zoom = zoom

        # Zoom changed, time to recalculate view matrix
        self._update_view_matrix()

    # /set_zoom()

    def get_zoom(self):
        """Get zoom value

        :returns: Zoom value as relative number
        :rtype: float
        """
        return (self._zoom)

    # /get_zoom()

    # =================================| Angle |==============================
    def set_angle_x(self, deg=45):
        """Set absolute camera angle in X axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self._angle_x = deg
        self._update_view_matrix()

    # /set_angle_x()

    def get_angle_x(self):
        """Get absolute angle of camera in X axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._angle_x)

    # /get_angle_x()

    def set_angle_y(self, deg=45):
        """Set absolute camera angle in Y axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self._angle_y = deg
        self._update_view_matrix()

    # /set_angle_y()

    def get_angle_y(self):
        """Get absolute angle of camera in Y axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._angle_y)

    # /get_angle_y()

    def set_angle_z(self, deg=45):
        """Set absolute camera angle in Z axe

        :param deg: Angle in degrees
        :type deg: int
        """
        self._angle_z = deg
        self._update_view_matrix()

    # /set_angle_z()

    def get_angle_z(self):
        """Get absolute angle of camera in Z axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self._angle_z)

    # /get_angle_z()

    def reset_cube_view(self):
        """Reset all angles and zoom"""
        self._reset_view_matrix()
        self._angle_x = self._angle_y = self._angle_z = 0
        self._zoom = 1.0

        self._update_view_matrix()

    # /reset_cube_view()

    # ===========================| Internal functions |=======================
    def _update_view_matrix(self):
        """When angles are changed, this function have to be called

        This function will update view matrix according to set angles
        """
        # Reset view matrix and if needed do rotations and movements
        self._reset_view_matrix()

        # Calculate sin and cos for x,y,z angles
        # only if angle is not zero -> if zero -> no transformation is
        # needed
        if (self.get_angle_x() != 0):
            sinx, cosx = self._get_sin_cos_angle(self.get_angle_x())
        if (self.get_angle_y() != 0):
            siny, cosy = self._get_sin_cos_angle(self.get_angle_y())
        if (self.get_angle_z() != 0):
            sinz, cosz = self._get_sin_cos_angle(self.get_angle_z())

        if (self.get_zoom() != 1.0):
            # Since zoom value and camera distance are opposite things, we need
            # to recalculate it. First get distance multiplier (2x zoom -> 1/2
            # of distance). But zoom 1x is already set -> only difference
            mov_x = 0
            mov_y = 0
            mov_z = ((self.default_cam_dist / self.get_zoom())
                     - self.default_cam_dist)

            mov_cam = [1, 0, 0, 0,
                       0, 1, 0, 0,
                       0, 0, 1, 0,
                       mov_x, mov_y, mov_z, 1]

            # Grab perspective view matrix and multiply it with movement
            # matrix
            self._vm = self._mul_matrix_4x4(mat_a=self._vm,
                                            mat_b=mov_cam)

        # /if zoom is needed

        # Multiply X Y and Z matrixes to get matrix for 3D view.
        # OpenGL index matrix from up to down, from left to right. So
        # format in Python is "rotated" by 90 degrees
        # http://nehe.ceske-hry.cz/cl_gl_matice.php

        if (self.get_angle_x() != 0):
            # Rotation on X axe
            xm = [1, 0, 0, 0,
                  0, cosx, sinx, 0,
                  0, -sinx, cosx, 0,
                  0, 0, 0, 1]
            self._vm = self._mul_matrix_4x4(self._vm, xm)
        # /if X rotation is needed

        if (self.get_angle_y() != 0):
            # Rotations on Y axe
            ym = [cosy, 0, -siny, 0,
                  0, 1, 0, 0,
                  siny, 0, cosy, 0,
                  0, 0, 0, 1]
            self._vm = self._mul_matrix_4x4(self._vm, ym)
        # /if Y rotation is needed

        if (self.get_angle_z() != 0):
            # Rotation on Z axe
            zm = [cosz, sinz, 0, 0,
                  -sinz, cosz, 0, 0,
                  0, 0, 1, 0,
                  0, 0, 0, 1]
            self._vm = self._mul_matrix_4x4(self._vm, zm)
        # /if Z rotation is needed

        glLoadMatrixf(self._vm)

    # /_update_view_matrix

    def _reset_view_matrix(self):
        # Base matrix -> used as accumulator
        # let's set initial view matrix, which replace gluPerspective()
        # function, which was removed in OpenGL 3. It also setup default
        # view distance
        # https://stackoverflow.com/questions/2417697/gluperspective-was-removed-in-opengl-3-1-any-replacements # noqa

        self._vm = [0] * 16
        # Copy data from default matrix
        for idx in range(len(self._vm)):
            self._vm[idx] = self._vm_default[idx]

    # /_reset_view_matrix()

    def _prepare_default_view_matrix(self):
        # Prepare more variables for view matrix. These values need to be
        # setup only once, so that is reason why are here

        # Specifies the field of view angle, in degrees, in y direction
        fov = 45  # Perspective - 45 deg

        # Specifies the aspect ratio that determines the field of view
        #   in x direction. The aspect ratio is the ratio of x (width)
        #   to y (height)
        aspect = self.settings.window.width / self.settings.window.height

        # Specifies the distance from the viewer to the near clipping
        #   plane (always positive)
        znear = 0.1

        #  Specifies the distance from the viewer to the far clipping
        #   plane (always positive).
        # Far perspective depends on real cube size, so it have to be
        # calculated
        zfar = 50 * self.settings.dimension
        f = 1 / math.tan(fov * self.pi_over_360)

        # Create default (backup) view matrix
        self._vm_default = [0] * 16
        # Now write only non-zero values
        self._vm_default[0] = f / aspect
        self._vm_default[5] = f
        self._vm_default[10] = (zfar + znear) / (znear - zfar)
        self._vm_default[11] = -1
        self._vm_default[14] = (2 * zfar * znear) / (znear - zfar)

        # We want to move camera from Z position 0.0 to ... little bit more
        # far away
        # Set default camera distance and set default zoom value
        self.default_cam_dist = -2.2 * self.settings.dimension

        # Define movements in all axes
        mov_x = 0
        mov_y = 0
        mov_z = self.default_cam_dist
        mov_cam = [1, 0, 0, 0,
                   0, 1, 0, 0,
                   0, 0, 1, 0,
                   mov_x, mov_y, mov_z, 1]

        # Grab perspective view matrix and multiply it with movement matrix
        self._vm_default = self._mul_matrix_4x4(mat_a=self._vm_default,
                                                mat_b=mov_cam)

    # /_prepare_default_view_matrix()

    def _mul_matrix_4x4(self, mat_a, mat_b):
        """Multiply matrix A and B and return result matrix
        """
        # Create empty result list/matrix
        res = [0] * 16

        # Indexes for result matrix
        res_column = 0
        res_row = 0

        # Go through every element in result matrix
        for res_idx in range(len(res)):
            # Calculate value for actual element of result (res[res_idx])
            for i in range(4):
                # mat_a[row] * mat_b[column]
                a = mat_a[res_row + i * 4]
                b = mat_b[res_column * 4 + i]
                res[res_idx] = res[res_idx] + a * b
                # /column
            # /row

            # Increase result row index & check if it is time to change
            # column
            res_row = res_row + 1
            if (res_row >= 4):
                res_column = res_column + 1
                res_row = 0
            # /If is time to move to another column
        # /res
        return (res)

    # /_mul_matrix_4x4()

    def _get_sin_cos_angle(self, angle):
        """Returns sin(angle) and cos(angle)

        :returns: sin(angle), cos(angle)
        :rtype: float, float
        """
        # Try fast calculation - kinda optimization
        if (angle == 0):
            return (0, 1)
        elif (angle == 90):
            return (1, 0)
        elif (angle == 180):
            return (0, -1)
        elif (angle == 270):
            return (-1, 0)
        # /optimization
        # Else we need to calculate sin and cos :(

        # Get angle in radians - use precalculated constant
        angle_rad = self.rad_coef * angle

        # Calculate sin and cos of angle
        sin_angle = math.sin(angle_rad)
        cos_angle = math.cos(angle_rad)

        return (sin_angle, cos_angle)
    # /_get_sin_cos_angle()
# /class _RuntimeVarCls

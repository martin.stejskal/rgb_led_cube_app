#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==========================================
Common functions/classes for most modules
==========================================

Why write same code for every module?

``author`` Martin Stejskal

``version`` 0.4.4
"""

# Logging module
import logging
# Traceback
import inspect
import traceback

import sys
import os.path

# This will define global variable which always keep absolute path to project
# root. Thanks to this we can distribute sources and binaries without
# any installation, since Pyinstaller actually change path when creating
# binaries
if getattr(sys, 'frozen', False):
    app_path = "{}/_internal/".format(os.path.dirname(sys.executable))
elif __file__:
    app_path = "{}/..".format(os.path.dirname(__file__))


# /define correct path


class CmnLgrCls:
    """Automatically add filename, line number and function name

    This can be useful when you want to keep track about layers and functions
    and you want to avoid copy/paste
    """

    def __init__(self,
                 logger_name):
        """
        :param logger_name: Logger name - will be used to link logger with
                            "qualname" in logger configuration file
        :type logger_name: str
        """
        self.lgr = logging.getLogger(logger_name)

    # /__init__()
    def debug(self, msg):
        self.lgr.debug(self._mod_msg(msg))

    def info(self, msg):
        self.lgr.info(self._mod_msg(msg))

    def warn(self, msg):
        self.lgr.warn(self._mod_msg(msg))

    def warning(self, msg):
        self.lgr.warn(self._mod_msg(msg))

    def error(self, msg):
        self.lgr.error(self._mod_msg(msg))

    def critical(self, msg):
        self.lgr.critical(self._mod_msg(msg))

    # ===========================| Internal functions |=======================
    def _mod_msg(self, msg):
        """Modify message

        Add msg_prefix if not empty

        :param msg: logger message
        :type msg: str
        """
        # Get data
        frame, filename, line_number, function_name, lines, index = \
            inspect.stack()[2]

        if (function_name == "<module>"):
            function_name = "main"

        # Remove absolute path from filename
        if getattr(sys, 'frozen', False):
            filename = filename[len(app_path):]
        elif __file__:
            # There is extra "lib/.." in path
            filename = filename[len(app_path) - 6:]

        # Put them to variable as prefix
        pre_msg = "    {}:{} {}()\n".format(filename,
                                            line_number,
                                            function_name)

        # merge & return
        return ("{}{}".format(pre_msg, msg))
    # /_mod_msg()


# /CmnLgrCls


class DummyCls:
    """Just dummy class"""
    pass


# /DummyCls


def log_traceback(logger, type='error'):
    """Log actual traceback to logger

    :param type: Log traceback as debug/info/warning/error/critical at
                 logger
    :type type: str (debug/info/warning/error/critical)

    Very useful in cases when exception is raised and traceback need to be
    shown.
    """
    exc_type, exc_value, exc_tb = sys.exc_info()
    tb = traceback.format_exception(exc_type, exc_value, exc_tb)

    # Generate message
    msg = ""
    for line in tb:
        msg = msg + line

    # Unify type
    type = type.lower()

    if (type == 'debug'):
        logger.debug(msg)
    elif (type == 'info'):
        logger.info(msg)
    elif ((type == 'warn') or (type == 'warning')):
        logger.warning(msg)
    elif (type == 'error'):
        logger.error(msg)
    elif (type == 'critical'):
        logger.critical(msg)
    else:
        raise Exception('Unexpected type "{}"!'.format(type))
# /log_traceback()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
==============
Console API
==============

This module connects GUI with low-level cube control system. Target is to
distinguish GUI and "console" interface.

``author`` Martin Stejskal

``version`` 0.6.5
"""

# Our 3D LED cube window
from lib.cube_3D import Cube3DWSettingsCls

# tuned logger
from lib.common import CmnLgrCls

# Background process
from lib.ca.ca_bg_proc import ConsoleAPIBgCls

# IPC
from lib.ca.ca_ipc import ConAPIIPCMasterCls, ConAPIIPCSettingsCls

# Settings for effects
from lib.ca.ca_bg_proc_effects import ConsoleAPIBgEffectsSettingsCls

# Common things for some functions
from lib.ca.ca_common import ConAPICommonCls

# Modules/functionality
from lib.ca.ca_pos import ConAPIPosCls
from lib.ca.ca_wall import ConAPIWallCls
from lib.ca.ca_pwm import ConAPIPWMCls
from lib.ca.ca_real_cube import ConAPIRealCubeCls
from lib.ca.ca_op import ConAPIOpCls
from lib.ca.ca_btn_array import ConAPIBtnArrayCls
from lib.ca.ca_play import ConAPIPlayCls
from lib.ca.ca_load_save import ConAPILoadSaveCls
from lib.ca.ca_rndm import ConAPIRndmCls
from lib.ca.ca_notifications import ConAPINotificationsCls
from lib.ca.ca_edit import ConAPIEditCls
from lib.ca.ca_view import ConAPIViewCls
from lib.ca.ca_effects import ConAPIEffectsCls


class ConsoleAPISettingsCls:
    """Setting structure for ConsoleAPICls class
    """

    def __init__(self):
        # Mode options: draw, anim, random
        self.mode = "anim"

        # Timer sampling time
        # To keep good timer responsibility, it is better to sleep multiple
        # times, but with lower period. In meantime it can be checked whether
        # background process require to close application or change timer
        # value and so on
        self.timer_check_period_ms = 10

        # Set IP and port for interprocess communication
        self.ipc = ConAPIIPCSettingsCls()

        # Settings for 3D cube window
        self.cube_3D = Cube3DWSettingsCls()

        # Settings for effects
        self.effects = ConsoleAPIBgEffectsSettingsCls()

    # /__init__()

    def __str__(self):
        msg = "== Console API ({}) ==\n" \
              " default mode: {}\n" \
              " Background timer check period: {} ms\n\n" \
              "{}\n" \
              "{}\n" \
              "".format(self.__class__.__name__, self.mode,
                        self.timer_check_period_ms, self.ipc, self.cube_3D)
        return (msg)
    # /__str__()


# /ConsoleAPISettingsCls


class ConsoleAPICls:
    """Class for controlling cube without GUI

    Since GUI is quite complex, this class allow to send commands to lower
    layers as well as send commands to GUI (for example when slider is need to
    be moved we need to inform GUI)
    """

    def __init__(self, settings):
        """
        :param settings: Settings structure
        :type settings: ConsoleAPISettingsCls
        """
        self.logger = CmnLgrCls("console_api")

        # Save settings to instance
        self.settings = settings

        # Run background process, so it can connect to this server and also
        # we can connect to bg process. It will automatically call "run()"
        self.bg_console_api = ConsoleAPIBgCls(settings=self.settings)
        self.bg_console_api.start()

        # IPC class
        self.ipc = ConAPIIPCMasterCls(settings=self.settings.ipc,
                                      logger=self.logger)

        # Common stuff
        self.ca_cmn = ConAPICommonCls(self.logger)

        # Is closed?
        self.closed = False

        # To make it somehow structured, every module will have own "object"
        # on console API level. This will allow to keep code somehow cleaner
        self._init_modules()

    # /__init__()

    # ============================| Master functions |========================
    def close(self):
        """Kill background process properly and close 3D view"""
        # If already closed, it is pointless to close again. Also sending
        # request to non-existing process would probably fail
        if (self.closed):
            return

        # Disconnect real cube if connected
        if (self.real_cube.get_status() == "Connected"):
            self.logger.info("Automatically disconnecting real cube...")
            self.real_cube.disconnect()

        self.logger.debug("Closing background process...")
        self.ipc.send_req(cmd="close")

        # And wait for exit
        self.bg_console_api.join()
        self.logger.debug("Background process closed")

        self.closed = True

    # /close()

    def get_dimension(self):
        variable, value = self.ipc.send_req_get_rply(cmd="get_dimension")

        # One more check
        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="dimension",
                                        function_name="get_dimension()")

        return (int(value))

    # /get_dimension()

    def set_dimension(self, dimension=5):
        raise Exception("Not actually finished as command yet.\n"
                        "But it is already implemented on lower layer")

    # /set_dimension

    def set_mode(self, mode):
        """Change mode"""
        if ((mode == "anim") or (mode == "draw") or (mode == "random")):
            self.ipc.send_req(cmd="set_mode", val=mode)
        else:
            msg = "Can not set following unsupported mode: {}".format(mode)
            self.logger.critical(msg)
            raise Exception(msg)

    # /set_mode()

    def get_mode(self):
        variable, value = self.ipc.send_req_get_rply(cmd="get_mode")

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="mode",
                                        function_name="get_mode()")

        return (value)

    # /get_mode()

    def ping(self, timeout=None):
        """Send ping request and wait for pong

        This can be used for simple test if all commands were already processed
        and they're not in queue anymore.

        :param timeout: When set to None, default timeout is used. Otherwise
                        value is timeout in miliseconds.
        :type timeout: int
        """
        variable, value = self.ipc.send_req_get_rply(
            cmd="ping", timeout=timeout)

        self.ca_cmn.check_variable_name(received_var=variable,
                                        expected_var="pong",
                                        function_name="ping()")

        return (value)

    # /ping()

    # ===========================| Internal functions |=======================
    def _init_modules(self):
        """This function initialize all console API modules"""
        self.logger.debug("Initializing modules...")

        self.pos = ConAPIPosCls(self.ipc, self.logger)
        self.wall = ConAPIWallCls(self.ipc, self.logger)
        self.pwm = ConAPIPWMCls(self.ipc, self.logger)
        self.real_cube = ConAPIRealCubeCls(self.ipc, self.logger)
        self.op = ConAPIOpCls(self.ipc, self.logger)
        self.btn_array = ConAPIBtnArrayCls(self.ipc, self.logger)
        self.play = ConAPIPlayCls(self.ipc, self.logger)
        self.load_save = ConAPILoadSaveCls(self.ipc, self.logger)
        self.rndm = ConAPIRndmCls(self.ipc, self.logger)
        self.notif = ConAPINotificationsCls(self.ipc, self.logger)
        self.edit = ConAPIEditCls(self.ipc, self.logger)
        self.view = ConAPIViewCls(self.ipc, self.logger)
        self.effects = ConAPIEffectsCls(
            self.ipc, self.logger, self.get_mode,
            self.ping)

        self.logger.info("All console API modules loaded (^_^)")
    # /_init_modules()
# /ConsoleAPICls


# ============================| Internal classes |============================

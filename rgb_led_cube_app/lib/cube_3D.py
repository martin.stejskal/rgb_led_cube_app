#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
======================================
Module for rendering 3D cube with LEDs
======================================


This module allow to perform basic operations with 3D cube. Such as rotating,
setting LED's color and much more!

``author`` Martin Stejskal

``version`` 0.5.3
"""

# Our logging system
from lib.common import CmnLgrCls

# Our library - LED cube (basic, no GUI)
from lib.cube_LED import CubeLEDCls

# Our 3D LED cube model
from lib.cube_3D_OpenGL import Cube3DOpenGLCls, Cube3DOpenGLSettingsCls, \
    Cube3DOpenGLLEDStructCls


class Cube3DWSettingsCls:
    def __init__(self):
        # Settings of 3D cube view
        self.cube_3D_view = Cube3DOpenGLSettingsCls()

    # /__init__()

    def __str__(self):
        msg = "== Cube 3D window ({}) ==\n" \
              "{}\n" \
              "".format(self.__class__.__name__,
                        self.cube_3D_view)
        return (msg)
    # /__str__()


# /Cube3DWSettingsCls


class Cube3DWCls:
    """Cube 3D window manager

    This class add some functionality over Cube3DOpenGLCls class
    """

    def __init__(self, settings):
        """
        :param settings: Settings based on Cube3DWSettingsCls
        :type settings: struct (Cube3DWSettingsCls)
        """
        self.logger = CmnLgrCls("cube3D")

        self.settings = settings

        self.cube_3D_view = Cube3DOpenGLCls(self.settings.cube_3D_view)

        # Basic LED structure for 3D view update
        init_clr = self.settings.cube_3D_view.led.init_color

        # This is LED cube for OpenGL view
        self._ledsGL = Cube3DOpenGLLEDStructCls(
            dimension=self.get_dimension(),
            init_color=init_clr)
        # This is LED cube for upper layers (slightly different logic ->
        # -> format)
        self._leds = CubeLEDCls(dimension=self.get_dimension())

        # Runtime variables
        self.rt = _Cube3DWRuntimeVar(self.get_dimension())

        # Keep RT values in sync with backend
        self.set_active_wall(self.rt.selected_wall)
        self.set_show_disabled(self.rt.show_disabled)
        self.set_show_inactive(self.rt.show_inactive)

    # /__init__()

    def get_err_code_3D_view(self):
        """Get error code of 3D view

        You can check status of 3D window by getting error code. More elegant
        way is enable "on_close" at settings and let upper layer to connect
        notification server. When user click to "X", notification will be send
        immediately. This way you can avoid polling this function.

        Anyway, it can take time to upper layer to process close event, so we
        need to make 100% sure, that no request is send to closed window.
        Trying sending any request to already closed process would cause
        freeze (cube 3D is trying to connect 3D engine, but not response...).
        So due to this, in every function is double checked if 3D view is still
        alive and if not, it will just ignore request.

        :returns: None (still running), integer (finished -> error code)
        :rtype: None/int
        """
        return (self.cube_3D_view.get_err_code())

    # /get_err_code_3D_view()

    def close(self):
        """If you want to close 3D view in advance, you should call this
           function
        """
        self.cube_3D_view.close()

    # /close()

    def get_dimension(self):
        """Get cube dimensions

        :returns: Cube dimension
        :rtype: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return (-1)

        return (self.cube_3D_view.get_dimension())

    # /get_dimension()

    def set_dimension(self, dimension=5):
        """Set cube dimension

        :param dimension: Cube dimension
        :type dimension: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.logger.debug('Setting new dimension: {}'.format(dimension))
        self.cube_3D_view.set_dimension(dimension)

        # Set default values
        init_clr = self.settings.cube_3D_view.led.init_color
        self._ledsGL = Cube3DOpenGLLEDStructCls(dimension=self.get_dimension(),
                                                init_color=init_clr)
        self._leds = CubeLEDCls(dimension=self.get_dimension())
        self.rt = _Cube3DWRuntimeVar(dimension)

    # /set_dimension()

    def set_base_led_struct(self, base_led_strct):
        """Set whole LED structure (RGB)

        :param base_led_strct: Structure in format CubeLEDCls
        :type base_led_strct: struct
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        # Since 3D view is using very simplified LED structure (only RGB),
        # we need to transform base LED structure
        dim = self.get_dimension()
        base_led_strct_dim = base_led_strct.get_dimension()

        assert base_led_strct_dim == dim, (
            'Cube dimensions have to be same.\n'
            f'Base led struct: {base_led_strct_dim} ; cube 3D module: {dim}')

        # Save base led struct to self._leds directly. Non-copying whole
        # structure will save a lot of CPU time. On the another hand,
        # when called get_base_led_struct() it will return basically pointer
        # to original structure and if this structure was changed it will not
        # match with actual 3D image. Hopefully this will not be problem.
        self._leds = base_led_strct

        for wll_idx in range(dim):
            for clmn_idx in range(dim):
                for led_idx in range(dim):
                    # Define source and destination LED
                    led_src = base_led_strct.w[wll_idx].c[clmn_idx].led[
                        led_idx]
                    led_dstGL = self._ledsGL.w[wll_idx].c[clmn_idx].led[
                        led_idx]

                    # Calculate value for OpenGL structure
                    led_dstGL.r = led_src.r * self._leds.pwm.r
                    led_dstGL.g = led_src.g * self._leds.pwm.g
                    led_dstGL.b = led_src.b * self._leds.pwm.b
                # /for LED
            # /for column
        # /for wall
        self.cube_3D_view.set_base_led_struct(self._ledsGL)

    # /set_base_led_struct()

    def get_base_led_struct(self):
        """Returns CubeLEDCls object

        :returns: LED cube object
        :rtype: CubeLEDCls
        """
        return (self._leds)

    # /get_base_led_struct()

    def set_one_led(self, wall_idx=0, column_idx=0, led_idx=0,
                    r=0, g=0, b=0,
                    pwm_r=0, pwm_g=0, pwm_b=0):
        """Set RGB value for one LED

        If only one LED need to be changed, it is more effective to set
        directly this LED instead of setting all LED on cube

        :param wall_idx: Wall index
        :type wall_idx: int

        :param column_idx: Column index
        :type column_idx: int

        :param led_idx: LED index
        :type led_idx: int

        :param r: Intensity of red spectrum
        :type r: int (0~255)

        :param g: Intensity of green spectrum
        :type g: int (0~255)

        :param b: Intensity of blue spectrum
        :type b: int (0~255)
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        # Also write to local structure, so in case that user will want to
        # get actual colors, it can be done
        led_dst = self._leds.w[wall_idx].c[column_idx].led[led_idx]
        led_dstGL = self._ledsGL.w[wall_idx].c[column_idx].led[led_idx]

        # Save new R/G/B values (on/off + PWM for image)
        led_dst.r = r
        led_dst.g = g
        led_dst.b = b

        self._leds.pwm.r = pwm_r
        self._leds.pwm.g = pwm_g
        self._leds.pwm.b = pwm_b

        led_dstGL.r = r * pwm_r
        led_dstGL.g = g * pwm_g
        led_dstGL.b = b * pwm_b

        # And write to 3D view
        raise Exception("Add darken in consideration")
        self.cube_3D_view.set_one_led(wall_idx=wall_idx,
                                      column_idx=column_idx,
                                      led_idx=led_idx,
                                      r=led_dstGL.r,
                                      g=led_dstGL.g,
                                      b=led_dstGL.b)

    # /set_one_led()

    def set_active_wall(self, wall_idx=None):
        """Set active wall

        :param wall_idx: Wall index. If set to none, no wall will be
                         highlighted (equal to disabled)
        :type wall_idx: int (0~(dimension-1))/None
        """
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.rt.selected_wall = wall_idx
        self.cube_3D_view.set_active_wall(wall_idx)

    # /set_active_wall

    def get_active_wall(self):
        """Get active wall

        :returns: Active wall number. None if no wall is active
        :rtype: int (0~(dimension-1))/None
        """
        return (self.rt.selected_wall)

    # /get_active_wall()

    def set_show_disabled(self, show_disabled=True):
        """Show disabled (off) LEDs?

        :param show_disabled: When disabled, LEDs which are off (0,0,0) will
                              not be rendered
        :type show_disabled: bool
        """
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.rt.show_disabled = show_disabled
        self.cube_3D_view.set_show_disabled(show_disabled)

    # /set_show_disabled()

    def get_show_disabled(self):
        """Get "show disabled" option

        :returns: Show disabled value
        :rtype: bool
        """
        return (self.rt.show_disabled)

    # /get_show_disabled()

    def set_show_inactive(self, show_inactive=False):
        """Show inactive LEDs when wall is selected?

        By default, actual wall is highlighted by special grid. However in
        some cases other "inactive" (deselected) LEDs can be disruptive. This
        option allow to not render them, so actual wall is pretty visible.

        :param show_inactive: Enable/disable rendering inactive LEDs.
        :type show_inactive: bool
        """
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.rt.show_inactive = show_inactive
        self.cube_3D_view.set_show_inactive(show_inactive)

    # /set_show_inactive()

    def get_show_inactive(self):
        """Get "show inactive" option

        :returns: Show inactive LEDs when wall is selected
        :rtype: bool
        """
        return (self.rt.show_inactive)

    # /get_show_inactive()

    def set_zoom(self, zoom_value=1):
        """Set zoom value

        :param zoom_value: Zoom value
        :type zoom_value: float
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.rt.zoom = zoom_value
        self.cube_3D_view.set_zoom(zoom_value)

    # /set_zoom()

    def get_zoom(self):
        """Get zoom value

        :returns: Zoom value
        :rtype: float
        """
        return (self.rt.zoom)

    # /get_zoom()

    def set_angle_x(self, deg=0):
        """Set camera angle in X axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        # Check angle value and if needed, correct it
        deg = self._check_angle(deg)
        # Save value
        self.rt.angle_x = deg
        # and set it
        self.cube_3D_view.set_angle_x(deg)

    # /set_angle_x()

    def get_angle_x(self):
        """Get camera angle in X axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self.rt.angle_x)

    # /get_angle_x()

    def set_angle_y(self, deg=0):
        """Set camera angle in Y axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        deg = self._check_angle(deg)
        self.rt.angle_y = deg
        self.cube_3D_view.set_angle_y(deg)

    # /set_angle_y()

    def get_angle_y(self):
        """Get camera angle in Y axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self.rt.angle_y)

    # /get_angle_y()

    def set_angle_z(self, deg=0):
        """Set camera angle in Z axe

        :param deg: Angle in degrees
        :type deg: int
        """
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        deg = self._check_angle(deg)
        self.rt.angle_z = deg
        self.cube_3D_view.set_angle_z(deg)

    # /set_angle_z()

    def get_angle_z(self):
        """Get camera angle in Z axe

        :returns: Angle in degrees
        :rtype: int
        """
        return (self.rt.angle_z)

    # /get_angle_z()

    def reset_cube_view(self):
        """Reset all angles and zoom"""
        # Check if window is not already closed
        if (not (self.get_err_code_3D_view() is None)):
            # Process terminated
            self.logger.warning('3D view is already closed')
            return

        self.cube_3D_view.reset_cube_view()

        self.rt.angle_x = self.rt.angle_y = self.rt.angle_z = 0
        self.rt.zoom = 1.0

    # /reset_cube_view()

    # ===========================| Internal functions |=======================
    def _check_wall_idx(self, wall_idx):
        """Internal function for checking wall index

        Rise exception if wall_idx parameter is wrong

        :param wall_idx: Index number for "wall". Start with 0 (most closer
                         to you)
        :type wall_idx: int
        """
        msg = None

        if (wall_idx < 0):
            msg = "Wall index can not be smaller than 0"

        max = self.get_dimension() - 1

        if (wall_idx > max):
            msg = "Wall index can not be higher than {}".format(max)

        if (msg is not None):
            # Error detected
            self.logger.error(msg)
            raise Exception(msg)

    # /_check_wall_idx()
    ##########################################################################

    def _check_column_idx(self, column_idx):
        """Internal function for checking column index

        Rise exception if column_idx parameter is wrong

        :param column_idx: Index number for "column". Start with 0 (left one)
        :type column_idx: int

        """
        msg = None

        if (column_idx < 0):
            msg = "Column index can not be smaller than 0"

        max = self.get_dimension() - 1

        if (column_idx > max):
            msg = "Column index can not be higher than {}".format(max)

        if (msg is not None):
            # Error -> raise exception
            self.logger.error(msg)
            raise Exception(msg)

    # /_check_column_idx()
    ##########################################################################

    def _check_led_idx(self, led_idx):
        """Internal function for checking LED index

        Rise exception if led_idx parameter is wrong

        :param led_idx: Index number for "LED". Start with 0 (most down one)
        :type led_idx: int

        """
        msg = None

        if (led_idx < 0):
            msg = "LED index can not be smaller than 0"

        max = self.get_dimension() - 1

        if (led_idx > max):
            msg = "LED index can not be higher than {}".format(max)

        if (msg is not None):
            # Error -> raise exception
            self.logger.error(msg)
            raise Exception(msg)

    # /_check_led_idx()
    ##########################################################################

    def _check_angle(self, deg=55):
        """Check angle value

        If angle value is lower/higher than abs(360), then it is cut by 360

        :param deg: Angle in degrees
        :type deg: int
        """
        # Just in case, retype to integer
        deg = int(deg)

        while (deg >= 360):
            deg = deg - 360
        # /while angle is higher than 360

        while (deg <= -360):
            deg = deg + 360
        # /while angle is lower than 360

        # And return correct value
        return (deg)
    # /_check_angle()
    # ===========================| /Internal functions |======================


# /Cube3DWCls()


class _Cube3DWRuntimeVar:
    """Runtime variables for Cube3DWCls
    """

    def __init__(self, dimension):
        # Zoom in 3D view
        self.zoom = 1

        # Camera angle in X, Y and Z axis
        self.angle_x = 0
        self.angle_y = 0
        self.angle_z = 0

        # Selected wall
        self.selected_wall = None

        # Show disabled LEDs?
        self.show_disabled = True

        # Show inactive LEDs when wall is selected?
        self.show_inactive = False
    # /__init__()
# /_Cube3DWRuntimeVar

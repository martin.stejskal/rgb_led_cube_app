#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
======================================
GUI API layer
======================================

This API should allow user to select GUI core (tkinter/QT/GTK/...).
GUI interpretation and handling console_api is completely up to gui_core
itself. So it allow quite freedom in design and it keeps basic compatibility
through console_api.

``author`` Martin Stejskal

``version`` 0.2.3
"""
from time import sleep

# Console API
from lib.console_api import ConsoleAPISettingsCls

# tuned logger
from lib.common import CmnLgrCls


class GUISwitcherSettingsCls:

    def __init__(self):
        # Right now only "tkinter" is supported
        self.gui_core = "tkinter"

        # Inherit settings from no GUI API
        self.console_api = ConsoleAPISettingsCls()

    # /__init__()

    def __str__(self):
        msg = "== GUI switcher ({}) ==\n" \
              " GUI core: {}\n\n" \
              "{}\n" \
              ''.format(self.__class__.__name__,
                        self.gui_core, self.console_api)
        return (msg)
    # /__str__()


# /GUISwitcherSettingsCls


class GUISwitcherCls:

    def __init__(self, settings):
        # Setup logger
        self.logger = CmnLgrCls("gui_switcher")

        # Initialize runtime variables
        self.rt = _GUISwitcherRtCls()

        # Check selected GUI core
        gui_core = settings.gui_core
        try:
            self.gui = self._import_and_init_gui_core(gui_core, settings)
        except Exception as e:
            # Something went wrong -> try to close background process
            self.close()
            raise Exception(e)
        # /try to init GUI core

    # /__init__()

    def update(self):
        """Non blocking function for GUI

        This performs only "one" update of GUI on demand
        """
        # Check if there is close request
        if (self.gui.is_closing()):
            # There is close requirement - can not perform update -> skip
            return
        # /if app is closing

        self.gui.update()

    # /update()

    def is_closing(self):
        return (self.gui.is_closing())

    # /is_closing()

    def close(self):
        """Try to close all background process

        This function have to be called only if another higher layer intend
        to close GUI. Else GUI itself will close itself when user click to
        "X" button
        """
        try:
            self.gui.close()
        except Exception as e:
            msg = 'Can not close "GUI"!\n\n{}'.format(e)
            self.logger.error(msg)
        # /try to close GUI

    # /close()

    def blocking_run(self, sleep_time=50):
        """Blocking function that keeps GUI alive

        When GUI is supposed to be terminated, this function will quit
        """
        while (not self.gui.is_closing()):
            # Run one instance of "update"
            self.update()
            sleep(sleep_time / 1000)
        # /while there is not close request

    # /blocking_run()

    # |GUISwitcherCls
    # ===========================| Internal functions |=======================

    def _import_and_init_gui_core(self, gui_core, settings):
        """Try to import given GUI

        :param gui_core: name of GUI core (tkinter, gtk, and so on)
        :type gui_core: str

        :returns: control window object, which should have generic API
        :rtype: obj
        """
        try:
            # Try to load module
            ctrl_w = __import__("lib.gui.{}.control_w".format(gui_core),
                                fromlist=["ControlWCls"])
        except ModuleNotFoundError as e:
            msg = 'It seems that something is missing :/ You will have to\n' \
                  'install some packages.'
            if (gui_core == "tkinter"):
                msg = '{}\nOn Ubnutnu just install tkinter: ' \
                      '"sudo apt install python3-tk"'.format(msg)
            # /if Tkinter
            msg = "{}\n{}".format(msg, e)
            self.logger.critical(msg)
            raise Exception(msg)
        except Exception as e:
            msg = f'Unsupported GUI core "{gui_core}"\n\n{e}'
            self.logger.critical(msg)
            raise Exception(msg)
        # /else not supported GUI core
        try:
            # Try to initialize GUI module. Well, get class initialized
            # This is quite black magic/advanced Python, but it works
            gui = getattr(ctrl_w, "ControlWCls")(settings.console_api)

            self.logger.debug("GUI control window class initialized")
        except Exception as e:
            msg = 'Initialization of GUI core "{}" failed!\n\n{}' \
                  ''.format(gui_core, e)
            self.logger.critical(msg)
            raise Exception(msg)
        # /Try initialize GUI

        return (gui)
    # /import_gui_core()


# /GUISwitcherCls

# ============================| Internal classes |============================


class _GUISwitcherRtCls:
    """Runtime variables for GUI switcher"""

    def __init__(self):
        self.is_closing = False
    # /__init__()
# /_GUISwitcherRtCls

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
========================================
Module for basic operations with LEDs
========================================

This is sub-module for cube_3D_w module. Basically this module handle only
LEDs and there is no relation to 3D window.

``author`` Martin Stejskal

``version`` 0.8.2
"""

# tuned logger
from lib.common import CmnLgrCls


# Class (variables + functions) for LEDs
class CubeLEDCls:
    """This class keep all LED information (RGB, PWM)
    """

    def __init__(self, dimension):
        """Initialization function for "leds" class which generate "leds"
           structure

        :param: dimension: Dimension of LED cube. It have to be >0
        :type: dimension: int
        """
        # Initialize logger
        self.logger = CmnLgrCls("cubeLED")
        self.logger.debug("Initializing LED cube....")

        # Check dimension. It have to be bigger than 0, else it does not make
        # sense
        if (dimension < 0):
            msg = "Dimension can not be negative"
            self.logger.critical(msg)
            raise Exception(msg)

        # Save dimension for later use
        self._dimension = dimension

        # Operations for all LEDs
        # Add PWM structure
        self.pwm = self.PWMStruct()

        # Add "wall array" (so far void)
        self.w = []

        for wall_idx in range(dimension):
            # To "wall array" add another "wall"
            self.w.append(self.DummyCls())

            # To every "wall" add "column array"
            self.w[wall_idx].c = []

            for column_idx in range(dimension):
                # To "column array" add another "column"
                self.w[wall_idx].c.append(self.DummyCls())

                # To every "column" add "column operations" plus "LED array"
                self.w[wall_idx].c[column_idx].led = []

                for led_idx in range(dimension):
                    # To every single LED add "LEDStruct"
                    self.w[wall_idx].c[column_idx].led.append(self.LEDStruct())
                # /for LED
            # /for column
        # /for wall

    # /__init__

    # Dummy class will be handy later ;)
    class DummyCls:
        pass

    def get_dimension(self):
        """Get cube dimension

        :returns: cube dimension
        :rtype: int
        """
        return (self._dimension)

    ##########################################################################

    # ===========================| Basic LED structure |======================

    class LEDStruct:

        def __init__(self):
            """Every LED have these variables"""

            # Single LED (on/off)
            self.r = 1
            self.g = 1
            self.b = 1

        def __str__(self):
            msg = "R: {}\n" \
                  "G: {}\n" \
                  "B: {}\n" \
                  "".format(self.r, self.g, self.b)
            return (msg)

    # /LED Struct

    class PWMStruct:

        def __init__(self):
            # By default using "255" -> max bright
            self.r = 255
            self.g = 255
            self.b = 255

        def __str__(self):
            msg = "PWM R: {}\n" \
                  "PWM G: {}\n" \
                  "PWM B: {}\n" \
                  "".format(self.r, self.g, self.b)
            return (msg)
    # /PWMStruct


# /CubeLEDCls


class CubeLEDOpCls:
    """Generic operations for cube

    Since keeping all these function in CubeLEDCls is quite memory exhausting,
    it was decided to split CubeLED and oprerations. Result is less consuming
    application when animation is used.

    All functions take as argument CubeLEDCls object (plus other arguments).
    They does not return anything, since argument is basically pointer
    to object.
    """

    def __init__(self):
        # Initialize logger
        self.logger = CmnLgrCls("cubeLED")

        # Column operations
        self.column = self._ColumnOpCls()

        # Wall operations
        self.wall = self._WallOpCls()

        # Cube operations (sometimes we need also wall and column operations,
        # so let's inherit them. We do not need another instances)
        self.cube = self._CubeOpCls(wall_op=self.wall,
                                    column_op=self.column)

    # /__init__()

    def get_led_idx_abs(self, wall_idx, column_idx, led_idx):
        """Get absolute LED index number.

        Value is calculated from cube dimension and relative position
        (wall/column/led).

        :param wall_idx: Index number for "wall". Start with 0 (most closer
                         to you)
        :type wall_idx: int

        :param column_idx: Index number for "column". Start with 0 (left one)
        :type column_idx: int

        :param led_idx: Index number for "LED". Start with 0 (most down one)
        :type led_idx: int

        :returns: Absolute index for LED
        :rtype: int
        """
        dimension = self._dimension

        led_idx_abs = ((wall_idx * dimension * dimension) +
                       (column_idx * dimension) +
                       led_idx)
        return (led_idx_abs)

    ##########################################################################

    def get_led_idx_rel(self, led_idx_abs):
        """
        Get relative indexes for LED.

        Value is calculated from absolute index and cube dimension

        :param led_idx_abs: Absolute index of LED.
        :type led_idx_abs: int

        :returns: Structure of "wall", "column" and "LED". All items starts
                  count with 0 value. Wall with number 0 is "most close to
                  you", column 0 is "the most left one" and LED with number
                  0 is the bottom one.
        :rtype: struct[wall/column/led]
        """

        rel_idx = self.rel_idx_Struct()
        dimension = self._dimension

        rel_idx.wall = int(led_idx_abs / (dimension * dimension))
        rel_idx.column = int((led_idx_abs -
                              (rel_idx.wall * dimension * dimension)) /
                             dimension)
        rel_idx.led = (led_idx_abs -
                       (rel_idx.wall * dimension * dimension) -
                       (rel_idx.column * dimension))

        return (rel_idx)

    class rel_idx_Struct:
        """Structure for relative LED position"""

        def __init__(self):
            self.wall = 0
            self.column = 0
            self.led = 0

        def __str__(self):
            msg = "<| LED position |>\n" \
                  " Wall: {}\n" \
                  " Column: {}\n" \
                  " LED: {}\n" \
                  "".format(self.wall, self.column, self.led)
            return (msg)

    # ============================| Internal classes |========================
    class _CubeOpCls:

        def __init__(self, wall_op, column_op):
            self.wall_op = wall_op
            self.column_op = column_op

        # /__init__()

        def copy(self, cube):
            """Create a copy (new object) from actual cube

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :returns: LED cube
            :rtype: obj (CubeLEDCls)
            """
            # Get cube dimension and create new cube
            dim = cube.get_dimension()

            new_cube = CubeLEDCls(dimension=dim)

            for wll_idx in range(dim):
                for clmn_idx in range(dim):
                    for led_idx in range(dim):
                        src_led = cube.w[wll_idx].c[clmn_idx].led[led_idx]
                        dst_led = new_cube.w[wll_idx].c[clmn_idx].led[led_idx]

                        # Copy RGB information
                        dst_led.r = src_led.r
                        dst_led.g = src_led.g
                        dst_led.b = src_led.b
                    # /for LED
                # /for column
            # /for wall

            # Copy PWM information
            src_pwm = cube.pwm
            dst_pwm = new_cube.pwm

            dst_pwm.r = src_pwm.r
            dst_pwm.g = src_pwm.g
            dst_pwm.b = src_pwm.b

            # done, just return new cube
            return (new_cube)

        # /copy()
        # ==============================| Set/clean all |=====================

        def set_all(self, cube):
            """Turn on every single LED in cube

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.r = led.g = led.b = 1
                    # /for LED
                # /for column
            # /for wall

        # /set_all()

        def clear_all(self, cube):
            """Turn off every single LED in cube

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.r = led.g = led.b = 0

        # /clear_all()
        # ==============================| RGB functions |=====================

        def set_rgb(self, cube, red=1, green=1, blue=1):
            """Set all RGB LED in 3D cube

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param red: Do you want to enable red LED?
            :type red: int bool

            :param green: Do you want to enable green LED?
            :type green: int bool

            :param blue: Do you want to enable blue LED?
            :type blue: int bool
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        if (red):
                            led.r = 1
                        else:
                            led.r = 0

                        if (green):
                            led.g = 1
                        else:
                            led.g = 0

                        if (blue):
                            led.b = 1
                        else:
                            led.b = 0
                    # /for led
                # /for column
            # /for wall

        # /set_rgb()
        # ===========================| R / G / B functions |===================

        def set_r(self, cube):
            """Turn on all red LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.r = 1

        def clear_r(self, cube):
            """Turn off all red LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.r = 0

        def set_g(self, cube):
            """Turn on all green LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.g = 1

        def clear_g(self, cube):
            """Turn off all green LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.g = 0

        def set_b(self, cube):
            """Turn on all blue LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.b = 1

        def clear_b(self, cube):
            """Turn off all blue LEDs

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)
            """
            for wall in cube.w:
                for column in wall.c:
                    for led in column.led:
                        led.b = 0

        # =================================| Floor |==========================
        def set_floor_all(self, cube, row_idx=0):
            """Turn on all LEDs in selected row through walls (create floor)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    led = column.led[row_idx]
                    led.r = led.g = led.b = 1
                # /for column
            # /for wall

        # /set_floor_all()

        def clear_floor_all(self, cube, row_idx=0):
            """Turn off all LEDs in selected row through walls (create floor)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    led = column.led[row_idx]
                    led.r = led.g = led.b = 0
                # /for column
            # /for wall

        # /clear_floor_all()

        def set_floor_rgb(self, cube, red=1, green=1, blue=1, row_idx=0):
            """Set all RGB LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param red: Do you want to enable red LED?
            :type red: int bool

            :param green: Do you want to enable green LED?
            :type green: int bool

            :param blue: Do you want to enable blue LED?
            :type blue: int bool

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    if (red):
                        column.led[row_idx].r = 1
                    else:
                        column.led[row_idx].r = 0

                    if (green):
                        column.led[row_idx].g = 1
                    else:
                        column.led[row_idx].g = 0

                    if (blue):
                        column.led[row_idx].b = 1
                    else:
                        column.led[row_idx].b = 0
                # /for column
            # /for wall

        # /set_floor_rgb()

        def set_floor_r(self, cube, row_idx=0):
            """Set red LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].r = 1
                # /for column
            # /for wall

        # /set_floor_r()

        def clear_floor_r(self, cube, row_idx=0):
            """Set red LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].r = 0
                # /for column
            # /for wall

        # /clear_floor_r()

        def set_floor_g(self, cube, row_idx=0):
            """Set green LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].g = 1
                # /for column
            # /for wall

        # /set_floor_g()

        def clear_floor_g(self, cube, row_idx=0):
            """Set green LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].g = 0
                # /for column
            # /for wall

        # /clear_floor_g()

        def set_floor_b(self, cube, row_idx=0):
            """Set blue LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].b = 1
                # /for column
            # /for wall

        # /set_floor_b()

        def clear_floor_b(self, cube, row_idx=0):
            """Set blue LEDs in selected row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for wall in cube.w:
                for column in wall.c:
                    column.led[row_idx].b = 0
                # /for column
            # /for wall

        # /clear_floor_b()

        # ============================| Transformations |=====================
        def t_bck2frnt(self, cube, shifts=1, repeat=True):
            """Make transformation from BACK to FRONT wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from front will appear in the
                           back. This will happen, when repeat parameter is
                           true. Else void data will appear in back.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If parameter repeat is not active, then transformation is
                # possible to do without any temporary buffer
                if (repeat):
                    # We need to backup RGB values at wall[0]. So we'll create
                    # new backup variable with format [column][led]
                    bckp = []
                    for column_idx in range(dimension):
                        bckp.append([])
                        for led_idx in range(dimension):
                            # To make code more easy to read, save original
                            # LED structure to object
                            led = cube.w[0].c[column_idx].led[led_idx]

                            # Create new LED structure for backup
                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[column_idx].append(led_bckp)
                        # /for LED index
                    # /for column index
                # /if repeat, then backup

                # We should not break references to objects, so we can not
                # simply use "=".
                for wall_idx in range(dimension):
                    # Check if we're not at the last iteration
                    if (wall_idx != (dimension - 1)):
                        for column_idx in range(dimension):
                            for led_idx in range(dimension):
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].r = cube.w[
                                    wall_idx + 1].c[
                                    column_idx].led[
                                    led_idx].r
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].g = cube.w[
                                    wall_idx + 1].c[
                                    column_idx].led[
                                    led_idx].g
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].b = cube.w[
                                    wall_idx + 1].c[
                                    column_idx].led[
                                    led_idx].b
                            # /for led
                        # /for column
                    # /if not last iteration
                    else:
                        # Kind a special case
                        if (repeat):
                            # Restore image from backup
                            for column_idx in range(dimension):
                                for led_idx in range(dimension):
                                    cube.w[
                                        dimension - 1].c[
                                        column_idx].led[
                                        led_idx].r = bckp[
                                        column_idx][
                                        led_idx].r
                                    cube.w[
                                        dimension - 1].c[
                                        column_idx].led[
                                        led_idx].g = bckp[
                                        column_idx][
                                        led_idx].g
                                    cube.w[
                                        dimension - 1].c[
                                        column_idx].led[
                                        led_idx].b = bckp[
                                        column_idx][
                                        led_idx].b
                                # /for led
                            # /for column
                        # /if repeat
                        else:
                            # Simply erase last wall
                            self.wall_op.clear_all(cube, dimension - 1)
                    # /else last iteration
                # /for wall
            # /for shifts

        # /def t_bck2frnt()

        def t_frnt2bck(self, cube, shifts=1, repeat=True):
            """Make transformation from FRONT to BACK wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from back will appear in the
                           front. This will happen, when repeat parameter is
                           true. Else void data will appear in front.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If parameter repeat is not active, then transformation is
                # possible to do without any temporary buffer
                if (repeat):
                    # We need to backup RGB values at wall[last]. So we'll
                    # create new backup variable with format [column][led]
                    bckp = []
                    for column_idx in range(dimension):
                        bckp.append([])
                        for led_idx in range(dimension):
                            # For easy handling simply use whole led object
                            led = cube.w[
                                dimension - 1].c[
                                column_idx].led[
                                led_idx]

                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[column_idx].append(led_bckp)
                        # /for LED index
                    # /for column index
                # /if repeat, then backup

                # We should not break references to objects, so we can not
                # simply use "=".
                for wall_idx in reversed(range(dimension)):
                    # Check if we're not in last iteration
                    if (wall_idx != 0):
                        for column_idx in range(dimension):
                            for led_idx in range(dimension):
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].r = cube.w[
                                    wall_idx - 1].c[
                                    column_idx].led[
                                    led_idx].r
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].g = cube.w[
                                    wall_idx - 1].c[
                                    column_idx].led[
                                    led_idx].g
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].b = cube.w[
                                    wall_idx - 1].c[
                                    column_idx].led[
                                    led_idx].b
                            # /for led
                        # /for column
                    # /if not last iteration
                    else:
                        # Last iteration
                        if (repeat):
                            for column_idx in range(dimension):
                                for led_idx in range(dimension):
                                    cube.w[0].c[
                                        column_idx].led[
                                        led_idx].r = bckp[
                                        column_idx][
                                        led_idx].r
                                    cube.w[0].c[
                                        column_idx].led[
                                        led_idx].g = bckp[
                                        column_idx][
                                        led_idx].g
                                    cube.w[0].c[
                                        column_idx].led[
                                        led_idx].b = bckp[
                                        column_idx][
                                        led_idx].b
                                # /for led
                            # /for column
                        # /if repeat
                        else:
                            # Simply erase first wall
                            self.wall_op.clear_all(cube, 0)
                    # /else last iteration
                # /for wall
            # /for shifts

        # /def t_frnt2bck()

        def t_lft2rght(self, cube, shifts=1, repeat=True):
            """Make transformation from LEFT to RIGHT wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from right will appear in the
                           left. This will happen, when repeat parameter is
                           true. Else void data will appear in left.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If repeat parameter is active, we need to backup right
                # "wall"
                if (repeat):
                    # Since cube logic is little bit different, there have to
                    # be some extra processing. Backup format will be:
                    # [wall][led]
                    bckp = []
                    for wall_idx in range(dimension):
                        bckp.append([])
                        for led_idx in range(dimension):
                            # For easy handling "led" variable will be used
                            led = cube.w[
                                wall_idx].c[
                                dimension - 1].led[led_idx]

                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[wall_idx].append(led_bckp)
                        # /for led
                    # /for wall
                # /if repeat

                # Now it is time for some data processing....
                # As usual, there is catch. To preserve references to objects,
                # we can not simply use "="
                for wall_idx in range(dimension):
                    for column_idx in reversed(range(dimension)):
                        if (column_idx != 0):
                            for led_idx in range(dimension):
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].r = cube.w[
                                    wall_idx].c[
                                    column_idx - 1].led[
                                    led_idx].r
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].g = cube.w[
                                    wall_idx].c[
                                    column_idx - 1].led[
                                    led_idx].g
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].b = cube.w[
                                    wall_idx].c[
                                    column_idx - 1].led[
                                    led_idx].b
                            # /for led
                        # /if normal case
                        else:
                            if (repeat):
                                # Copy data from backup to left column
                                for led_idx in range(dimension):
                                    led = cube.w[
                                        wall_idx].c[0].led[
                                        led_idx]

                                    led.r = bckp[wall_idx][led_idx].r
                                    led.g = bckp[wall_idx][led_idx].g
                                    led.b = bckp[wall_idx][led_idx].b
                            # /if repeat
                            else:
                                self.column_op.clear_all(cube, wall_idx, 0)
                            # /else not repeat (clean left columns)
                        # /else last case (column)
                    # /for column
                # /for wall
            # /for shifts

        # /def t_lft2rght()

        def t_rght2lft(self, cube, shifts=1, repeat=True):
            """Make transformation from RIGHT to LEFT wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from right will appear in the
                           right. This will happen, when repeat parameter is
                           true. Else void data will appear at right.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If repeat parameter is active, we need to backup right
                # "wall"
                if (repeat):
                    # Since cube logic is little bit different, there have to
                    # be some extra processing. Backup format will be:
                    # [wall][led]
                    bckp = []
                    for wall_idx in range(dimension):
                        bckp.append([])
                        for led_idx in range(dimension):
                            # For easy handling "led" variable will be used
                            led = cube.w[
                                wall_idx].c[0].led[led_idx]

                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[wall_idx].append(led_bckp)
                        # /for led
                    # /for wall
                # /if repeat

                # and time for data processing. Since we do not wanna break
                # references to objects, we can not simply use "="
                for wall_idx in range(dimension):
                    for column_idx in range(dimension):
                        if (column_idx != (dimension - 1)):
                            for led_idx in range(dimension):
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].r = cube.w[
                                    wall_idx].c[
                                    column_idx + 1].led[
                                    led_idx].r
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].g = cube.w[
                                    wall_idx].c[
                                    column_idx + 1].led[
                                    led_idx].g
                                cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx].b = cube.w[
                                    wall_idx].c[
                                    column_idx + 1].led[
                                    led_idx].b
                            # /for led
                        # /if normal case
                        else:
                            if (repeat):
                                # Copy data from backup to right column
                                for led_idx in range(dimension):
                                    led = cube.w[
                                        wall_idx].c[
                                        dimension - 1].led[
                                        led_idx]

                                    led.r = bckp[wall_idx][led_idx].r
                                    led.g = bckp[wall_idx][led_idx].g
                                    led.b = bckp[wall_idx][led_idx].b
                            # /if repeat
                            else:
                                self.column_op.clear_all(cube,
                                                         wall_idx,
                                                         dimension - 1)
                        # /special case
                    # /for column
                # /for wall
            # /for shifts

        # /def t_rght2lft()

        def t_btm2top(self, cube, shifts=1, repeat=True):
            """Make transformation from BOTTOM to TOP wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from top will appear in the
                           bottom. This will happen, when repeat parameter is
                           true. Else void data will appear in bottom.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If parameter repeat is not active, then transformation is
                # possible to do without any temporary buffer
                if (repeat):
                    # We need to backup RGB values at led[dimension -1].
                    # So we'll create
                    # new backup variable with format [wall][column]
                    bckp = []
                    for wall_idx in range(dimension):
                        bckp.append([])
                        for column_idx in range(dimension):
                            led = cube.w[
                                wall_idx].c[
                                column_idx].led[dimension - 1]

                            # Create new LED structure for backup
                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[wall_idx].append(led_bckp)
                        # /for column
                    # /for wall
                # /if repeat

                # data processing
                for wall_idx in range(dimension):
                    for column_idx in range(dimension):
                        for led_idx in reversed(range(dimension)):
                            if (led_idx != 0):
                                # normal processing
                                led_new = cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx]

                                led_old = cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx - 1]

                                led_new.r = led_old.r
                                led_new.g = led_old.g
                                led_new.b = led_old.b
                            else:
                                # last iteration
                                if (repeat):
                                    # Restore backup
                                    led_new = cube.w[
                                        wall_idx].c[
                                        column_idx].led[0]
                                    led_bckp = bckp[wall_idx][column_idx]

                                    led_new.r = led_bckp.r
                                    led_new.g = led_bckp.g
                                    led_new.b = led_bckp.b
                                # /if repeat
                                else:
                                    led_new = cube.w[
                                        wall_idx].c[
                                        column_idx].led[0]

                                    led_new.r = 0
                                    led_new.g = 0
                                    led_new.b = 0
                                # /else no repeat
                        # /for led
                    # /for column
                # /for wall
            # /for shifts

        # /def t_btm2top()

        def t_top2btm(self, cube, shifts=1, repeat=True):
            """Make transformation from TOP to BOTTOM wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is "1px" (1 LED) distance.
            :type shifts: int

            :param repeat: During transformation there are 2 options: image
                           is repeated and data from bottom will appear in the
                           top. This will happen, when repeat parameter is
                           true. Else void data will appear in top.
            :type repeat: bool
            """
            dimension = len(cube.w)

            for i in range(shifts):
                # If parameter repeat is not active, then transformation is
                # possible to do without any temporary buffer
                if (repeat):
                    # We need to backup RGB values at led[dimension -1].
                    # So we'll create
                    # new backup variable with format [wall][column]
                    bckp = []
                    for wall_idx in range(dimension):
                        bckp.append([])
                        for column_idx in range(dimension):
                            led = cube.w[
                                wall_idx].c[
                                column_idx].led[0]

                            # Create new LED structure for backup
                            led_bckp = CubeLEDCls.LEDStruct()

                            led_bckp.r = led.r
                            led_bckp.g = led.g
                            led_bckp.b = led.b

                            bckp[wall_idx].append(led_bckp)
                        # /for column
                    # /for wall
                # /if repeat

                # data processing
                for wall_idx in range(dimension):
                    for column_idx in range(dimension):
                        for led_idx in range(dimension):
                            if (led_idx != (dimension - 1)):
                                # normal processing
                                led_new = cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx]

                                led_old = cube.w[
                                    wall_idx].c[
                                    column_idx].led[
                                    led_idx + 1]

                                led_new.r = led_old.r
                                led_new.g = led_old.g
                                led_new.b = led_old.b

                            else:
                                # last iteration
                                if (repeat):
                                    # Restore backup
                                    led_new = cube.w[
                                        wall_idx].c[
                                        column_idx].led[dimension - 1]
                                    led_bckp = bckp[wall_idx][column_idx]

                                    led_new.r = led_bckp.r
                                    led_new.g = led_bckp.g
                                    led_new.b = led_bckp.b

                                # /if repeat
                                else:
                                    led_new = cube.w[
                                        wall_idx].c[
                                        column_idx].led[dimension - 1]

                                    led_new.r = 0
                                    led_new.g = 0
                                    led_new.b = 0
                                # /else no repeat
                        # /for led
                    # /for column
                # /for wall
            # /for shifts

        # /def t_top2btm()

        # ===============================| Rotations |========================
        # I guess that this will be tough if real 3D rotation will be
        # required, since there is problem with "aliasing".

        # Follwing functions are making rotations by 90 deg, so they are
        # easy
        def r_top2frnt(self, cube, shifts=1):
            """Rotate top floor (dimension) to front wall (0)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is rotation by 90 deg.
            :type shifts: int
            """
            # Backup cube keep original LEDs
            bckp_cbe = self.copy(cube)

            # Get cube dimension - will be used later
            dim = cube.get_dimension()

            # Maximum index
            max_idx = dim - 1

            for i in range(shifts):
                # Copy data through floors
                for flr_idx in range(dim):
                    # In floor we need to move through walls
                    for clm_idx in range(dim):
                        for wll_idx in range(dim):
                            led_src = bckp_cbe.w[wll_idx].c[clm_idx].led[
                                flr_idx]
                            led_dst = cube.w[
                                max_idx - flr_idx].c[clm_idx].led[wll_idx]

                            led_dst.r = led_src.r
                            led_dst.g = led_src.g
                            led_dst.b = led_src.b
                        # /for walls
                    # /for columns
                # /for floors
            # /for shifts

        # /r_top2frnt()

        def r_top2bck(self, cube, shifts=1):
            """Rotate top floor (dimension) to back wall (dimension)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is rotation by 90 deg.
            :type shifts: int
            """
            # Backup cube keep original LEDs
            bckp_cbe = self.copy(cube)

            # Get cube dimension - will be used later
            dim = cube.get_dimension()

            # Maximum index
            max_idx = dim - 1

            for i in range(shifts):
                # Copy data through floors
                for flr_idx in range(dim):
                    # In floor we need to move through walls
                    for clm_idx in range(dim):
                        for wll_idx in range(dim):
                            led_src = bckp_cbe.w[wll_idx].c[clm_idx].led[
                                flr_idx]
                            led_dst = cube.w[flr_idx].c[clm_idx].led[
                                max_idx - wll_idx]

                            led_dst.r = led_src.r
                            led_dst.g = led_src.g
                            led_dst.b = led_src.b
                        # /for walls
                    # /for columns
                # /for floors
            # /for shifts

        # /r_top2bck()

        def r_frnt2lft(self, cube, shifts=1):
            """Rotate front wall (0) to the left side (wall 0~dimension)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is rotation by 90 deg.
            :type shifts: int
            """
            bckp_cube = self.copy(cube)

            dim = cube.get_dimension()

            # Maximum index
            max_idx = dim - 1

            for i in range(shifts):
                for wll_idx in range(dim):
                    for clmn_idx in range(dim):
                        for led_idx in range(dim):
                            led_src = bckp_cube.w[wll_idx].c[clmn_idx].led[
                                led_idx]
                            led_dst = cube.w[max_idx - clmn_idx].c[
                                wll_idx].led[led_idx]

                            led_dst.r = led_src.r
                            led_dst.g = led_src.g
                            led_dst.b = led_src.b
                        # /for LEDs
                    # /for columns
                # /for walls
            # /for shifts

        # /r_frnt2lft()

        def r_frnt2rght(self, cube, shifts=1):
            """Rotate front wall (0) to the right side (wall 0~dimension)

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param shifts: Define how many times shift should be performed.
                           Every shift is rotation by 90 deg.
            :type shifts: int
            """
            bckp_cube = self.copy(cube)

            dim = cube.get_dimension()

            # Maximum index
            max_idx = dim - 1

            for i in range(shifts):
                for wll_idx in range(dim):
                    for clmn_idx in range(dim):
                        for led_idx in range(dim):
                            led_src = bckp_cube.w[wll_idx].c[clmn_idx].led[
                                led_idx]
                            led_dst = cube.w[clmn_idx].c[
                                max_idx - wll_idx].led[led_idx]

                            led_dst.r = led_src.r
                            led_dst.g = led_src.g
                            led_dst.b = led_src.b
                        # /for LEDs
                    # /for columns
                # /for walls
            # /for shifts
        # /r_frnt2rght()

        # ===========================| Internal functions |===================

    # /_WholeCubeOpCls

    class _WallOpCls:
        # ==============================| Set/clean all |=====================

        def set_all(self, cube, wall_idx=0):
            """Turn on every single LED at selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.r = led.g = led.b = 1

        def clear_all(self, cube, wall_idx=0):
            """Turn off every single LED at selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.r = led.g = led.b = 0

        # ==============================| RGB functions |=====================

        def set_rgb(self, cube, wall_idx=0, red=1, green=1, blue=1):
            """Turn on/off all RGB LED in selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param red: Do you want to enable red LED?
            :type red: int bool

            :param green: Do you want to enable green LED?
            :type green: int bool

            :param blue: Do you want to enable blue LED?
            :type blue: int bool
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    if (red):
                        led.r = 1
                    else:
                        led.r = 0

                    if (green):
                        led.g = 1
                    else:
                        led.g = 0

                    if (blue):
                        led.b = 1
                    else:
                        led.b = 0

        # ===========================| R / G / B functions |==================

        def set_r(self, cube, wall_idx=0):
            """Turn on all red LEDs in selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.r = 1

        def clear_r(self, cube, wall_idx=0):
            """Turn off all red LEDs in selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.r = 0

        def set_g(self, cube, wall_idx=0):
            """Turn on all green LEDs in selected wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.g = 1

        def clear_g(self, cube, wall_idx=0):
            """Turn off all green LEDs in actual wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.g = 0

        def set_b(self, cube, wall_idx=0):
            """Turn on all blue LEDs in actual wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.b = 1

        def clear_b(self, cube, wall_idx=0):
            """Turn off all blue LEDs in actual wall

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))
            """
            for column in cube.w[wall_idx].c:
                for led in column.led:
                    led.b = 0

        # ==============================| Row functions |=====================
        # These are special, since row is across columns, we need to handle
        # them at this level.
        # ===========================| Row : Set/Clean all |==================

        def set_row_all(self, cube, wall_idx=0, row_idx=0):
            """Turn every single LED in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            # Iterate across columns
            for column in cube.w[wall_idx].c:
                column.led[row_idx].r = column.led[
                    row_idx].g = column.led[row_idx].b = 1

        def clear_row_all(self, cube, wall_idx=0, row_idx=0):
            """Turn every single LED in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            # Iterate across columns
            for column in cube.w[wall_idx].c:
                column.led[row_idx].r = column.led[
                    row_idx].g = column.led[row_idx].b = 0

        # ===========================| Row : RGB functions |==================

        def set_row_rgb(self, cube, wall_idx=0,
                        row_idx=0, red=1, green=1, blue=1):
            """Set all RGB LED in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int

            :param red: Do you want to enable red LED?
            :type red: int bool

            :param green: Do you want to enable green LED?
            :type green: int bool

            :param blue: Do you want to enable blue LED?
            :type blue: int bool
            """
            for column in cube.w[wall_idx].c:
                if (red):
                    column.led[row_idx].r = 1
                else:
                    column.led[row_idx].r = 0

                if (green):
                    column.led[row_idx].g = 1
                else:
                    column.led[row_idx].g = 0

                if (blue):
                    column.led[row_idx].b = 1
                else:
                    column.led[row_idx].b = 0

        # ========================| Row : R / G / B functions |===============

        def set_row_r(self, cube, wall_idx=0, row_idx=0):
            """Turn on all red LEDs in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].r = 1

        def clear_row_r(self, cube, wall_idx=0, row_idx=0):
            """Turn off all red LEDs in selected wall row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].r = 0

        def set_row_g(self, cube, wall_idx=0, row_idx=0):
            """Turn on all green LEDs in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].g = 1

        def clear_row_g(self, cube, wall_idx=0, row_idx=0):
            """Turn off all green LEDs in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].g = 0

        def set_row_b(self, cube, wall_idx=0, row_idx=0):
            """Turn on all blue LEDs in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].b = 1

        def clear_row_b(self, cube, wall_idx=0, row_idx=0):
            """Turn off all blue LEDs in selected wall and row

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param row_idx: Row index. 0 is bottom one.
            :type row_idx: int
            """
            for column in cube.w[wall_idx].c:
                column.led[row_idx].b = 0

    # /_WallOpCls

    class _ColumnOpCls:
        # ==============================| Set/clean all |=====================

        def set_all(self, cube, wall_idx=0, column_idx=0):
            """Turn on all LED in selected column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.r = led.g = led.b = 1

        def clear_all(self, cube, wall_idx=0, column_idx=0):
            """Turn off all LED in selected column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.r = led.g = led.b = 0

        # ==============================| RGB functions |=====================
        def set_rgb(self, cube, wall_idx=0, column_idx=0,
                    red=1, green=1, blue=1):
            """Set all RGB in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))

            :param red: Do you want to enable red LED?
            :type red: int bool

            :param green: Do you want to enable green LED?
            :type green: int bool

            :param blue: Do you want to enable blue LED?
            :type blue: int bool
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                if (red):
                    led.r = 1
                else:
                    led.r = 0

                if (green):
                    led.g = 1
                else:
                    led.g = 0

                if (blue):
                    led.b = 1
                else:
                    led.b = 0

        # ===========================| R / G / B functions |==================

        def set_r(self, cube, wall_idx=0, column_idx=0):
            """Turn on all red LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.r = 1

        def clear_r(self, cube, wall_idx=0, column_idx=0):
            """Turn off all red LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.r = 0

        def set_g(self, cube, wall_idx=0, column_idx=0):
            """Turn on all green LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.g = 1

        def clear_g(self, cube, wall_idx=0, column_idx=0):
            """Turn off all green LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.g = 0

        def set_b(self, cube, wall_idx=0, column_idx=0):
            """Turn on all blue LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.b = 1

        def clear_b(self, cube, wall_idx=0, column_idx=0):
            """Turn off all blue LEDs in column

            :param cube: LED cube
            :type cube: obj (CubeLEDCls)

            :param wall_idx: Wall index.
            :type wall_idx: int (0~(dimension -1))

            :param column_idx: Column index
            :type column_idx: int (0~(dimension -1))
            """
            for led in cube.w[wall_idx].c[column_idx].led:
                led.b = 0
    # /_ColumnOpCls


# /CubeLEDOpCls


def copy_cube_led(src, dst):
    """This function will make true copy from one cube to second

    Since in Python is nearly everything pointer, "true" copy can be quite
    tricky. So that is why you should use this function.

    :param src: Source cube
    :type src: obj (CubeLEDCls)

    :param src: Destination cube
    :type src: obj (CubeLEDCls)
    """
    dim = src.get_dimension()
    assert src.get_dimension() == dst.get_dimension(), (
        'It is expected that source and destination cube have same'
        ' dimensions.')

    for wll_idx in range(dim):
        for clmn_idx in range(dim):
            for led_idx in range(dim):
                src_led = src.w[wll_idx].c[clmn_idx].led[led_idx]
                dst_led = dst.w[wll_idx].c[clmn_idx].led[led_idx]

                # Now copy RGB
                dst_led.r = src_led.r
                dst_led.g = src_led.g
                dst_led.b = src_led.b
            # /for LED
        # /for column
    # /for wall
    src_pwm = src.pwm
    dst_pwm = dst.pwm

    dst_pwm.r = src_pwm.r
    dst_pwm.g = src_pwm.g
    dst_pwm.b = src_pwm.b
# /copy_cube_led()

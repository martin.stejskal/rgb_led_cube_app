#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
====================
LED cube application
====================

GUI application for LED cube. Module with "main function"

``author`` Martin Stejskal

``Created``  2015.07.26

``Modified`` 2019.01.06

"""
# To detect python version and work with Python's path
import sys

# Libraries for log events
import logging
import logging.config

# We do not support "old" Python. Let's hope that Python 3 will be compatible
# with Python 4...
if (sys.version_info[0] == 2):
    raise Exception("Python2 was not tested and it is not supported!\n"
                    "If you have Python3 on your system, you can try"
                    "following:\n   python3 led_cube_app")
elif (sys.version_info[0] == 3):
    # Python 3 was tested
    pass
elif (sys.version_info[0] == 4):
    raise Exception("We did not tested this under Python 4. So you can try"
                    " it by commenting this exception.\n"
                    " Good luck!")
else:
    raise Exception("Unknown or unsupported Python version")

# Because of PEP8 standard, there is small hack for importing our modules
try:
    from lib.gui_switcher import GUISwitcherCls
    # Dynamic loading of settings is postponed for now
    # from load_cfg import settings

    # tuned logger
    from lib.common import CmnLgrCls, app_path

    # Configuration class
    from lib.load_cfg import LoadCfgCls
except Exception as e:
    msg = "Importing our libraries failed. Maybe some dependency is missing." \
          "\n Please check following error:\n{}".format(e)
    raise Exception(msg)

__version__ = "1.2.8"

# Location of configuration file for logging
log_cfg_file = "{}/cfg/logging.cfg".format(app_path)
default_cfg_file = "{}/cfg/app.yaml".format(app_path)


def show_help(app_name):
    print('Usage:\n$ python3 {} [PARAMETERS]\n\n== Available parameters ==\n'
          '  --help                    Show this help and exit\n'
          '  --test_import_and_syntax  Just check for syntax and import\n'
          '                            errors (only static imports)\n'
          ''.format(app_name))


# Main function
if __name__ == "__main__":
    """Main function.
    """
    # Parameter check. If no parameter is defined -> set it to empty
    params = sys.argv
    # First argument is mandatory
    app_name = params[0]

    # By default we expect no parameter
    param1 = ''
    if (len(params) > 1):
        param1 = params[1]
    # /if there are some parameters

    if (param1 == '--help'):
        show_help(app_name)
        sys.exit(0)
    elif (param1 == '--test_import_and_syntax'):
        print('Syntax and imports seems to be fine.')
        sys.exit(0)
    elif (param1 == ''):
        # Main program.
        # Prepare logger, load settings, initialize GUI, run GUI

        # Load configure file for logging
        logging.config.fileConfig(log_cfg_file, None, False)

        # Assign logger to this function
        logger = CmnLgrCls("led_cube_app")
        logger.info("\n\n\n\n\n\n\nStarting application - v{}\n\n\n"
                    "".format(__version__))

        # Settings is just one object. Thanks god for this.
        # User settings is disabled for now, due to wild development stage.
        user_settings = LoadCfgCls(cfg_file=default_cfg_file)

        logger.debug("Settings loaded")

        gui = GUISwitcherCls(settings=user_settings.get())
        logger.debug("GUI initialized. Running GUI ...")

        gui.blocking_run(sleep_time=50)

        logger.debug("GUI terminated. Closing app...")
    else:
        # Unknown parameter
        print('Unknown parameter "{}"\n\n'.format(param1))
        show_help(app_name)
        sys.exit(1)
